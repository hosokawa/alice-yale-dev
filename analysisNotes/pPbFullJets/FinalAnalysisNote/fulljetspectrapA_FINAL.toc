\contentsline {section}{\numberline {1}Introduction}{2}{section.1}
\contentsline {section}{\numberline {2}Event, Track, Cluster and Jet Selections}{3}{section.2}
\contentsline {subsection}{\numberline {2.1}Used Period and Runs}{3}{subsection.2.1}
\contentsline {subsection}{\numberline {2.2}Event Selection}{3}{subsection.2.2}
\contentsline {subsection}{\numberline {2.3}Track Selection}{3}{subsection.2.3}
\contentsline {subsection}{\numberline {2.4}Cluster Selection}{4}{subsection.2.4}
\contentsline {subsection}{\numberline {2.5}Jet Selection}{4}{subsection.2.5}
\contentsline {subsection}{\numberline {2.6}Train Settings and Used Tag}{5}{subsection.2.6}
\contentsline {section}{\numberline {3}Underlying Event}{5}{section.3}
\contentsline {subsection}{\numberline {3.1}Special case of underlying event in p+Pb}{6}{subsection.3.1}
\contentsline {subsection}{\numberline {3.2}Scale Factor}{6}{subsection.3.2}
\contentsline {section}{\numberline {4}Background Fluctuations: \ensuremath {\delta \ensuremath {p_T}}}{7}{section.4}
\contentsline {section}{\numberline {5}Unfolding}{10}{section.5}
\contentsline {section}{\numberline {6}Normalization}{12}{section.6}
\contentsline {section}{\numberline {7}Results}{12}{section.7}
\contentsline {subsection}{\numberline {7.1}Unfolded Spectra}{12}{subsection.7.1}
\contentsline {subsection}{\numberline {7.2}Jet Shape Ratio}{13}{subsection.7.2}
\contentsline {subsection}{\numberline {7.3}$R_{pPb}^{PYTHIA}$}{13}{subsection.7.3}
\contentsline {subsubsection}{\numberline {7.3.1}pp Reference spectrum}{17}{subsubsection.7.3.1}
\contentsline {subsubsection}{\numberline {7.3.2}Scaled ATLAS as a Reference}{17}{subsubsection.7.3.2}
\contentsline {subsection}{\numberline {7.4}Derived Plot}{17}{subsection.7.4}
\contentsline {section}{\numberline {8}Systematic Uncertainties}{18}{section.8}
\contentsline {subsection}{\numberline {8.1}Tracking Efficiency Uncertainty}{18}{subsection.8.1}
\contentsline {subsection}{\numberline {8.2}Hadronic Correction Uncertainty}{18}{subsection.8.2}
\contentsline {subsection}{\numberline {8.3}Uncertainty in the Background Estimation}{23}{subsection.8.3}
\contentsline {subsection}{\numberline {8.4}Unfolding Uncertainties}{23}{subsection.8.4}
\contentsline {subsection}{\numberline {8.5}Emcal Related Uncertainties}{29}{subsection.8.5}
\contentsline {subsection}{\numberline {8.6}Summary of Systematic Uncertainties}{29}{subsection.8.6}
\contentsline {section}{\numberline {9}Requested Plots QM14}{32}{section.9}
\contentsline {subsection}{\numberline {9.1}Technical Preliminary}{32}{subsection.9.1}
\contentsline {subsection}{\numberline {9.2}Physics Preliminary}{42}{subsection.9.2}
