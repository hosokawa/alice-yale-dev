#!/usr/bin/env python

import zmq
from time import time
import pickle
import processRootFile
from os import makedirs
from os.path import exists
from ROOT import gROOT,TFile,TH1

debug = True

def zmqReceiver(context):
    # Setup ZMQ
    # The port may need to change 
    socket = context.socket(zmq.SUB)
    socket.connect("tcp://localhost:5556")

    # ZMQ Configuration
    # Selects which hists we are interested in
    identifier = "hist"
    # May be able to pick this up from somewhere
    runNumber = 10

    # Set for the proper HLT subscription
    typeFilter = "EMCAL"
    #typeFilter = u"12345"

    # Python 2 - ascii bytes to unicode str
    # Can also just resolve by prefixing with u as commented above
    if isinstance(typeFilter, bytes):
        print "Subscription is ASCII. Converting"
        typeFilter = typeFilter.decode('ascii')

    # Set the subscription to receive content
    socket.setsockopt_string(zmq.SUBSCRIBE, typeFilter)

    # TEMP
    if debug:
        syncClient = context.socket(zmq.REQ)
        syncClient.connect("tcp://localhost:5557")

        # Send sync
        syncClient.send(b'')
        # Receive sync
        syncClient.recv()
    # END TEMP

    # Collect the information
    # We should wait for each histogram, test it, and then save if necessary. If the wait is over some time (say 30 s), then decide the run is over and start again
    currentTime = time()

    # Create necessary directory structure
    if not exists("Run%i" % runNumber):
        makedirs("Run%i" % runNumber)
    fOut = TFile("Run%i/hists.root" % runNumber, "RECREATE")

    hists = dict()

    while True:
        # Retreive message
        [topic, msg] = socket.recv_multipart()

        # Check if the run ended
        if time() > (currentTime + 5):
            # End of run
            # Save previous hists, call function to write webpage and clear dict
            # Save hists
            for index in hists:
                hists[index].Write()

            fOut.Write()
            fOut.Close()
            # Call function to write webpage
            processRootFile.processRun(runNumber)

            print "End of run %d" % runNumber

            #TEMP
            if debug:
                if runNumber > 11:
                    exit()
            #END TEMP

            # Prepare for next run
            hists.clear()
            runNumber += 1
            if not exists("Run%i" % runNumber):
                makedirs("Run%i" % runNumber)
            fOut = TFile("Run%i/hists.root" % runNumber, "RECREATE")

            # Now we can just continue as usual
        else:
            pass

        # Udpate timing
        currentTime = time()

        # Unpack message
        hist = pickle.loads(msg)
        print "\n%s" % hist.GetName()
        if identifier not in hist.GetName():
            continue
        else:
            print "Recognized"
            if hist.GetName() in hists:
                print "Already have hist. Addding"
                hists[hist.GetName()].Add(hist)
            else:
                print "New hist. Inserting"
                hists[hist.GetName()] = hist

def startZMQReceiver():
    context = zmq.Context()

    try:
        zmqReceiver(context)
    except KeyboardInterrupt:
        context.destroy()
        print("Caught keyboard interrupt. Exiting.")
        sys.exit(0)

if __name__ == "__main__":
    startZMQReceiver()

