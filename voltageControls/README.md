# MPOD

## General Information

These are WIENER MPOD voltage control scripts

The WIENER MIBS file included in this repository must be installed to ensure that commands are translated correctly. On a Linux system, it should be installed to `~/.snmp/mibs`. To use on a Debian based system, the packages `snmp` and `snmp-mibs-downloader` must be installed!

On Mac OS X, the file should be installed to `/usr/share/snmp/mibs/`

## Usage

This will describe usage of the various different functions

```
./mpod.sh
```

This will display the status of the voltage controller. This information includes the voltage ramp rise and fall rates, as well as the set voltage, measured voltage, and measured current for each channel.

```
./mpod.sh status
```

This will enable and disable (respectively) the voltage controller. The voltage controller should be enabled before attempting to specify any voltages.

```
./mpod.sh enable
./mpod.sh disable
```

This will set a single voltage for a specific slot and channel. If anything more complicated is required, a configuration file should be used. 

**NOTE:** If the voltage is too large for an element (as set in the section below on voltage limits), then the voltage will not be set. This behavior can be disabled with the `-e` option.

```
./mpod.sh voltage -s <slot> -c <channel> -v <voltage>
```

This will set a voltage by taking the current voltage and adding the desired value.

```
./mpod.sh voltage -s <slot> -c <channel> -v <voltage> -a
```

This will set voltages from configuration file. This allows for more complicated configuration. See the section of configuration files below or look at an example configuration file. The voltages can be reversed with -r. This will ramp the voltages down starting from the highest voltage down to the lowest, avoiding the need for a dedicated ramp down file.

In addition to setting voltages, one can also set the ramp rate, wait time between steps, and the number of steps.

**NOTE:** For safety, the first line in a new configuration file must match the previous set of values. Therefore, a new configuration file (ie. for when the system is starting from 0 V everywhere), the first step must be 0 for all channels that will be changed.

**NOTE:** If the voltage is too large for an element (as set in the section below on voltage limits), then the voltage will not be set.

```
./mpod.sh voltage -f <filename> [-r]
```

This will log all voltages to file (voltage.log), as well as display them on screen to monitor the voltage controller. If the time is not specified, it will default to five seconds. Each new logging instance will be appended to the same file. Each measurement is timestamped, and is denoted with a dashed line. The log will run continuously until told to stop via ctrl-c

**NOTE:** Although logging can be started at any time, it is best to start it before changing voltages to ensure that everything is recorded.

```
./mpod.sh log -t <refresh rate in seconds>
```

## Configuration File Format

### Voltage Limits Configuration
An example configuration is below. Limits to voltages over the various elements (GEMs + MMG) can be set in `configureVoltageLimits.sh`. If the voltage attempting to be set exceeds the voltage set in the file, then the voltage will not be changed. In addition, the location of each element can be set to correspond to the appropriate channel.

The voltage control limits can be disabled by passing the `-e` option to `mpod.sh`

```
#!/usr/bin/env bash

# Set the index of each element.
# 0 corresponds with the lower voltage of the element
# 1 corresponds with the higher voltage of the element
# In the standard configuration, the elements are configured as:
#       MMG=1
#       MGEM_0=2
#       MGEM_1=3
#       TGEM_0=4
#       TGEM_1=5
MMG=101
MGEM_0=102
MGEM_1=103
TGEM_0=104
TGEM_1=105

# Set the maximum voltage over any particular element
MAXVOLTAGE_MMG=600
MAXVOLTAGE_MGEM=490
MAXVOLTAGE_TGEM=490
```

### General Configuration
An example configuration is below. The labels are important. Protect can currently be ignored, and should only have a asterisk at the end. In the future, we can use this to ensure that the system respond correctly to a possible short or other issue.

```
RampRate 20
Slot Channel Voltages 			Protect
1 	 2 		 0, 10, 30, 60, 100 	*
2 	 3 		 0, 12, 16, 30, 180 	*
1 	 1 		 0, 30, 50, 70, 200 	*
Wait 		 -, -, -, -, - 
```

### IP Address Configuration
The IP address of the voltage controller can be configured in `privateConfig.sh`. An example configuration file is copied below.

```bash
# Make a copy of this file in your local copy of the repository and call it privateConfig.sh
# Never commit the local copy of privateConfig.sh the git repository

# Define IP addresses of the voltage control units
caenIP="127.0.0.1"
ip=127.0.0.1 
```

The variable `ip` in line 6 should be changed to the appropriate IP address. If the IP address of the MPOD controller is changed (using the MUSEcontrol tool), then the variable `ip` should be changed here to match it.

## Example Usage

```bash
 # Turn on the hardware switch on the voltage controller.
./mpod.sh status                        # Should return that the system is off.
./mpod.sh enable                        # Enables the system.
./mpod.sh log -t 3                      # Enables logging of voltages every 3 seconds. This will continuously refresh, so a second terminal window is needed (or one may use GNU Screen, tmux or another terminal multiplexer if desired.). NOTE: This can be done at a different time if desired.
./mpod.sh voltage -f voltages.txt       # Sets voltages via a file.
 # Interact with the waits as necessary to get to the desired voltages
 # Check the terminal running mpod.sh log to ensure that the voltages are at the expected values.
 # Run tests at the set voltages.
 # When finished, continue with the steps below.
./mpod.sh voltage -f rampDown.txt       # Ramp the voltages back down safely. Remember to set the first set of voltages in rampDown.txt to the same values as the last voltages in voltages.txt
 # To ramp down, one could alternatively use:
 #./mpod.sh voltage -r -f voltages.txt
./mpod.sh status                        # Confirm that the all the voltages have ramped down correctly.
 # Assuming that everything is at the expected voltages (likely 0 V), then finish up by running the last few commands
 # Press ctrl-c in the mpod logging terminal window to terminate voltage recording. This may be down sooner if desired.
./mpod.sh disable
 # Shut down the hardware switch on the voltage controller
```
