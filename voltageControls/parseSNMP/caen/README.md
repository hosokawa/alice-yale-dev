# CAEN Libraries

## Setup

The CAEN Wrapper libraries are needed **BEFORE COMPILATION**, but they should not be distributed in the repo. Instead, they are stored on the rhig server. To copy them over, use the following command:

```bash
scp -r user@rhig.physics.yale.edu:/home/ehlers/libraries/caen/* .
```

Replace user with your username on the RHIG server.

Then, ensure the dependencies for CAEN are installed. Next, run make as normal.
