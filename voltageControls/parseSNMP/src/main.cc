#include "parseSNMP.h"

#include <iostream>
#include <string>
#include <sstream>
#include <vector>

#include <cstdlib>
#include <cstring>

#include <CAENHVWrapper.h>

#ifndef DEBUG
#define DEBUG 1
#endif

int main(int argc, char * argv[])
{
	// Check that there are enough arguments to parse
	if ((argc != 10) && (argc != 12))
	{
		std::cout << "Must pass 9 or 11 arguments!" << std::endl;
		std::exit(-1);
	}

	///////////////////////////////////////////////////////////////////
	// Parse input
	///////////////////////////////////////////////////////////////////

	std::vector <std::string> splitInput;
	for (int i = 1; i < argc; i++)
	{
		splitInput.push_back(argv[i]);
	}

	std::clog << "Input: ";
	for (unsigned int i = 0; i < splitInput.size(); i++)
	{
		std::clog << " " << splitInput.at(i);
	}
	std::clog << std::endl;

	// Determine the operation type
	std::vector<std::string> validOperationTypes {"get", "set", "walk"};
	std::string operationType = "";
	operationType = parseSNMP::checkValidString(splitInput.at(0), validOperationTypes);
	// snmp takes 4 characters, so take the characters after that
	operationType = operationType.substr(4);
	std::clog << "Operation type: " << operationType << std::endl;

	// The ip is always located in the eigth word
	std::string ip = splitInput.at(7); 
	char * ipCStr = new char [ip.length() + 1];
	std::strcpy(ipCStr, ip.c_str());

	///////////////////////////////////////////////////////////////////
	// Initialize the connection to the CAEN crate here
	///////////////////////////////////////////////////////////////////
	// (SystemType = SY2527, LinkType = 0 (tcp/ip), ipAddress, username, password, handlePtr)
	int * tempHandle = 0;
	parseSNMP::systemParameters sysParams(-1);
	if (DEBUG == 0)
	{
		CAENHV_InitSystem(SY2527, 0, ipCStr, "admin", "admin", tempHandle);
		sysParams.handle = *tempHandle;
	}
	else
	{
		sysParams.handle = 0;
	}
	
	// Determine the operation and call the appropraite operation type for sorting
	std::string result = "";
	if (operationType == "set")
	{
		// Set operation is always size() - 3
		// Set voltage is always size() - 1
		parseSNMP::setFunctions(sysParams, splitInput.at(splitInput.size() - 3), splitInput.at(splitInput.size() - 1) );
	}
	if (operationType == "get")
	{
		// Get or Walk is always size() - 1
		result = parseSNMP::getFunctions(sysParams, splitInput.at(splitInput.size() - 1));
	}
	if (operationType == "walk")
	{
		// Get or Walk is always size() - 1
		result = parseSNMP::walkFunctions(sysParams, splitInput.at(splitInput.size() - 1));
	}

	// Pass result
	// Whatever is sent to setFunctions doesn't matter, as it is never parsed
	std::cout << result << std::endl;

	CAENHV_DeinitSystem(sysParams.handle);

	// Delete allocated
	delete [] ipCStr;

	// The argument is printed, so the return value should not  matter
	return 0;
}

