#!/usr/bin/env bash

ip=""
#######################################
# Set commands
#######################################
# Set voltage
#outputVoltage.channel F voltage
arg[10]="snmpset -v 2c -m +WIENER-CRATE-MIB -c guru $ip outputVoltage.103 F 180"
# Set channel status (on(1) or off(0))
#outputSwitch.channel i status
arg[11]="snmpset -v 2c -m +WIENER-CRATE-MIB -c guru $ip outputSwitch.103 i 1"
# Set channel ramp rise rate
#outputVoltageRiseRate.channel F voltage (v/s)
arg[12]="snmpset -v 2c -m +WIENER-CRATE-MIB -c guru $ip outputVoltageRiseRate.1 F 10" 
# Set chanel ramp fall rate
#outputVoltageFallRate.channel F voltage (v/s)
arg[13]="snmpset -v 2c -m +WIENER-CRATE-MIB -c guru $ip outputVoltageFallRate.1 F 10" 
# Set system status (on(1) or off(0))
#sysMainSwitch.0 i status
arg[14]="snmpset -v 2c -m +WIENER-CRATE-MIB -c private $ip sysMainSwitch.0 i 1"

#######################################
# Get commands
#######################################
# Get voltage
#outputVoltage.channel
arg[20]="snmpget -v 2c -m +WIENER-CRATE-MIB -c public $ip outputVoltage.102"
# Get channel status (on(1) or off(0))
#outputSwitch.channel
arg[21]="snmpget -v 2c -m +WIENER-CRATE-MIB -c public $ip outputSwitch.102"
# Get channel ramp rise rate
#outputVoltageRiseRate.channel
arg[22]="snmpget -v 2c -m +WIENER-CRATE-MIB -c public $ip outputVoltageRiseRate.1"
# Get chanel ramp fall rate
#outputVoltageFallRate.channel
arg[23]="snmpget -v 2c -m +WIENER-CRATE-MIB -c public $ip outputVoltageFallRate.1"
# Get system status (on(1) or off(0))
#sysMainSwitch.0
arg[24]="snmpget -v 2c -m +WIENER-CRATE-MIB -c public $ip sysMainSwitch.0"

#######################################
# Walk commands
#######################################
# Get all voltages
arg[30]="snmpwalk -v 2c -m +WIENER-CRATE-MIB -c public $ip outputVoltage"
# Get all nominal voltages
arg[31]="snmpwalk -v 2c -m +WIENER-CRATE-MIB -c public $ip outputMeasurementSenseVoltage"
# Get all currents
arg[32]="snmpwalk -v 2c -m +WIENER-CRATE-MIB -c public $ip outputMeasurementCurrent"

#######################################
# Test parsing for error handling
#######################################
arg[40]="hello world asdkfjslkf asdjfj23 234234kl dgjdlg sfjklsef sdfjsklkfs sdfjksdfl"
arg[41]="hello world asdkfjslkf asdjfj23 234234kl dgjdlg sfjklsef sdfjsklkfs sdfjksdfl sfj sdfj"

echo -e "\tSet commands"
for i in {10..14}; do
	parseSNMP ${arg[i]}
	echo -e '\n'
done

echo -e "\tGet commands"
for i in {20..24}; do
	parseSNMP ${arg[i]}
	echo -e '\n'
done

echo -e "\tWalk commands"
for i in {30..32}; do
	parseSNMP ${arg[i]}
	echo -e '\n'
done

echo -e "\tParsing test commands"
for i in {40..41}; do
	parseSNMP ${arg[i]}
	echo -e '\n'
done

