#!/usr/bin/env bash
# vim: expandtab tabstop=4 shiftwidth=4 softtabstop=4

# To trap a control-c during the log
# See: http://rimuhosting.com/knowledgebase/linux/misc/trapping-ctrl-c-in-bash
trap ctrl_c INT

function ctrl_c() {
# Return to the cursor position saved in printStatus
tput rc
exit
}

# Define flag for CAEN usage. Probably should take via command line input below later.
caen=0
caenParseExecutable="parseSNMP"

# Debugging
debug=1

source privateConfig.sh

# Force voltage setting without checking limits
disableLimits=0   # if -d is passed as argument this will be set to 1

# Source bash script that contains some global variable definitions
source configureVoltageLimits.sh 

# Include functions
source functions.sh

# Borrowing from here: http://stackoverflow.com/a/16496491
usage() { echo "Usage: $0 [disable | enable | log -t <refresh rate in seconds> | status | voltage -f <filename> -r[everse] -e[nableUnlimitedVoltages] -x[changeController] | voltage -s <slot> -c <channel> -v <voltage> -a[dd] -e[nableUnlimitedVoltages] -x[changeController]]  "; exit 1; }

printValues()
{
    echo "commandType: " "${commandType}"
    echo "filename: " "${filename}"
    echo "slot: " "${slot}"
    echo "channel: " "${channel}"
    echo "voltage: " "${voltage}"
    echo "refreshRate" "${refreshRate}"
    echo "reverse: " "${reverse}"
    echo "add: " "${add}"
    echo "CAEN: " "${caen}"
}

if [[ "$1" == "voltage" || "$1" == "status" || "$1" == "log" || "$1" == "enable" || "$1" == "disable" ]]
then
    commandType="${1}"
    # Shifts the input variables by one to ensure that getopts doesn't break on the first input word.
    shift 1
else
    usage
fi

while getopts "t:f:s:c:v:raex" arg; do
    case "${arg}" in 
        t)
            refreshRate="${OPTARG}"
            ;;
        f)
            filename="${OPTARG}"
            ;;
        r)
            reverse=1
            ;;
        s)
            slot="${OPTARG}"
            ;;
        c)
            channel="${OPTARG}"
            ;;
        v)
            voltage="${OPTARG}"
            ;;
        a)
            add=1
            ;;
        e)
            disableLimits=1
            ;;
        x)
            caen=1
            ;;
        *)
            usage
            ;;
    esac
done

# Handle setup for CAEN crate, or set values as appropriate for mpod
if [[ "${caen}" -eq 1 ]]
then
    ip="$caenIP"
    # This should be the name of the executable which we use to call the caen wrapper
    # Note the space after the executable name
    appendToCommand="$caenParseExecutable "
    minChannel=1
    maxChannel=12
else
    appendToCommand=""
    minChannel=1
    maxChannel=8
fi

# Parse options for validity
if [[ "${commandType}" == "voltage" ]]
then
    if [[ -n "${filename}" || (-n "${slot}" && -n "${channel}" && -n "${voltage}") ]]
    then
        if [[ -n "${filename}" ]]
        then
            #echo "Call with filename"
            if [[ ${reverse} == "1" ]];
            then
                rampDown=1
            else
                rampDown=0
            fi
            setVoltageFromFile "$ip" "$filename" "${rampDown}"
        else
            if [[ "$debug" -eq 1 ]]; then printValues; fi
            #echo "Call with slot, channel, votlage"

            if [[ "$add" -eq 1 ]]
            then
                addVoltageFromSlotChannel "$ip" "$slot" "$channel" "$voltage"
            else
                setVoltageFromSlotChannel "$ip" "$slot" "$channel" "$voltage"
            fi

            getSwitchFromSlotChannel ${ip} ${slot} ${channel}
            # Switch is whether the voltage is enabled
            switch=$value
            value=-1
            if [[ ${switch} == "0" ]]; then
                while true; do
                    read -p "The selected channel is off. Do you want to also turn it on? (y/n) [n] " answer
                    if [[ "$answer" == "y" ]]; then
                        setSwitchFromSlotChannel "${ip}" "${slot}" "${channel}" 1
                        break
                    elif [[ -z "${answer}" || "$answer" == "n" ]]; then
                        break
                    fi
                done
            fi
        fi
        exit 0;
    else
        echo "Forgot to add necessary arguments to voltage!"
        usage
    fi
fi

if [[ "${commandType}" == "log" ]]
then
    echo "Beginning logging at $(date)"
    echo "Hit Ctrl-c to exit logging"

    if [[ -z "${refreshRate}" ]]
    then
        refreshRate=5
        echo "NOTE: No refresh rate set, so it will be set to the default"
    fi

    # Pretty hacky, but sufficient
    # See: http://stackoverflow.com/a/3173242
    {
        echo ""
        echo "--------------------------------------------------------------------------------"
    } | tee -a voltage.log

    echo "Refresh rate = ${refreshRate} seconds" | tee -a voltage.log

    while true
    do
        printStatus "${ip}" true
        sleep "${refreshRate}"
    done 
fi

if [[ "${commandType}" == "status" ]]
then
    printStatus "${ip}"
    exit 0;
fi

if [[ "${commandType}" == "enable" ]]
then
    changeCratePowerState "${commandType}"
fi


if [[ "${commandType}" == "disable" ]]
then
    changeCratePowerState "${commandType}"
fi

echo "Failed to call with anything. Something with wrong. Check the script"
usage
