#!/usr/bin/env bash

# Set the index of each element.
# 0 corresponds with the lower voltage of the element
# 1 corresponds with the higher voltage of the element
# In the standard configuration, the elements are configured as:
# 	MMG=1
# 	MGEM_0=2
# 	MGEM_1=3
# 	TGEM_0=4
# 	TGEM_1=5
MMG=101
MGEM_0=102
MGEM_1=103
TGEM_0=104
TGEM_1=105

# Set the maximum voltage over any particular element
MAXVOLTAGE_MMG=600
MAXVOLTAGE_MGEM=490
MAXVOLTAGE_TGEM=490
