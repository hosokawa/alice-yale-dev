#ifdef __CINT__
/* Copyright(c) 1998-1999, ALICE Experiment at CERN, All rights reserved. *
 * See cxx source for full Copyright notice                               */

#pragma link off all globals;
#pragma link off all classes;
#pragma link off all functions;

#pragma link C++ class GEMEvent+;
#pragma link C++ class GEMTrack+;
#pragma link C++ class GEMCombinedTrack+;
#pragma link C++ class GEMCluster+;
#pragma link C++ class GEMBunch+;
#pragma link C++ class GEMMapper+;

#endif
