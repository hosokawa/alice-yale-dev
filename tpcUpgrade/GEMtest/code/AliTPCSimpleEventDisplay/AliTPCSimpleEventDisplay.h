#ifndef AliTPCSimpleEventDisplay_H
#define AliTPCSimpleEventDisplay_H

#include "THnSparse.h"
#include "TH2D.h"

#include "AliTPCCalPad.h"
#include "AliRawEventHeaderBase.h"
#include "AliTPCmapper.h"

#include "AliTPCCalibRawBase.h"

class AliRawReader;
class TH2S;

class AliTPCSimpleEventDisplay : public AliTPCCalibRawBase {
public:
  AliTPCSimpleEventDisplay();
  
  virtual Int_t Update(const Int_t isector, const Int_t iRow, const Int_t iPad,
                       const Int_t iTimeBin, const Float_t signal);

  AliTPCCalPad* GetCalPadMax() {return &fPadMax;}
//   TH1D* MakePadSignals(Int_t sec, Int_t channel);
  TH1D* MakePadSignals(Int_t sec, Int_t row, Int_t pad);
  
  Bool_t ProcessEvent(); 
  void SetRawReader (AliRawReader *reader) {fRawReader=reader;}
  
// private:
  THnSparseS  *fHnDataIROC;      //Event Data IROCs
  THnSparseS  *fHnDataOROC;      //Event Data OROCs
  AliTPCCalPad fPadMax;          //Cal Pad with max Entry per channel
  TH2D        *fHSigIROC;        //iroc signals
  TH2D        *fHSigOROC;        //oroc signals
  
  Int_t     fCurrentChannel;         //! current channel processed
  Int_t     fCurrentSector;          //! current sector processed
  Int_t     fLastSector;             //! Last sector processed
  Int_t     fSelectedSector;         //! Sector selected for processing
  Int_t     fLastSelSector;          //! Last sector selected for processing
  Int_t     fCurrentRow;             //! current row processed
  Int_t     fCurrentPad;             //! current pad processed
  Float_t   fMaxPadSignal;           //! maximum bin of current pad
  Int_t     fMaxTimeBin;             //! time bin with maximum value
  Bool_t    fSectorLoop;             //! only process one sector

  AliRawReader *fRawReader;             //! raw reader pointer
  AliTPCmapper  fTPCmapper;          //! mapper

  void ProcessPad();
  virtual void ResetEvent();
  virtual void EndEvent() {++fNevents; ProcessPad(); }
  
  ClassDef(AliTPCSimpleEventDisplay,0)
};

//----------------------------------------------------------------
// Inline Functions
//----------------------------------------------------------------
inline Bool_t AliTPCSimpleEventDisplay::ProcessEvent()
{
  if (!fRawReader) return kFALSE;
  return AliTPCCalibRawBase::ProcessEvent(fRawReader);
}
#endif

