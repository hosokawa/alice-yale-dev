#ifndef GEMEvent_H
#define GEMEvent_H

#include <TClonesArray.h>
#include <TObject.h>

#include "GEMTrack.h"
#include "GEMCombinedTrack.h"
#include "GEMBunch.h"

class GEMEvent : public TObject {
public:
  GEMEvent();
  GEMEvent(const GEMEvent &event);
  virtual ~GEMEvent();
  GEMEvent& operator = (const GEMEvent &event);
  
  void NewEvent(Int_t eventNumber, Int_t cherenkov, Double_t temperature, Double_t relativeHum, Double_t pressure);

  Int_t GetNumberOfTracks()             const { return fTracks.GetEntriesFast();  }
  Int_t GetNumberOfTracks(UChar_t roc)  const { return (roc==255)?GetNumberOfCombinedTracks():((roc<72)?fNtracks[roc]:0);  }
  Int_t GetNumberOfCombinedTracks()     const { return fCombinedTracks.GetEntriesFast();  }
  //   Int_t GetNumberOfBunches() const { return fBunches.GetEntriesFast(); }
  
//   GEMBunch* AddBunch(UChar_t row, UChar_t pad, UShort_t length, UShort_t *sig, UShort_t startTime);
  GEMTrack* AddTrack(const GEMTrack &track);
  GEMCombinedTrack* AddCombinedTrack(const GEMCombinedTrack &track);
  TClonesArray* GetCombinedTracks() {return &fCombinedTracks;}

//   const GEMBunch* GetBunch(Int_t track) const { return static_cast<const GEMBunch*>(fBunches.At(track)); }
  GEMTrack* GetTrack(Int_t track) const { return static_cast<GEMTrack*>(fTracks.At(track)); }
  GEMCombinedTrack* GetCombinedTrack(Int_t track) const { return static_cast<GEMCombinedTrack*>(fCombinedTracks.At(track)); }
  
  // setters
  void SetRunNumber       (UInt_t run)   { fRunNumber=run;            }
  void SetEventNumber     (UInt_t event) { fEventNumber=event;        }
  void SetTimeStamp       (UInt_t time)  { fTimeStamp=time;           }
  void SetCherenkovValue  (Double_t val) { fCherenkov=val;            }
  void SetPressure        (Double_t val) { fPressure=val;             }
  void SetTemperature     (Double_t val) { fTemperature=val;          }
  void SetRelativeHumidity(Double_t val) { fRelativeHumidity=val;     }

  void SetBeam (Char_t   beam)      { fBeam=beam;                }
  void SetIBF  (Double_t ibf )      { fIBF=ibf;                  }
  void SetSF   (UChar_t  sf  )      { fSF=sf;                    }
  void SetGain (UShort_t gain)      { fGain=gain;                }
  void SetTF3  (UShort_t tf3 )      { fTF3=tf3;                  }
  void SetG3G4 (Double_t g3g4)      { fG3G4=g3g4;                }
  void SetSigma(Double_t sigma)     { fSigma=sigma;              }
  void SetCompensated(Bool_t c)     { fCompensated=c;            }
  
  
  void SetNclusters    (UChar_t roc, UInt_t ncls)  { if (roc<72) fNclusters    [roc]=ncls; }
  void SetNclustersUsed(UChar_t roc, UInt_t ncls)  { if (roc<72) fNclustersUsed[roc]=ncls; }
  void SetNtracks      (UChar_t roc, UInt_t ntrk)  { if (roc<72) fNtracks      [roc]=ntrk; }
  
  // getters
  UInt_t   GetRunNumber()         const { return fRunNumber;        }
  UInt_t   GetEventNumber()       const { return fEventNumber;      }
  UInt_t   GetTimeStamp()         const { return fTimeStamp;        }
  Double_t GetCherenkovValue()    const { return fCherenkov;        }
  Double_t GetPressure()          const { return fPressure;         }
  Double_t GetTemperature()       const { return fTemperature;      }
  Double_t GetRelativeHumidity()  const { return fRelativeHumidity; }
  
  Char_t   GetBeam()              const { return fBeam;             }
  Double_t GetIBF()               const { return fIBF;              }
  UChar_t  GetSF()                const { return fSF;               }
  UShort_t GetGain()              const { return fGain;             }
  UShort_t GetTF3()               const { return fTF3;              }
  Double_t GetG3G4()              const { return fG3G4;             }
  Double_t GetSigma()             const { return fSigma;            }
  Bool_t   GetCompensated()       const { return fCompensated;      }
  
  UInt_t   GetNclusters    (UChar_t roc)  const { return (roc<72)?fNclusters[roc]    :0; }
  UInt_t   GetNclustersUsed(UChar_t roc)  const { return (roc<72)?fNclustersUsed[roc]:0; }
  
  void Print(Option_t* option = "") const;
  
  void Reset();
private:
  UInt_t fRunNumber;          // run number
  UInt_t fEventNumber;        // event number
  UInt_t fTimeStamp;          // time stamp

  UInt_t fNclusters[72];      // number of total clusters found in the event per chamber
  UInt_t fNclustersUsed[72];  // number of clusters used in tracks per chamber
  UInt_t fNtracks[72];        // number of tracks in a sector
  
  Double32_t fCherenkov;      // cherenkov signal

  Double32_t fPressure;         // Pressure value
  Double32_t fTemperature;      // Temperature value
  Double32_t fRelativeHumidity; // Relative Humidity value

  Char_t     fBeam;           // beam energy
  Double32_t fIBF;            // IBF from small prototypes
  UChar_t    fSF;             // HV scaling factor
  UShort_t   fGain;           // gain setting
  UShort_t   fTF3;            // transfer field 2 setting (V/cm)
  Double32_t fG3G4;           // G3/G4 ration
  Double32_t fSigma;          // Fe55 resolution
  Bool_t     fCompensated;    // compensated settings ()
  
  TClonesArray fTracks;           // Array of tracks per chamber
  TClonesArray fCombinedTracks;   // Array of combined tracks
  
  ClassDef(GEMEvent,6);
};

#endif
