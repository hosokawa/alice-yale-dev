#include <TMath.h>
#include <TBits.h>
#include <TLinearFitter.h>
#include <TObjArray.h>
#include <TObjString.h>
#include <TH1F.h>
#include <TF1.h>

#include "GEMTrack.h"

ClassImp(GEMTrack);

// GEMTrack::fgGainMap=0x0;
TExMap GEMTrack::fRemoveRows=TExMap();

GEMTrack::GEMTrack()
: TObject()
, fOffset(0.)
, fSlope(0.)
, fSlopeError(0.)
, fOffsetError(0.)
, fChi2(0.)
, fOffsetTime(0.)
, fSlopeTime(0.)
, fROC(0)
, fClusters("GEMCluster")
{
  
}

//________________________________________________________________________________
GEMTrack::GEMTrack(const GEMTrack &track)
: TObject(track)
, fOffset(track.fOffset)
, fSlope(track.fSlope)
, fSlopeError(track.fSlopeError)
, fOffsetError(track.fOffsetError)
, fChi2(track.fChi2)
, fOffsetTime(track.fOffsetTime)
, fSlopeTime(track.fSlopeTime)
, fROC(track.fROC)
, fClusters(track.fClusters)
{
  // copy ctor
}

//________________________________________________________________________________
GEMTrack::GEMTrack(UChar_t roc, Double_t offset, Double_t slope, Double_t offsetError, Double_t slopeError, Double_t chi2, Double_t offsetTime, Double_t slopeTime)
: TObject()
, fOffset(offset)
, fSlope(slope)
, fSlopeError(slopeError)
, fOffsetError(offsetError)
, fChi2(chi2)
, fOffsetTime(offsetTime)
, fSlopeTime(slopeTime)
, fROC(roc)
, fClusters("GEMCluster")
{
  // ctor with parameters
}

//________________________________________________________________________________
GEMTrack& GEMTrack::operator = (const GEMTrack &track)
{
  // assignment operator
  if (&track == this) return *this;
  new (this) GEMTrack(track);
  
  return *this;
}

//________________________________________________________________________________
GEMCluster* GEMTrack::AddCluster(const GEMCluster &cluster)
{
  // add a cluster
  GEMCluster *c = new(fClusters[GetNumberOfClusters()]) GEMCluster(cluster);
  c->SetTrack(this);
  return c;
}

//________________________________________________________________________________
Double_t GEMTrack::GetTruncatedMean(Double_t low/*=0.05*/, Double_t high/*=0.7*/, Int_t type/*=1*/) const
{
  //
  // calculate truncated mean with truncation from low to high
  // type = n*10 + m
  // qmax (m=0) or qtot (m=1)
  // truncated mean (n=0) or landau fit (n=1)
  //

  Int_t qtotRequested = type % 10;
  Int_t fitRequested = type / 10;
  
  const Int_t nClusters=fClusters.GetEntriesFast();
  
  Double_t *values=new Double_t[nClusters];
  Int_t    *index =new Int_t[nClusters];

  Int_t nClustersUsed=0;
  for (Int_t i=0; i<nClusters; ++i){
    const GEMCluster* c = GetClusterFast(i);
    values[nClustersUsed] = qtotRequested == 0 ? c->GetQMax() : c->GetQTot();
    ++nClustersUsed;
  }
  TMath::Sort(nClustersUsed,values,index,kFALSE);

  static TH1F hist("___Temp_ClusterDistr____", "___Temp_ClusterDistr____", 10, 0, 800);
  static TF1 fit("___Temp_ClusterDistr_LandauFit____", "landau/[2]", 0, 800);
  
  if (fitRequested) {
    hist.Reset("ICESM");
    if (hist.GetSumw2()->GetSize() == 0) hist.Sumw2();
  }
  
  Double_t dEdx=0;
  Int_t    nclusterSel=0;
  for (Int_t icl=0; icl<nClustersUsed; ++icl){
    if ( icl<TMath::Nint(low*nClustersUsed) ) continue;
    if ( icl>TMath::Nint(high*nClustersUsed))  break;

    if (fitRequested == 0) {
      dEdx += values[index[icl]];
      ++nclusterSel;
    }
    else {
      hist.Fill(values[index[icl]]);
    }
  }

  if (fitRequested == 0) {
    if (nclusterSel>0){
      dEdx /= nclusterSel;
    }
  }
  else {
    hist.Scale(1.,"width");
    fit.SetParameter(0, hist.GetEntries());
    fit.SetParameter(1, hist.GetMean());
    fit.SetParameter(2, hist.GetRMS());
    fit.SetParameter(0, hist.GetEntries()*hist.GetEntries()/fit.Integral(0,800));
    Int_t fitStatus = hist.Fit(&fit, "QN", "", values[index[TMath::Nint(low*nClustersUsed)]], values[index[TMath::Min(TMath::Nint(high*nClustersUsed), nClustersUsed-1)]]);
    if (fitStatus==0) {
      dEdx = fit.GetParameter(1);
    }
    else {
      dEdx = 0;
    }
  }

  delete [] index;
  delete [] values;
  
  return dEdx;
}

//________________________________________________________________________________
void GEMTrack::Refit(Int_t minNPads, Int_t maxNPads, TBits *badClusters/*=0x0*/)
{
  Double_t fitClustersX[63] = {0};
  Double_t fitClustersY[63] = {0};
  Double_t fitClustersTime[63] = {0};
  Int_t nAccepted = 0;
  for(Int_t i = 0; i< fClusters.GetEntries(); i++){
    const GEMCluster *temp = GetClusterFast(i);

    if ( temp->GetNPads()<minNPads || temp->GetNPads() > maxNPads) continue;
    if (badClusters && badClusters->TestBitNumber(temp->GetRow())) continue;
    fitClustersX[nAccepted]    = temp->GetX();
    fitClustersY[nAccepted]    = temp->GetY();
    fitClustersTime[nAccepted] = temp->GetTimeBinW();
    nAccepted++;
  }

  TLinearFitter fitter(1,"pol1","D");

  // fit pad vs row space
  fitter.AssignData(nAccepted, 1, fitClustersX, fitClustersY);
  fitter.Eval();
  fitter.Chisquare();

  fChi2        =  fitter.GetChisquare();
  fOffset      = fitter.GetParameter(0);
  fSlope       = fitter.GetParameter(1);
  fSlopeError  = fitter.GetParError(1);
  fOffsetError = fitter.GetParError(0);

  // fit time vs row space
  fitter.ClearPoints();
  fitter.AssignData(nAccepted, 1, fitClustersX, fitClustersTime);
  fitter.Eval();
  fitter.Chisquare();
  
  fOffsetTime  = fitter.GetParameter(0);
  fSlopeTime   = fitter.GetParameter(1);
}

//________________________________________________________________________________
void GEMTrack::SetRemoveRowsdEdx(TString rows)
{
  TObjArray *arr=rows.Tokenize(",");
  fRemoveRows.Expand(arr->GetEntriesFast());
  for (Int_t i=0;i<arr->GetEntriesFast();++i) {
    Int_t row=((TObjString*)arr->At(i))->String().Atoi();
    fRemoveRows(row)=1;
  }
}

//________________________________________________________________________________
void GEMTrack::AddClustersFromTrack(GEMTrack *track)
{
  if (!track) return;
  Int_t nclus = track->fClusters.GetEntriesFast();
  // Maybe this is not the best way since it allocated more memory for the clusters.
  // Instead it could use the same pointers, but then fClusters should be a TObjArray rather than a TClonesArray
  for (Int_t i = 0; i < nclus; i++) AddCluster(*static_cast<GEMCluster*>(track->fClusters.At(i)));
}

//________________________________________________________________________________
Double_t GEMTrack::GetAveragePad(Bool_t w) const
{
  // Calculates the average pad.

  Double_t weigthSum = 0;
  Double_t avg = 0;
  
  for (Int_t i = 0; i < GetNumberOfClusters(); i++) {
    const GEMCluster* clus = GetCluster(i);
    if (!clus) continue;

    if (w) {
      avg += clus->GetPad()*clus->GetQTot();
      weigthSum += clus->GetQTot();
    }
    else {
      avg += clus->GetPad();
      weigthSum += 1.;
    }
  }

  avg /= weigthSum;

  return avg;
}
