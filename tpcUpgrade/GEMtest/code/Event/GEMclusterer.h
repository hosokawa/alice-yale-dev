#ifndef GEMCLUSTERER_H
#define GEMCLUSTERER_H
/* Copyright(c) 1998-1999, ALICE Experiment at CERN, All rights reserved. *
 * See cxx source for full Copyright notice                               */


#include <TString.h>
#include <TClonesArray.h>
#include <TArray.h>
#include <TExMap.h>
#include <TObject.h>

//GEM stuff
#include "GEMEvent.h"
#include "GEMCombinedTrack.h"
#include "AliRawReaderGEM.h"
#include "AliRawReaderGEMDate.h"

class TFile;
class TGraph;
class TTree;
class TObjArray;

class AliTPCCalROC;
class AliGEMRawStreamV3;
class AliTPCAltroMapping;
class AliTPCCalROC;

class GEMclusterer : public TObject {

public:
  static const Int_t kNumberOfROC = 3;

  enum { kGoodChannel = 0, kDeadChannel = 1, kWarmChannel = 2, kHotChannel = 3 };
  
  GEMclusterer();
  GEMclusterer(const GEMclusterer &ped);
  virtual ~GEMclusterer();

  GEMclusterer& operator = (const  GEMclusterer &source);

  //
  Bool_t ProcessEvent();

  //
  Bool_t ProcessAllEvents(Int_t min = 0, Int_t max = -1);

  //
  void SetRawReader(AliRawReader* reader) {
    if (dynamic_cast<AliRawReaderGEMDate*>(reader) != 0) {
      fDateMode = kTRUE;
    }
    else if (dynamic_cast<AliRawReaderGEM*>(reader) != 0) {
      fDateMode = kFALSE;
    }
    else {
      Printf("Fatal error: the raw reader provided is not of the expected type!");
      return;
    }
    fRawReaderGEM = reader;
  }

  //
  void  SetSkipROC(Int_t roc) { fSkipROC(roc) = 1;             }
  Int_t GetSkipROC(Int_t roc) { return fSkipROC.GetValue(roc); }
  
  //
  AliTPCAltroMapping **GetAltroMapping() const { return fMapping; };
  void  SetAltroMapping(AliTPCAltroMapping **mapp) { fMapping = mapp; };
  //
  //
  Int_t  GetFirstTimeBin() const { return fFirstTimeBin; }
  Int_t  GetLastTimeBin()  const { return fLastTimeBin;  }
  Int_t  GetAdcMin()       const { return fAdcMin;       }
  Int_t  GetAdcMax()       const { return fAdcMax;       }
  Int_t  GetEventCounter() const { return fEventCounter; }
  Int_t  GetSignalCounter()  const { return fSignalCounter; }
  Int_t  GetClusterCounter() const { return fClusterCounter;}
  Double_t GetWindowTime()   const { return fWindowTime;    }
  Double_t GetWindowPad()    const { return fWindowPad;     }
  Int_t  GetAllowedMisses() const { return fNAllowedMisses;}
  Int_t  GetMinClusters()    const { return fNMinClusters;   }

  const TClonesArray* GetClusters(UChar_t roc) const {return (roc<kNumberOfROC)?(TClonesArray*)fArrClusterArrays[roc]:0x0;}

  void TrackFinder(Int_t roc, Int_t nTries, Bool_t reverse);
  void DoCombineTracks();
  void GetFit(TGraph *trackGraph, Double_t& slope, Double_t& offset, Double_t& chi2, Double_t& slopeError, Double_t& offsetError);

  void  SetRangeTime(Int_t tMin, Int_t tMax){ fFirstTimeBin=tMin; fLastTimeBin=tMax;}  // Set time bin range that is used for the pedestal calibration
  void  SetRangeAdc (Int_t aMin, Int_t aMax=1050){ fAdcMin=aMin; fAdcMax=aMax; }  // Set adc range for the pedestal calibration
  void  SetEventsPerBin(Int_t value) { fEventsPerBin = value; }
  void  SetWindowTime(Double_t window) { fWindowTime=window; }
  void  SetWindowPad(Double_t window)  { fWindowPad =window; }
  void  SetAllowedMisses(Int_t nMisses) { fNAllowedMisses = nMisses; }
  void  SetMinClusters(Int_t minCl)     { fNMinClusters = minCl; }
  void  SetBlackEvent()                 { fBlackEvent = 1; }
  void  SetExtraPedestal()                 { fExtraPedestal = 1; }
  void  SetExcludeHotWarmDead(Bool_t h, Bool_t w, Bool_t d) { fExcludeHotChannels = h; fExcludeWarmChannels = w; fExcludeDeadChannels = d; }

  void ResetData();
  void CloseFile();

  void SetOutputFileName(const char* name) { fOutFileName=name; }
  void SetLogFileName(const char* logFile) { fLogFile=logFile; }
  void SetRunResUBiased(Bool_t runubiased) {fRunResUBiased=runubiased;}
  void SetCalibFile(const char* calFile)       { fCalibFile = calFile     ; }
  void SetPressureValuesFile(const char* map)  { fPressureValuesFile = map; }
  void SetPedDir(const char* peddir)           { fPedDir = peddir; }
  void SetExtraPedestalFile(const char* peddir)           { fExtraPedestalFile = peddir; }

  // switch bunch storing on. In this case we switch off the clustering and tracking
  // if both are needed the clustering needs to be switched on again afterwards
  void SetStoreBunches(Bool_t storeBunches=kTRUE) { fStoreBunches=storeBunches; if (storeBunches) fClusterAndTracking=kFALSE; }

  void SetDoClusteringAndTracking(Bool_t clusterAndTracking=kTRUE) { fClusterAndTracking=clusterAndTracking; }

  void SetDebugLevel(Int_t level) { fDebugLevel=level; }

  const GEMEvent* GetGEMEvent() const {return fGEMEvent;}

  void AddCombineROC(UChar_t roc) {
    if (fNcombineROC == GEMCombinedTrack::kNMaxTracks) {
      Printf("ERROR: cannot combine an additional ROC, the maximum has been reached (%d).", fNcombineROC);
      return;
    }

    for (Int_t i = 0; i < fNcombineROC; i++) {
      if (fCombineROC[i] == roc) return;
    }

    fCombineROC[fNcombineROC] = roc;
    fNcombineROC++;
  }
  
private:
  Int_t Update(const Int_t iRoc, const Int_t iRow, const Int_t iPad,
               const Int_t iTimeBin, Float_t signal,
               const Int_t iPatch=-1, const Int_t iBranch=-1);
  void  FindLocalMaxima(const Int_t iRoc);

  void AddCluster(const GEMCluster &cluster);
  GEMBunch* AddBunch(UChar_t roc, UChar_t row, UChar_t pad, UShort_t length, Int_t *sig, UShort_t startTime);
  
  void MakeArrays();                // Create arrays for random data acces
  void CleanArrays();               // Clean arrays for random data acces
  void SetExpandDigit(const Int_t iRow, Int_t iPad, Int_t iTimeBin, 
                      const Float_t signal); // Fill arrays with signals
  void GetPadAndTimeBin(Int_t bin, Int_t& iPad, Int_t& iTimeBin); // Get pad and time bin corresponding to the 1d bin
  Float_t GetQ(const Float_t* adcArray, const Int_t time,
               const Int_t pad, const Int_t maxTimeBins,
               Int_t& timeMin,Int_t& timeMax,Int_t& padMin,Int_t& padMax) const;

  void Init(Int_t run);

  TClonesArray* GetClusterArray(UChar_t roc) const {return (TClonesArray*)fArrClusterArrays[roc];}

  Bool_t ProcessEventTPC(AliGEMRawStreamV3 *const rawStreamV3);
  Bool_t ProcessEventGEM(AliRawReaderGEM   *const rawReader);
  Bool_t ProcessEventGEMDate(AliRawReaderGEMDate   *const rawReader);
  
  Int_t fFirstTimeBin;              //  First Time bin needed for analysis
  Int_t fLastTimeBin;               //  Last Time bin needed for analysis
  Int_t fAdcMin;                    //  min adc channel for cluster qmax
  Int_t fAdcMax;                    //  max adc channel for cluster qmax
  Double_t fWindowPad;              //  search window in pad direction
  Double_t fWindowTime;             //  search window in time direction
  Int_t fNAllowedMisses;            //  number of allowed row misses in track
  Int_t fNMinClusters;              //  minimum number of clusters required in track

  TExMap fSkipROC;                  // map of ROCs to skip

  AliTPCAltroMapping **fMapping;    //! Altro Mapping object

  //event stuff
  TObjArray    fArrClusterArrays;    // array with cluster arrays
  TClonesArray *fBunches;            // array of raw data
  GEMEvent *fGEMEvent;               // GEM Event structure
  TTree    *fTree;                   // output Tree
  TFile    *fFile;                   // output File
  TString   fOutFileName;            // output file name
  TString   fLogFile;                // log file name
  TString   fPressureValuesFile;     // pressure value file
  TString   fCalibFile;              // file with calib info (bad channels, gain map)
  Bool_t    fLoadCalib;              // load calib info
  Bool_t    fExcludeHotChannels;     // exclude hot channels
  Bool_t    fExcludeWarmChannels;    // exclude warm channels
  Bool_t    fExcludeDeadChannels;    // exclude dead channels
  TObjArray *fLogData;               // logbook data
  Bool_t    fBlackEvent;             // specifies whether black event
  TString   fPedDir;                 // directory containing pedestal files (for black events)
  Bool_t    fExtraPedestal;          // specifies whether using extra pedestal
  TString   fExtraPedestalFile;      // file containing extra pedestal files from black events
  //

  Int_t   fEventCounter;            // event Counter
  
  Int_t fEventsPerBin;              // Events per bin for event histograms
  Int_t fSignalCounter;             // Signal counter
  Int_t fClusterCounter;            // Cluster counter

  AliGEMRawStreamV3*    fTPCRawStream; // TPC raw stream object, used to read TPC digits
  AliRawReader*         fRawReaderGEM; // GEM raw stream object, used to read general info of the event
  Bool_t                fDateMode;     // Whether the AliRawReaderGEMBase object provided in fRawReaderGEM is of type AliRawReaderGEM or AliRawReaderGEMDate
  //
  //  Expand buffer
  //
  Float_t** fAllBins;              //! array for digit using random access
  Int_t**   fAllSigBins;           //! array of pointers to the indexes over threshold
  Int_t*    fAllNSigBins;          //! 
  Int_t fRowsMax;                  //!  Maximum number of time bins
  Int_t fPadsMax;                  //!  Maximum number of time bins
  Int_t fTimeBinsMax;              //!  Maximum number of time bins

  Bool_t fRunResUBiased;           // Run with unbiased residual fits
  Bool_t fStoreBunches;            // store bunches in the tree
  Bool_t fClusterAndTracking;      // do cluster finding and tracking


  Int_t fDebugLevel;             // debug level

  // calibration data
  TH2           *fGainMap[kNumberOfROC];         // gain map file
  TH2           *fDeadChannelMap[kNumberOfROC];  // dead channel map
  TH2           *fPedestalMap[kNumberOfROC];     // pedestal map
  TH2           *fExtraPedestalMap[kNumberOfROC];// Extra pedestal map
  TGraph        *fPressureValues;      // pressure values

  // Additional options
  Int_t      fNcombineROC;                                  // Number of ROC whose tracks are to be combined
  UChar_t    fCombineROC[GEMCombinedTrack::kNMaxTracks];    // List of ROC indexes whose tracks are to be combined

  ClassDef(GEMclusterer, 4)  // Simple GEM tracker
};



#endif

