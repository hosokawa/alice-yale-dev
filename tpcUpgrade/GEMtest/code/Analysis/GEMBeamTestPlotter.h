class TList;
class TFile;
class TTree;
class TArrayI;

#define kNROC   3
#define kNPAD  40
#define kNROW  32

class GEMBeamTestPlotter : public TNamed {
 public:
  GEMBeamTestPlotter();
  GEMBeamTestPlotter(const char* outputPath);
  ~GEMBeamTestPlotter();

  enum EPartType { kElectron = 0, kPion = 1 };

  // If tree==0 only the run loop w/o terminate is called (LoadOutput, RunLoop); otherwise the full analysis is called (Init, EventLoop, RunLoop, SaveOutput).
  Bool_t         Start(TTree* tree=0);  // If tree=NULL try to load previous output and plot histograms

  // Init the tree branches, plus other initializations. It is executed once at the beginning of the analysis.
  virtual Bool_t Init(TTree* tree);

  // Load the previously generated histograms.
  virtual Bool_t LoadOutput();

  // Sabe the generated histograms.
  virtual Bool_t SaveOutput();

  // The event loop is implemented here. This function will call Exec() for each event.
  virtual Bool_t EventLoop();

  // This function will call the "exec" functions of each analysis.
  virtual Bool_t Exec();

  // This function implements the run loop. Terminate() (if requested) and Plot() are called for each run.
  virtual Bool_t RunLoop(Bool_t terminate=kTRUE);

  // This function will call the "terminate" functions of each analysis.
  virtual Bool_t Terminate();

  // This function will call the "plot" functions of each analysis.
  virtual Bool_t Plot();

  void LoadCalib(Int_t run);
  void RecalibrateTracks();
  void RecalibrateTracks(Bool_t combTracks);
  void GetBinMinMax(const TH1 *hist, const Float_t frac, Int_t &bin1, Int_t &bin2);
  TProfile* PlotAndProfile(TH2* histo);

  Bool_t SaveAllPlots();
  
  // Each analysis can implement a set of three functions, an "exec" function (event-by-event), a "terminate" function (run-by-run) and a "plot" function (run-by-run).

  // Pad-by-pad gain map analysis
  Bool_t         ExecGainMap();
  Bool_t         TerminateGainMap(const char* suffix="", Int_t rescale=1);
  // rescale: 0 = do not rescale, 1 = rescale by the average of the map and by the ROC 1-2 ratio, 2 = rescale by the ROC 1-2 ratio using the *Red* histogram
  Bool_t         PlotGainMap(const char* suffix="");

  // Cluster-by-cluster gain map analysis
  Bool_t         ExecClusterGainMap();
  Bool_t         TerminateClusterGainMap();
  Bool_t         PlotClusterGainMap();

  // dEdx analysis
  Bool_t         ExecDEDX();
  Bool_t         TerminateDEDX();
  Bool_t         PlotDEDX();

  // Track QA
  Bool_t         ExecTrackQA();
  Bool_t         TerminateTrackQA();
  Bool_t         PlotTrackQA();
  
  Bool_t         ExecDEDX(Bool_t combTracks);
  Bool_t         PlotDEDX(Int_t roc);
  Bool_t         PlotSeparation(TH1* histo1, TH1* histo2, const char* cname, const char* ctitle, const char* fitFunct);

  Bool_t         ExecTrackQA(Bool_t combTracks);
  Bool_t         ExecTrackQA(Bool_t combTracks, const GEMTrack* track, TString partTypeStr, TString suffix="");
  Bool_t         PlotTrackQA(TString suffix);
    
  void           SetMinADC(Float_t min)                           { fMinADC                  = min; }
  void           SetTruncatedMeanFraction(Double_t f)             { fTruncatedMeanFraction   = f  ; }
  void           SetRenormalizeROC2(Bool_t b)                     { fRenormalizeROC2         = b  ; }
  void           SetEventRange(Int_t min, Int_t max)              { fMinEvent                = min; fMaxEvent                = max; }
  void           SetPlotHistograms(Bool_t b)                      { fPlotHistograms          = b  ; }
  void           SetCalibFileName(const char* c)                  { fCalibFileName           = c  ; }
  void           SetSavePlots(Bool_t b)                           { fSavePlots               = b  ; }
  
  // static methods
  static TTree* GenerateChain(const char* dataPath, const char* fileName, Int_t run); // if run == 0 use all files in dataPath
  static Bool_t IsTrackAccepted(const GEMTrack* track);
  static void   SetDebugLevel(Int_t lev) { fDebugLevel = lev; }
  
 protected:
  void SavePlot(TCanvas *c);
  TH1* GetHistogram(const char* hname, const char* htitle = "hist", Int_t nbins = 0, Double_t xmin = 0, Double_t xmax = 0, Bool_t overwrite = kFALSE);
  TH2* GetHistogram(const char* hname, const char* htitle, Int_t nbinsx, Double_t xmin, Double_t xmax, Int_t nbinsy, Double_t ymin, Double_t ymax, Bool_t overwrite = kFALSE);
  TH1* DuplicateHistogram(const char* hname, const char* htitle, TH1* orig, Bool_t overwrite = kFALSE);
  TCanvas* GetCanvas(const char* cname, const char* ctitle, Int_t w = 700, Int_t h = 500, Bool_t overwrite = kFALSE);
  void AddRun();
  
  TString       fOutputPath             ;
  TString       fFileOutName            ;
  Int_t         fNumberOfROCs           ;
  Int_t         fMaxPad                 ;
  Int_t         fMaxRow                 ;
  Bool_t        fSaveOutput             ;
  Bool_t        fSavePlots              ;
  TString       fPlotFormat             ;
  Bool_t        fPlotHistograms         ;
  Bool_t        fPlotSignalPerPad       ;
  Float_t       fMinADC                 ;
  Int_t         fMinEvent               ;
  Int_t         fMaxEvent               ;
  Double_t      fTruncatedMeanFraction  ;
  Bool_t        fRenormalizeROC2        ;
  TTree*        fTree                   ;
  Int_t         fRun                    ;
  GEMEvent*     fEvent                  ;
  TClonesArray* fBunches                ;
  TList*        fHistograms             ;
  TList*        fCanvases               ;
  TFile*        fOutputFile             ;
  Bool_t        fRunChanged             ;
  TH1I*         fRunList                ;
  Int_t         fNRuns                  ;
  TString       fCalibFileName          ;
  TFile*        fCalibFile              ;
  TH2*          fGainMap[kNROC]         ;
  TH2*          fDeadChannelMap[kNROC]  ;
  Bool_t        fCalibOk                ;
  Int_t         fNSelTracks[kNROC]      ;
  Int_t         fNSelCombTracks         ;

  // static fields
  static Int_t fDebugLevel;
  
 private:
   
  GEMBeamTestPlotter(const GEMBeamTestPlotter &source);
  GEMBeamTestPlotter& operator=(const GEMBeamTestPlotter& source);
  
  ClassDef(GEMBeamTestPlotter, 2);
  
};

