#ifndef MAKETRACKGAINMAP_H
#define MAKETRACKGAINMAP_H

void MakeTrackGainMap(TTree* t);
void computePadGains(TTree* t);
void computeGainMaps(Int_t roc);
void scaleGainMaps();
void writeGainMaps(Int_t roc, TFile* f);
void writePadGains(Int_t roc, TFile* f);
TH1F* GetHistoPadGains(const TString &id, Int_t row, Int_t pad);
TH1F* GetHistoClusterQTot(const TString &id);
TH1F* GetHistoNumClusPerTrack(const TString &id);
TH2F* GetHistoGainMaps(const TString &name);
void truncateHisto(TH1F* h);

#endif 
