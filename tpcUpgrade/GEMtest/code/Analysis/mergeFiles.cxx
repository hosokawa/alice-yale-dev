#ifndef __CINT__

#include <TFileMerger.h>
#include <TString.h>
#include <TObjArray.h>
#include <TSystem.h>

#include "../Analysis/configurationParameters.h"

#endif

void mergeFiles(const TString dir)
{
	// Create file merger
	TFileMerger merger;

	// Gathers files to process
	TString fBasedir(dir);

	TString s = gSystem->GetFromPipe(Form("ls %s/data1646_*.root",fBasedir.Data()));
	TObjArray *arr=s.Tokenize("\n");

	// Selects files
	for (Int_t ifile=0; ifile < arr->GetEntriesFast(); ++ifile)
	{
		merger.AddFile(arr->At(ifile)->GetName());
                Printf("File %s added.", arr->At(ifile)->GetName());
	}

	merger.OutputFile(Form("%s/data1646.root", fBasedir.Data()));

	merger.Merge();
}
