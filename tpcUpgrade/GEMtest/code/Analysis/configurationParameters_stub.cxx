// Configuration parameters
#include "configurationParameters.h"

// Number of ROCs
//const Int_t configurationParameters::kNumberOfROC = 3;  // for techinical reason this had to be defined in the header file.

// Parameters for GEMclusterer
const Double_t configurationParameters::fWindowPad = 2;
const Double_t configurationParameters::fWindowTime = 2;
const Int_t configurationParameters::fADCMin = 3;
const Int_t configurationParameters::fNAllowedMisses = 3;
const Int_t configurationParameters::fNMinClusters = 25;

// Parameters for FillHistos/GEMBeamTestPlotter
const Int_t configurationParameters::fMinClusters[configurationParameters::kNumberOfROC] = {25, 25, 25}; // suggested 30 for ROC 1 and 25 for ROC 2
const Double_t configurationParameters::fMaxChi2[configurationParameters::kNumberOfROC] = {10, 10, 10};  // suggested chi2 cut = 0.7
const Double_t configurationParameters::fMinSlope[configurationParameters::kNumberOfROC] = {-10, -0.0050, -0.0015};
const Double_t configurationParameters::fMaxSlope[configurationParameters::kNumberOfROC] = {10, 0.0700, 0.0015};
const Int_t configurationParameters::fMinClustersCombined = 55;
const Int_t configurationParameters::fTimeCutMin[configurationParameters::kNumberOfROC] = {0, 38, 41};
const Int_t configurationParameters::fTimeCutMax[configurationParameters::kNumberOfROC] = {300, 77, 82};
const Int_t configurationParameters::fEdgePadMin = 30;
const Int_t configurationParameters::fEdgePadMax = 36;
const Double_t configurationParameters::fSlopeCut = 0.1;
const Double_t configurationParameters::fTruncatedMeanFraction = 0.70;
const Double_t configurationParameters::fMaxEdgeClusterFraction = 0.3;

// File locations
const TString configurationParameters::fDigitsLocation = "";
const TString configurationParameters::fTracksLocation = "";
const TString configurationParameters::fAnalysisOutputLocation = "";
const TString configurationParameters::fRawDataLocation = "";
//TString configurationParameters::fROCMapLocation = "";
//TString configurationParameters::fGainMapLocation = "";
