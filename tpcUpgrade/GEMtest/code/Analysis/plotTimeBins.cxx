// Future user: I wrote this as quickly as possible, sorry.
//

#include <TTree.h>
#include <TChain.h>
#include <TList.h>
#include <TString.h>
#include <TFile.h>
#include <TCanvas.h>
#include <TSystem.h>
#include <TSystemDirectory.h>
#include <TRegexp.h>
#include <TClonesArray.h>
#include <TH2F.h>

#include "plotTimeBins.h"
#include "../Event/GEMEvent.h"
#include "../Event/GEMTrack.h"
#include "../Event/GEMCluster.h"
#include "../Event/GEMCombinedTrack.h"

#include "configurationParameters.h"
#include "GEMBeamTestPlotter.h"

GEMEvent *fEvent=0x0;
TClonesArray * fClusters=0x0;
TString outdir;

const TString gDataPath(configurationParameters::fTracksLocation);
const TString gFileName("data%d.root");
const TString gFileOutName("timeBinPlots.root");
const Int_t gNROC = 2;
const Int_t gNMAXPADN = 50;
const Bool_t gSaveRootFile = kTRUE;
const Bool_t gSavePlots = kTRUE;
const TString gPlotFormat("pdf");
const Bool_t gPlotSignalPerPad = kFALSE;

enum EPartType { kEle=0, kPio=1 };
const Int_t numPart = 2;

void plotTimeBins(Int_t run)
{
  // Determine output directory                                                                                          
  outdir = Form(configurationParameters::fAnalysisOutputLocation, "PlotTimeBins");

  // Create directory for output                                                                                         
  gSystem->Exec(Form("mkdir -p %s", outdir.Data()));
  printf("Output directory: %s\n", outdir.Data());

  TTree *chain = 0;
  if (run == 0) chain = GenerateFullChain();
  else chain = LoadTree(run);
  
  if (!chain) {
    Printf("Unable to load chain. Returning...");
    return;
  }

  EPartType partType = kEle;
  
  Double32_t * offsetTime = 0x0;
  
  chain->SetBranchAddress("Events",&fEvent);
  
  TH2F* timeBins[gNROC][numPart] = {0};
  TCanvas* timeBinsCanvs[gNROC][numPart] = {0};
  
  TH1F * timeOffset = 0;
  
  TString hname = "";
  TString htitle = "";
  
  GEMTrack * track = 0x0;
  GEMCombinedTrack * combinedTrack = 0x0;
  TClonesArray * clusters = 0x0;
  const GEMCluster * cluster = 0x0;
  
  // Define hists
  for (Int_t roc = 0; roc < gNROC; roc++) {
    Printf("ROC = %d", roc+1);  
    for(Int_t part=kEle; part<kPio+1; part++){  
      if(part==kEle){
	hname = Form("ROC%dTimeBins_Ele", roc+1);
	htitle = Form("Time Bins, ROC %d, Ele;Time Bin;QMax;counts", roc+1);    
      } else{
	hname = Form("ROC%dTimeBins_Pio", roc+1);
	htitle = Form("Time Bins, ROC %d, Pio;Time Bin;QMax;counts", roc+1);    
      }
      timeBins[roc][part] = new TH2F(hname, htitle, 200, 0, 200, 100, 0, 100);
    }
  }
  
  timeOffset = new TH1F("timeOffset", "Time Offset", 200, 0, 100);
  
  for (Int_t iev=0; iev < chain->GetEntries(); ++iev){
    if(iev%5000 == 0) Printf("Processing event: %d",iev);
    chain->GetEntry(iev);
    
    for (Int_t roc = 0; roc < gNROC; roc++){
      if (fEvent->GetNumberOfTracks(roc+1) != 1) { continue; }
      
      // loop through all tracks in event, and get the one corresponding to current ROC
      Int_t numberOfTracks = fEvent->GetNumberOfTracks();
      for (Int_t itrack=0; itrack < numberOfTracks; ++itrack) {
	track=fEvent->GetTrack(itrack);
	if ( track->GetROC()== roc+1 ) break;  
      }
      if ( track->GetROC()!= roc+1 ) continue;

      //----- PID                                                                                                    
      if (fEvent->GetCherenkovValue()<40){ // PIONS                                                                  
	partType=kPio;
      } else if (fEvent->GetCherenkovValue()>50){ // Electrons                                                       
	partType=kEle;
      } else {
	continue;
      }

      // Make same cuts as for dE/dx (will need to modify time bin cut in config file
      if(!GEMBeamTestPlotter::IsTrackAccepted(track)) continue;

      int nClusters = track->GetNumberOfClusters();
      for (int i = 0; i < nClusters; i++) {
	cluster = track->GetCluster(i);
	timeBins[roc][partType]->Fill(cluster->GetTimeMax(), cluster->GetQMax());  
      }
    }
    
    // Combined tracks
    if (fEvent->GetNumberOfTracks(255) != 1) { continue; }
    combinedTrack = fEvent->GetCombinedTrack(0);

    Int_t num = combinedTrack->GetNumberOfSubTracks();    
    for (int i = 0; i < num; i++){
      timeOffset->Fill(combinedTrack->GetOffsetTime(i));
    }
    
  }
  
  TString cname = "";
  TString ctitle = "";
  
  Bool_t plotProjections = true;
  const Int_t timeIntervals = 100;
  const Int_t timeMax = 100;
  const Int_t timeSlices = timeMax/timeIntervals;
  Printf("time slices = %d",timeSlices);
  TH1D* timeSlice[gNROC][numPart][timeIntervals];
  TCanvas* timeSliceCanvs = new TCanvas();
  for (Int_t roc = 0; roc < gNROC; roc++) {
    for(Int_t part=kEle; part<kPio+1; part++){
      if(part==kEle){
 	cname = Form("ROC%dTimeBinCanv_Ele", roc+1);
	ctitle = Form("ROC %d, Ele, Time Bin", roc+1);
      } else{
	cname = Form("ROC%dTimeBinCanv_Pio", roc+1);
	ctitle = Form("ROC %d, Pio, Time Bin", roc+1);
      }
      timeBinsCanvs[roc][part] = new TCanvas(cname, ctitle);
      
      timeBins[roc][part]->Draw("colz");
      
      if (gSavePlots) SavePlot(timeBinsCanvs[roc][part]);
      
      if(plotProjections){
	// project the Qmax vs time histogram onto a 1d qmax histogram (for a given time slice)
	for(Int_t i=0; i<timeIntervals; i++){
	  Int_t firstBin = i*timeSlices;
	  Int_t lastBin = (i+1)*timeSlices - 1; 
	  timeSlice[roc][part][i] = timeBins[roc][part]->ProjectionY("timeSlice", firstBin, lastBin);
	  if(part==kEle){
	    cname = Form("ClusProf_ROC%d_Ele_t%d-%d",roc+1,firstBin,lastBin);
	    ctitle = Form("cluster profile, ROC %d, Ele, t=%d-%d",roc+1,firstBin,lastBin);
	  } else{
	    cname = Form("ClusProf_ROC%d_Pio_t%d-%d",roc+1,firstBin,lastBin);
	    ctitle = Form("cluster profile, ROC %d, Pio, t=%d-%d",roc+1,firstBin,lastBin);
	  }
	  timeSlice[roc][part][i]->SetTitle(ctitle);
	  timeSliceCanvs->cd();
	  timeSliceCanvs->Clear();
	  timeSliceCanvs->SetName(cname);
	  timeSliceCanvs->SetTitle(ctitle);
	  timeSlice[roc][part][i]->Draw();
	  SavePlot(timeSliceCanvs);
	}
      }
    }
  }  

  TCanvas * c = new TCanvas("timeOffsetCanv", "Time Offset");
  timeOffset->Draw();
  if (gSavePlots) SavePlot(c);
  
}

//______________________________________________________
void SavePlot(TCanvas *c)
{
  TString fname(Form("%s/%s.%s", outdir.Data(), c->GetName(), gPlotFormat.Data()));
  c->Update();
  c->SaveAs(fname);
}

//______________________________________________________
TChain* GenerateFullChain()
{
	// Generate a TChain using all files contained in gDataPath that follows the pattern specified in gFileName.

	TSystemDirectory dir(".", gDataPath);
	TList *files = dir.GetListOfFiles();

	TString strRegexp(gFileName);
	strRegexp.ReplaceAll("%d","*");
	TRegexp regexp(strRegexp,kTRUE);

	TChain *chain = new TChain("GEM");

	if (files) {
		TSystemFile *file = 0;
		TString fname;
		TIter next(files);
		while ((file=(TSystemFile*)next())) {
			fname = file->GetName();
			if (!file->IsDirectory() && fname.Index(regexp) != kNPOS) {
				TString fullName(Form("%s/%s",gDataPath.Data(),fname.Data()));
				chain->AddFile(fullName);
			}
		}
	}

	delete files;
	files = 0;

	return chain;
}


//______________________________________________________
TTree* LoadTree(Int_t run)
{
	// Return the TTree contained in the data file of one run.

	TString fname(Form(gFileName, run));
	TString fullName(Form("%s/%s",gDataPath.Data(),fname.Data()));
	TFile *file = TFile::Open(fullName);
	if (!file || file->IsZombie()) return 0;
	TTree *tree = dynamic_cast<TTree*>(file->Get("GEM"));
	return tree;
}
