void RunGEMBeamTestPlotter(const char* suffix="")
{
  gROOT->LoadMacro("loadlibs.C");

  loadlibs("../..", 1);

  gROOT->LoadMacro("../Analysis/GEMBeamTestPlotter.cxx+g");

  Bool_t useCalibMap = kFALSE;
  TString calibFileName;
    
  TString dataPath = configurationParameters::fTracksLocation;
  //TString dataPath = configurationParameters::fDigitsLocation;
  
  const char * username = 0;
  username = gSystem->Getenv("USER");
  if (username == 0) {
    Printf("Failed to find username!");
    gSystem->Exit(0);
  }

  if (useCalibMap) {
    if (dataPath.Contains("BadChannelsOnly")) {
      calibFileName = Form("/home/%s/TPCupgrade/CalibFiles/ClusterBased.root", username);
    }
    else if (dataPath.Contains("NoGainMap")) {
      calibFileName = Form("/home/%s/TPCupgrade/CalibFiles/ClusterBasedGainMapOnly.root", username);
    }
    else {
      Printf("Cannot use gain map with calibrated data!");
      useCalibMap = kFALSE;
    }
  }

  TString outputPath = Form("%s_output", dataPath.Data());

  outputPath += suffix;
  
  if (useCalibMap) {
    outputPath += "_calib";
  }

  GEMBeamTestPlotter::SetDebugLevel(1);
  
  GEMBeamTestPlotter* plotter = new GEMBeamTestPlotter(outputPath);
  plotter->SetMinADC(configurationParameters::fADCMin);
  plotter->SetTruncatedMeanFraction(configurationParameters::fTruncatedMeanFraction);
  plotter->SetRenormalizeROC2(kTRUE);
  plotter->SetCalibFileName(calibFileName);
  plotter->SetSavePlots(kTRUE);

  // Use 0 for all runs, or select a specific run number
  TTree* tree = GEMBeamTestPlotter::GenerateChain(dataPath, "data%d.root", 1646);
  plotter->Start(tree);
  //plotter->Start();
}
