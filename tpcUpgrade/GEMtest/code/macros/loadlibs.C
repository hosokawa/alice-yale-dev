void loadlibs(const char *dir=".", Int_t eventDisplay = 1)
{ 
  TString exDir(gSystem->ExpandPathName(dir));
  Printf("Running loadLibs.C from %s/%s", gSystem->WorkingDirectory(), exDir.Data());

  //fix ld path for par files
  gSystem->Exec("export LD_LIBRARY_PATH=.:$LD_LIBRARY_PATH");
  
  //set inlcude paths
  gSystem->AddIncludePath("-I$ROOTSYS/include");
  Bool_t hasAR = !TString(gSystem->Getenv("ALICE_ROOT")).IsNull();
  if (hasAR) gSystem->AddIncludePath("-I$ALICE_ROOT/ -I$ALICE_ROOT/include -I$ALICE_ROOT/STEER  -I$ALICE_ROOT/ANALYSIS -I$ALICE_ROOT/TPC -I$ALICE_ROOT/TPC/Base -I$ALICE_ROOT/RAW");

  // With newer versions of root/aliroot it is not necessary anymore to load aliroot libraries (taken care of automatically bu "rootmaps")
  // However, it stills seems to be necessary on some systems
  
  gSystem->Load("libCore");
  gSystem->Load("libPhysics");
  gSystem->Load("libMinuit");
  gSystem->Load("libGui.so");

  gSystem->Load("libGeom");
  gSystem->Load("libVMC");

  gSystem->Load("libNet");
  gSystem->Load("libTree");
  gSystem->Load("libProof");

  gSystem->Load("libSTEERBase");
  gSystem->Load("libESD");
  gSystem->Load("libCDB");
  gSystem->Load("libRAWDatabase");
  gSystem->Load("libRAWDatarec");
  gSystem->Load("libANALYSIS");
  gSystem->Load("libSTEER");
  gSystem->Load("libSTAT");

  gSystem->Load("libTPCbase");
  gSystem->Load("libTPCrec");
  
  gSystem->Load("libThread");
  
  gSystem->AddIncludePath(Form("-I%s/code/RawReader",exDir.Data()));
  gSystem->AddIncludePath(Form("-I%s/code/Event",exDir.Data()));

  gSystem->Exec(Form("cd %s/code; make",exDir.Data()));

  gSystem->Load(Form("%s/code/libGEMEvent.so",exDir.Data()));
  if (!gSystem->AccessPathName(Form("%s/code/libGEMtest.so",exDir.Data()))){
    gSystem->Load(Form("%s/code/libGEMtest.so",exDir.Data()));
  }  

  if (gSystem->FindFile(Form("%s/code/Analysis/", exDir.Data() ), "configurationParameters.cxx") == 0)
  {
    gSystem->Exec(Form("cp %s/code/Analysis/configurationParameters_stub.cxx %s/code/Analysis/configurationParameters.cxx", exDir.Data(), exDir.Data()));
  }

  // Load configuration parameters
  gROOT->LoadMacro(Form("%s/code/Analysis/configurationParameters.cxx+", exDir.Data()));

  // Change the options if we are running with or without the alternative mapping
  GEMMapper::SetUseAlternativeMapping(eventDisplay);

  if (GEMMapper::GetUseAlternativeMapping()) {
    Printf("The alternative mapping for ROC > 0 is enabled!");
  }
  else {
    Printf("The alternative mapping for ROC > 0 is disabled!");
  }
}
