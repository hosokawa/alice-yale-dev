// Macro to run PlotPedestals.C
// Run from macros directory
// Outputs can be found in user's scratch/PedestalPlots
//
// Relevant pedestal runs: 1603,1624,1637

void RunPlotPedestals() {
  gROOT->LoadMacro("loadlibs.C");
  loadlibs("../..");
  
  gROOT->LoadMacro("../Analysis/PlotPedestals.cxx+");
  PlotPedestals(1603);
  PlotPedestals(1624);
  PlotPedestals(1637);
}
