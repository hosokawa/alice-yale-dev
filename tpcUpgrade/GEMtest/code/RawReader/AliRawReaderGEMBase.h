#ifndef ALIRAWREADERGEMBASE_H
#define ALIRAWREADERGEMBASE_H

struct equipmentHeaderStruct;
struct commonDataHeaderStruct;

class AliRawReaderGEMBase {
public:
  AliRawReaderGEMBase();
  virtual ~AliRawReaderGEMBase() {;}

  enum { kMaxCAMAC=100 };
  enum { kCamacEvent=0, kCamacCherenkov, kCamacFstScinti, kCamacSndScinti, kCamacPbGlass, kCamacTemperature, kCamacRelativeHumidity, kCamacPressure };
  Int_t    GetEventFromTag() const      {return fEventFromTag;}
  
  const Double_t* GetCamacData()          const { return fCamacData; }
  Double_t GetCamacData(UInt_t data)      const { return (data<fNCamacData)?fCamacData[data]:0; }
  UInt_t   GetNCamacData()                const { return fNCamacData; }

  static const char* GetCamacDataName(UInt_t data);
  
protected:
  UInt_t                  fEventInFile;    // event number in file
  UInt_t                  fEventFromTag;   // event from the event tag
  Double_t                fCamacData[kMaxCAMAC]; //camac values
  UInt_t                  fNCamacData;     // number of camac values
  
  ClassDef(AliRawReaderGEMBase,0);      // Raw data reader for GEM test 2012  
};

#endif

