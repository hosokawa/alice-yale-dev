#!/usr/bin/env bash
# Get the root path of the project code from the caller string
export GEM_ROOT=$(dirname $(dirname $(readlink -f $0)))

function error {
  if [ -z "$2" ]; then usage; fi

    echo "" >&2
    echo "-----------------------------------------------------------" >&2
    echo -e "\e[00;31mERROR in '$0': $1\e[00m" >&2
    echo "-----------------------------------------------------------" >&2

    exit 1
}

function usage {
  echo "Usage:"
  echo "makeTree <options>"
  echo
  echo "Options:  -i,--infile=  'file'     input file"
  echo "          -o,--outfile= 'file'     output file"
  echo "          -l,--logbook= 'file'     logbook file (default: $logbook)"
  echo "          -g,--gainfile='file'     gain calibration file (default: $gainMap)"
  echo "                                   switch off using -g ''"
  echo "          -n,--nevents= 'num'      run max 'num' events"
  echo "          -u,--ubiased             calculate ubiased residuals (takes longer)"
  echo "          --mapdir=                use a different mapping directory"
  echo "          --pedDir=                pass pedestal directory (for black events)"
  echo "          --bunches-only           write only raw data bunches, don't do clustering and tracking"
  echo "          --black                  process black event runs"
  echo "          --eventDisplay           process events for an event display by changing the x and y coordinates"
  echo ""
}

infile=
outfile=
nevents=100000000
logbook=
resunbiased=0
bunches=0
blackEvents=0
eventDisplay=1
gainMap=
mapDir=
peddir=

TRAIN_OPTS=`getopt -o i:o:n:m:l:g:u --long infile:,outfile:,nEventMin:,nEventMax:,logbook:,gainfile:,ubiased,mapdir:,pedDir:,bunches-only,bunches,eventDisplay,black \
-n 'run' -- "$@"`

if [ $? != 0 ] ; then usage; exit 1 ; fi
  
eval set -- "$TRAIN_OPTS"
  
while true ; do
  case "$1" in
    -i|--infile)    infile=$2;              shift 2;;
    -o|--outfile)   outfile=$2;             shift 2;;
    -l|--logbook)   logbook=$2;             shift 2;;
    -g|--gainfile)  gainMap=$2;             shift 2;;
    -n|--nEventMin) nEventMin=$2;           shift 2;;
    -m|--nEventMax) nEventMax=$2;           shift 2;;  
    -u|--unbiased)  resunbiased=1;          shift;;
    --bunches)      bunches=1;              shift;;
    --bunches-only) bunches=1; gainMap="";  shift;;
    --black)        blackEvents=1;          shift;;
    --eventDisplay) eventDisplay=0;         shift;;
    --mapdir)       mapDir=$2;              shift 2;;
    --pedDir)       peddir=$2;              shift 2;;
    --) shift ; break ;;
    *) echo "Internal error!" ; exit 1 ;;
  esac
done

if [ -z "$infile" ]||[ -z "$outfile" ]; then
  error "You need to select input and output file"
fi

root.exe -l -q -b $GEM_ROOT/code/macros/loadlibs.C\(\"$GEM_ROOT\",$eventDisplay\) $GEM_ROOT/code/macros/makeTree.C\(\"$infile\",\"$outfile\",\"$logbook\",\"$gainMap\",$nEventMin,$nEventMax,$resunbiased,$bunches,$blackEvents,\"$mapDir\",\"$peddir\"\)

