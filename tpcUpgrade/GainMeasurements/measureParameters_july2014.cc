#include "measureParametersUtils.cc"

int measureParameters_july2014()
{
  localPath = "/Users/saiola/Google Drive/MMG_Measurements/GainEnergyRes/july2014";
  saveRootFile = true;
  savePdfFiles = true;
  saveTxtFile = true;
  plotSpectra = true;
  
  TH1::AddDirectory(kFALSE);

  std::vector <dataSet> dataSets;

  gStyle->SetOptStat(0);
  gStyle->SetOptTitle(0);

  if (saveTxtFile) {
    std::string txtFileName(localPath);
    txtFileName += "/GainResIBF.txt";
    fout.open(txtFileName);
  }

  dataSets.push_back(dataSet("ne-co2-ch4", "Ne(80) + CO_{2}(10) + CH_{4}(10)"));
  dataSets.at(0).calibFileName = "calibration/calibration_2014-07-17.txt";
  dataSets.at(0).K = 185.;
  dataSets.at(0).color = kBlue+2;
  calibration(dataSets.at(0));
  generateFileList(dataSets.at(0).measurements,0);
  measureParametersForDataset(dataSets.at(0),plotSpectra);
  measureIbfForDataset(dataSets.at(0));
  generateTGraphsForDataset(dataSets.at(0));

  dataSets.push_back(dataSet("ne-co2-cf4", "Ne(80) + CO_{2}(10) + CF_{4}(10)"));
  dataSets.at(1).calibFileName = "calibration/calibration_2014-07-22.txt";
  dataSets.at(1).K = 185.;
  dataSets.at(1).color = kRed+2;
  calibration(dataSets.at(1));
  generateFileList(dataSets.at(1).measurements,1);
  measureParametersForDataset(dataSets.at(1),plotSpectra);
  measureIbfForDataset(dataSets.at(1));
  generateTGraphsForDataset(dataSets.at(1));

  printGainVsRes(dataSets);
  printGainVsIbf(dataSets);
  printResVsIbf(dataSets);
  
  if (saveRootFile) saveDatasets(dataSets);

  return 0;
}

int main()
{
  return measureParameters_july2014();
}
