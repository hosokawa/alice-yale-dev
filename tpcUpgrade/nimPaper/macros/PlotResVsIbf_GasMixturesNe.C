#include <TGraph.h>
#include <TString.h>
#include <TROOT.h>

class PlottingUtilities;

void PlotResVsIbf_GasMixturesNe()
{
  gROOT->LoadMacro("HistoStyler.cxx+g");
  gROOT->LoadMacro("GEMmeasurement.cxx+g");
  gROOT->LoadMacro("PlottingUtilities.cxx+g");
  
  TString name  = "GasMixturesNe";
  TString title = "Neon-based gas mixtures";

  PlottingUtilities* plotUtils = new PlottingUtilities(name, title);
  plotUtils->ReadFile("Ne", 0.9, 0.1, "", 0., "_best");
  plotUtils->ReadFile("Ne", 0.9, 0.1, "N2", 0.05);
  plotUtils->ReadFile("Ne", 0.82, 0.09, "CH4", 0.09);
  plotUtils->ReadFile("Ne", 0.82, 0.09, "CF4", 0.09);

  /*
  plotUtils->WriteFile("Ne", 0.9, 0.1, "", 0., "_best");
  plotUtils->WriteFile("Ne", 0.9, 0.1, "N2", 0.05);
  plotUtils->WriteFile("Ne", 0.8, 0.1, "CH4", 0.1);
  plotUtils->WriteFile("Ne", 0.8, 0.1, "CF4", 0.1);
  */
  
  plotUtils->SetXLegendPos(0.45);
  plotUtils->SetExpandWidth(1.1);
  plotUtils->SetLegendTextSize(16);
  
  TCanvas* c = plotUtils->PlotResVsIbf(GEMmeasurement::kGasMix | GEMmeasurement::kEtran, GEMmeasurement::kMMGvolt | GEMmeasurement::kGEM1volt | GEMmeasurement::kGEM2volt);

  TH1* blankHist = dynamic_cast<TH1*>(c->GetListOfPrimitives()->At(0));
  if (!blankHist) {
    Printf("Error could not find blank histogram!");
    return;
  }
  blankHist->GetXaxis()->SetRangeUser(0.1,1.3);
  blankHist->GetYaxis()->SetRangeUser(8,18);

  TIter next(c->GetListOfPrimitives());
  TLegend* leg = 0;
  TObject* obj = 0;
  while ((obj = next())) {
    if (obj->InheritsFrom("TLegend")) {
      leg = dynamic_cast<TLegend*>(obj);
      if (leg) break;
    }
  }
  if (leg) leg->SetX1(0.3);


  TGraph* g1 = dynamic_cast<TGraph*>(c->cd(1)->GetListOfPrimitives()->At(1));
  g1->SetMarkerStyle(kFullCircle);
  g1->SetMarkerSize(0.9);
  g1->SetLineStyle(1);

  TGraph* g2 = dynamic_cast<TGraph*>(c->cd(1)->GetListOfPrimitives()->At(2));
  g2->SetMarkerStyle(kFullSquare);
  g2->SetMarkerSize(0.9);
  g2->SetLineStyle(1);

  TGraph* g3 = dynamic_cast<TGraph*>(c->cd(1)->GetListOfPrimitives()->At(3));
  g3->SetMarkerStyle(kFullCross);
  g3->SetMarkerSize(1.3);
  g3->SetLineStyle(1);

  TGraph* g4 = dynamic_cast<TGraph*>(c->cd(1)->GetListOfPrimitives()->At(4));
  g4->SetMarkerStyle(kFullStar);
  g4->SetMarkerSize(1.4);
  g4->SetLineStyle(2);

  TGraph* g5 = dynamic_cast<TGraph*>(c->cd(1)->GetListOfPrimitives()->At(5));
  g5->SetMarkerStyle(kFullStar);
  g5->SetMarkerSize(1.4);
  g5->SetLineStyle(3);

  TLegend* leg = dynamic_cast<TLegend*>(c->cd(1)->GetListOfPrimitives()->At(6));
  TList* l = leg->GetListOfPrimitives();
  TIter next(l);
  TLegendEntry* entry = 0;
  while (entry = (TLegendEntry*)next()) {
    //entry->SetOption("l");
  }
  
  PlottingUtilities::SavePlot(c);
}
