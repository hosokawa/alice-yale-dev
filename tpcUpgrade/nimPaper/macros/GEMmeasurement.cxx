#include <TString.h>
#include <TMath.h>
#include <iomanip>

#include "GEMmeasurement.h"

ClassImp(GEMmeasurement);

//____________________________________________________________________________________
GEMmeasurement::GEMmeasurement() :
  TObject(),
  fMainGasName(""),
  fMainGasFrac(1.),
  fCO2Frac(0.),
  fQuencherGasName(""),
  fQuencherGasFrac(0.),
  fMMGvolt(0.),
  fGEM1volt(0.),
  fGEM2volt(0.),
  fEdrift(0.),
  fEtran(0.),
  fEind(0.),
  fGA(0.),
  fEres(0.),
  fIBF(0.),
  fAnodeCurrent(0.),
  fCathodeCurrent(0.),
  fCrosstalkConfig(0.),
  fCrosstalkMeas(0)
{
}

//____________________________________________________________________________________
GEMmeasurement::GEMmeasurement(const char* gasName, Double_t frac, Double_t CO2frac, const char* quencherName, Double_t quencherfrac) :
  TObject(),
  fMainGasName(gasName),
  fMainGasFrac(frac),
  fCO2Frac(CO2frac),
  fQuencherGasName(quencherName),
  fQuencherGasFrac(quencherfrac),
  fMMGvolt(0.),
  fGEM1volt(0.),
  fGEM2volt(0.),
  fEdrift(0.),
  fEtran(0.),
  fEind(0.),
  fGA(0.),
  fEres(0.),
  fIBF(0.),
  fAnodeCurrent(0.),
  fCathodeCurrent(0.),
  fCrosstalkConfig(0.),
  fCrosstalkMeas(0)
{
}

//____________________________________________________________________________________
Bool_t GEMmeasurement::ReadDataOld(std::istream& s)
{
  if (!s.good()) return kFALSE;
  s >> fMMGvolt >> fGEM1volt >> fGEM2volt;
  if (!s.good()) return kFALSE;
  s >> fGA >> fEres >> fIBF;

  return kTRUE;
}

//____________________________________________________________________________________
Bool_t GEMmeasurement::ReadData(std::istream& s)
{
  if (!s.good()) return kFALSE;
  s >> fMMGvolt >> fGEM1volt >> fGEM2volt;

  if (!s.good()) return kFALSE;
  s >> fEdrift >> fEtran >> fEind;
  
  if (!s.good()) return kFALSE;
  s >> fGA >> fEres >> fIBF;

  if (!s.good()) return kFALSE;
  s >> fAnodeCurrent >> fCathodeCurrent;
    
  if(fCrosstalkMeas) {
    if (!s.good()) return kFALSE;
    s >> fCrosstalkConfig;
  }

  return kTRUE;
}

//____________________________________________________________________________________
Bool_t GEMmeasurement::WriteData(ostream& s)
{
  if (!s.good()) return kFALSE;
  
  s.setf(std::ios::fixed, std:: ios::floatfield);
  
  s.precision(0);
  s << std::setw(10) << fMMGvolt << std::setw(10) << fGEM1volt << std::setw(10) << fGEM2volt;

  s.precision(3);
  s.width(10);
  s << std::setw(10) << fEdrift << std::setw(10) << fEtran << std::setw(10) << fEind;

  s.precision(3);
  s.width(10);
  s << std::setw(10) << fGA << std::setw(10) << fEres << std::setw(10) << fIBF;

  s.precision(3);
  s.width(10);
  s << std::setw(10) << fAnodeCurrent << std::setw(10) << fCathodeCurrent;
    
  s << std::endl;

  return kTRUE;
}

//____________________________________________________________________________________
const char* GEMmeasurement::GetName() const
{
  static TString name;  // needs to be static, otherwise it is deleted when the function is out-of-scope
  name = GenerateMeasurementName(this);
  return name;
}

//____________________________________________________________________________________
const char* GEMmeasurement::GetTitle() const
{
  static TString title; // needs to be static, otherwise it is deleted when the function is out-of-scope
  title = GenerateMeasurementTitle(this);
  return title;
}

//____________________________________________________________________________________
const char* GEMmeasurement::GetTitle(UInt_t params) const
{
  static TString title; // needs to be static, otherwise it is deleted when the function is out-of-scope
  
  TString gasMix;
  if ((params & kGasMix) == kGasMix) gasMix = GenerateGasMixtureTitle(fMainGasName, fMainGasFrac, fCO2Frac, fQuencherGasName, fQuencherGasFrac);

  Double_t mmgV = -1;
  if ((params & kMMGvolt) == kMMGvolt) mmgV = fMMGvolt;

  Double_t gem1V = -1;
  if ((params & kGEM1volt) == kGEM1volt) gem1V = fGEM1volt;

  Double_t gem2V = -1;
  if ((params & kGEM2volt) == kGEM2volt) gem2V = fGEM2volt;

  Double_t edrift = -1;
  if ((params & kEdrift) == kEdrift) edrift = fEdrift;

  Double_t etran = -1;
  if ((params & kEtran) == kEtran) etran = fEtran;

  Double_t eind = -1;
  if ((params & kEind) == kEind) eind = fEind;
    
  Double_t ctalk = -1;
  if ((params & kCrosstalkConfig) == kCrosstalkConfig) ctalk = fCrosstalkConfig;
  
  title = GenerateMeasurementTitle(gasMix, mmgV, gem1V, gem2V, edrift, etran, eind, ctalk);
  return title;
}

//____________________________________________________________________________________
const char* GEMmeasurement::GetName(UInt_t params) const
{
  static TString name; // needs to be static, otherwise it is deleted when the function is out-of-scope
  
  TString gasMix;
  if ((params & kGasMix) == kGasMix) gasMix = GenerateGasMixtureName(fMainGasName, fMainGasFrac,fCO2Frac, fQuencherGasName, fQuencherGasFrac);

  Double_t mmgV = -1;
  if ((params & kMMGvolt) == kMMGvolt) mmgV = fMMGvolt;

  Double_t gem1V = -1;
  if ((params & kGEM1volt) == kGEM1volt) gem1V = fGEM1volt;

  Double_t gem2V = -1;
  if ((params & kGEM2volt) == kGEM2volt) gem2V = fGEM2volt;

  Double_t edrift = -1;
  if ((params & kEdrift) == kEdrift) edrift = fEdrift;

  Double_t etran = -1;
  if ((params & kEtran) == kEtran) etran = fEtran;

  Double_t eind = -1;
  if ((params & kEind) == kEind) eind = fEind;
  
  name = GenerateMeasurementName(gasMix, mmgV, gem1V, gem2V, edrift, etran, eind);
  return name;
}

//____________________________________________________________________________________
void GEMmeasurement::SetMeasuredParameters(Double_t ga, Double_t eres, Double_t ibf, Double_t anode, Double_t cathode)
{
  fGA = ga;
  fEres = eres;
  fIBF = ibf;
  fAnodeCurrent = anode;
  fCathodeCurrent = cathode;
}

//____________________________________________________________________________________
TString GEMmeasurement::GenerateMeasurementName(const GEMmeasurement* meas)
{
  return GenerateMeasurementName(meas->fMainGasName, meas->fMainGasFrac, meas->fCO2Frac, meas->fQuencherGasName, meas->fQuencherGasFrac, meas->fMMGvolt, meas->fGEM1volt, meas->fGEM2volt, meas->fEdrift, meas->fEtran, meas->fEind);
}

//____________________________________________________________________________________
TString GEMmeasurement::GenerateMeasurementTitle(const GEMmeasurement* meas)
{
  return GenerateMeasurementTitle(meas->fMainGasName, meas->fMainGasFrac, meas->fCO2Frac, meas->fQuencherGasName, meas->fQuencherGasFrac, meas->fMMGvolt, meas->fGEM1volt, meas->fGEM2volt, meas->fEdrift, meas->fEtran, meas->fEind, meas->fCrosstalkConfig);
}

//____________________________________________________________________________________
TString GEMmeasurement::GenerateMeasurementName(const char* gasName, Double_t gasfrac, Double_t CO2frac, const char* quencherName, Double_t quencherfrac,
						Double_t mmg, Double_t gem1, Double_t gem2, Double_t edrift, Double_t etran, Double_t eind)
{
  TString gasMix = GenerateGasMixtureName(gasName, gasfrac, CO2frac, quencherName, quencherfrac);
  return GenerateMeasurementName(gasMix, mmg, gem1, gem2, edrift, etran, eind);
}

//____________________________________________________________________________________
TString GEMmeasurement::GenerateMeasurementTitle(const char* gasName, Double_t gasfrac, Double_t CO2frac, const char* quencherName, Double_t quencherfrac,
						 Double_t mmg, Double_t gem1, Double_t gem2, Double_t edrift, Double_t etran, Double_t eind, Double_t ctalk)
{
  TString gasMix = GenerateGasMixtureTitle(gasName, gasfrac, CO2frac, quencherName, quencherfrac);
  return GenerateMeasurementTitle(gasMix, mmg, gem1, gem2, edrift, etran, eind, ctalk);
}

//____________________________________________________________________________________
TString GEMmeasurement::GenerateGasMixtureName(const char* gasName, Double_t gasfrac, Double_t CO2frac, const char* quencherName, Double_t quencherfrac)
{
  if (strcmp(gasName, "")==0) return "";

  TString gasMix(gasName);

  if (gasfrac < 1.) {
    gasMix += Form("_%.0f", gasfrac*100);
    
    if (CO2frac > 0) gasMix += Form("_CO2_%.0f", CO2frac*100);
    if (quencherfrac > 0) gasMix += Form("_%s_%.0f", quencherName, quencherfrac*100);
  }

  return gasMix;
}

//____________________________________________________________________________________
TString GEMmeasurement::GenerateGasMixtureTitle(const char* gasName, Double_t gasfrac, Double_t CO2frac, const char* quencherName, Double_t quencherfrac)
{ 
  if (strcmp(gasName, "")==0) return "";

  TString gasMix(gasName);

  Double_t mainGasFrac = 1. - CO2frac - quencherfrac;
  if (mainGasFrac < 1.) {

    Int_t mainPerc = TMath::FloorNint(gasfrac*100);
    Int_t CO2perc = TMath::FloorNint(CO2frac*100);
    Int_t quenchPerc = TMath::FloorNint(quencherfrac*100);

    if (CO2perc > 0 && quenchPerc > 0) {
      gasMix += TString::Format(" + %s + %s (%d-%d-%d)", BeautifyChemicalFormula("CO2").Data(), BeautifyChemicalFormula(quencherName).Data(), mainPerc, CO2perc, quenchPerc);
    }
    else if (CO2perc > 0) {
      gasMix += TString::Format(" + %s (%d-%d)", BeautifyChemicalFormula("CO2").Data(), mainPerc, CO2perc);
    }
    else if (quenchPerc > 0) {
      gasMix += TString::Format(" + %s (%d-%d)", BeautifyChemicalFormula(quencherName).Data(), mainPerc, quenchPerc);
    }

  }

  return gasMix;
}

//____________________________________________________________________________________
TString GEMmeasurement::GenerateMeasurementName(const char* gasMixName, Double_t mmg, Double_t gem1, Double_t gem2, Double_t edrift, Double_t etran, Double_t eind)
{
  TString name(gasMixName);
  
  if (mmg >=0) {
    if (!name.IsNull()) name += "_";
    name += Form("MMG_%.0f", mmg);
  }

  if (gem1 >=0) {
    if (!name.IsNull()) name += "_";
    name += Form("GEM1_%.0f", gem1);
  }

  if (gem2 >=0) {
    if (!name.IsNull()) name += "_";
    name += Form("GEM2_%.0f", gem2);
  }

  if (edrift >=0) {
    if (!name.IsNull()) name += "_";
    name += Form("Edrift_%.0f", edrift*1000);
  }

  if (etran >=0) {
    if (!name.IsNull()) name += "_";
    name += Form("Etran_%.0f", etran*1000);
  }

  if (eind >=0) {
    if (!name.IsNull()) name += "_";
    name += Form("Eind_%.0f", eind*1000);
  }

  return name;
}

//____________________________________________________________________________________
TString GEMmeasurement::GenerateMeasurementTitle(const char* gasMixTitle, Double_t mmg, Double_t gem1, Double_t gem2, Double_t edrift, Double_t etran, Double_t eind, Double_t ctalk)
{
  TString title(gasMixTitle);
  
  if (mmg >=0) {
    if (!title.IsNull()) title += " - ";
    title += Form("V_{MMG} = %.0f V", mmg);
  }

  if (gem1 >=0) {
    if (!title.IsNull()) title += " - ";
    title += Form("#DeltaV_{GEM}^{top} = %.0f V", gem1);
  }

  if (gem2 >=0) {
    if (!title.IsNull()) title += " - ";
    title += Form("#DeltaV_{GEM}^{mid} = %.0f V", gem2);
  }

  if (edrift >=0) {
    if (!title.IsNull()) title += " - ";
    title += Form("E_{drift} = %.1f kV/cm", edrift);
  }

  if (etran >=0) {
    if (!title.IsNull()) title += " - ";
    title += Form("E_{transfer} = %.1f kV/cm", etran);
  }

  if (eind >=0) {
    if (!title.IsNull()) title += " - ";
    title += Form("E_{ind} = %.3f kV/cm", eind);
  }
    
  if (ctalk >= 0) {
    if(ctalk==1) title += "Narrow collimation, square pads";
    if(ctalk==2) title += "Wide collimation, square pads";
    if(ctalk==3) title += "Wide collimation, strips";
  }

  return title;
}

//____________________________________________________________________________________
void GEMmeasurement::Print(Option_t* /*option*/) const
{
  Printf("%s", GetTitle());
}

//____________________________________________________________________________________
TString GEMmeasurement::BeautifyChemicalFormula(const char* input)
{
  TString output;

  Int_t i = 0;
  while(input[i] != '\0') {
    Int_t j = 0;
    while (isdigit(input[i+j])) j++;
    if (j > 0) {
      output += "_{";
      output.Append(&input[i], j);
      output += "}";
    }
    else {
      output.Append(input[i]);
    }
    i++;
  }

  return output;
}

//____________________________________________________________________________________
Bool_t GEMmeasurement::IsSimilarTo(GEMmeasurement* meas, UInt_t params) const
{
  if (!meas) return kFALSE;
  
  Bool_t isSimilar = kTRUE;

  if ((params & kGasMix) == kGasMix) {
    TString prevGas = meas->GetTitle(kGasMix);
    TString currGas = GetTitle(kGasMix);
    if (prevGas != currGas) isSimilar = kFALSE;
  }
      
  if ((params & kMMGvolt) == kMMGvolt && 
      meas->GetMMGvolt() != GetMMGvolt()) isSimilar = kFALSE;

  if ((params & kGEM1volt) == kGEM1volt && 
      meas->GetGEM1volt() != GetGEM1volt()) isSimilar = kFALSE;

  if ((params & kGEM2volt) == kGEM2volt && 
      meas->GetGEM2volt() != GetGEM2volt()) isSimilar = kFALSE;

  if ((params & kEdrift) == kEdrift && 
      meas->GetEdrift() != GetEdrift()) isSimilar = kFALSE;

  if ((params & kEtran) == kEtran && 
      meas->GetEtran() != GetEtran()) isSimilar = kFALSE;
      
  if ((params & kEind) == kEind && 
      meas->GetEind() != GetEind()) isSimilar = kFALSE;
    
    if ((params & kCrosstalkConfig) == kCrosstalkConfig &&
        meas->GetCrosstalkConfig() != GetCrosstalkConfig()) isSimilar = kFALSE;

  return isSimilar;
}


//____________________________________________________________________________________
TString GEMmeasurement::GetParamAxisName(UInt_t param)
{
  TString n;
  
  if ((param & kMMGvolt) == kMMGvolt) {
    n = "V_{MMG} (V)";
  }
  else if ((param & kGEM1volt) == kGEM1volt) {
    n = "#DeltaV_{GEM}^{top} (V)";
  }
  else if ((param & kGEM2volt) == kGEM2volt) {
    n = "#DeltaV_{GEM}^{mid} (V)";
  }
  else if ((param & kEdrift) == kEdrift) {
    n = "E_{drift} (kV/cm)";
  }
  else if ((param & kEtran) == kEtran) {
    n = "E_{transfer} (kV/cm)";
  }
  else if ((param & kEind) == kEind) {
    n = "E_{ind} (kV/cm)";
  }
  else if ((param & kGA) == kGA) {
    n = "<GA>";
  }
  else if ((param & kEres) == kEres) {
    n = "#sigma / Mean (%)";
  }
  else if ((param & kIBF) == kIBF) {
    n = "IBF (%)";
  }
  else if ((param & kAnode) == kAnode) {
    n = "I_{anode} (nA)";
  }
  else if ((param & kCathode) == kCathode) {
    n = "I_{cathode} (nA)";
  }

  return n;
}

//____________________________________________________________________________________
Double_t GEMmeasurement::GetParam(UInt_t param) const
{
  if ((param & kMMGvolt) == kMMGvolt) {
    return GetMMGvolt();
  }
  else if ((param & kGEM1volt) == kGEM1volt) {
    return GetGEM1volt();
  }
  else if ((param & kGEM2volt) == kGEM2volt) {
    return GetGEM2volt();
  }
  else if ((param & kEdrift) == kEdrift) {
    return GetEdrift();
  }
  else if ((param & kEtran) == kEtran) {
    return GetEtran();
  }
  else if ((param & kEind) == kEind) {
    return GetEind();
  }
  else if ((param & kGA) == kGA) {
    return GetGA();
  }
  else if ((param & kEres) == kEres) {
    return GetEres();
  }
  else if ((param & kIBF) == kIBF) {
    return GetIBF();
  }
  else if ((param & kAnode) == kAnode) {
    return GetAnodeCurrent();
  }
  else if ((param & kCathode) == kCathode) {
    return GetCathodeCurrent();
  }
  else {
    return -1;
  }
}
