#ifndef GEMMEASUREMENT_H
#define GEMMEASUREMENT_H

#include <istream>

#include <TObject.h>

class TString;

class GEMmeasurement : public TObject {
 public:

  enum EParams { kGasMix   = 1<<0,
		 kMMGvolt  = 1<<1,
		 kGEM1volt = 1<<2,
		 kGEM2volt = 1<<3,
		 kEdrift   = 1<<4,
		 kEtran    = 1<<5,
		 kEind     = 1<<6,
		 kGA       = 1<<7,
		 kAnode    = 1<<8,
		 kEres     = 1<<9,
		 kIBF      = 1<<10,
		 kCathode  = 1<<11,
         kCrosstalkConfig = 1<<12,
         kIonDensity = 1<<13 };
  
  GEMmeasurement();
  GEMmeasurement(const char* gasName, Double_t frac = 0.9, Double_t CO2frac = 0.1, const char* quencherName = "", Double_t quencherfrac = 0.0);

  void SetMainGasName(const char* gasName, Double_t frac)         { fMainGasName      = gasName  ; fMainGasFrac = frac     ; }
  void SetCO2Frac(Double_t frac)                                  { fCO2Frac          = frac     ; }
  void SetQuencherGas(const char* gasName, Double_t frac)         { fQuencherGasName  = gasName  ; fQuencherGasFrac = frac ; }
  void SetVoltages(Double_t mmg, Double_t gem1, Double_t gem2)    { fMMGvolt          = mmg      ; fGEM1volt        = gem1 ; fGEM2volt        = gem2 ; }
  void SetEfields(Double_t edrift, Double_t etran, Double_t eind) { fEdrift           = edrift   ; fEtran           = etran; fEind            = eind ; }
  void SetCrosstalkMeas(Bool_t b) { fCrosstalkMeas = b; }

  TString     GetMainGasName()    const { return fMainGasName   ; }
  Double_t    GetMMGvolt()        const { return fMMGvolt       ; }
  Double_t    GetGEM1volt()       const { return fGEM1volt      ; }
  Double_t    GetGEM2volt()       const { return fGEM2volt      ; }
  Double_t    GetEdrift()         const { return fEdrift        ; }
  Double_t    GetEtran()          const { return fEtran         ; }
  Double_t    GetEind()           const { return fEind          ; }
  Double_t    GetCrosstalkConfig()   const { return fCrosstalkConfig; }

  Double_t    GetGA()             const { return fGA            ; }
  Double_t    GetEres()           const { return fEres          ; }
  Double_t    GetIBF()            const { return fIBF           ; }
  Double_t    GetAnodeCurrent()   const { return fAnodeCurrent  ; }
  Double_t    GetCathodeCurrent() const { return fCathodeCurrent; }

  Bool_t ReadDataOld(std::istream& s);
  Bool_t ReadData(std::istream& s);
  Bool_t WriteData(ostream& s);
  void   SetMeasuredParameters(Double_t ga, Double_t eres, Double_t ibf, Double_t anode = 0., Double_t cathode = 0.);
  Bool_t IsSimilarTo(GEMmeasurement* meas, UInt_t params) const;

  void	 Print(Option_t* /*option*/ = "") const;
  
  const char* GetName()  const;
  const char* GetTitle() const;

  const char* GetName(UInt_t params) const;
  const char* GetTitle(UInt_t params) const;

  Double_t GetParam(UInt_t param) const;

  static TString GetParamAxisName(UInt_t param);

  static TString GenerateMeasurementName(const GEMmeasurement* meas);
  static TString GenerateMeasurementTitle(const GEMmeasurement* meas);
  static TString GenerateMeasurementName(const char* gasName, Double_t gasfrac, Double_t CO2frac, const char* quencherName, Double_t quencherfrac,
					 Double_t mmg, Double_t gem1, Double_t gem2, Double_t edrift, Double_t etran, Double_t eind);
  static TString GenerateMeasurementTitle(const char* gasName, Double_t gasfrac, Double_t CO2frac, const char* quencherName, Double_t quencherfrac,
					  Double_t mmg, Double_t gem1, Double_t gem2, Double_t edrift, Double_t etran, Double_t eind, Double_t ctalk);
  static TString GenerateGasMixtureName(const char* gasName, Double_t gasfrac, Double_t CO2frac, const char* quencherName, Double_t quencherfrac);
  static TString GenerateGasMixtureTitle(const char* gasName, Double_t gasfrac, Double_t CO2frac, const char* quencherName, Double_t quencherfrac);
  static TString GenerateMeasurementName(const char* gasMixName, Double_t mmg, Double_t gem1, Double_t gem2, Double_t edrift, Double_t etran, Double_t eind);
  static TString GenerateMeasurementTitle(const char* gasMixName, Double_t mmg, Double_t gem1, Double_t gem2, Double_t edrift, Double_t etran, Double_t eind, Double_t ctalk);

  static TString BeautifyChemicalFormula(const char* input);
  
 protected:  
  TString   fMainGasName;       //
  Double_t  fMainGasFrac;       //
  Double_t  fCO2Frac;           //
  TString   fQuencherGasName;   //
  Double_t  fQuencherGasFrac;   //

  Double_t  fMMGvolt;           //
  Double_t  fGEM1volt;          //
  Double_t  fGEM2volt;          //
  Double_t  fEdrift;            //
  Double_t  fEtran;             //
  Double_t  fEind;              //
  Double_t  fGA;                //
  Double_t  fEres;              //
  Double_t  fIBF;               //
  Double_t  fAnodeCurrent;      //
  Double_t  fCathodeCurrent;    //
  Double_t  fCrosstalkConfig;   //
  Bool_t    fCrosstalkMeas;     //

 private:
   
  GEMmeasurement(const GEMmeasurement &source);
  GEMmeasurement& operator=(const GEMmeasurement& source); 

  ClassDef(GEMmeasurement, 1);
};

#endif
