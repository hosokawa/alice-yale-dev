#include <fstream>
#include <iomanip>

#include <TGraph.h>
#include <TLegend.h>
#include <TCanvas.h>
#include <TObjArray.h>
#include <TPaveText.h>
#include <TH1.h>
#include <TList.h>
#include <TStyle.h>

#include "PlottingUtilities.h"
#include "HistoStyler.h"

ClassImp(PlottingUtilities);

//_____________________________________________________________
PlottingUtilities::PlottingUtilities() :
  TNamed(),
  fDataPath("data"),
  fXLegendPos(0.45),
  fExpandWidth(1.),
  fLegendTextSize(18),
  fMeasurements(10),
  fGraphs(10),
  fCanvases(10),
  fCrosstalk(0)
{
  fMeasurements.SetOwner();
  fGraphs.SetOwner();
  fCanvases.SetOwner();
}

//_____________________________________________________________
PlottingUtilities::PlottingUtilities(const char* name, const char* title) :
  TNamed(name, title),
  fDataPath("data"),
  fXLegendPos(0.45),
  fExpandWidth(1.),
  fLegendTextSize(18),
  fMeasurements(10),
  fGraphs(10),
  fCanvases(10),
  fCrosstalk(0)
{
  fMeasurements.SetOwner();
  fGraphs.SetOwner();
  fCanvases.SetOwner();
}

//_____________________________________________________________
GEMmeasurement* PlottingUtilities::AddMeasurement(const char* gasName, Double_t gasfrac, Double_t CO2frac, const char* quencherName, Double_t quencherfrac)
{
  GEMmeasurement* meas = new GEMmeasurement(gasName, gasfrac, CO2frac, quencherName, quencherfrac);
  fMeasurements.Add(meas);
  return meas;
}

//_____________________________________________________________
Bool_t PlottingUtilities::RemoveMeasurement(GEMmeasurement* meas)
{
  GEMmeasurement* measRem = static_cast<GEMmeasurement*>(fMeasurements.Remove(meas));
  if (measRem) {
    delete measRem;
    return kTRUE;
  }
  else {
    return kFALSE;
  }
}

//_____________________________________________________________
Int_t PlottingUtilities::ReadFile(const char* fname, const char* gasName, Double_t gasfrac, Double_t CO2frac, const char* quencherName, Double_t quencherfrac, Double_t edrift, Double_t etran, Double_t eind)
{
  Int_t n = 0;
  
  ifstream file(fname);

  // throw away the first line containing the table headers
  file.ignore(500, '\n');
  
  while (file.good()) {
    GEMmeasurement* meas = AddMeasurement(gasName, gasfrac, CO2frac, quencherName, quencherfrac);
    meas->SetEfields(edrift, etran, eind);
    Bool_t dataOk = meas->ReadDataOld(file);
    if (dataOk) {
      n++;
    }
    else {
      RemoveMeasurement(meas);
    }
  }

  return n;
}

//_____________________________________________________________
Int_t PlottingUtilities::ReadFile(const char* mainGasName, Double_t gasfrac, Double_t CO2frac, const char* quencherName, Double_t quencherfrac, const char* suffix)
{
  Int_t n = 0;

  TString gasName = GEMmeasurement::GenerateGasMixtureName(mainGasName, gasfrac, CO2frac, quencherName, quencherfrac);
  TString fname = Form("%s/%s%s.txt", fDataPath.Data(), gasName.Data(), suffix);

  Printf("Reading file %s", fname.Data());
  
  ifstream file(fname);

  // throw away the first line containing the table headers
  file.ignore(500, '\n');
  
  while (file.good()) {
    GEMmeasurement* meas = AddMeasurement(mainGasName, gasfrac, CO2frac, quencherName, quencherfrac);
      if(fCrosstalk) meas->SetCrosstalkMeas(fCrosstalk);
    Bool_t dataOk = meas->ReadData(file);
    if (dataOk) {
      n++;
    }
    else {
      RemoveMeasurement(meas);
    }
  }

  return n;
}

//_____________________________________________________________
Int_t PlottingUtilities::WriteFile(const char* mainGasName, Double_t gasfrac, Double_t CO2frac, const char* quencherName, Double_t quencherfrac, const char* suffix)
{
  Int_t n = 0;

  TString gasName = GEMmeasurement::GenerateGasMixtureName(mainGasName, gasfrac, CO2frac, quencherName, quencherfrac);
  TString fname = Form("%s/%s%s.txt", fDataPath.Data(), gasName.Data(), suffix);
  
  ofstream file(fname);

  file << std::setw(10) << "MMGvolt" << std::setw(10) << "GEM1volt" << std::setw(10) << "GEM2volt" << std::setw(10) << "Edrift" << std::setw(10) << "Etran" << std::setw(10) << "Eind"
       << std::setw(10) << "GA" << std::setw(10) << "Eres" << std::setw(10) << "IBF" << std::setw(10) << "Anode" << std::setw(10) << "Cathode" << std::endl;
  
  TIter next(&fMeasurements);
  GEMmeasurement* meas = 0;
  
  while ((meas = static_cast<GEMmeasurement*>(next()))) {
    if (gasName != meas->GetName(GEMmeasurement::kGasMix)) continue;
    Bool_t dataOk = meas->WriteData(file);
    if (dataOk) n++;
  }

  return n;
}

//_____________________________________________________________
GEMmeasurement* PlottingUtilities::GetMeasurement(const char* name) const
{
  return static_cast<GEMmeasurement*>(fMeasurements.FindObject(name));
}

//_____________________________________________________________
GEMmeasurement* PlottingUtilities::GetMeasurement(const char* gasName, Double_t gasfrac, Double_t CO2frac, const char* quencherName, Double_t quencherfrac,
						  Double_t mmg, Double_t gem1, Double_t gem2, Double_t edrift, Double_t etran, Double_t eind) const
{
  TString name = GEMmeasurement::GenerateMeasurementName(gasName, gasfrac, CO2frac, quencherName, quencherfrac, mmg, gem1, gem2, edrift, etran, eind);
  return static_cast<GEMmeasurement*>(fMeasurements.FindObject(name));
}

//_____________________________________________________________
TCanvas* PlottingUtilities::PlotMeasurement(GEMmeasurement::EParams freeParam, UInt_t groupBy, UInt_t measParams)
{
  Int_t nPlots = __builtin_popcount(measParams);

  TObjArray* graphs = GeneratePlots(freeParam, groupBy, measParams);
  graphs->SetOwner(kFALSE);
  
  Int_t nGraphs = graphs->GetEntriesFast();

  if (nGraphs % nPlots != 0) {
    Printf("Error: number of graphs (%d) is different from what expected (should be multiple of %d)", nGraphs, nPlots);
    delete graphs;
    return 0;
  }

  gStyle->SetOptStat(kFALSE);
  
  // This needs to be fixed to take into account the possibility of multiple series of plots
  HistoStyler styler;
  styler.SetMarkerSize(0.8);
  styler.SetVariableMarkerColor();
  styler.SetVariableMarkerStyle();
  styler.SetVariableLineColor();
  styler.SetLineWidth(1);
  if ((measParams & GEMmeasurement::kGA) == 0 && (measParams & GEMmeasurement::kAnode) == 0) styler.SetIndexShift(1);
  styler.Apply(graphs);

  TGraph* graph = 0;

  TLegend* leg = 0;

  Int_t nEntries = nGraphs / nPlots;
  
  if (nEntries > 1) {
    leg = SetUpLegend(fXLegendPos, 0.76-0.0032*fLegendTextSize*nEntries, fXLegendPos+0.3, 0.76, fLegendTextSize);
  }

  TCanvas* canvas = SetUpMultiPlotCanvas(GetName(), GetTitle(), freeParam, measParams);
  
  for (Int_t i = 0; i < nGraphs; i += nPlots) {
    if (leg) leg->AddEntry(graph, graph->GetTitle(), "pl");
    for (Int_t j = 0; j < nPlots; j++) {
      graph = static_cast<TGraph*>(graphs->At(i+j));
      FitGraphInPad(graph, canvas->cd(j+1), "PL", kFALSE, 1.1);
    }
  }

  if (leg) {
    canvas->cd(1);
    leg->Draw();
  }

  return canvas;
}

//_____________________________________________________________
TObjArray* PlottingUtilities::GeneratePlots(GEMmeasurement::EParams freeParam, UInt_t groupBy, UInt_t measParams)
{
  Printf("Entering PlottingUtilities::GeneratePlots...");

  TObjArray* array = new TObjArray(10);
  array->SetOwner(kFALSE);
  
  TIter next(&fMeasurements);

  GEMmeasurement* meas = 0;
  GEMmeasurement* prevMeas = 0;

  TGraph* graphs[32] = {0};
  Int_t ipoint = 0;
  
  while ((meas = static_cast<GEMmeasurement*>(next()))) {
    meas->Print();

    Bool_t newGraph = !(meas->IsSimilarTo(prevMeas, groupBy));

    if (prevMeas && newGraph) {
      for (Int_t i = 0; i < 32; i++) {
	if (!graphs[i]) continue;
	Printf("Graph %s completed!", graphs[i]->GetName());
	graphs[i]->Set(ipoint);
	graphs[i] = 0;
      }
      ipoint = 0;
    }

    UInt_t bit = 1;

    for (Int_t i = 0; i < 32; i++) {
      if ((measParams & bit) == bit) {
	if (!graphs[i]) {
	  graphs[i] = CreateGraph(meas, groupBy, freeParam, bit);
	  array->Add(graphs[i]);
	}
	graphs[i]->SetPoint(ipoint, meas->GetParam(freeParam), meas->GetParam(bit));
      }
      bit <<= 1;
    }
    
    ipoint++;
    prevMeas = meas;
  }

  if (ipoint > 0) {
    for (Int_t i = 0; i < 32; i++) {
      if (!graphs[i]) continue;
      Printf("Graph %s completed!", graphs[i]->GetName());
      graphs[i]->Set(ipoint);
      graphs[i] = 0;
    }
  }

  return array;
}

//_____________________________________________________________
TGraph* PlottingUtilities::CreateGraph(GEMmeasurement* meas, UInt_t groupBy, UInt_t xAxisParam, UInt_t yAxisParam)
{
  TGraph* graph = new TGraph(10);
  graph->SetName(meas->GetName(groupBy));
  graph->SetTitle(meas->GetTitle(groupBy));
  graph->GetXaxis()->SetTitle(GEMmeasurement::GetParamAxisName(xAxisParam));
  graph->GetYaxis()->SetTitle(GEMmeasurement::GetParamAxisName(yAxisParam));
  fGraphs.Add(graph);
  Printf("Creating a new graph %s...", graph->GetName());
  return graph;
}

//_____________________________________________________________
TCanvas* PlottingUtilities::PlotResVsIbf(UInt_t groupBy, UInt_t freeParams)
{
  Printf("Entering PlottingUtilities::PlotResVsIbf...");
  
  TObjArray* graphs = GenerateResVsIbf(groupBy);
  bool ionDensityFlag = false;
  if ((groupBy & GEMmeasurement::kIonDensity) == GEMmeasurement::kIonDensity) {
    ionDensityFlag = true;
  }
  TCanvas* canvas = PlotGraphs(graphs, ionDensityFlag);
  delete graphs;

  canvas->cd();

  TPaveText* pave = SetUpPaveText(0.10, 0.90, 0.95, 0.78, fLegendTextSize);
  pave->SetTextAlign(22);
  GEMmeasurement* meas = static_cast<GEMmeasurement*>(fMeasurements.At(0));
  UInt_t extraInfoBits = ~(groupBy | freeParams);
  TString extraInfoString = meas->GetTitle(extraInfoBits & ~GEMmeasurement::kGasMix);
  if ((extraInfoBits & GEMmeasurement::kGasMix) == GEMmeasurement::kGasMix) pave->AddText(meas->GetTitle(GEMmeasurement::kGasMix));
  pave->AddText(extraInfoString);
  pave->Draw();

  return canvas;
}

//_____________________________________________________________
TObjArray* PlottingUtilities::GenerateResVsIbf(UInt_t groupBy)
{
  Printf("Entering PlottingUtilities::GenerateResVsIbf...");

  TObjArray* array = new TObjArray(10);
  array->SetOwner(kFALSE);
  
  TIter next(&fMeasurements);

  GEMmeasurement* meas = 0;
  GEMmeasurement* prevMeas = 0;

  TGraph* graph = 0;
  Int_t ipoint = 0;
  
  while ((meas = static_cast<GEMmeasurement*>(next()))) {
    meas->Print();
    if (meas->GetIBF() > 1) {
      Printf("Skipping this measurement...");
      continue;
    }
    Bool_t newGraph = !(meas->IsSimilarTo(prevMeas, groupBy));

    if (graph && newGraph) {
      Printf("Graph %s completed!", graph->GetName());
      graph->Set(ipoint);
      graph = 0;
    }

    if (newGraph || !graph) {
      graph = CreateGraph(meas, groupBy, GEMmeasurement::kIBF, GEMmeasurement::kEres);
      array->Add(graph);
      ipoint = 0;
    }
    // Argon Ion density factor
    // This is porbably not how groupBy is supposed to be done...
    Double_t ionDensityScaleFactor = 1;
    //Printf("MainGasName(): %s", meas->GetMainGasName().Data());
    if ( ((groupBy & GEMmeasurement::kIonDensity) == GEMmeasurement::kIonDensity) && (meas->GetMainGasName() == "Ar") )
    {
        //Printf("Increasing scaling factor");
        ionDensityScaleFactor = 3;
    }
    graph->SetPoint(ipoint, meas->GetIBF()*ionDensityScaleFactor, meas->GetEres());
    ipoint++;
    prevMeas = meas;
  }

  if (graph && ipoint > 0) {
    Printf("Graph %s completed!", graph->GetName());
    graph->Set(ipoint);
    graph = 0;
  }

  return array;
}

//_____________________________________________________________
TCanvas* PlottingUtilities::PlotGraphs(TObjArray* graphs, bool ionDensityFlag)
{
  Printf("Entering PlottingUtilities::PlotGraphs...");

  gStyle->SetOptStat(kFALSE);

  Int_t n = graphs->GetEntriesFast();
  
  HistoStyler styler;
  styler.SetMarkerSize(0.8);
  styler.SetVariableMarkerStyle();
  styler.SetVariableMarkerColor();
  styler.SetVariableLineColor();
  styler.SetLineWidth(1);
  styler.Apply(graphs);
  
  TIter next(graphs);
  TGraph* graph = 0;

  // For Ion density plot
  TString xAxisTitle = "IBF (%)";
  if (ionDensityFlag == true) {
    xAxisTitle = "Ion Density";
  }
  TCanvas* canvas = SetUpCanvas(GetName(),
				xAxisTitle.Data(), 0, 1.2*fExpandWidth, kFALSE,
				"#sigma / Mean (%)", 12, 12, kFALSE);

  TLegend* leg = SetUpLegend(fXLegendPos, 0.76-0.0032*fLegendTextSize*n, fXLegendPos+0.3, 0.76, fLegendTextSize);
  
  while ((graph = static_cast<TGraph*>(next()))) {
    Printf("Plotting graph %s", graph->GetName());
    FitGraphInPad(graph, canvas, "PL", kFALSE, 1.2);
    leg->AddEntry(graph, graph->GetTitle(), "pl");
  }

  canvas->cd();
  leg->Draw();

  return canvas;
}


//____________________________________________________________________________________
void PlottingUtilities::SavePlot(TCanvas* canvas)
{
  if (!canvas) return;
  
  TString fname("../img/");
  fname += canvas->GetName();
  fname += ".eps";

  canvas->SaveAs(fname);
}

//____________________________________________________________________________________
TVirtualPad* PlottingUtilities::SetUpPad(TVirtualPad* pad,
					 const char* xTitle, Double_t minX, Double_t maxX, Bool_t logX,
					 const char* yTitle, Double_t minY, Double_t maxY, Bool_t logY,
					 Double_t lmar, Double_t rmar, Double_t bmar, Double_t tmar)
{
  if (!pad) return 0;

  pad->SetLeftMargin(lmar);
  pad->SetRightMargin(rmar);
  pad->SetBottomMargin(bmar);
  pad->SetTopMargin(tmar);
  
  pad->SetLogx(logX);
  pad->SetLogy(logY);

  pad->SetTicks(1, 1);

  pad->cd();

  TString blankHistName(Form("%s_blankHist", pad->GetName()));
  TH1* blankHist = new TH1D(blankHistName, blankHistName, 1000, minX, maxX);

  if (logY) {
    if (maxY <= 0 && minY > 0) {
      maxY = minY * 100;
    }
    else if (minY <= 0 && maxY > 0) {
      minY = maxY / 100;
    }
    else {
      minY = 1;
      maxY = 1;
    }
  }

  blankHist->SetMinimum(minY);
  blankHist->SetMaximum(maxY);
  
  blankHist->GetXaxis()->SetTitle(xTitle);
  blankHist->GetYaxis()->SetTitle(yTitle);

  blankHist->GetXaxis()->SetTitleOffset(1.12);
  blankHist->GetYaxis()->SetTitleOffset(1.14);

  blankHist->GetXaxis()->SetTitleFont(43);
  blankHist->GetYaxis()->SetTitleFont(43);

  blankHist->GetXaxis()->SetTitleSize(20);
  blankHist->GetYaxis()->SetTitleSize(20);

  blankHist->GetXaxis()->SetLabelOffset(0.010);
  blankHist->GetYaxis()->SetLabelOffset(0.010);

  blankHist->Draw("AXIS");

  return pad;
}

//____________________________________________________________________________________
TCanvas* PlottingUtilities::SetUpCanvas(TH1* histo, Bool_t logX, Bool_t logY,
					Double_t w, Double_t h, Int_t rows, Int_t cols,
					Double_t lmar, Double_t rmar, Double_t bmar, Double_t tmar)
{
  return SetUpCanvas(histo->GetName(),
                     histo->GetXaxis()->GetTitle(), histo->GetXaxis()->GetXmin(), histo->GetXaxis()->GetXmax(), logX,
                     histo->GetYaxis()->GetTitle(), histo->GetYaxis()->GetXmin(), histo->GetYaxis()->GetXmax(), logY,
                     w, h, rows, cols, lmar, rmar, bmar, tmar);
}

//____________________________________________________________________________________
TCanvas* PlottingUtilities::SetUpCanvas(const char* name,
					const char* xTitle, Double_t minX, Double_t maxX, Bool_t logX,
					const char* yTitle, Double_t minY, Double_t maxY, Bool_t logY,
					Double_t w, Double_t h, Int_t rows, Int_t cols,
					Double_t lmar, Double_t rmar, Double_t bmar, Double_t tmar)
{
  Printf("Info-PlottingUtilities::SetUpCanvas : Setting up canvas '%s'", name);

  TString cname(name);

  TCanvas* canvas = static_cast<TCanvas*>(fCanvases.FindObject(cname));
  if (canvas) {
    Printf("Warning-PlottingUtilities::SetUpCanvas : Canvas '%s' already exists.", cname.Data());
    return canvas;
  }
  
  canvas = new TCanvas(cname, cname, w, h);
  
  if (rows == 1 && cols == 1) {
    SetUpPad(canvas, xTitle, minX, maxX, logX, yTitle, minY, maxY, logY);
    canvas->SetLeftMargin(lmar);
    canvas->SetRightMargin(rmar);
    canvas->SetBottomMargin(bmar);
    canvas->SetTopMargin(tmar);
  }
  else {
    canvas->Divide(cols, rows);
    Int_t n = rows * cols;
    for (Int_t i = 1; i <= n; i++) {
      TVirtualPad* pad = canvas->cd(i);
      SetUpPad(pad, xTitle, minX, maxX, logX, yTitle, minY, maxY, logY,
               lmar, rmar, bmar, tmar); 
    }
  }

  fCanvases.Add(canvas);
  
  return canvas;
}

//____________________________________________________________________________________
TLegend* PlottingUtilities::SetUpLegend(Double_t x1, Double_t y1, Double_t x2, Double_t y2, Int_t textSize)
{
  TLegend* leg = new TLegend(x1, y1, x2, y2);
  leg->SetTextFont(43);
  leg->SetTextSize(textSize);
  leg->SetFillStyle(0);
  leg->SetBorderSize(0);

  return leg;
}

//____________________________________________________________________________________
TPaveText* PlottingUtilities::SetUpPaveText(Double_t x1, Double_t y1, Double_t x2, Double_t y2, Int_t textSize, const char* text)
{
  TPaveText* pave = new TPaveText(x1, y1, x2, y2, "brNDC");
  pave->SetTextFont(43);
  pave->SetTextAlign(11);
  pave->SetTextSize(textSize);
  pave->SetFillStyle(0);
  pave->SetBorderSize(0);
  
  if (text) {
    TString stext(text);
    TObjArray* lines = stext.Tokenize("\n");
    TIter next(lines);
    TObject *obj = 0;
    while ((obj = next())) {
      pave->AddText(obj->GetName());
    }
    delete lines;
  }

  return pave;
}

//____________________________________________________________________________________
void PlottingUtilities::FitGraphInPad(TGraph* graph, TVirtualPad* pad, Option_t* opt, Bool_t copyAxisTitle, Double_t extraFactor)
{
  TString strOpt(opt);
  if (!strOpt.Contains("same")) strOpt += "same";

  TH1* blankHist = dynamic_cast<TH1*>(pad->GetListOfPrimitives()->At(0));
  if (!blankHist) {
    Printf("Error-PlottingUtilities::FitHistogramInPad : Could not find blank histogram!");
    return;
  }

  Int_t nGraphs = 0;
  TObject* obj = 0;
  TIter next(pad->GetListOfPrimitives());

  Double_t minx = 0;
  Double_t maxx = 0;
  Double_t miny = 0;
  Double_t maxy = 0;
  
  while ((obj = next())) {
    TGraph* graphP = dynamic_cast<TGraph*>(obj);
    if (!graphP) continue;
    GetMinMax(graphP, minx, maxx, miny, maxy, (nGraphs!=0), pad->GetLogy());
    nGraphs++;
  }

  GetMinMax(graph, minx, maxx, miny, maxy, (nGraphs!=0), pad->GetLogy());


  Double_t shifty = maxy * extraFactor - maxy;
  maxy += shifty;
  
  if (pad->GetLogy()) {
    miny /= extraFactor;
  }
  else {
    if (miny < 0) {
      miny -= shifty;
    }
    else {
      miny -= shifty;
      if (miny < 0) miny = 0;
    }
  }

  Double_t shiftx = maxx * extraFactor - maxx;
  maxx += shiftx;
  if (minx < 0) {
    minx -= shiftx;
  }
  else {
    minx -= shiftx;
    if (minx < 0) minx = 0;
  }
  
  pad->cd();
  graph->Draw(strOpt);
  
  if (copyAxisTitle) {
    blankHist->GetXaxis()->SetTitle(graph->GetXaxis()->GetTitle());
    blankHist->GetYaxis()->SetTitle(graph->GetYaxis()->GetTitle());
  }

  blankHist->SetMinimum(miny);
  blankHist->SetMaximum(maxy);
  
  blankHist->GetXaxis()->SetRangeUser(minx, maxx);
}

//____________________________________________________________________________________
void PlottingUtilities::GetMinMax(TGraph* graph, Double_t& minx, Double_t& maxx, Double_t& miny, Double_t& maxy, Bool_t useInitialValues, Bool_t logY)
{
  for (Int_t i = 0; i < graph->GetN(); i++) {
    if (logY && graph->GetY()[i] <= 0) continue;
    
    Double_t x1 = graph->GetX()[i];
    if (graph->GetErrorXlow(i) > 0) x1 -= graph->GetErrorXlow(i);
    Double_t x2 = graph->GetX()[i];
    if (graph->GetErrorXhigh(i) > 0) x2 += graph->GetErrorXhigh(i);

    Double_t y1 = graph->GetY()[i];
    if (graph->GetErrorYlow(i) > 0) y1 -= graph->GetErrorYlow(i);
    Double_t y2 = graph->GetY()[i];
    if (graph->GetErrorYhigh(i) > 0) y2 += graph->GetErrorYhigh(i);
    
    if (minx > x1 || (!useInitialValues && i == 0)) minx = x1;
    if (maxx < x2 || (!useInitialValues && i == 0)) maxx = x2;
    
    if (miny > y1 || (!useInitialValues && i == 0)) miny = y1;
    if (maxy < y2 || (!useInitialValues && i == 0)) maxy = y2;
  } 
}

//____________________________________________________________________________________
TLegend* PlottingUtilities::GetLegend(TPad* pad)
{
  TLegend* leg = 0;

  for (Int_t i = 0; i < pad->GetListOfPrimitives()->GetEntries(); i++) {
    leg = dynamic_cast<TLegend*>(pad->GetListOfPrimitives()->At(i));
    if (leg) break;
  }

  return leg;
}

//____________________________________________________________________________________
TCanvas* PlottingUtilities::SetUpMultiPlotCanvas(const char* name, const char* title, UInt_t xAxisParam, UInt_t yAxisParams)
{
  Int_t n = __builtin_popcount(yAxisParams);
  TCanvas* canvas = new TCanvas(name, title, 700, 400*n);
  canvas->Divide(1, n);

  Double_t topBottomMargin = 0.07;
  Double_t midPadH = (1 - 2 * topBottomMargin) / n;
  Double_t topBottomPadH = midPadH + topBottomMargin;
  Double_t topBottomMarginPrime = n * topBottomMargin / (1 - 2 * topBottomMargin);

  TVirtualPad* pad1 = canvas->cd(1);  
  pad1->SetMargin(0.12, 0.08, 0., topBottomMarginPrime);
  pad1->SetPad(0, 1-topBottomPadH, 1, 1);

  TVirtualPad* padn = canvas->cd(n);
  padn->SetMargin(0.12, 0.08, topBottomMarginPrime, 0.);
  padn->SetPad(0, 0, 1, topBottomPadH);

  for (Int_t i = 2; i < n; i++) {
    TVirtualPad* pad = canvas->cd(i);
    pad->SetMargin(0.12, 0.08, 0., 0.);
    pad->SetPad(0, topBottomPadH+(i-2)*midPadH, 1, topBottomPadH+(i-1)*midPadH);
  }

  Double_t minX = 0;
  Double_t maxX = 0;
  TString xAxisTitle = GEMmeasurement::GetParamAxisName(xAxisParam);

  if ((xAxisParam & (GEMmeasurement::kMMGvolt | GEMmeasurement::kGEM1volt | GEMmeasurement::kGEM2volt)) != 0) {
    maxX = 500;
  }
  else if ((xAxisParam & (GEMmeasurement::kEdrift | GEMmeasurement::kEtran | GEMmeasurement::kEind)) != 0) {
    maxX = 5;
  }

  TH1* blankHist = 0;
  TString hname;
  
  TString yAxisTitle;

  Int_t i = 1;
  
  if ((yAxisParams & GEMmeasurement::kGA) == GEMmeasurement::kGA) {
    yAxisTitle = GEMmeasurement::GetParamAxisName(GEMmeasurement::kGA);
    DrawBlankHistogram(canvas->cd(i), xAxisTitle, minX, maxX, yAxisTitle, 0, 0, n);
    i++;
  }

  if ((yAxisParams & GEMmeasurement::kAnode) == GEMmeasurement::kAnode) {
    yAxisTitle = GEMmeasurement::GetParamAxisName(GEMmeasurement::kAnode);
    DrawBlankHistogram(canvas->cd(i), xAxisTitle, minX, maxX, yAxisTitle, 0, 0, n);
    i++;
  }

  if ((yAxisParams & GEMmeasurement::kEres) == GEMmeasurement::kEres) {
    yAxisTitle = GEMmeasurement::GetParamAxisName(GEMmeasurement::kEres);
    DrawBlankHistogram(canvas->cd(i), xAxisTitle, minX, maxX, yAxisTitle, 0, 0, n);
    i++;
  }

  if ((yAxisParams & GEMmeasurement::kIBF) == GEMmeasurement::kIBF) {
    yAxisTitle = GEMmeasurement::GetParamAxisName(GEMmeasurement::kIBF);
    DrawBlankHistogram(canvas->cd(i), xAxisTitle, minX, maxX, yAxisTitle, 0, 0, n);
    i++;
  }

  if ((yAxisParams & GEMmeasurement::kCathode) == GEMmeasurement::kCathode) {
    yAxisTitle = GEMmeasurement::GetParamAxisName(GEMmeasurement::kCathode);
    DrawBlankHistogram(canvas->cd(i), xAxisTitle, minX, maxX, yAxisTitle, 0, 0, n);
    i++;
  }

  fCanvases.Add(canvas);
  return canvas;
}

//____________________________________________________________________________________
TH1* PlottingUtilities::DrawBlankHistogram(TVirtualPad* pad, TString xAxisTitle, Double_t minX, Double_t maxX, TString yAxisTitle, Double_t minY, Double_t maxY, Int_t n)
{   
    TH1* blankHist = new TH1F("blankHist", "blankHist", 1000, minX, maxX);
    
    blankHist->GetXaxis()->SetTitle(xAxisTitle);
    blankHist->GetXaxis()->SetTitleFont(43);
    blankHist->GetXaxis()->SetTitleSize(19);
    blankHist->GetXaxis()->SetTitleOffset(1.0 + n*0.80);
    blankHist->GetXaxis()->SetLabelFont(43);
    blankHist->GetXaxis()->SetLabelSize(17);
    
    blankHist->GetYaxis()->SetTitle(yAxisTitle);
    blankHist->GetYaxis()->SetRangeUser(minY,maxY);
    blankHist->GetYaxis()->SetTitleFont(43);
    blankHist->GetYaxis()->SetTitleSize(19);
    blankHist->GetYaxis()->SetTitleOffset(1.5);
    blankHist->GetYaxis()->SetLabelFont(43);
    blankHist->GetYaxis()->SetLabelSize(17);
    
    pad->cd();
    blankHist->Draw("AXIS");

    return blankHist;
}
