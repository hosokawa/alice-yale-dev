#include <TGraph.h>
#include <TString.h>
#include <TROOT.h>

class PlottingUtilities;

void PlotResVsIonDensity()
{
  gROOT->LoadMacro("HistoStyler.cxx+g");
  gROOT->LoadMacro("GEMmeasurement.cxx+g");
  gROOT->LoadMacro("PlottingUtilities.cxx+g");
  
  TString name  = "ionDensity";
  TString title = "Argon and Neon Ion Density";

  PlottingUtilities* plotUtils = new PlottingUtilities(name, title);
  plotUtils->ReadFile("Ne", 0.9, 0.1, "", 0., "_best");
  plotUtils->ReadFile("Ar", 0.9, 0.1, "", 0.);

  /*
  plotUtils->WriteFile("Ne", 0.9, 0.1, "", 0., "_best");
  plotUtils->WriteFile("Ar", 0.9, 0.1, "", 0.);
  */
  
  plotUtils->SetXLegendPos(0.45);
  // Max effective value seems to be ~2 for some reason?
  plotUtils->SetExpandWidth(2);
  plotUtils->SetLegendTextSize(17);
  
  TCanvas* c = plotUtils->PlotResVsIbf(GEMmeasurement::kGasMix | GEMmeasurement::kIonDensity, GEMmeasurement::kMMGvolt | GEMmeasurement::kGEM1volt | GEMmeasurement::kGEM2volt);

  PlottingUtilities::SavePlot(c);
}

//change x-axis label
//scale argon x-axis data by 3
