#!/usr/bin/env python

from __future__ import print_function

import ROOT
import os
import math
import pprint

# Run in batch mode
ROOT.gROOT.SetBatch(True)

def getListOfHistograms(filename, listTaskName = "AliAnalysisTaskJetH_tracks_caloClusters_clusbias5R2GA"):
    """ Get histograms from the file and make them available in a dict """
    fIn = ROOT.TFile(filename)
    if not fIn or fIn.IsZombie():
        print("Could not find file '{0}".format(filename))
        return
    taskOutputList = fIn.Get(listTaskName)
    if not taskOutputList:
        print("Could not find list '{0}'".format(taskOutputList))
        return
    #list = listTask.FindObject(listName)
    #if not list:
    #    print("Could not find list '{0}'".format(listName))
    #    return
    
    histList = dict()
    next = ROOT.TIter(taskOutputList)
    obj = next()
    while obj:
        histList[obj.GetName()] = obj
        obj = next()
    
    return histList

def generateJetPtRangeString(iJetPtBin):
    jetPtRange = "{0:.1f}<=p_{{T}}^{{jet}}".format(jetPtBins[iJetPtBin])
    # -2 because we have 3 jet pt bins, which will be index 0, 1, 2, but there are 4 values in jetPtBins
    if not iJetPtBin == len(jetPtBins)-2:
        jetPtRange = jetPtRange + "<{0:.1f}".format(jetPtBins[iJetPtBin+1])

    # Add GeV/c label
    jetPtRange = jetPtRange + " GeV/c"

    return jetPtRange

def generateTrackPtRangeString(iTrackPtBin):
    trackPtRange= "{0:.1f}<=p_{{T}}^{{assoc}}<{1:.1f} GeV/c".format(trackPtBins[iTrackPtBin], trackPtBins[iTrackPtBin+1])

    return trackPtRange

def ptSpectra(jetH):
    """ Plot basic Jet and Track spectra from the JetH THnSparse """
    canvas = ROOT.TCanvas("canvas", "canvas")
    canvas.SetLogy(ROOT.kTRUE)

    jetPt = jetH.Projection(1)
    # Just to suppress root warnings
    jetPt.SetName("ptSpectra_jetPt")
    trackPt = jetH.Projection(2)

    nTrig = jetPt.GetEntries()

    jetPt.GetXaxis().SetTitle("p_{T}")
    jetPt.GetYaxis().SetTitle("#frac{1}{n_{trig}}#frac{dN}{dp_{T}}")
    jetPt.Scale(1.0/nTrig)
    jetPt.Draw()
    canvas.SaveAs(os.path.join("output", "plotting", "jetPt.pdf"))

    trackPt.GetXaxis().SetTitle("p_{T}")
    trackPt.GetYaxis().SetTitle("#frac{1}{n_{trig}}#frac{dN}{dp_{T}}")
    trackPt.Scale(1.0/nTrig)
    trackPt.Draw()
    canvas.SaveAs(os.path.join("output", "plotting", "trackPt.pdf"))

# dEtaDPhi distributions
# TODO: Loop over track pt bins - same as below
def deltaPlotsIn2D(jetH, mixedEvents, hists):
    """ Plot the 2D DeltaEta-DeltaPhi distributions"""
    canvas = ROOT.TCanvas("canvas", "canvas")

    # TEMP
    ROOT.gStyle.SetOptStat(0)
    # END TEMP

    # Set Jet Pt range
    # Get the 20-60 GeV bin (arbitrarily selected)
    iJetPtBin = 1
    jetH.GetAxis(1).SetRangeUser(jetPtBins[iJetPtBin], jetPtBins[iJetPtBin+1])
    mixedEvents.GetAxis(1).SetRangeUser(jetPtBins[iJetPtBin], jetPtBins[iJetPtBin+1])
    # Set track pt range
    # Get the 2-3 GeV bin (arbitrarily selected)
    iTrackPtBin = 3
    jetH.GetAxis(2).SetRangeUser(trackPtBins[iTrackPtBin], trackPtBins[iTrackPtBin+1])
    mixedEvents.GetAxis(2).SetRangeUser(trackPtBins[iTrackPtBin], trackPtBins[iTrackPtBin+1])
    # Set the centrality (0-10%)
    jetH.GetAxis(0).SetRangeUser(0, 10)
    mixedEvents.GetAxis(0).SetRangeUser(0, 10)

    jetPtBinsTitle = generateJetPtRangeString(iJetPtBin)
    trackPtBinsTitle = generateTrackPtRangeString(iTrackPtBin)

    jetPtDist = jetH.Projection(1)
    # Just to suppress root warnings
    jetPtDist.SetName("deltaPlotsIn2D_jetPtDist")
    # This is the number of tracks, not jets! Because the THnSparse fills per track!
    #nTrig = jetPtDist.GetEntries()
    nTrig = 0
    nTrigHistName = "fHistJetPtBias_{0}"
    # Cent bins = 6
    #for i in xrange(0, 6):
    #    hist = hists[nTrigHistName.format(i)]
    #    # 15 = Min pt in THnSparse
    #    #print("Find bin: {0}".format(hist.FindBin(15)in ))
    #    nTrig += hist.Integral(hist.FindBin(15), hist.GetNbinsX())
    hist = hists[nTrigHistName.format(0)]
    # 15 = Min pt in THnSparse
    print("Find bin({0}): {1} to Find bin({2}): {3}".format(jetPtBins[iJetPtBin], hist.FindBin(jetPtBins[iJetPtBin]), jetPtBins[iJetPtBin+1], hist.FindBin(jetPtBins[iJetPtBin+1])))
    #nTrig = hist.Integral(hist.FindBin(15), hist.GetNbinsX())
    nTrig = hist.Integral(hist.FindBin(jetPtBins[iJetPtBin]), hist.FindBin(jetPtBins[iJetPtBin+1]))
    print("nTrig for [{0}, {1}]: {2}".format(jetPtBins[iJetPtBin], jetPtBins[iJetPtBin+1], nTrig))

    #print("nTrig: {0}".format(nTrig))

    zAxisTitle = "1/N_{{trig}}d^{{2}}N{nLabel}/d#Delta#varphi d#Delta#eta"

    # Create legend
    # Technically for DPhi, but it should work fine here too, although perhaps the size needs to be changed
    #legExtended = createDPhiLegend(iJetPtBin, iTrackPtBin, minimal = False)

    # Signal
    dEtaDPhi = jetH.Projection(3,4)
    # Normalize by nTrig
    dEtaDPhi.Scale(1.0/nTrig)
    # Label, draw, and save
    dEtaDPhi.GetXaxis().SetTitle("#Delta#varphi")
    dEtaDPhi.GetYaxis().SetTitle("#Delta#eta")
    dEtaDPhi.GetZaxis().SetTitle(zAxisTitle.format(nLabel = "_{raw}"))
    dEtaDPhi.GetZaxis().SetTitleOffset(1.3)
    #canvas.SetLeftMargin(0.15)
    dEtaDPhi.SetTitle("Jet-H Signal with {0}, {1}".format(jetPtBinsTitle, trackPtBinsTitle))
    dEtaDPhi.Draw("surf2")
    #legExtended.Draw("same")
    canvas.SaveAs(os.path.join("output", "plotting", "jetHDEtaDPhi.pdf"))

    # Mixed events
    dEtaDPhiMixed = mixedEvents.Projection(3,4)
    # Normalize at (0,0)
    mixedEventNormalization = dEtaDPhiMixed.GetBinContent(dEtaDPhi.GetXaxis().FindBin(0.0),
                                                          dEtaDPhi.GetYaxis().FindBin(0.0))
    dEtaDPhiMixed.Scale(1.0/mixedEventNormalization)
    # Label, draw and save
    dEtaDPhiMixed.GetXaxis().SetTitle("#Delta#varphi")
    dEtaDPhiMixed.GetYaxis().SetTitle("#Delta#eta")
    dEtaDPhiMixed.GetZaxis().SetTitle("a(#Delta#varphi,#Delta#eta)")
    dEtaDPhiMixed.SetTitle("Mixed Events with {0}, {1}".format(jetPtBinsTitle, trackPtBinsTitle))
    dEtaDPhiMixed.Draw("surf2")
    #legExtended.Draw("same")
    canvas.SaveAs(os.path.join("output", "plotting", "mixedEventsDEtaDPhi.pdf"))

    # signal/miaxed
    corr = dEtaDPhi.Clone()
    print("Corr entries before divide: {0}".format(corr.GetEntries()))
    corr.Divide(dEtaDPhiMixed)
    print("Corr entries after divide: {0}".format(corr.GetEntries()))
    print("Corr value at (0,0): {0}".format(corr.GetBinContent(corr.FindBin(0,0))))
    #corr.GetXaxis().SetTitle("#Delta#varphi")
    #corr.GetYaxis().SetTitle("#Delta#eta")
    # Label, draw, and save
    corr.GetZaxis().SetTitle(zAxisTitle.format(nLabel = ""))
    corr.SetTitle("Signal/Mixed Event with {0}, {1}".format(jetPtBinsTitle, trackPtBinsTitle))
    corr.Draw("surf2")
    #legExtended.Draw("same")
    canvas.SaveAs(os.path.join("output", "plotting", "jetHCorr.pdf"))

def fitDeltaPhiBackground(deltaPhi, iTrackPtBin, zyam = True, disableVN = True, setFixedVN = False):
    """ Fit the delta phi background to subtract it """
    fitFunc = ROOT.TF1("deltaPhiBackground", "[0]*1 + (2*[1]*cos(2*x) + 2*[2]*cos(3*x))",
                        -0.5*ROOT.TMath.Pi(), 1.5*ROOT.TMath.Pi())

    # Backgound
    # Pedestal
    fitFunc.SetParLimits(0, 0.,100)
    if zyam:
        # Seed with ZYAM value
        fitFunc.SetParameter(0, deltaPhi.GetBinContent(deltaPhi.GetMinimumBin()))
    fitFunc.SetParName(0, "Pedestal")
    # v2
    fitFunc.SetParLimits(1, -1, 1)
    fitFunc.SetParName(1, "v_{2}")
    # v3
    fitFunc.SetParLimits(2, -1, 1)
    fitFunc.SetParName(2, "v_{3}")
    if disableVN:
        fitFunc.FixParameter(1, 0)
        fitFunc.FixParameter(2, 0)
    if setFixedVN:
        #v2Assoc = math.sqrt((v2Cent00_05Values[iTrackPtBin] + v2Cent05_10Values[iTrackPtBin])/2.)
        v2Assoc = (v2Cent00_05Values[iTrackPtBin] + v2Cent05_10Values[iTrackPtBin])/2.
        # From https://arxiv.org/pdf/1509.07334v2.pdf
        v2Jet = 0.03
        v3Assoc = 0
        v3Jet = v3Assoc
        fitFunc.FixParameter(7, v2Assoc*v2Jet)
        fitFunc.FixParameter(8, v3Assoc*v3Jet)

    # Set styling
    fitFunc.SetLineColor(ROOT.kBlue+2)
    fitFunc.SetLineStyle(1)

    # Fit to the given histogram
    # R uses the range defined in the fit function
    # 0 ensures that the fit isn't drawn
    # Q ensures minimum printing
    # + adds the fit to function list to ensure that it is not deleted on the creation of a new fit
    deltaPhi.Fit(fitFunc, "RIB0")

    return fitFunc

def fitDeltaPhi(deltaPhi, iTrackPtBin, zyam = True, disableVN = True, setFixedVN = False):
    """ Define 1D gaussian fit function with one gaussian each for the near and away sides, along with a gaussian offset by +/-2Pi"""

    #fitFunc = ROOT.TF1("symmetricGaussian","[0]*exp(-0.5*((x-[1])/[2])**2)+[3]+[4]*exp(-0.5*((x-[5])/[6])**2)+[0]*exp(-0.5*((x-[1]+2.*TMath::Pi())/[2])**2)+[4]*exp(-0.5*((x-[5]-2.*TMath::Pi())/[6])**2)", -0.5*ROOT.TMath.Pi(), 1.5*ROOT.TMath.Pi())
    # TODO: This is not symmetric! Instead, the extra fits are because of how it wraps around. Even if our data doesn't go there, it is still relevant
    fitFunc = ROOT.TF1("gaussian",
                        "[6]*1 + (2*[7]*cos(2*x) + 2*[8]*cos(3*x)) + "
                        "[0]*(TMath::Gaus(x, [1], [2]) + TMath::Gaus(x, [1]-2.*TMath::Pi(), [2]) + TMath::Gaus(x, [1]+2.*TMath::Pi(), [2])) + "
                        "[3]*(TMath::Gaus(x, [4], [5]) + TMath::Gaus(x, [4]-2.*TMath::Pi(), [5]) + TMath::Gaus(x, [4]+2.*TMath::Pi(), [5]))",
                        -0.5*ROOT.TMath.Pi(), 1.5*ROOT.TMath.Pi())

    # Setup parameters
    amplitudeLimits = [0.0, 100.0]
    sigmaLimits = [0.05, 2.0]
    # Near side
    # Amplitude
    fitFunc.SetParLimits(0, amplitudeLimits[0], amplitudeLimits[1])
    fitFunc.SetParName(0, "NS Amplitude")
    # Offset
    fitFunc.FixParameter(1, 0)
    fitFunc.SetParName(1, "NS Offset")
    # Sigma
    fitFunc.SetParLimits(2, sigmaLimits[0], sigmaLimits[1])
    fitFunc.SetParName(2, "NS #sigma")
    # Seed for sigma
    fitFunc.SetParameter(2, sigmaLimits[0])

    # Away side
    # Amplitude
    fitFunc.SetParLimits(3, amplitudeLimits[0], amplitudeLimits[1])
    fitFunc.SetParName(3, "AS Amplitude")
    # Offset
    fitFunc.FixParameter(4, ROOT.TMath.Pi())
    fitFunc.SetParName(4, "AS Offset")
    # Sigma
    fitFunc.SetParLimits(5, sigmaLimits[0], sigmaLimits[1])
    fitFunc.SetParName(5, "AS #sigma")
    # Seed for sigma
    fitFunc.SetParameter(5, sigmaLimits[0])

    # Backgound
    # Pedestal
    fitFunc.SetParLimits(6, 0.,100)
    if zyam:
        # Seed with ZYAM value
        fitFunc.SetParameter(6, deltaPhi.GetBinContent(deltaPhi.GetMinimumBin()))
    fitFunc.SetParName(6, "Pedestal")
    # v2
    fitFunc.SetParLimits(7, -1, 1)
    fitFunc.SetParName(7, "v_{2}")
    # v3
    fitFunc.SetParLimits(8, -1, 1)
    fitFunc.SetParName(8, "v_{3}")
    if disableVN:
        fitFunc.FixParameter(7, 0)
        fitFunc.FixParameter(8, 0)
    if setFixedVN:
        #v2Assoc = math.sqrt((v2Cent00_05Values[iTrackPtBin] + v2Cent05_10Values[iTrackPtBin])/2.)
        v2Assoc = (v2Cent00_05Values[iTrackPtBin] + v2Cent05_10Values[iTrackPtBin])/2.
        # From https://arxiv.org/pdf/1509.07334v2.pdf
        v2Jet = 0.03
        v3Assoc = 0
        v3Jet = v3Assoc
        #fitFunc.SetParameter(7, v2Assoc*v2Jet)
        fitFunc.FixParameter(7, v2Assoc*v2Jet)
        fitFunc.FixParameter(8, v3Assoc*v3Jet)

    # Set styling
    fitFunc.SetLineColor(ROOT.kRed + 2)
    fitFunc.SetLineStyle(1)

    # Fit to the given histogram
    # R uses the range defined in the fit function
    # 0 ensures that the fit isn't drawn
    # Q ensures minimum printing
    # + adds the fit to function list to ensure that it is not deleted on the creation of a new fit
    deltaPhi.Fit(fitFunc, "RIB0")

    # And return the fit
    return fitFunc

def jetHDPhi(jetH, mixedEvents, hists):
    """ Plot Jet-H using THnSparseF values for jetH, mixedEvents """
    # Setup canvases
    canvasAll = ROOT.TCanvas("canvas", "canvas")
    subtractionCanvas = ROOT.TCanvas("subtractionCanvas", "subtractionCanvas")

    # Setup the upper and low pads to make the comparison
    subtractionCanvas.cd()
    # Ratio configuration from: https://root.cern.ch/root/html/tutorials/hist/ratioplot.C.html
    upperPad = ROOT.TPad("upperPad", "upperPad", 0, 0.3, 1, 1.0)
    # Upper and lower plot are joined
    upperPad.SetBottomMargin(0)
    upperPad.Draw()
    # Return to the main canvas to draw the lower pad
    subtractionCanvas.cd()
    # Lower pad = subtracted plot
    lowerPad = ROOT.TPad("lowerPad", "lowerPad", 0, 0.05, 1, 0.3)
    lowerPad.SetTopMargin(0)
    lowerPad.SetBottomMargin(0.2)
    lowerPad.Draw()

    # Turn off stats box
    ROOT.gStyle.SetOptStat(0)
    ROOT.gStyle.SetOptTitle(0)

    # Select only 0-10%
    # Selected below!
    #jetH.GetAxis(0).SetRangeUser(0, 10)
    #mixedEvents.GetAxis(0).SetRangeUser(0, 10)

    # Define widths and widths errors
    widths = {}
    widthErrors = {}

    # Define yields and yield errors
    yields = {}
    yieldErrors = {}
    # Setup yield limits
    yieldLimit = ROOT.TMath.Pi()/3.

    histName = "jetHDPhi_eta{0}_trackPt{1}_jetPt{2}{3}"
    #for iEtaBin in xrange(0, len(etaBins)-1):
    # Disabled eta bin -> Fully integrated eta range
    for iEtaBin in xrange(0, 1):
        canvasAll.Divide(7, 3)
        for iJetPtBin in xrange(0, len(jetPtBins)-1):
            # This corresponds to 0-10%
            # NOTE: This is wrong, since it is eta integrated, but cannot be fixed without a new train!
            #       For now, we just run as eta integrated
            nTrigHistName = "fHistJetPtBias_{0}"
            hist = hists[nTrigHistName.format(0)]
            # 15 = Min pt in THnSparse
            print("Find bin({0}): {1} to Find bin({2}): {3}".format(jetPtBins[iJetPtBin], hist.FindBin(jetPtBins[iJetPtBin]), jetPtBins[iJetPtBin+1], hist.FindBin(jetPtBins[iJetPtBin+1])))
            #nTrig = hist.Integral(hist.FindBin(15), hist.GetNbinsX())
            nTrig = hist.Integral(hist.FindBin(jetPtBins[iJetPtBin]), hist.FindBin(jetPtBins[iJetPtBin+1]))
            print("nTrig for [{0}, {1}]: {2}".format(jetPtBins[iJetPtBin], jetPtBins[iJetPtBin+1], nTrig))

            # Now loop over the track pt bins
            for iTrackPtBin in xrange(0, len(trackPtBins)-1):
                # Set ranges, and then plot in canvas
                # Set Jet Pt range
                jetH.GetAxis(1).SetRangeUser(jetPtBins[iJetPtBin], jetPtBins[iJetPtBin+1])
                # Set track pt range
                jetH.GetAxis(2).SetRangeUser(trackPtBins[iTrackPtBin], trackPtBins[iTrackPtBin+1])
                # Set eta range
                #jetH.GetAxis(3).SetRangeUser(etaBins[iEtaBin], etaBins[iEtaBin+1])
                # Set the centrality (0-10%)
                jetH.GetAxis(0).SetRangeUser(0, 10)

                # Get the basic dPhi hist and normalize
                dPhi = jetH.Projection(4)
                dPhi.SetName(histName.format(iEtaBin, iTrackPtBin, iJetPtBin, ""))
                dPhi.GetXaxis().SetTitle("#Delta#varphi")
                dPhi.SetLineColor(ROOT.kBlue+2)
                # Normalize dPhi hist
                dPhi.Scale(1/nTrig)

                # Do the mixed events need to have restricted ranges? It seems like they should
                # Set Jet Pt range
                mixedEvents.GetAxis(1).SetRangeUser(jetPtBins[iJetPtBin], jetPtBins[iJetPtBin+1])
                # Set track pt range
                mixedEvents.GetAxis(2).SetRangeUser(trackPtBins[iTrackPtBin], trackPtBins[iTrackPtBin+1])
                # Set eta range
                #mixedEvents.GetAxis(3).SetRangeUser(etaBins[iEtaBin], etaBins[iEtaBin+1])
                # Set the centrality (0-10%)
                mixedEvents.GetAxis(0).SetRangeUser(0, 10)

                # Get mixed events and normalize
                dPhiMixed = mixedEvents.Projection(4)
                dPhiMixed.SetName(histName.format(iEtaBin, iTrackPtBin, iJetPtBin, "_mixed"))
                dPhiMixed.SetLineColor(ROOT.kRed+2)
                # Normalize
                mixedEventNormalization = dPhiMixed.GetBinContent(dPhiMixed.GetXaxis().FindBin(0.0))
                if (mixedEventNormalization != 0):
                    dPhiMixed.Scale(1.0/mixedEventNormalization)
                else:
                    print("WARNING: Could not normalize the mixed event hist due to no data at (0,0)!")

                # Rebin
                # TODO: Work on rebin quality...
                #dPhi.Rebin(2)
                #dPhiMixed.Rebin(2)

                # Divide signal by mixed events
                dPhi.Divide(dPhiMixed)

                # Fit the unsubtracted delta phi hist
                unsubtractedFit = fitDeltaPhi(dPhi, iTrackPtBin = iTrackPtBin, disableVN = True, setFixedVN = False)

                # Create a clone of the dPhi hist to subtract
                dPhiSubtracted = dPhi.Clone("{0}_subtracted".format(dPhi.GetName()))

                # Fit background
                bgFit = fitDeltaPhiBackground(dPhi, iTrackPtBin = iTrackPtBin, disableVN = True, setFixedVN = False)
                # Get the minimum from the fit to be less sensitive to fluctuations
                bg = unsubtractedFit.GetMinimum()
                bgFit.SetParameter(0, bg)

                # Remove background
                dPhiSubtracted.Add(bgFit, -1)
                # Create subtracted fit from previous fits
                # TODO: This should be improved! This should actually do the fit!
                subtractedFit = unsubtractedFit.Clone("subtractedFit")
                # For now, manually zero out the backgroud in the fit, which is what the above subtraction does for the hist
                # We need to subtract the bg fit from the pedistal, because they don't always agree
                # (sometimes the pedistal is smaller than a flat background - the reason is unclear!)
                subtractedFit.SetParameter(6, subtractedFit.GetParameter(6) - bg)
                #printFitParameters(bgFit)
                #printFitParameters(unsubtractedFit)
                #printFitParameters(subtractedFit)

                # Fit the subtracted dPhi plot
                # For some reason, this doesn't currently converge to a reasonable fit...
                #subtractedFit = fitDeltaPhi(dPhiSubtracted)

                # Widths
                extractWidths(widths, widthErrors, iTrackPtBin, subtractedFit)

                # Yields
                extractYields(dPhiSubtracted, yields, yieldErrors, yieldLimit, iJetPtBin, iTrackPtBin)

                #print("yields: {0}".format(yields))
                #print("yieldErrors: {0}".format(yieldErrors))

                #######
                # Plotting
                #######

                ## TEMP
                #if iJetPtBin == 1 and (iTrackPtBin == 0 or iTrackPtBin == 6) and iEtaBin == 0:
                #    temp = ROOT.TCanvas("temp{0}".format(iTrackPtBin), "temp{0}".format(iTrackPtBin))
                #    temp.SetLeftMargin(0.15)
                #    temp.cd()
                #    dPhi.GetYaxis().SetTitleOffset(1.75)
                #    dPhi.GetYaxis().SetTitle("#frac{1}{N_{trig}}#frac{dN_{assoc}}{d#Delta#varphi}")
                #    dPhi.Draw()
                #    temp.SaveAs(os.path.join("output", "plotting", "jetHDPhi_jetPtBin{0}_trackPtBin{1}.pdf".format(iJetPtBin, iTrackPtBin) ))
                ## END TEMP

                # dPhi All
                leg = createDPhiLegend(iJetPtBin, iTrackPtBin, minimal = True)
                plotAllCanvas(canvasAll, dPhi, leg, iTrackPtBin, iJetPtBin)
                
                # dPhi Subtracted
                legExtended = createDPhiLegend(iJetPtBin, iTrackPtBin, minimal = False)
                plotSubtractionCanvas(subtractionCanvas, upperPad, lowerPad, dPhi, dPhiSubtracted,
                                      fits = [bgFit, unsubtractedFit, subtractedFit], leg = legExtended,
                                      iJetPtBin = iJetPtBin, iTrackPtBin = iTrackPtBin, iEtaBin = iEtaBin,
                                      ensureBothPlotsFit = False)

        # We want for a particular eta bin
        canvasAll.SaveAs(os.path.join("output", "plotting", "jetHDPhi_eta{0}.pdf".format(iEtaBin) ))
        canvasAll.Clear()

    plotYieldsOrWidths(widths, widthErrors, plotType = "widths")
    plotYieldsOrWidths(yields, yieldErrors, plotType = "yields", yieldLimit = yieldLimit)

    # Save out hists
    #canvas.SaveAs(os.path.join("output", "plotting", "deltaEta.pdf"))

def printFitParameters(fit):
    """ Print out all of the fit parameters. """
    outputParameters = []
    for i in range(0, fit.GetNpar()):
        parameter = fit.GetParameter(i)
        parameterName = fit.GetParName(i)
        lowerLimit = ROOT.Double(0.0)
        upperLimit = ROOT.Double(0.0)
        fit.GetParLimits(i, lowerLimit, upperLimit)

        outputParameters.append("{0}: {1} = {2} from {3} - {4}".format(i, parameterName, parameter, lowerLimit, upperLimit))

    pprint.pprint(outputParameters)
    #print("subtractedFitParameters: {0}".format([param for param in subtractedFit.GetParameters()]))

def extractWidths(widths, widthErrors, iTrackPtBin, subtractedFit):
    """ Extract widths from the fit. """
    # Store results
    locations = {"NS": 2, "AS": 5}
    for location, parameterNumber in locations.iteritems():
        widths.setdefault(location, {}).setdefault(iTrackPtBin, []).append(subtractedFit.GetParameter(parameterNumber))
        widthErrors.setdefault(location, {}).setdefault(iTrackPtBin, []).append(subtractedFit.GetParError(parameterNumber))

def plotYieldsOrWidths(inputValues, inputValueErrors, plotType, yieldLimit = 0):
    """ Plot the previously calculated values. """
    for location in inputValues:
        # Done for both NS, AS
        values = []
        for i in range(0, len(jetPtBins)-1):
            values.append(ROOT.TGraphErrors(len(trackPtBins)-1))

        # iTrackPtBin is the label
        # valueInJetPtBins is a list with values for various jet pt bins
        for (iTrackPtBin, valueInJetPtBins), valueInJetPtBinsErrors in zip(inputValues[location].iteritems(), inputValueErrors[location].values()):
            for i, y in enumerate(valueInJetPtBins):
                #print("Adding {0} jetPtBin {1} at point {2} at ({3}, {4})".format(location, i, iTrackPtBin, trackPtBins[iTrackPtBin], y))
                # Center points in the bin
                halfBinWidth = (trackPtBins[iTrackPtBin+1] - trackPtBins[iTrackPtBin])/2.0
                binCenterPoint = trackPtBins[iTrackPtBin] + halfBinWidth
                #print("Center point: {0}, binWidth: {1}, trackPtBins[iTrackPtBin+1]: {2}, trackPtBins[iTrackPtBin]: {3}".format(binCenterPoint, halfBinWidth,
                #                                                                                                           trackPtBins[iTrackPtBin+1],
                #                                                                                                           trackPtBins[iTrackPtBin]))
                values[i].SetPoint(iTrackPtBin, binCenterPoint, valueInJetPtBins[i])
                # Set x error to bin width
                values[i].SetPointError(iTrackPtBin, halfBinWidth, valueInJetPtBinsErrors[i])

        # Plot and save graphs
        canvas = ROOT.TCanvas("{0}{1}Canvas".format(plotType,location), "{0}{1}Canvas".format(plotType, location))
        if plotType == "yields":
            canvas.SetLogy()

        # Create legend
        legend = createYieldsAndWidthsLegend(location, plotType, yieldLimit)

        # Draw graph
        firstToDraw = True
        for i, value in enumerate(values):
            value.SetLineColorAlpha(colors[i], 0.7)
            value.SetMarkerSize(3)
            value.SetLineWidth(1)
            value.SetMarkerColorAlpha(colors[i], 0.7)
            if firstToDraw:
                # Draw axes on the first plot
                value.GetXaxis().SetTitle("p_{T}^{assoc}")
                if plotType == "yields":
                    # TODO: Label should be 1/N_trig dN/dp_{T} (?)
                    value.GetYaxis().SetTitle("dN/dp_{T} (GeV/c)^{-1}")
                    # Adjust the plotting range
                    if location == "NS":
                        value.GetYaxis().SetRangeUser(2e-2, 3)
                    elif location == "AS":
                        value.GetYaxis().SetRangeUser(5e-3, 3)
                    else:
                        print("Location {0} not recognized!".format(location))

                elif plotType == "widths":
                    value.GetYaxis().SetTitle("#sigma")
                    # Adjust the plotting range
                    if location == "NS":
                        value.GetYaxis().SetRangeUser(0, 1)
                    elif location == "AS":
                        value.GetYaxis().SetRangeUser(0, 2.5)
                    else:
                        print("Location {0} not recognized!".format(location))
                else:
                    print("Cannot recognize plot type {0}".format(plotType))
                value.Draw("AP")
                firstToDraw = False
            else:
                # Draw the yield
                value.Draw("P")

            legend.AddEntry(value, generateJetPtRangeString(i), "LEP")

        legend.Draw("same")

        canvas.SaveAs(os.path.join("output", "plotting", "{0}{1}.pdf".format(plotType, location)))
        canvas.Clear()

def createYieldsAndWidthsLegend(location, plotType, yieldLimit):
    """ Legend for yields """
    if plotType == "yields":
        leg = ROOT.TLegend(0.12, 0.12, 0.5, 0.4)
        leg.SetFillColorAlpha(0, 0)
    elif plotType == "widths":
        leg = ROOT.TLegend(0.5, 0.55, 0.89, 0.87)
    leg.SetBorderSize(0)
    leg.SetTextSize(0.03)
    leg.AddEntry("", "Pb-Pb #sqrt{s_{NN}}=2.76 TeV, 0-10%", "")
    leg.AddEntry("", "Anti-k_{T} full jets, R=0.2", "")
    # NOTE: Hand coded yieldLimit and centralValue!
    #if location == "AS":
    #    centralValue = TMath.Pi()
    #elif location = "NS":
    #    centralValue = 0
    #else:
    #    print("ERROR: Location {0} not recognized!".format(location))
    #leg.AddEntry("", "{0} Yield Range: {1:.2f}<#Delta#varphi<{2:.2f}".format(centralValue - yieldLimit, centralValue + yieldLimit), "")
    if location == "AS":
        if plotType == "yields":
            # TODO: Implement throughout 
            #lowerValue = hist.GetXaxis().GetBinLowEdge(hist.FindBin(centralValue - binLimit))
            #upperValue = hist.GetXaxis().GetBinLowEdge(hist.FindBin(centralValue + binLimit))

            leg.AddEntry("", "{0} Yield Range: {1}<#Delta#varphi<{2}".format(location, "2#pi/3", "4#pi/3"), "")
        elif plotType == "widths":
            leg.AddEntry("", "{0} Width".format(location), "")
        else:
            print("Cannot recognize plot type {0}".format(plotType))
    elif location == "NS":
        if plotType == "yields":
            leg.AddEntry("", "{0} Yield Range: {1}<#Delta#varphi<{2}".format(location, "-#pi/3", "#pi/3"), "")
        elif plotType == "widths":
            leg.AddEntry("", "{0} Width".format(location), "")
        else:
            print("Cannot recognize plot type {0}".format(plotType))
    else:
        print("ERROR: Location {0} not recognized!".format(location))

    # Just in case
    ROOT.SetOwnership(leg, False)

    return leg

def calculateFinalYields(yields, yieldErrors, dPhi, iTrackPtBin):
    """ Calculate final yields based on measurements """
    # Whether we should be dividing by the track pt bin width is a bit unclear, but that seems like what Megan did (maybe?)
    #  See: line 140 in plot plotJetHpp.cxx
    trackPtBinWidth = trackPtBins[iTrackPtBin + 1] - trackPtBins[iTrackPtBin]
    # Bin 2 is just a proxy for any bin, since the bin size should be fixed
    #  This is what Megan did. Perhaps she was trying to avoid the edge bins??
    dPhiBinWidth = dPhi.GetXaxis().GetBinWidth(2)

    # Calculate yields
    #yieldfarerr = sqrt(yieldfarerr2)*JHdphi[icent][iptt][ipta]->GetBinWidth(2)/dpt[ipta];
    #yieldnearerr = sqrt(yieldnearerr2)*JHdphi[icent][iptt][ipta]->GetBinWidth(2)/dpt[ipta];
    # Float ensures that the divison is float and not integer...
    yields = [y * float(dPhiBinWidth)/trackPtBinWidth for y in yields]

    # Calculate yield errors
    # Take yieldErrors**2 -> yieldErrors
    #yieldfar = yieldfar*JHdphi[icent][iptt][ipta]->GetBinWidth(2)/dpt[ipta];
    #yieldnear = yieldnear*JHdphi[icent][iptt][ipta]->GetBinWidth(2)/dpt[ipta];
    # Float ensures that the divison is float and not integer...
    yieldErrors = [math.sqrt(y) * float(dPhiBinWidth)/trackPtBinWidth for y in yieldErrors]

def extractYields(dPhi, storedYields, storedYieldErrors, yieldLimit, iJetPtBin, iTrackPtBin):
    """ Extract yields for the given histogram from -Pi/3 to Pi/3 around 0 and Pi"""
    # Calculate yields
    yields = [0., 0.]
    yieldErrors = [0., 0.]
    centralValues = [0, ROOT.TMath.Pi()]
    for i, val in enumerate(centralValues):
        # Make it +1 to be inclusive!
        for iBin in range(dPhi.FindBin(val-yieldLimit), dPhi.FindBin(val+yieldLimit)+1):
            yields[i] = yields[i] + dPhi.GetBinContent(iBin)
            # We want the errors squared
            yieldErrors[i] = yieldErrors[i] + (dPhi.GetBinError(iBin))**2

    # Manipulates into the final values by accounting for bin widths
    calculateFinalYields(yields, yieldErrors, dPhi, iTrackPtBin)

    # Store results
    locations = ["NS", "AS"]
    for i, location in enumerate(locations):
        storedYields.setdefault(location, {}).setdefault(iTrackPtBin, []).append(yields[i])
        storedYieldErrors.setdefault(location, {}).setdefault(iTrackPtBin, []).append(yieldErrors[i])

def createDPhiLegend(iJetPtBin, iTrackPtBin, minimal):
    """ Create a legend for DPhi histograms noting the track and jet pt bins"""
    # Create a legend for clarity
    leg = ROOT.TLegend(0.35, 0.74, 0.89, 0.87)
    if not minimal:
        leg.AddEntry("", "Pb-Pb #sqrt{s_{NN}}=2.76 TeV, 0-10%", "")
        leg.AddEntry("", "Anti-k_{T} full jets, R=0.2", "")
    leg.AddEntry("", generateJetPtRangeString(iJetPtBin), "")
    leg.AddEntry("", generateTrackPtRangeString(iTrackPtBin), "")
    leg.SetBorderSize(0)
    #leg.SetTextSize(1.1)
    ROOT.SetOwnership(leg, False)

    return leg

def plotAllCanvas(canvasAll, dPhi, leg, iTrackPtBin, iJetPtBin):
    """ Hanldes plotting all jet and track pt bins onto one plot """
    # Linear index
    # +1 on trackPtBin since cd(0) erases and overwrites the Divide()
    canvasId = iTrackPtBin+1 + iJetPtBin*7
    #print("CanvasId: {0}".format(canvasId))
    # Move to proper canvas
    canvasAll.cd(canvasId)

    # Make marker size a bit more managable for many plots on a single canvas
    dPhi.SetMarkerSize(0.8)

    # Draw
    dPhi.Draw()

    # Draw legend
    leg.Draw("same")

def plotSubtractionCanvas(subtractionCanvas, upperPad, lowerPad, dPhi, dPhiSubtracted, fits, leg, iJetPtBin, iTrackPtBin, iEtaBin, ensureBothPlotsFit = False):
    """ Handles plotting the dPhi signal and the associated fit """
    # Undivided hist
    subtractionCanvas.cd()
    upperPad.cd()
    # Double curly bracket to display a literal {}
    dPhi.SetTitle("#Delta#varphi Jet p_{{T}} bin: {0} track p_{{T}} bin {1}".format(iJetPtBin, iTrackPtBin))
    dPhi.GetYaxis().SetTitle("#frac{1}{N_{trig}}#frac{dN_{assoc}}{d#Delta#varphi}")

    # Attempt to ensure the plot is over the right range...
    if ensureBothPlotsFit:
        minVal = min(dPhi.GetMinimum(), dPhiSubtracted.GetMinimum())
        if minVal > 0:
            minVal = 0
        maxVal = max(dPhi.GetMaximum(), dPhiSubtracted.GetMaximum())
        # Ensure that both plots fit on the pad
        dPhi.GetYaxis().SetRangeUser(minVal, math.ceil(maxVal))

    # Plot dPhi and mixed events
    dPhi.Draw()
    #dPhiSubtracted.Draw("same")
    leg.Draw("same")

    # Draw bg and subtracted fit
    fits[0].Draw("same")
    #fits[1].SetLineColor(ROOT.kRed+2)
    fits[1].Draw("same")

    # This should be the divided histogram
    lowerPad.cd()

    # Help make the range reasonable
    minVal = dPhiSubtracted.GetMinimum()
    if minVal < 0:
        # Go 20% below min val
        minVal = minVal * 1.2
    maxVal = dPhiSubtracted.GetMaximum()
    # Go 20% above max val
    maxVal = maxVal * 1.2
    # Set titles
    dPhiSubtracted.GetYaxis().SetRangeUser(minVal, maxVal)
    dPhiSubtracted.GetYaxis().SetTitle("Pedestal subtracted")
    dPhiSubtracted.SetTitle("")
    # Increase axis mark label size and tick size
    dPhiSubtracted.GetYaxis().SetLabelSize(0.1)
    dPhiSubtracted.GetXaxis().SetLabelSize(0.1)
    dPhiSubtracted.GetXaxis().SetTickSize(0.1)
    # Increase x-axis label size and make sure that it fits
    dPhiSubtracted.GetXaxis().SetTitleSize(0.15)
    dPhiSubtracted.GetXaxis().SetTitleOffset(0.6)
    # Increase y-axis label size
    dPhiSubtracted.GetYaxis().SetTitleSize(0.1)
    dPhiSubtracted.GetYaxis().SetTitleOffset(0.4)
    # Draw
    dPhiSubtracted.Draw()

    # Draw subtracted fit
    fits[2].SetLineColor(ROOT.kGreen+2)
    fits[2].Draw("same")

    subtractionCanvas.SaveAs(os.path.join("output", "plotting", "jetHDPhiSubtracted_etaBin{0}_jetPtBin{1}_trackPtBin{2}.pdf".format(iEtaBin, iJetPtBin, iTrackPtBin) ))
    upperPad.Clear()
    lowerPad.Clear()

# Bins
# eta is absolute value!
etaBins = [0, 0.4, 0.8, 1.5]
# NOTE: 8-20 will likely be only 8-10 because of how the THnSparse is defined
#trackPtBins = [0, 0.5, 1, 2, 3, 5, 8, 20]
trackPtBins = [0, 0.5, 1, 2, 3, 5, 8, 10]
# These are a more useful range
jetPtBins = [15, 20, 60, 200]
# These are from the task
#jetPtBins = [15, 20, 25, 30, 60, 200]
# From: https://arxiv.org/pdf/1405.4632v2.pdf
v2Cent00_05Values = [0.0125, 0.03, 0.04, 0.07, 0.07, 0.07, 0.06]
v2Cent05_10Values = [ 0.015, 0.04, 0.075, 0.1, 0.101, 0.101, 0.101]

# Use with alpha of 0.3 for a good look!
colors = [ROOT.kRed + 2, ROOT.kBlue + 2, ROOT.kGreen + 2, ROOT.kBlack, ROOT.kMagenta + 2]

def plotJetH(filename):
    # Setup output area
    if not os.path.exists(os.path.join("output", "plotting")):
        os.makedirs(os.path.join("output", "plotting"))

    # Retrieve all histograms
    hists = getListOfHistograms(filename)

    # Retrieve THnSparse
    jetH = hists["fhnJH"]
    mixedEvents = hists["fhnMixedEvents"]

    # Inclusive plots
    #ptSpectra(jetH)
    deltaPlotsIn2D(jetH, mixedEvents, hists)

    # Differential plots
    jetHDPhi(jetH, mixedEvents, hists)

if __name__ == "__main__":
    # Load reasonable style...
    #ROOT.gROOT.ProcessLine(".x {0}".format(os.path.expandvars("${MYINSTALL}/include/readableStyle.h")))
    #ROOT.gROOT.ProcessLine(".x readableStyle.h")

    # Plot
    plotJetH(os.path.expanduser("~/code/alice/jetH/train1183/AnalysisResults.root"))
