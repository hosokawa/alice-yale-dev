// Plot Mixed Events for correlation analysis
// Standard library includes
#include <iostream>
#include <vector>
#include <string>
#include <sstream>

// Root includes
#include <TLatex.h>
#include <TSystem.h>
#include <TMath.h>
#include <stdio.h>
#include <TH1.h>
#include <TH2.h>
#include <TFile.h>
#include <TF1.h>
#include <TColor.h>
#include <TCanvas.h>
#include <TStyle.h>
#include <THnSparse.h>
#include <TROOT.h>
#include <TLegend.h>

void plotMixedEvents(int runno = 1307022239, bool save = false)
{
	gStyle->SetOptTitle(0);
	gStyle->SetOptStat(0);

	std::stringstream sFilename;
	sFilename << "/home/rehlers/code/alice/train/lhc11a/" << runno << "/merge.root";
	std::cout << "Opening filename: " << sFilename.str() << std::endl;

	// Open file and retreive the list and mixed events
	// JHcore is defined by:
	//  Double_t triggerEntries[9] = {fcent,jetPt,track->Pt(),dR,deta,dphijh,track->Charge(),leadjet, zVtx};
	TFile *fin = TFile::Open(sFilename.str().c_str());
	TList* list = dynamic_cast <TList *> (fin->Get("Correlations_Jet_AKTFullR040_PicoTracks_pT0500_CaloClustersCorr_ET0500"));
	THnSparseF* JHcore = dynamic_cast <THnSparseF *> (fin->Get("fhnMixedEvents"));

	if (!JHcore) { std::cout << "Cannot find JHcore" << std::endl; }
	if(!list)
	{
		std::cout << "Cannot find list" << std::endl;
	}

	if(!list || !JHcore) 
	{
		std::cout << "List or mixed events (JHcore) not found" <<std::endl; 
		return;
	}
	else
	{
		std::cout << "Found list and mixed events -> Continuing." << std::endl;
	}

	JHcore->Sumw2();

	TCanvas *cJH;

	TH1* JHdphi[8][7];
	TH1* JHeta[6][4][7];
	TH2* JHetaphi[6][4][7];

	TH1* TrackPt;
	TH1* JetPt;
	
	char etaphiname[100];
	char ptname[100];
	char cname[100];

	int centbin[5]={0,10,30,50,90};
	int trigbin[2]={20,60};
	Double_t etabin[4]={0.0,0.4,0.8,1.2};
	Double_t hadbin[8]={0.15,0.5,1.0,2.0,3.,5.,8.,20};

	// Centrality loop
	for(int icent = 0; icent < 1; icent++)
	{
		// 0 = cent
		JHcore->GetAxis(0)->SetRangeUser(centbin[icent]+1,centbin[icent+1]);
		//JHcore->GetAxis(4)->SetRangeUser(-1.6,-0.8);
		//JHcore->SetAxisRange(centbin[icent]+1,centbin[icent+1],7);

		// Get jetPt hist
		JetPt = static_cast<TH1 *> (JHcore->Projection(1));
		sprintf(ptname,"Jetpt%ito%i",centbin[icent],centbin[icent+1]);
		JetPt->SetName(ptname);
		
		// Get trackPt hist
		TrackPt = static_cast<TH1 *> (JHcore->Projection(2));
		sprintf(ptname,"trackpt%ito%i",centbin[icent],centbin[icent+1]);
		TrackPt->SetName(ptname); 
		TrackPt->GetYaxis()->SetTitle("N_{tracks}");
		TrackPt->GetYaxis()->SetTitleOffset(1.6);
		TrackPt->GetXaxis()->SetTitle("Track Pt");

		TrackPt->SetLineColor(icent+1);
		TrackPt->SetLineWidth(2);
		TrackPt->SetAxisRange(0,20,"X");
		
		// Delta is apparently necessary to ensure that we only look at the desired bins
		// ie. To consider the range 2-6, we must look at (2+delta) through (6-delta). Otherwise, extra bins will be included.
		float delta=0.00001;
		double fnorm[7]={0.0};

		// Setup canvas
		// ipad is the linear index of the divided canvas
		sprintf(cname,"cJH_%i",icent);
		cJH = new TCanvas(cname, cname, 1200, 600);
		cJH->Divide(7,3);
		int ipad = 1;

		// trigger track loop
		for(int iptt = 0; iptt < 1; iptt++)
		{
			// 1 = jetPt
			JHcore->GetAxis(1)->SetRangeUser(trigbin[iptt]+delta,trigbin[iptt+1]-delta);

			// associated track pt loop
			for(int ipta = 0; ipta < 7; ipta++)
			{
				// 2 = trackPt
				// 4 = deta
				JHcore->GetAxis(2)->SetRangeUser(hadbin[ipta]+delta,hadbin[ipta+1]-delta);
				std::cout << "upper Hadron pt  " << hadbin[ipta+1] <<std::endl;
				JHcore->GetAxis(4)->SetRangeUser(-1.6,1.6);	

				// JHdeltaEta
				// Why is the 5th argument different than below? -> It looks like some values were overflowing before?
				// I think (hadbin[ipta]+1)*100 is a bug
				JHeta[icent][iptt][ipta] = static_cast<TH1 *> (JHcore->Projection(4));
				sprintf(etaphiname, "JHeta_%i_%ix%i_%i_c%i", trigbin[iptt]+1, trigbin[iptt+1],
								static_cast<int> ((hadbin[ipta]+1)*100), 
								static_cast<int> (hadbin[ipta+1]*100), icent);
				JHeta[icent][iptt][ipta]->SetName(etaphiname);
				std::cout << "************etaphiname " << etaphiname <<std::endl;

				// 4 = deta, 5 = dphijh
				// JHdeltaEtaDeltaPhi
				JHetaphi[icent][iptt][ipta]=(TH2*)JHcore->Projection(4,5);
				sprintf(etaphiname, "JHetaphi_%i_%ix%i_%i_c%i", trigbin[iptt]+1, trigbin[iptt+1], static_cast<int> (hadbin[ipta]*100),
																static_cast<int> (hadbin[ipta+1]*100),icent);
				JHetaphi[icent][iptt][ipta]->SetName(etaphiname);
				std::cout << "************etaphiname " << etaphiname <<std::endl;

				// Normalize the 2d deta dphi distribution to coordinates (0,0)
				// Inside the nearside jet? -> That seems strange
				double fnorm2d = JHetaphi[icent][iptt][ipta]->GetBinContent(JHetaphi[icent][iptt][ipta]->GetXaxis()->FindBin(0.0),
																			JHetaphi[icent][iptt][ipta]->GetYaxis()->FindBin(0.0));
				JHetaphi[icent][iptt][ipta]->Scale(1.0/fnorm2d);

				// Find total number of pairs
				double Npairs = JHetaphi[icent][iptt][ipta]->Integral();
				std::cout << "Npairs: " << Npairs <<std::endl;

				// Counts down with jeta as ieta counts up
				// jeta = index for eta, but i is already used, so use j instead
				// NOT jet a!
				//int jeta=2;

				// eta loop
				for(int ieta = 0, jeta = 2; ieta < 3; ieta++, jeta--)
				{
					// 4 = deta
					// JHDeltaPhi
					JHcore->GetAxis(4)->SetRangeUser(etabin[ieta]+delta, etabin[ieta+1]-delta);	
					JHdphi[jeta][ipta] = static_cast<TH1 *> (JHcore->Projection(5));
					sprintf(ptname, "JH_%i_%ix%i_%i_c%i_eta%i", trigbin[iptt]+1, trigbin[iptt+1], static_cast<int> (hadbin[ipta]*100),
																static_cast<int> (hadbin[ipta+1]*100), icent, jeta);
					JHdphi[jeta][ipta]->SetName(ptname);

					std::cout << ipta << " had bin " << hadbin[ipta] <<std::endl;
					std::cout << "eta bin: " << ieta << " " << etabin[ieta]+delta << "  " << etabin[ieta+1]-delta <<std::endl;
					std::cout << "Drawing: " << ptname <<std::endl;

					// Why is this selected in particular?
					if(ipta == 2) 
					{
						std::cout << "************BinContent" << JHdphi[jeta][ipta]->GetBinContent(2) <<std::endl;
					}

					JHcore->GetAxis(4)->SetRangeUser(-1.0*etabin[ieta]-delta,-1.0*etabin[ieta+1]+delta);	
					std::cout << "eta bin: " << ieta << " " << -1.0*etabin[ieta]-delta << "  " << -1.0*etabin[ieta+1]+delta <<std::endl;

					// Temporary histogram with the content of dphijh
					TH1* hTemp = static_cast<TH1 *> (JHcore->Projection(5));	
					hTemp->SetName("temphisto");

					std::cout << "************BinContent" << hTemp->GetBinContent(2) <<std::endl;

					// Why?
					JHdphi[jeta][ipta]->Add(hTemp);

					if(ipta == 2)
					{
						std::cout << "************BinContent" << JHdphi[jeta][ipta]->GetBinContent(2) <<std::endl;
					}

					// Remove temporary histogram
					hTemp->Delete();

					if(ipta == 2)
					{
						std::cout << "*********NewBinContent" << JHdphi[jeta][ipta]->GetBinContent(2) <<std::endl;
					}

					// bw = bin width
					double bw=JHdphi[jeta][ipta]->GetBinWidth(1);

					/* // Everything commented here can probably be removed
					double deltaEta=2.0*(etabin[ieta+1]-etabin[ieta]);
					int ptbinlo= JetPt->FindBin(trigbin[iptt]);
					int ptbinhi= JetPt->FindBin(trigbin[iptt+1]);
					double ntrigs=JetPt->Integral(ptbinlo,ptbinhi);*/
					//DO NOT divide mixed evts by ntrig because weight in code?? 
					//if(ntrigs>0 && bw>0) JHdphi[jeta][ipta]->Scale(1./ntrigs/bw);

					//if(Npairs>0 && bw>0) JHdphi[jeta][ipta]->Scale(1./Npairs/bw/deltaEta/0.087);
					//else std::cout << "Ntrig " << ntrigs << " binwidth: " << bw <<std::endl;

					// Find and set normalization
					if (ieta == 0)
					{
						fnorm[ipta] = JHdphi[jeta][ipta]->Integral();
					}

					// If this is not greater than 0, should an error be thrown?
					if(fnorm[ipta] > 0) 
					{
						JHdphi[jeta][ipta]->Scale(2.0*TMath::Pi()/fnorm[ipta]/bw);
					}

					// Set hist properties
					JHdphi[jeta][ipta]->GetXaxis()->SetTitle("#Delta#phi");
					JHdphi[jeta][ipta]->GetYaxis()->SetTitle("#frac{dN}{N_{pairs}d#Delta#phi}");
					JHdphi[jeta][ipta]->SetMinimum(0.0);
					JHdphi[jeta][ipta]->GetXaxis()->SetLabelSize(0.08);
					JHdphi[jeta][ipta]->GetXaxis()->SetTitleSize(0.08);
					JHdphi[jeta][ipta]->GetYaxis()->SetTitleOffset(1.9);
					JHdphi[jeta][ipta]->GetYaxis()->SetLabelSize(0.06);
					JHdphi[jeta][ipta]->GetYaxis()->SetTitleSize(0.07);
					JHdphi[jeta][ipta]->GetXaxis()->SetTitleOffset(1.0);

					// Determine the (serialized) index for the location in the canvas
					ipad = ieta*7+ ipta+1;

					// Set canvas properties
					cJH->cd(ipad);
					cJH->GetPad(ipad)->SetTopMargin(0.001);
					cJH->GetPad(ipad)->SetRightMargin(0.001);
					cJH->GetPad(ipad)->SetBottomMargin(0.17);
					cJH->GetPad(ipad)->SetLeftMargin(0.275);

					// Draw hist with error bars
					JHdphi[jeta][ipta]->DrawCopy("e");
				}
			}
		}
	}

	sFilename.str("");
	sFilename << "evtmixed_dphi_" << runno << ".pdf";

	cJH->SaveAs(sFilename.str().c_str());

	std::cout << "Done creating hists..." <<std::endl;

	if(save == true)
	{
		std::cout << "Save to root file..." << std::endl;
		sFilename.str("");
		sFilename << "mixed_" << runno << "_semi1.root";

		TFile fout(sFilename.str().c_str(),"recreate");

		for(int ipta=0; ipta<7; ipta++)
		{
			// cent = 0, ptTT = 0
			JHetaphi[0][0][ipta]->Write();
			for(int ieta=0; ieta<3; ieta++)
			{
				JHdphi[ieta][ipta]->Write();
			}
		}

		fout.Close();
	}

	// Cleanup originally opened file
	fin->Close();
}
