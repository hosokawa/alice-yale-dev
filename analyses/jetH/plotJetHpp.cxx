#include <TLatex.h>
#include <TSystem.h>
#include <TMath.h>
#include <TH1.h>
#include <TH2.h>
#include <TH3.h>
#include <TFile.h>
#include <TF1.h>
#include <TColor.h>
#include <TCanvas.h>
#include <TStyle.h>
#include <vector>
#include <string>
#include <TROOT.h>
#include <TLegend.h>
#include <TGraphErrors.h>
#include <TGraphAsymmErrors.h>
#include <THnSparse.h>
#include <TFitResult.h>
#include <TFitResultPtr.h>
#include <cstdio>
#include <fstream>
#include <iostream>

// Set properties of the legend
void myLegendSetUp(TLegend * currentLegend = 0,float currentTextSize = 0.07)
{
	currentLegend->SetTextFont(42);
	currentLegend->SetBorderSize(0);
	currentLegend->SetFillStyle(0);
	currentLegend->SetFillColor(0);
	currentLegend->SetMargin(0.25);
	currentLegend->SetTextSize(currentTextSize);
	currentLegend->SetEntrySeparation(0.5);
	return;
}

// Apply tracking efficiency (?)
void ApplyTrkEff(TH2* hJH, int contam)
{
	std::cout << "Applying tracking" << std::endl;

	int Nbinsdphi = hJH->GetNbinsX();
	int Nbinspt = hJH->GetNbinsY();

	TFile *fineff = TFile::Open("/eliza6/alice/mconnors/TrkEffFit.root");
	TF1 *fit1 = static_cast<TF1 *> (fineff->Get("Trk_eff_fit_1"));
	TF1 *fit2 = static_cast<TF1 *> (fineff->Get("Trk_eff_fit_2"));
	TF1 *fit3 = static_cast<TF1 *> (fineff->Get("Trk_eff_fit_3"));

	TFile *fincont = TFile::Open("../root_files/Contamination.root");
	TGraphAsymmErrors *fakes = static_cast<TGraphAsymmErrors *> (fincont->Get("divide_B1_by_M+B1"));

	for(int ipt = 1; ipt < (Nbinspt+1); ipt++)
	{
		float ptcent = 0.1*ipt-0.05;

		float eff = 0;
		float contamcorr = 0;

		if (ptcent<0.5) 
		{
		   	eff = fit1->Eval(ptcent); 
		}
		else
	   	{
			if
			{
				(ptcent<3) eff = fit2->Eval(ptcent);
			}
			else
			{
				eff = fit3->Eval(ptcent);
			}
		}

		if (!contam) { contamcorr = 1.0; }
		else { contamcorr = 1.0-fakes->Eval(ptcent); }

		if (eff == 0) { cout << "WARNING: dividing by eff==0!!!" << std::endl; }

		for(int ix = 1; ix<(Nbinsdphi+1); ix++)
		{
			//std::cout << "ix = " << ix << " eff " << eff << " contamcorr: " << contamcorr << std::endl;

			float bc = hJH->GetBinContent(ix,ipt);
			float berr = hJH->GetBinError(ix,ipt);
			//std::cout << "eff corr " <<  1.0/eff << std::endl;
			hJH->SetBinContent(ix,ipt,bc/eff*contamcorr);
			hJH->SetBinError(ix,ipt,berr/eff*contamcorr);
		}
	}
}

// Create the colors for the plot style
void setPlotStyle()
{
	const Int_t NRGBs = 5;
	const Int_t NCont = 20;
	Double_t stops[NRGBs] = { 0.00, 0.34, 0.61, 0.84, 1.00 };
	Double_t red[NRGBs]   = { 0.00, 0.00, 0.87, 1.00, 0.51 };
	Double_t green[NRGBs] = { 0.00, 0.81, 1.00, 0.20, 0.00 };
	Double_t blue[NRGBs]  = { 0.51, 1.00, 0.12, 0.00, 0.00 };
	TColor::CreateGradientColorTable(NRGBs, stops, red, green, blue, NCont);
	gStyle->SetNumberContours(NCont);
}

// Main function
void plotJetHpp(Int_t runno = 1210011125)
{
	gStyle->SetOptTitle(0);
	gStyle->SetOptStat(0);
	setPlotStyle();
	
	// Toggles
	Int_t thnsp = 1; // Toggle for whether the THnSparse exists
	Int_t dofits = 1; // Toggle to enable fits
	Int_t dosubtract = 0; // Toggle to enable (background?) subtraction
	Int_t charge = 0; // Toggle related to the charge entry stored in the THnSparse
	Int_t bias = 0; // Toggle to consider biased hists
	Int_t pp = 1; // Toggle for pp data
	Int_t evtmix = 1; // Toggle to enable event mixing
	Int_t effcorr = 1; // Toggle to apply efficiency correction
	Int_t contam = 1; // Toggle something about contamnation (related to the tracking efficiency correction?)

	// Values
	Int_t RB = 1; // Rebin value (Implicit toggle. 1 simply does nothing)
	Double_t RBsc = 1.0; // Scale to apply to hist (always seems to be applied after the rebin)
	Int_t Npta = 7; // Number of associated pt bins
	bool saveall = false; // Toogle to save histograms to pdfs
	int trigbin[2]={20,60}; // Trigger bin -> Denotes the jet pt range of interest
	// Determines the min and max jet pt that are considered. In this case, jetPt of 20-60 is considered
	Int_t mintrig = 1; // 0 = 15-20, 1 = 20-25, 2 = 25-40, 3 = 40-60, 4 > 60 
	Int_t maxtrig = 4; 

	// I have no idea
	Int_t Nptaref = 4; // It seems to be commonoly associated with rebinning
	Int_t findex = evtmix+10*dosubtract; 
	Int_t hardBin = 0; 
	Double_t dpt[7]={0.35,0.5,1,1,2,5,20}; // It seems to be related to calculating yield
	int hadbin[8]={0,5,10,20,30,50,80,200};  // Very carefully set, based on line ~500. These bins correspond to the bin of a cut. ie. 5 corresponds to 0.5 GeV?

	TLegend *legJH[7]; // Legend for jetHadron plots

	char name[100]; // General name c string
	char cname[100]; // Centrality name (?)

	// Open reference file? -> What is that? -> Perhaps pp comparison?
	TFile *finpp = TFile::Open("../root_files/pp_3GeV_1210011125_trig_1_4_sp0_mix1_sub0_ch0_contam1.root");

	if (!finpp)
	{
		std::cout << "CANNOT FIND pp REFerence File!!!" << std::endl;
	}

	// Reference widths
	// near side
	TGraphErrors* gppwidthnear = static_cast<TGraphErrors *> (finpp->Get("gWidthNear_0"));
	gppwidthnear->SetLineColor(2);
	gppwidthnear->SetMarkerColor(2);
	// far side
	TGraphErrors* gppwidthfar = static_cast<TGraphErrors *> (finpp->Get("gWidthFar_0"));
	gppwidthfar->SetLineColor(2);
	gppwidthfar->SetMarkerColor(2);

	// pp jetPt
	TH1F* ppJetPt = static_cast<TH1F *> (finpp->Get("AKtjetpt0to10"));
	ppJetPt->SetLineColor(2);
	ppJetPt->SetMarkerColor(2);
	ppJetPt->Sumw2();

	// JH (comparison?) for pp
	TH1 *JHpp[7];

	for(int ipta = 1; ipta<Nptaref; ipta++)
	{
		sprintf(name,"JH_0_pt_%i_%i_c0_10",hadbin[ipta]+1,hadbin[ipta+1]);
		//std::cout << hbinlo << "Getting bg histo: " << name << std::endl;
		JHpp[ipta]=(TH1*)finpp->Get(name);
		sprintf(name,"ppJH_0_pt_%i_%i_c0_10",hadbin[ipta]+1,hadbin[ipta+1]);
		JHpp[ipta]->SetName(name);
	}

	std::cout << "Got pp histos" << std::endl;

	// sysup -> Systematic errors for the upper part of the error? 
	TFile *finppup = TFile::Open("../root_files/pp_3GeV_1210011125_sp1_mix1_sub1.root");

	// Widths
	// Near side
	TGraphErrors* gppupwidthnear = static_cast<TGraphErrors *> (finppup->Get("gWidthNear_0"));
	gppupwidthnear->SetLineColor(2);
	gppupwidthnear->SetMarkerColor(2);
	// Far side
	TGraphErrors* gppupwidthfar = static_cast<TGraphErrors *> (finppup->Get("gWidthFar_0"));
	gppupwidthfar->SetLineColor(2);
	gppupwidthfar->SetMarkerColor(2);

	// sysup pp jetPt
	TH1F* ppupJetPt = static_cast<TH1F *> (finppup->Get("AKtjetpt0to10"));
	ppupJetPt->SetLineColor(2);
	ppupJetPt->SetMarkerColor(2);
	ppupJetPt->Sumw2();

	// sysdown -> Systematic errors for the lower part of the error? 
	TFile *finppdown = TFile::Open("../root_files/pp_3GeV_1210011125_sp1_mix0_sub0.root");

	// Widths
	// Near side
	TGraphErrors* gppdownwidthnear = static_cast<TGraphErrors *> (finppdown->Get("gWidthNear_0"));
	gppdownwidthnear->SetLineColor(2);
	gppdownwidthnear->SetMarkerColor(2);
	// Away side
	TGraphErrors* gppdownwidthfar = static_cast<TGraphErrors *> (finppdown->Get("gWidthFar_0"));
	gppdownwidthfar->SetLineColor(2);
	gppdownwidthfar->SetMarkerColor(2);

	// sysdown pp jetPt
	TH1F* ppdownJetPt = static_cast<TH1F *> (finppdown->Get("AKtjetpt0to10"));
	ppdownJetPt->SetLineColor(2);
	ppdownJetPt->SetMarkerColor(2);
	ppdownJetPt->Sumw2();

	// To combine all of the widths together?
	TGraphAsymmErrors* gWidthNearErr = new TGraphAsymmErrors(7);
	TGraphAsymmErrors* gppwidthnearErr = new TGraphAsymmErrors(7);
	sprintf(cname,"gppWidthNearErr");
	gppwidthnearErr->SetName(cname);

	// ip = index of points
	for(int ip = 0; ip<6; ip++)
	{
		Double_t pth, nsw, nswdown, nswup;

		// Get the points
		gppdownwidthnear->GetPoint(ip,pth,nswdown);
		gppupwidthnear->GetPoint(ip,pth,nswup);
		gppwidthnear->GetPoint(ip,pth,nsw);

		// Set the value on the near side based on the pp value
		gppwidthnearErr->SetPoint(ip,pth,nsw);

		if(nsw>0) std::cout << pth << " err up: " << (nsw-nswup)/nsw << "% err down: " << (nswdown-nsw)/nsw << std::endl;

		// Sets the errors. The errors on the x position are 0.05
		// It appears that the values can be switched. I'm not sure of why
		// Nonetheless, this appears to be able to account for it
		if(nswdown>nsw && nswup<nsw)
		{
			gppwidthnearErr->SetPointError(ip,0.05,0.05,nsw-nswup,nswdown-nsw);
			gWidthNearErr->SetPointError(ip,0.05,0.05,nsw-nswup,nswdown-nsw);
		}
		else 
		{
			if(nswup>nsw && nswdown<nsw)
			{
				gppwidthnearErr->SetPointError(ip,0.05,0.05,nsw-nswdown,nswup-nsw);
				gWidthNearErr->SetPointError(ip,0.05,0.05,nsw-nswdown,nswup-nsw);
			}
			else
			{ 
				gppwidthnearErr->SetPointError(ip,0.05,0.05,fabs(nsw-nswdown),fabs(nswup-nsw));
				gWidthNearErr->SetPointError(ip,0.05,0.05,fabs(nsw-nswdown),fabs(nswup-nsw));
			}
		}
	}

	// For flow?
	TH1 *flow[7];

	// JH event mixing
	TH1 *JHmix[3][7];
	TH2 *JHmix2D[7];
	Double_t bghadbin[8]={0.15,0.5,1.0,2.0,3.,5.,8.,20};

	// Retreive event mixing hists
	if(evtmix)
	{
		// Open event mixing file
		sprintf(name,"../root_files/mixed_%i.root", runno);
		TFile *finmix = TFile::Open(name);
		if(!finmix)
	   	{
		   	std::cout << "Could not open mixed evt file" << std::endl;
			return;
	   	}

		for(int icent = 0; icent<1; icent++)
		{
			// jeta = j index of eta (NOT jet a!)
			for(int ieta = 0, jeta = 2; ieta<3; ieta++, jeta--)
			{
				for(int ipta = 0; ipta<7; ipta++)
				{
					int iptt = 0;

					// Needed to get the approrpriate values for event mixing
					int hbinlo = static_cast<int> (100*bghadbin[ipta]);
					int hbinhi = static_cast<int> (100*bghadbin[ipta+1]);

					sprintf(name,"JH_%i_%ix%i_%i_c%i_eta%i",trigbin[iptt]+1,trigbin[iptt+1],hbinlo,hbinhi,icent,jeta);
					std::cout << hbinlo << "Getting bg histo: " << name << std::endl;
					JHmix[ieta][ipta] = static_cast <TH1 *> (finmix->Get(name));

					sprintf(name,"JHetaphi_%i_%ix%i_%i_c%i",trigbin[iptt]+1,trigbin[iptt+1],hbinlo,hbinhi,icent);
					std::cout << hbinlo << "Getting bg histo: " << name << std::endl;
					JHmix2D[ipta] = static_cast<TH2 *> (finmix->Get(name));

					if(!JHmix2D[ipta]) { std::cout << "CANNOT FIND JHMIX2d" << std::endl; }

					std::cout << "Bins in JHmix: " << JHmix[ieta][ipta]->GetNbinsX() << std::endl;
				}
			}
		}
		std::cout << "read in mixed events" << std::endl;
	}

	std::cout << "get main file" << std::endl;
	
	char filename[100];
	sprintf(filename, "/home/rehlers/train/lhc11a/%i/merge.root", runno);
	TFile *fin = TFile::Open(filename);
	std::cout << "Opening " << filename << std::endl;

	// Retreive list and THnSparseF
	sprintf(name, "Correlations_Jet_AKTFullR040_PicoTracks_pT0500_CaloClustersCorr_ET0500");
	TList* l = static_cast<TList *> (gROOT->FindObject(name));
	if(!l)
	{
		std::cout << "Lst not found " << name << std::endl;
		return;
	}
	else
	{
		std::cout << "found list...getting histos..." << std::endl;
	}

	THnSparseF * JHcore;
	if(thnsp)
	{
		JHcore = static_cast<THnSparseF *> (l->FindObject("fhnJH"));

		if(!JHcore) 
		{
		   	std::cout << "JHcore not found" << std::endl; 
			return;
		}

		JHcore->Sumw2();
	}

	// Create relevant histograms
	TH1* JetPt[6];
	TH2* fHistJetH[6][5][3];
	TH2* fHistJetH_sum[6][3];

	int centbin[7]={0,10,20,30,40,50,90};
	Double_t etabin[4]={0.0,0.4,0.8,1.2};

	// Centrality histogram
	if(!pp)
	{
		TH1* fHistCent= (TH1*)l->FindObject("fHistCentrality");
		if(!fHistCent) std::cout << "fHistCent not found" << std::endl;

		TCanvas* cancent = new TCanvas("canCent","",600,600);
		fHistCent->GetXaxis()->SetTitle("Centrality");
		fHistCent->GetYaxis()->SetTitle("N_{events}");
		fHistCent->GetYaxis()->SetTitleOffset(1.6);
		cancent->SetRightMargin(0.005);
		fHistCent->Draw();

		sprintf(filename,"Centrality_%i.pdf",runno);
		if(saveall)cancent->SaveAs(filename);

	}

	// Canvas for AktPts? and trackPt -> Akt = Anti-K_T
	TCanvas* canAktPts = new TCanvas("canAktPts","canAktPts",600,600);
	TCanvas* cantrackpt = new TCanvas("cantrackpt","cantrackpt",600,600);
	canAktPts->SetLogy();

	TH1* TrackPt[6];
	char ptname[100];

	// Why are the last two values 0?
	Double_t hadpt[7] = {0.7,1.3,2.8,7,15,0,0};

	// Centrality loop
	for(int icent = 0; icent<1; icent++)
	{
		// Select a biased histogram, if desired
		if (bias) { sprintf(name,"fHistJetPtBias_%i",icent); }
		else { sprintf(name,"fHistJetPt_%i",icent); }

		// Select canvas
		canAktPts->cd();

		JetPt[icent] = static_cast<TH1 *> (l->FindObject(name));
		if(!JetPt[icent]) { std::cout << "ERROR no JetPt" << std::endl; }

		// Set jetPt hist properties
		sprintf(ptname,"AKtjetpt%ito%i",centbin[icent],centbin[icent+1]);
		JetPt[icent]->SetName(ptname); 
		JetPt[icent]->Sumw2();
		JetPt[icent]->GetYaxis()->SetTitle("jet counts");
		JetPt[icent]->GetYaxis()->SetTitleOffset(1.6);
		JetPt[icent]->GetXaxis()->SetTitle("Anti kT JetPt");
		JetPt[icent]->SetLineColor(icent+1);
		JetPt[icent]->SetLineWidth(2);
		JetPt[icent]->SetMarkerStyle(8);
		JetPt[icent]->SetMarkerColor(1);

		if(icent==0) { JetPt[icent]->Draw(); }

		// eta loop
		for(int ieta = 0; ieta<3; ieta++)
		{
			// jetPt loop
			for(int itrig = mintrig, triggers = 0; itrig<maxtrig; itrig++, triggers++)
			{
				// Select biased hist if desired
				if (bias) { sprintf(ptname,"fHistJetHBias_%i_%i_%i",icent,itrig,ieta); }
				else { sprintf(ptname,"fHistJetH_%i_%i_%i",icent,itrig,ieta); }
				std::cout << "getting " << ptname << std::endl;

				// Create a temporary hist with jetH pt
				TH2 *Histtemp = static_cast<TH2 *> (l->FindObject(ptname));
				fHistJetH[icent][itrig][ieta] = static_cast<TH2 *> (Histtemp->Clone());

				sprintf(ptname,"tempJetH_%i_%i_%i",icent,itrig,ieta);
				Histtemp->SetName(ptname);

				if (!fHistJetH[icent][itrig][ieta]) { std::cout << "ERROR no fHistJetH" << std::endl; }
				sprintf(ptname,"JetH_%i_%i_%i",icent,itrig,ieta);
				fHistJetH[icent][itrig][ieta]->SetName(ptname);
				fHistJetH[icent][itrig][ieta]->Sumw2();

				if (effcorr) { ApplyTrkEff(fHistJetH[icent][itrig][ieta], contam); }

				if(triggers==0)
				{
					fHistJetH_sum[icent][ieta] = static_cast<TH2 *> (fHistJetH[icent][itrig][ieta]->Clone());
					sprintf(ptname,"JetH_%i_sum_%i",icent,ieta);
					fHistJetH_sum[icent][ieta]->SetName(ptname);
				}
				else
				{
					fHistJetH_sum[icent][ieta]->Add(fHistJetH[icent][itrig][ieta]);
				}

				if(!fHistJetH_sum) { std::cout << "Could not create "; } else { std::cout << "Created "; }
				std::cout << ptname << std::endl;
			}
		}

		std::cout << " ready to make track pt plot now" << std::endl;
		//Tracks
		cantrackpt->cd();

		// Power law fit (with some somewhat strange coefficients)
		// Appears to be unused
		/*TF1 *trkpow = new TF1("trkpow","[0]*x*([1]-1.0)/[2]*pow((1+x/[2]),-1.0*[1])",0.25,10);

		trkpow->SetParameter(2,1.0);
		trkpow->SetParameter(1,6.0);
		trkpow->SetParameter(0,10000);*/

		if(!fHistJetH_sum[icent][0]) 
		{
		   	std::cout <<"missing JetH_sum histo" << std::endl; 
			return;
	   	}

		std::cout << "project tracks" << std::endl;
		sprintf(ptname,"trackpt%ito%i",centbin[icent],centbin[icent+1]);
		TrackPt[icent] = static_cast<TH1 *> (fHistJetH_sum[icent][0]->ProjectionY());

		std::cout << "projected" << std::endl;
		TrackPt[icent]->SetName(ptname); 
		TrackPt[icent]->GetYaxis()->SetTitle("N_{tracks}");
		TrackPt[icent]->GetYaxis()->SetTitleOffset(1.6);
		TrackPt[icent]->GetXaxis()->SetTitle("Track Pt");

		/*std::cout << "fit the spectrum" << std::endl;
		TrackPt[icent]->Fit(trkpow,"R");
		std::cout << "power of track distrib " << trkpow->GetParameter(1) << std::endl;*/
	
		//calculate mean pt of bins
		if(icent==0)
		{
			std::cout << "start ipta loop" << std::endl;
			for(int ipta = 0; ipta<Npta; ipta++)
			{
				double sumbins = 0.0;
				double sumtrks = 0.0;
				// I don't understand the boundaries of this for loop -> Maybe
				// What is hadbin? Is it hadron? If so, this seems somewhat strange
				for(int ibin = hadbin[ipta]+1; ibin<hadbin[ipta+1]; ibin++)
				{
					double midpt = TrackPt[icent]->GetBinCenter(ibin);
					double numtrk = TrackPt[icent]->GetBinContent(ibin);
					sumbins = sumbins+numtrk*midpt;
					sumtrks = sumtrks+numtrk;
				}
				std::cout << ipta << " pt= " << hadpt[ipta] << std::endl;
				std::cout << hadbin[ipta]+1 << " to " << hadbin[ipta+1] << std::endl;
				hadpt[ipta]=sumbins/sumtrks;
			}
		}

		// Set hist properties
		TrackPt[icent]->SetLineColor(icent+1);
		TrackPt[icent]->SetLineWidth(2);
		TrackPt[icent]->SetAxisRange(0,30,"X");

		// Setup canvas
		cantrackpt->SetLogy();
		cantrackpt->cd();
		if(icent==0) { TrackPt[icent]->Draw(); }
		else { TrackPt[icent]->Draw("same"); }
	}

	// Setup legend for the canvas
	TLegend *legE = new TLegend(0.45,0.60,0.90,0.85);
	legE->SetBorderSize(0);
	legE->SetFillColor(0);
	legE->AddEntry(JetPt[0], "pp", "p");
	
	// These two lines can almost certainly be removed. Before, only drawing the legend was commented out
	//canAktPts->cd();
	//legE->Draw();

	// Draw the legend on the canvas
	cantrackpt->cd();
	legE->Draw();

	// Save the histograms, if desired.
	if(saveall)
	{
		sprintf(ptname,"Trackpt_%iFull_trig_%i_%i_%i.pdf",runno,mintrig,maxtrig,hardBin);
		cantrackpt->SaveAs(ptname);

		sprintf(ptname,"Jetpt_%iFull_%i.pdf",runno,hardBin);
		canAktPts->SaveAs(ptname);
	}

	// --------------------------------------------------------------------------------------	
	// Different task from above - Perhaps refactor into a new function 
	// --------------------------------------------------------------------------------------	

	// Histograms
	TH1* JHdphi[6][3][5];
	TH2* JHdphideta[6][5];
	TH2* JHdphipt[3];

	// Canvases
	// JH
	TCanvas *cJH[6];
	TCanvas *cJHpretty[2];
	// Widths
	TCanvas * cWidthFar =new TCanvas("cfarwidth","cfarwidth",600,600);
	TCanvas * cWidthNear =new TCanvas("cnearwidth","cnearwidth",600,600);
	// Yields
	TCanvas * cYieldFar =new TCanvas("cfaryield","cfaryield",600,600);
	TCanvas * cYieldNear =new TCanvas("cnearyield","cnearyield",600,600);

	// Delta constants necessary to select the proper bins in THnSparse.
	// Perhaps these should be defined as constants?
	float delta = 0.0000000001;
	float deltab = 0.000000;

	// Arrays to hold the widths of the near and away side.
	Double_t sigmanear[6][5];
	Double_t sigmafar[6][5];

	// Widths
	TGraphErrors* gWidthNear[6];
	TGraphErrors* gWidthFar[6];

	// Yields
	TGraphErrors* gYieldNear[6];
	TGraphErrors* gYieldFar[6];
	
	for(int icent = 0; icent<1; icent++)
	{
		// Bounds are selected based on the range of jetPt that we are interested in
		float ntrigs = JetPt[icent]->Integral(20,60);

		if(thnsp)
		{
			// 0 = centrality, 6 = charge 
			JHcore->GetAxis(0)->SetRange( JHcore->GetAxis(0)->FindBin(centbin[icent]+delta), JHcore->GetAxis(0)->FindBin(centbin[icent+1]-delta));
			if (charge != 0) { JHcore->GetAxis(6)->SetRange(2+charge,2+charge); }
		}

		// Define new TGraphErrors for widths and yields
		gWidthFar[icent]=new TGraphErrors(7);
		sprintf(cname,"gWidthFar_%i",icent);
		gWidthFar[icent]->SetName(cname);

		gWidthNear[icent]=new TGraphErrors(7);
		sprintf(cname,"gWidthNear_%i",icent);
		gWidthNear[icent]->SetName(cname);

		gYieldFar[icent]=new TGraphErrors(7);
		sprintf(cname,"gYieldFar_%i",icent);
		gYieldFar[icent]->SetName(cname);

		gYieldNear[icent]=new TGraphErrors(7);
		sprintf(cname,"gYieldNear_%i",icent);
		gYieldNear[icent]->SetName(cname);

		/*// This could be useful in the future
		gWidthNearErr[icent]=new TGraphAsymmErrors(7);
		sprintf(cname,"gWidthNearErr_%i",icent);
		gWidthNearErr[icent]->SetName(cname);*/

		// Define canvases for JH
		sprintf(cname,"cJH_%i",icent);
		cJH[icent]=new TCanvas(cname,cname,1200,600);
		cJH[icent]->Divide(Npta-1, 3);

		sprintf(cname,"cJHpretty_%i",icent);
		cJHpretty[icent]=new TCanvas(cname,cname,1200,300);
		cJHpretty[icent]->Divide(Npta-1, 1);

		std::cout << "made pretty canvas...start loops" << std::endl;

		int ipad = 1;
		//iptt is ieta now

		if(thnsp)
		{
			// 1 = jetPt
			JHcore->GetAxis(1)->SetRange(JHcore->GetAxis(1)->FindBin(trigbin[0]+delta),JHcore->GetAxis(1)->FindBin(trigbin[1]-delta));

			// Loop over trigger track pt
			for(int iptt = 0; iptt<1; iptt++)
			{
				// 4 = deltaEta
				JHcore->GetAxis(4)->SetRangeUser(etabin[iptt]+delta,etabin[iptt+1]-delta);

				// 2 = trackPt, 5 = deltaPhi
				// Why are these histograms added like this?
				JHdphipt[iptt] = static_cast<TH2 *> (JHcore->Projection(2,5));
				JHdphipt[iptt]->SetName("phiptproj");
				JHcore->GetAxis(4)->SetRangeUser(-1.0*etabin[iptt+1]+delta,-1.0*etabin[iptt]-delta);
				JHdphipt[iptt]->Add(static_cast<TH2 *> (JHcore->Projection(2,5)));

				std::cout << JHdphipt[iptt]->GetNbinsX() << " by " << JHdphipt[iptt]->GetNbinsY() << std::endl; 
				std::cout << fHistJetH_sum[icent][iptt]->GetNbinsX() << " by " << fHistJetH_sum[icent][iptt]->GetNbinsY() << std::endl; 

				// The commented code here attempted two different techniques to divide JHphipt by fHistJetH_sum
				// However, both appear to be only commented out, rather than actually applied
				//JHdphipt[iptt]->Divide(fHistJetH_sum[icent][iptt]);
				/*
				for(int ix = 1; ix<65; ix++)
				{
					for(int iy = 1; iy<101; iy++)
					{
						//int ibin = JHdphipt[iptt]->FindBin(ix,iy);
						double newbc = JHdphipt[iptt]->GetBinContent(ix,iy)/fHistJetH_sum[icent][iptt]->GetBinContent(ix,iy);
						if(iy<30)std::cout << "Dividing: " << JHdphipt[iptt]->GetBinContent(ix,iy) << " by: " << fHistJetH_sum[icent][iptt]->GetBinContent(ix,iy) << std::endl;
						JHdphipt[iptt]->SetBinContent(ix,iy,newbc);
					}
				}*/
				 
				// Create canvas and draw histogram
				// However, nothing is done with the canvas...
				/*TCanvas *c2d = new TCanvas("c2d","c2d",600,600);
				JHdphipt[iptt]->Draw("colz");*/
			}
		}

		// Define variables to hold the yields and yield errors
		double yieldfar = 0.0;
		double yieldnear = 0.0;
		double yieldfarerr = 0.0;
		double yieldnearerr = 0.0;

		// Why start at 1?
		for(int ipta = 1; ipta<Npta; ipta++)
		{
			//std::cout << ipta << " bin2: " << JHdphi[icent][2][ipta]->GetBinContent(2) << std::endl;
			if(thnsp)
			{
				// 4 = deltaEta
				JHcore->GetAxis(4)->SetRangeUser(-1.6,1.6);

				// 2 = trackPt
				JHcore->GetAxis(2)->SetRangeUser(bghadbin[ipta]+deltab,bghadbin[ipta+1]-delta);

				// 4 = deltaEta, 5 = deltaPhi
				JHdphideta[icent][ipta] = static_cast<TH2 *> (JHcore->Projection(4,5));
				sprintf(ptname,"JH2d_pt_%i_%i_c%i_%i",hadbin[ipta]+1,hadbin[ipta+1],centbin[icent],centbin[icent+1]);
				JHdphideta[icent][ipta]->SetName(ptname);

				// Normalize and apply event mixing
				if (evtmix)
				{
					JHmix2D[ipta]->Scale(1.0/(JHmix2D[ipta]->GetBinContent(JHmix2D[ipta]->GetXaxis()->FindBin(0.0),JHmix2D[ipta]->GetYaxis()->FindBin(0.0))));
					JHdphideta[icent][ipta]->Divide(JHmix2D[ipta]);
				}

				// Scale by ntrig
				JHdphideta[icent][ipta]->Scale(1.0/ntrigs);
				if (ipta == 1)
				{
					// Create canvas and save JHdeltaPhiDeltaEta 
					TCanvas *c2detaphi = new TCanvas("c2detaphi","c2detaphi",600,600);
					JHdphideta[icent][ipta]->Draw("surf1");
					sprintf(ptname,"detadphi_%i_assoc%i_mix%i.pdf",runno,ipta,evtmix);

					c2detaphi->SaveAs(ptname);
				}
			}

			// Count ippt down (Why?)
			// Perhaps the ipad imcrement should be done in the for loop declaration
			for(int iptt = 2; iptt>-1; iptt--)
			{
				if (thnsp) { JHcore->GetAxis(4)->SetRangeUser(etabin[iptt]+delta,etabin[iptt+1]-delta); }

				std::cout << "icent: " << icent << "\tiptt: " << iptt << "\tipta: " << ipta << std::endl;
				sprintf(ptname,"JH_%i_pt_%i_%i_c%i_%i",iptt,hadbin[ipta]+1,hadbin[ipta+1],centbin[icent],centbin[icent+1]);
				std::cout << "before projection" << std::endl;

				if (icent == 0)
				{
					if (thnsp)
					{
						// 5 = deltaPhi
						JHdphi[icent][iptt][ipta] = static_cast<TH1 *> (JHcore->Projection(5));
						JHdphi[icent][iptt][ipta]->SetName(ptname);
						// 4 = deltaEta
						JHcore->GetAxis(4)->SetRangeUser(-1.0*etabin[iptt+1]+delta,-1.0*etabin[iptt]-delta);
						JHdphi[icent][iptt][ipta]->Add((TH1*)JHcore->Projection(5));
					}
					else
					{
						// I will have JHcore, so this is not particularly relevant
						std::cout << "projection pta bins: " << hadbin[ipta]+1  << " " << hadbin[ipta]+1<< std::endl;
						JHdphi[icent][iptt][ipta] = static_cast<TH1 *> (fHistJetH_sum[icent][iptt]->ProjectionX(ptname,hadbin[ipta]+1,hadbin[ipta+1]));
					}
				}
				else 
				{
					if (icent == 1)
					{
						// When icent is 1, why is fHistJetH_sum selecting cent of 4?
						JHdphi[icent][iptt][ipta] = static_cast<TH1 *> (fHistJetH_sum[4][iptt]->ProjectionX(ptname,hadbin[ipta]+1,hadbin[ipta+1]));
					}
				}

				// Set the name defined above
				JHdphi[icent][iptt][ipta]->SetName(ptname);

				// Divide by event mixing
				if(evtmix)
				{
					JHdphi[icent][iptt][ipta]->Divide(JHmix[iptt][ipta]);
				}

				// Normalize by ntrig and binwidth
				float bw = JHdphi[icent][iptt][ipta]->GetBinWidth(1);
				if(ntrigs>0 && bw>0) JHdphi[icent][iptt][ipta]->Scale(1./ntrigs/bw);

				else std::cout << "Ntrig " << ntrigs << " binwidth: " << bw << std::endl;

				// Set histogram properties
				JHdphi[icent][iptt][ipta]->GetXaxis()->SetTitle("#Delta#phi");
				JHdphi[icent][iptt][ipta]->GetYaxis()->SetTitle("dN/N_{trig}d#Delta#phi");
				JHdphi[icent][iptt][ipta]->GetXaxis()->SetLabelSize(0.08);
				JHdphi[icent][iptt][ipta]->GetXaxis()->SetTitleSize(0.09);
				JHdphi[icent][iptt][ipta]->GetYaxis()->SetLabelSize(0.075);
				JHdphi[icent][iptt][ipta]->GetYaxis()->SetTitleSize(0.08);
				JHdphi[icent][iptt][ipta]->GetYaxis()->SetTitleOffset(1.2);
				JHdphi[icent][iptt][ipta]->GetXaxis()->SetTitleOffset(0.8);
				JHdphi[icent][iptt][ipta]->GetXaxis()->SetLabelOffset(0.002);

				// Linear index for canvas
				ipad = iptt*(Npta-1)+(ipta);
				std::cout << " draw pad:  " << ipad << std::endl;

				// Move to the correct canvas and set properties
				cJH[icent]->cd(ipad);
				cJH[icent]->GetPad(ipad)->SetLeftMargin(0.2);
				cJH[icent]->GetPad(ipad)->SetBottomMargin(0.15);
				cJH[icent]->GetPad(ipad)->SetTopMargin(0.01);

				if (iptt == 0)
				{
					if(dosubtract) { JHdphi[icent][iptt][ipta]->Add(JHdphi[icent][2][ipta],-1.0); }
					std::cout << " = " << JHdphi[icent][iptt][ipta]->GetBinContent(2) << std::endl;
				}

				std::cout << "Drawing: " << ptname << std::endl;

				JHdphi[icent][iptt][ipta]->SetMarkerStyle(8);
				JHdphi[icent][iptt][ipta]->DrawCopy();

				//Do Fits
				if(dofits)
				{
					TF1 *sig = new TF1("fitsig","[0]*exp(-0.5*((x-[1])/[2])**2)+[3]+[4]*exp(-0.5*((x-[5])/[6])**2)+[0]*exp(-0.5*((x-[1]+2.*TMath::Pi())/[2])**2)+[4]*exp(-0.5*((x-[5]-2.*TMath::Pi())/[6])**2)",-0.5*TMath::Pi(),1.5*TMath::Pi());

					// Setup parameters and fit to hist
					// I'm not certain what a number of these parameters are. However, it should become clear under close inspection.
					sig->SetParLimits(0,0.,100); 		//po
					sig->FixParameter(1,0); 			//<x> 
					sig->SetParLimits(2,0.05,2.); 		//sigma 
					sig->SetParameter(2,0.05);  

					sig->SetParLimits(3,0.,100); 		//pedestal 

					sig->SetParLimits(4,0.,100); 		//po 
					sig->FixParameter(5,TMath::Pi()); 	//<x> 
					sig->SetParLimits(6,0.05,2.); 		//sigma
					sig->SetParameter(6,0.05);  

					sig->SetLineColor(4);
					sig->SetLineStyle(1);

					JHdphi[icent][iptt][ipta]->Fit(sig,"RIB");

					std::cout << ipta << "   ******************  Chi2 of Fit: " << sig->GetChisquare() << "\tDOF: " << (JHdphi[icent][iptt][ipta]->GetNbinsX()-3)<< std::endl;

					// This is for flow?
					// I hope this is never run with pp
					if (!dosubtract && iptt == 0)
					{
						std::cout << __PRETTY_FUNCTION__ << " " << __LINE__ << ": Flow section is running. This assumes an invalid fit function, and shsould not be run!" << std::endl;
						sprintf(ptname,"FLOW_%i_pt_%i_%i",iptt,hadbin[ipta]+1,hadbin[ipta+1]);

						flow[ipta] = static_cast<TH1 *> (JHdphi[icent][iptt][ipta]->Clone());
						flow[ipta]->Clear();
						flow[ipta]->SetName(ptname);

						// There is no parameter 7 defined above ...
						for(int idphi = 1; idphi < flow[ipta]->GetNbinsX()+1; idphi++)
						{
							flow[ipta]->SetBinContent(idphi,sig->GetParameter(7));
							flow[ipta]->SetBinError(idphi,sig->GetParError(7));
						}
						JHdphi[icent][iptt][ipta]->Add(flow[ipta],-1.0);

						for(int idphi = 1; idphi<flow[ipta]->GetNbinsX()+1; idphi++)
						{
							flow[ipta]->SetBinContent(idphi,0.0);
						}
					}

					if (iptt == 0)
					{
						// Rebinning -> (Why?)
						JHdphi[icent][iptt][ipta]->Rebin(RB);
						JHdphi[icent][iptt][ipta]->Scale(RBsc);

						if (ipta < Nptaref)
						{
							JHpp[ipta]->Rebin(RB);
							JHpp[ipta]->Scale(RBsc);
						}

						// Fit to the function defined above. It seems like this could be problem wrt calling the fit twice...
						JHdphi[icent][iptt][ipta]->Fit(sig,"R");

						std::cout << ipta << "   ******************  Chi2 of Fit: " << sig->GetChisquare() << "  DOF: " << (JHdphi[icent][iptt][ipta]->GetNbinsX()-3)<< std::endl;

						double yieldfarerr2 = 0;
						double yieldnearerr2 = 0;
						yieldfar = 0.0;
						yieldnear = 0.0;

						// Calculate yields
						for (int idphi = 1; idphi < 1+JHdphi[icent][iptt][ipta]->GetNbinsX(); idphi++)
						{
							if (idphi < JHdphi[icent][iptt][ipta]->GetNbinsX()/2.+1)
							{
								yieldnear = yieldnear+JHdphi[icent][iptt][ipta]->GetBinContent(idphi);
								yieldnearerr2 = yieldnearerr2+JHdphi[icent][iptt][ipta]->GetBinError(idphi)*JHdphi[icent][iptt][ipta]->GetBinError(idphi);
							}
							else
							{
								std::cout << "far dphi: " << JHdphi[icent][iptt][ipta]->GetBinCenter(idphi) << "\ttot yield: " << yieldfar << std::endl;
								yieldfar = yieldfar+JHdphi[icent][iptt][ipta]->GetBinContent(idphi);
								yieldfarerr2 = yieldfarerr2 + JHdphi[icent][iptt][ipta]->GetBinError(idphi)*JHdphi[icent][iptt][ipta]->GetBinError(idphi);
								std::cout << "far dphi: " << JHdphi[icent][iptt][ipta]->GetBinCenter(idphi) << "\ttot yield: " << yieldfar << std::endl;
							}
						}

						// Calculate yield errors
						yieldfarerr = sqrt(yieldfarerr2)*JHdphi[icent][iptt][ipta]->GetBinWidth(2)/dpt[ipta];
						yieldnearerr = sqrt(yieldnearerr2)*JHdphi[icent][iptt][ipta]->GetBinWidth(2)/dpt[ipta];

						// Calculate near and far yield
						yieldfar = yieldfar*JHdphi[icent][iptt][ipta]->GetBinWidth(2)/dpt[ipta];
						yieldnear = yieldnear*JHdphi[icent][iptt][ipta]->GetBinWidth(2)/dpt[ipta];

						// Rebin -> (Why?)
						JHdphi[icent][iptt][ipta]->Rebin(RB);
						JHdphi[icent][iptt][ipta]->Scale(RBsc);
						if (ipta < Nptaref)
						{
							JHpp[ipta]->Rebin(RB);
							JHpp[ipta]->Scale(RBsc);
						}

						double pi = TMath::Pi();

						// I have no idea why there is RRRRRRRRRRRRRRRRR...
						std::cout << "RRRRRRRRRRRRRRRRRRRRRRRRRRRR  " << sig->Eval(-1.0*pi/2.) << " " << sig->Eval(3*pi/2.) << std::endl;

						std::cout << "iptt: " << iptt << "\tipta: " << ipta << " YIELD: " << yieldfar << std::endl;
						std::cout << "iptt: " << iptt << "\tipta: " << ipta << " YIELD: " << sig->Integral(pi-pi/2.,pi+pi/2.)<< std::endl;

						// Set canvas properties
						std::cout << "before get pad" << std::endl;	  
						cJHpretty[icent]->cd(ipta);
						cJHpretty[icent]->GetPad(ipta)->SetLeftMargin(0.2);
						cJHpretty[icent]->GetPad(ipta)->SetBottomMargin(0.15);
						cJHpretty[icent]->GetPad(ipta)->SetTopMargin(0.01);
						cJHpretty[icent]->GetPad(ipta)->SetRightMargin(0.01);

						std::cout << "drawing dphi" << std::endl;

						JHdphi[icent][iptt][ipta]->Draw();

						std::cout << "drawn dphi" << std::endl;

						if (ipta < Nptaref) { JHpp[ipta]->SetMarkerStyle(4); }
						flow[ipta]->SetMarkerStyle(0);
						flow[ipta]->SetFillColor(kGray);

						// Show flow always be drawn?
						std::cout << "drawing flow" << std::endl;

						if(ipta < Nptaref) { JHpp[ipta]->Draw("same"); }
						flow[ipta]->Draw("E2,same");
						JHdphi[icent][iptt][ipta]->Draw("same");

						std::cout << "drawing legends" << std::endl;

						// Legend
						legJH[ipta] = new TLegend(0.48,0.62,0.90,0.95);
						legJH[ipta]->SetBorderSize(0);
						legJH[ipta]->SetFillColor(0);

						legJH[ipta]->AddEntry("", "20<p_{T}^{jet}<60 GeV/c", "");
						if(ipta==1)legJH[ipta]->AddEntry("", "0.5<p_{T}^{assoc}<1 GeV/c", "");
						if(ipta==2)legJH[ipta]->AddEntry("", "1<p_{T}^{assoc}<2 GeV/c", "");
						if(ipta==3)legJH[ipta]->AddEntry("", "2<p_{T}^{assoc}<3 GeV/c", "");
						if(ipta==4)legJH[ipta]->AddEntry("", "3<p_{Th}<5", "");
						if(ipta==5)legJH[ipta]->AddEntry("", "5<p_{Th}<8", "");
						if(ipta==6)legJH[ipta]->AddEntry("", "8<p_{Th}<20", "");

						legJH[ipta]->Draw();

						std::cout << "end if iptt==0 loop " << std::endl;
					}

					// Get parameters from fit above
					sigmanear[icent][ipta] = sig->GetParameter(2);
					sigmafar[icent][ipta] = sig->GetParameter(6);

					// Add values into the graph of widths
					// Why is the NearErr uniquely called? (and the farside is not?)
					gWidthFar[icent]->SetPoint(ipta,hadpt[ipta],sigmafar[icent][ipta]);
					gWidthFar[icent]->SetPointError(ipta,0.0,sig->GetParError(6));

					gWidthNear[icent]->SetPoint(ipta,hadpt[ipta],sigmanear[icent][ipta]);
					gWidthNearErr->SetPoint(ipta,hadpt[ipta],sigmanear[icent][ipta]);
					gWidthNear[icent]->SetPointError(ipta,0.0,sig->GetParError(2));

					gYieldFar[icent]->SetPoint(ipta,hadpt[ipta],yieldfar);
					gYieldFar[icent]->SetPointError(ipta,0.0,yieldfarerr);

					gYieldNear[icent]->SetPoint(ipta,hadpt[ipta],yieldnear);
					gYieldNear[icent]->SetPointError(ipta,0.0,yieldnearerr);
				}

				// Increment linear index
				ipad++;
			}
		}

		sprintf(ptname,"JH%i_%iFull_trig_%i_%i_HD%i_ch%i.pdf",icent,runno,mintrig,maxtrig,findex,charge);
		if (saveall) { cJH[icent]->SaveAs(ptname); }

		sprintf(ptname,"niceJH%i_%iFull_trig_%i_%i_HBin%i_ch%i.pdf",icent,runno,mintrig,maxtrig,hardBin,charge);
		if (saveall) { cJHpretty[icent]->SaveAs(ptname); }

		// Draw far side widths
		cWidthFar->cd();
		gWidthFar[icent]->SetMarkerStyle(8);
		gWidthFar[icent]->SetMarkerColor(icent+1);
		gWidthFar[icent]->SetLineColor(icent+1);
		gWidthFar[icent]->GetYaxis()->SetTitle("Awayside #sigma ");
		gWidthFar[icent]->GetXaxis()->SetTitle("p_{T}^{assoc} GeV/c");
		gWidthFar[icent]->GetYaxis()->SetRangeUser(0,0.75);
		if(icent==0) { gWidthFar[icent]->Draw("ap"); } 
		else { gWidthFar[icent]->Draw("p,same"); }
		gppwidthfar->Draw("p,same");

		// Draw near side widths
		cWidthNear->cd();
		cWidthNear->SetRightMargin(0.05);
		gWidthNear[icent]->SetMarkerStyle(8);
		gWidthNear[icent]->SetMarkerColor(icent+1);
		gWidthNear[icent]->SetLineColor(icent+1);
		gWidthNear[icent]->GetYaxis()->SetTitle("Nearside #sigma");
		gWidthNear[icent]->GetYaxis()->SetTitleOffset(1.6);
		gWidthNear[icent]->GetXaxis()->SetTitle("p_{T}^{assoc} GeV/c");
		gWidthNear[icent]->GetYaxis()->SetRangeUser(0,0.5);
		gWidthNear[icent]->GetXaxis()->SetRangeUser(0.5,3.0);
		if (icent==0) { gWidthNear[icent]->Draw("ap"); }
		else { gWidthNear[icent]->Draw("p,same"); }
		// Errors
		gWidthNearErr->SetFillStyle(0);
		gWidthNearErr->Draw("E2,same");
		gppwidthnearErr->SetLineColor(2);
		gppwidthnearErr->SetFillStyle(0);
		gppwidthnearErr->Draw("E2,same");
		gppwidthnear->Draw("p,same");

		// Plot Yields
		// Far side
		cYieldFar->cd();
		gYieldFar[icent]->SetMarkerStyle(8);
		gYieldFar[icent]->SetMarkerColor(icent+1);
		gYieldFar[icent]->SetLineColor(icent+1);
		gYieldFar[icent]->GetYaxis()->SetTitle("Awayside Yield ");
		gYieldFar[icent]->GetXaxis()->SetTitle("p_{T}^{assoc} GeV/c");
		gYieldFar[icent]->GetYaxis()->SetRangeUser(0,1.5);
		if (icent == 0) { gYieldFar[icent]->Draw("ap"); }
		else { gYieldFar[icent]->Draw("p,same"); }

		// Near side
		cYieldNear->cd();
		cYieldNear->SetRightMargin(0.05);
		gYieldNear[icent]->SetMarkerStyle(8);
		gYieldNear[icent]->SetMarkerColor(icent+1);
		gYieldNear[icent]->SetLineColor(icent+1);
		gYieldNear[icent]->GetYaxis()->SetTitle("Nearside Yield");
		gYieldNear[icent]->GetYaxis()->SetTitleOffset(3.);
		gYieldNear[icent]->GetXaxis()->SetTitle("p_{T}^{assoc} GeV/c");
		gYieldNear[icent]->GetYaxis()->SetRangeUser(0,1.8);
		gYieldNear[icent]->GetXaxis()->SetRangeUser(0.5,3.0);
		if(icent==0) gYieldNear[icent]->Draw("ap");
		else gYieldNear[icent]->Draw("p,same");
	}

	// Legend
	TLegend *myLegend = new TLegend(0.4,0.6,0.89,0.85);
	myLegendSetUp(myLegend,0.04);
	myLegend->AddEntry(ppJetPt, "Charge+Neutral Jets", "");
	myLegend->AddEntry(ppJetPt, "6 GeV/c particle bias", "");
	myLegend->AddEntry(JetPt[0], "pp subtracted", "p");
	myLegend->AddEntry(ppJetPt, "pp", "p");
	myLegend->AddEntry(ppJetPt, "R=0.4 Anti-k_{t}", "");
	myLegend->AddEntry(ppJetPt, "p_{t}^{track}, E_{T}^{cluster} > 3 GeV", "");
	myLegend->Draw();

	// Save canvases if desired
	if (saveall)
	{
		sprintf(ptname,"awaywidth_%i_trig_%i_%i_HD%i.pdf",runno,mintrig,maxtrig,hardBin);
		cWidthFar->SaveAs(ptname);

		sprintf(ptname,"nearwidth_%i_trig_%i_%i_HD%i.pdf",runno,mintrig,maxtrig,hardBin);
		cWidthNear->SaveAs(ptname);
	}

	// As it currently (7/8/2013) stands, everything will always be saved
	int saveroot = 1;
	if (saveroot)
	{
		std::cout << "save to root file " << std::endl;

		// I don't think this filename is still correct...
		if(pp) sprintf(filename,"pp_3GeV_%i_trig_%i_%i_sp%i_mix%i_sub%i_ch%i_contam%i.root",runno,mintrig,maxtrig,thnsp,evtmix,dosubtract,charge,contam);
		else sprintf(filename,"NSoutTHNsp%i_%i.root",thnsp,runno);

		TFile fout(filename,"recreate");

		std::cout << "creating " << filename << std::endl;

		for(int icent = 0; icent<1; icent++)
		{
			JetPt[icent]->Write();
			TrackPt[icent]->Write();
			gWidthNear[icent]->Write();
			gWidthFar[icent]->Write();
			for(int ipta = 1; ipta<Npta; ipta++)
			{
				// This seems like it could create a bug. At the very least, it is inefficient
				flow[ipta]->Write();
				for(int ieta = 0; ieta<3; ieta++)
				{
					JHdphi[icent][ieta][ipta]->Write();
				}
			}
		}

		std::cout << "Wrote histos to " << filename << std::endl;

		fout.Close();
		std::cout << "closing " << filename << std::endl;
	}

	// Close all(?) files
	finpp->Close();  
	finppup->Close();  
	finppdown->Close();
	fin->Close();
	std::cout << "ALL DONE!" << std::endl;

}
