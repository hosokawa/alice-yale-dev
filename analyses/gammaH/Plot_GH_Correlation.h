//- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
//- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
//- - - - - - - Here are functions that get the historgams from the files - - - - - - - - - - - - - -
//- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
//- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
TH1D* Get1DHistoFromFile(TFile* RootFile,TString Name)
{
	TString TListName;
	TListName="AliAnalysisTaskGammaHadron_EMCEGA_PicoTracks_CaloClustersCorr_histosgrams";
	//TListName="AliAnalysisTaskGammaHadron_HybridTracks_CaloClustersCorr_histosgrams";

	TList* lub    =(TList*)RootFile->Get(TListName);
	TH1D* Histo   =(TH1D*)lub->FindObject(Name);

	return Histo;
}
//- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
TH2D* Get2DHistoFromFile(TFile* RootFile,TString SubListName,TString Name,Bool_t SameMix)
{
	TString TListName;
	if(SameMix==0)TListName="AliAnalysisTask_GH_ME_tracks_caloClusters_histos";     //same event
	if(SameMix==1)TListName="AliAnalysisTaskPi0Hadron_PicoTracks_CaloClustersCorr_histosgrams";     //same event
//	if(SameMix==0)TListName="AliAnalysisTaskGH_PicoTracks_CaloClustersCorr_histosgrams";     //same event
//    if(SameMix==0)TListName="AliAnalysisTaskGammaHadron_EMCEGA_PicoTracks_CaloClustersCorr_histosgrams";
//    if(SameMix==1)TListName="AliAnalysisTaskGammaHadron_INT7_PicoTracks_CaloClustersCorr_histosgrams";
	//TListName="AliAnalysisTaskGammaHadron_HybridTracks_CaloClustersCorr_histosgrams";
	TList* IntermediatList;
	TList* FinalList;

	if(SubListName=="")
	{
		FinalList    =(TList*)RootFile->Get(TListName);
	}
	else
	{
		IntermediatList=(TList*)RootFile       ->Get(TListName);
		FinalList      =(TList*)IntermediatList->FindObject(SubListName);
	}
	TH2D* Histo   =(TH2D*)FinalList->FindObject(Name);

	return Histo;
}
//- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
//- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
//- - - - - - - Here are functions that make the plotting of figures nicer- - - - - - - - - - - - - -
//- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
//- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
void set_plot_style()
{
    const Int_t NRGBs = 5;
    const Int_t NCont = 99;//max possible?

    //Double_t stops[NRGBs] = { 0.00, 0.34, 0.61, 0.84, 1.00 };
    Double_t stops[NRGBs] = { 0.00, 0.25, 0.5, 0.75, 1.00 };
    Double_t red[NRGBs]   = { 0.00, 0.00, 0.87, 1.00, 0.51 };
    Double_t green[NRGBs] = { 0.00, 0.81, 1.00, 0.20, 0.00 };
    Double_t blue[NRGBs]  = { 0.51, 1.00, 0.12, 0.00, 0.00 };
    TColor::CreateGradientColorTable(NRGBs, stops, red, green, blue, NCont);
    gStyle->SetNumberContours(NCont);
}
//- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
void SetTH1Histo(TH1 *Histo,TString Xtitel,TString Ytitel)
{
	Histo->SetStats(0);
	Histo->SetTitle("");
	Histo->GetYaxis()->SetTitleOffset(1.4);
	Histo->GetXaxis()->SetTitleOffset(1.4);
	Histo->GetXaxis()->SetLabelSize(0.05);
	Histo->GetYaxis()->SetLabelSize(0.05);
	Histo->GetXaxis()->SetTitleSize(0.045);
	Histo->GetYaxis()->SetTitleSize(0.045);
	Histo->GetXaxis()->CenterTitle();
	Histo->GetYaxis()->CenterTitle();
	Histo->GetXaxis()->SetNdivisions(505);
	Histo->GetYaxis()->SetNdivisions(505);
	//make nice font
    Histo->GetXaxis()->SetLabelFont(42);
    Histo->GetYaxis()->SetLabelFont(42);
    Histo->GetXaxis()->SetTitleFont(42);
    Histo->GetYaxis()->SetTitleFont(42);
	if(Xtitel!="")Histo->GetXaxis()->SetTitle(Xtitel);
	if(Ytitel!="")Histo->GetYaxis()->SetTitle(Ytitel);

	Histo->SetLineColor(1);
	Histo->SetMarkerColor(1);
	Histo->SetMarkerStyle(20);
	Histo->SetMarkerSize(0.5);

}
//- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
void SetTH2Histo(TH2 *Histo,TString Xtitel,TString Ytitel)
{
	Histo->SetStats(0);
	Histo->SetTitle("");
	Histo->GetYaxis()->SetTitleOffset(1.7);
	Histo->GetXaxis()->SetTitleOffset(1.7);
	Histo->GetXaxis()->SetLabelSize(0.05);
	Histo->GetYaxis()->SetLabelSize(0.05);
	Histo->GetXaxis()->SetTitleSize(0.045);
	Histo->GetYaxis()->SetTitleSize(0.045);
	Histo->GetXaxis()->CenterTitle();
	Histo->GetYaxis()->CenterTitle();
	Histo->GetXaxis()->SetNdivisions(505);
	Histo->GetYaxis()->SetNdivisions(505);
	//make nice font
    Histo->GetXaxis()->SetLabelFont(42);
    Histo->GetYaxis()->SetLabelFont(42);
    Histo->GetXaxis()->SetTitleFont(42);
    Histo->GetYaxis()->SetTitleFont(42);
	if(Xtitel!="")Histo->GetXaxis()->SetTitle(Xtitel);
	if(Ytitel!="")Histo->GetYaxis()->SetTitle(Ytitel);
	Histo->SetLineColorAlpha(kBlue+2,0.095);
//	Histo->GetYaxis()->SetRangeUser(-1.3,1.3);

}
//- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
TLatex* plotTopLegend(char* label,Float_t x=-1,Float_t y=-1,Float_t size=0.06,Int_t color=1,Float_t angle=0.0)
{
	// coordinates in NDC!
	// plots the string label in position x and y in NDC coordinates
	// size is the text size
	// color is the text color

	if(x<0||y<0)
	{   // defaults
		x=gPad->GetLeftMargin()*1.15;
		y=(1-gPad->GetTopMargin())*1.04;
	}
	TLatex* text=new TLatex(x,y,label);
	text->SetTextSize(size);
	text->SetNDC();
	text->SetTextColor(color);
	text->SetTextAngle(angle);
	text->Draw();
	return text;
}
//- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
void Draw_Legend(TH1* File1Histo,TString File1HistoName,TH1* File2Histo,TString File2HistoName)
{
	TLegend *leg1=new TLegend(0.35,0.65,0.80,0.88);
	leg1->SetBorderSize(0);
	leg1->SetFillColor(0);
	leg1->AddEntry(File1Histo,File1HistoName,"L");
	leg1->AddEntry(File2Histo,File2HistoName,"L");
	leg1->Draw();
}
//- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
void Draw_Legend_V2(TH1* File1Histo,TString HistoName1,TH1* File2Histo,TString HistoName2,TH1* File3Histo,TString HistoName3,TH1* File4Histo,TString HistoName4)
{
	TLegend *leg1=new TLegend(0.35,0.78,0.85,0.88);
	leg1->SetBorderSize(0);
	leg1->SetTextSize(0.025);
	leg1->SetFillColor(0);
	leg1->AddEntry(File1Histo,HistoName1,"LP");
	leg1->AddEntry(File2Histo,HistoName2,"LP");
	leg1->AddEntry(File3Histo,HistoName3,"LP");
	leg1->AddEntry(File4Histo,HistoName4,"LP");
	leg1->Draw();
}
//-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-..-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-
Double_t PolyTwoGaussFitFunc(Double_t* x_val, Double_t* par)
{
    Double_t x, y, par0, par1, par2, par3, par4, par5, par6, par7, par8, par9, par10,CommomMean;
    par0  = par[0]; //amplitude gauss 1
    par1  = par[1]; //mean gauss 1
    par2  = par[2]; //width gauss 1
    //the second gaus is smaller in amplitude and larger in width
    par3  = par[3]*par[0]; //amplitude gauss 2 (parameter3 ranges from 0-1)
    par4  = par[4]; //mean gauss 2
    par5  = par[5]*par[2]; //width gauss 2 (parameter5 is larger than 1)
    par6  = par[6]; //a
    par7  = par[7]; //b x^1
    par8  = par[8]; //c x^2
    par9  = par[9]; //d x^3
    par10 = par[10];//e x^4
    x = x_val[0];

    //Do that so that the mean of the two gaussians are the same
    CommomMean=(par1+par4)*0.5;

 //   cout<<"current p0: "<<par0<<", current p3: "<<par3<<endl;

    y = par0*(TMath::Gaus(x,CommomMean,par2,0))+par3*(TMath::Gaus(x,CommomMean,par5,0))+par6;//+(par6+par7*x+par8*x*x+par9*x*x*x+par10*x*x*x*x);
    return y;
}
//-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-..-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-
Double_t FlowFunction(Double_t* x_val, Double_t* par)
{
    Double_t x, y, par0, par1, par2, par3;
    par0  = par[0]; //Background level
    par1  = par[1]; //
    par2  = par[2]; //
    par3  = par[3]; //

    x = x_val[0]*TMath::Pi()/180.0;  //transform from deg to rad

    y = par0*(1+2*par1*TMath::Cos(x)+2*par2*TMath::Cos(2*x)+2*par3*TMath::Cos(3*x));
    return y;
}
//- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
void PlotHorLine(Double_t x1_val, Double_t x2_val, Double_t y_val, Int_t Line_Col)
{
    TLine* Zero_line = new TLine();
    Zero_line -> SetX1(x1_val);
    Zero_line -> SetX2(x2_val);
    Zero_line -> SetY1(y_val);
    Zero_line -> SetY2(y_val);
    Zero_line -> SetLineWidth(2);
    Zero_line -> SetLineStyle(2);
    Zero_line -> SetLineColor(Line_Col);
    Zero_line -> Draw();
    //delete Zero_line;
}
//- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
void PlotVerLine(Double_t x_val, Double_t y_val_low, TH1* Histo, Double_t y_fac, Int_t Line_Col)
{
    TLine* Zero_line = new TLine();
    Zero_line -> SetX1(x_val);
    Zero_line -> SetX2(x_val);
    Zero_line -> SetY1(y_val_low);
    Zero_line -> SetY2(y_fac*Histo->GetBinContent(Histo->FindBin(x_val)));
    //cout << "x_val = " << x_val << ", Bin = " << Histo->FindBin(x_val) << ", Y2 = " << Histo->GetBinContent(Histo->FindBin(x_val)) << endl;
    Zero_line -> SetLineWidth(2);
    Zero_line -> SetLineStyle(1);
    Zero_line -> SetLineColor(Line_Col);
    Zero_line -> Draw();
    //delete Zero_line;
}
//- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
void PlotVerLine2(Double_t x_val, Double_t y_val_low, Double_t y_val_high, Int_t Line_Col)
{
    TLine* Zero_line = new TLine();
    Zero_line -> SetX1(x_val);
    Zero_line -> SetX2(x_val);
    Zero_line -> SetY1(y_val_low);
    Zero_line -> SetY2(y_val_high);
    //cout << "x_val = " << x_val << ", Bin = " << Histo->FindBin(x_val) << ", Y2 = " << Histo->GetBinContent(Histo->FindBin(x_val)) << endl;
    Zero_line -> SetLineWidth(2);
    Zero_line -> SetLineStyle(1);
    Zero_line -> SetLineColor(Line_Col);
    Zero_line -> Draw();
    //delete Zero_line;
}
//- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
//- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
//- - - - - - - Here are functions that take the histograms and modify them - - - - - - - - - - - - -
//- - - - - - - In order to obtain new prepresenations with clearer information - - - - - - - - - - -
//- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
//- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
Double_t FitGaussAndDraw(TH1D* Hist,TF1* Func1, TF1* Func2,TF1* Func3,Bool_t EtaPhi)
{
	Double_t Width;

	for(Int_t g = 0; g < 11; g++)
	{
		GaussFunc->ReleaseParameter(g);
		GaussFunc->SetParameter(g,0);
		GaussFunc->SetParError(g,0.0);
	}
	GaussFunc->SetParameter(0,30);
	GaussFunc->SetParameter(1,0);

	GaussFunc->SetParameter(3,0.5);
	GaussFunc->SetParameter(4,0);

	if(EtaPhi==0)//Eta case
	{
		GaussFunc->SetParameter(2,0.3);

		//big, narrow gaussian
		GaussFunc->SetParLimits(0,1,5);      //amplitude
		GaussFunc->SetParLimits(1,-0.1,0.1);  //mean limits
		GaussFunc->SetParLimits(2,0.05,0.5);  //width limits

		//small, wide gaussian
		GaussFunc->SetParameter(5,1.1);      //let width to be at least 10% larger than width 1
		GaussFunc->SetParLimits(3,0.2,1);    //amplitude limits
		GaussFunc->SetParLimits(4,-0.1,0.1); //mean limits
		GaussFunc->SetParLimits(5,1.1,3.5);  //width limits (width of broad gauss is max 4 times larger than the narrow one)

		//flat background
		GaussFunc->SetParameter(6,4);
		GaussFunc->SetParLimits(6,2,3);

		GaussFunc ->SetRange(-1.5,1.5);
		GaussFunc1->SetRange(-1.5,1.5);
		GaussFunc2->SetRange(-1.5,1.5);
	}

	if(EtaPhi==1)//phi case
	{
		GaussFunc->SetParameter(2,20);

		//big, narrow gaussian
		GaussFunc->SetParLimits(0,5,30);  //amplitude
		GaussFunc->SetParLimits(1,-1,1);  //mean limits
		GaussFunc->SetParLimits(2,1,40);  //width limits

		//small, wide gaussian
		GaussFunc->SetParameter(5,1.1);   //let width to be at least 10% larger than width 1
		GaussFunc->SetParLimits(3,0.2,1);   //amplitude limits
		GaussFunc->SetParLimits(4,-1,1);  //mean limits
		GaussFunc->SetParLimits(5,1.1,3.5); //width limits (width of broad gauss is max 4 times larger than the narrow one)

		//flat background
		GaussFunc->SetParameter(6,24.5);
		GaussFunc->SetParLimits(6,10,40);

		GaussFunc->SetRange(-100,200);
		GaussFunc1->SetRange(-50,50);
		GaussFunc2->SetRange(-50,50);
	}

	GaussFunc->SetLineColor(15);
	GaussFunc->SetLineStyle(2);

	TString Name= Func1->GetName();
	if(EtaPhi==0)Hist->Fit(Name,"Q","",-1,1);//Q = quiet mode, no printout
	if(EtaPhi==1)Hist->Fit(Name,"Q","",-70,70);//Q = quiet mode, no printout
	//width that is used to define an eta range not contaminated by the near side peak
	Width=GaussFunc->GetParameter(5)*GaussFunc->GetParameter(2);//bigger width in delta phi
  //  cout<<"Width gauss2: "<<GaussFunc->GetParameter(5)<<" times of width 1"<<endl;

	for(Int_t g = 0; g < 11; g++)
	{
		Func2->SetParameter(g,Func1->GetParameter(g));
		Func3->SetParameter(g,Func1->GetParameter(g));
	}
	//small, wide gaussian
	//due to the fact that param 0 and param 3 are proportional
	//we do a little hack here. Setting param0 to 0 is neseccary
	//to see only the small wiede gaussian. If we set param0 to 0
	//however, param3 will become 0 by defualt. We can however
	//set param0 to a negligibly small value x and multiply param3
	//by the inverse of x. (normally param3 is in the range 0-1, but we omit this for this specific case)
	Double_t Shrinkage=0.00001;
	Func2->SetParameter(0,Shrinkage);
	Func2->SetParameter(3,1.0*Func1->GetParameter(3)*Func1->GetParameter(0)/Shrinkage);

    //big, narrow gaussian
	Func3->SetParameter(3,0);
	Func2->SetLineColor(kPink-9);
	Func3->SetLineColor(kGreen-2);
	Func1->SetParameter(0,0);
	Func1->SetParameter(3,0);

	Func1 ->DrawCopy("same");
	Func2 ->DrawCopy("same");
	//big, narrow gaussian (green)
	Func3 ->DrawCopy("same");

	TString TopLegendText;
	Double_t x_Pos;
	if(EtaPhi==0)x_Pos=0.2;//Eta case
	if(EtaPhi==1)x_Pos=0.6;//Phi case
	TopLegendText=Form("#mu_{1}: %0.3f",Func1->GetParameter(1));
	plotTopLegend((char)TopLegendText,x_Pos,0.85,0.035,kGreen-2);
	TopLegendText=Form("#sigma_{1}: %0.2f",Func1->GetParameter(2));
	plotTopLegend((char)TopLegendText,x_Pos,0.80,0.035,kGreen-2);

	TopLegendText=Form("#mu_{2}: %0.3f",Func1->GetParameter(4));
	plotTopLegend((char)TopLegendText,x_Pos,0.75,0.035,kPink-9);
	TopLegendText=Form("#sigma_{2}: %0.2f",Func1->GetParameter(5)*Func1->GetParameter(2));
	plotTopLegend((char)TopLegendText,x_Pos,0.70,0.035,kPink-9);


	return Width;
}
//- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
void DetermineWidths(TH2* Histo,Double_t Array[],TCanvas* Can,Int_t CanvasPad)
{
	//cout<<"inside of DetermineWidths()"<<endl;
	TF1* GaussFunc = new TF1("GaussFunc",PolyTwoGaussFitFunc,-100,300,11);
	TF1* GaussFunc1 = new TF1("GaussFunc1",PolyTwoGaussFitFunc,-100,300,11);
	TF1* GaussFunc2 = new TF1("GaussFunc2",PolyTwoGaussFitFunc,-100,300,11);
	TString ProjectionName;
	//. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .

	Can->cd(CanvasPad*2+1);
	//project to the y-axis. But only the near side.
	ProjectionName= Histo->GetName();
	ProjectionName+="_projY";
	TH1D *PprojY=Histo->ProjectionY((char)ProjectionName,0,Histo->GetYaxis()->FindBin(180));
	SetTH1Histo(PprojY,"","");
	PprojY->DrawCopy("E");
	Array[0] = FitGaussAndDraw(PprojY,GaussFunc,GaussFunc1,GaussFunc2,0);

	Can->cd(CanvasPad*2+2);
	TH1D *projX=Histo->ProjectionX();
	SetTH1Histo(projX,"","");
	projX->DrawCopy("E");
	Array[1] = FitGaussAndDraw(projX,GaussFunc,GaussFunc1,GaussFunc2,1);

	//--the canvas only exists within this void
	//--to keep it for a moment update it and call WaitPrimitive()
	//-- to proceed eg. simply perform a zoom in on the axis with your mouse
}
//- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
TH1D* FitEtaSides(TH2* Histo,Double_t Array[],Int_t Sigmas,TCanvas* Can,Int_t CanvasPad)
{
	TString TopLegendText;
    TString ProjectionName;
    TF1* BackgroundFunction = new TF1("FlowFunction",FlowFunction,-100,300,4);

	//--select the eta range in which you want to project your signal
	Int_t SignalCenter   = Histo->GetYaxis()->FindBin(0);
	Int_t SignalEdgeLow  = Histo->GetYaxis()->FindBin(0-Array[0]*Sigmas);
	Int_t SignalEdgeHigh = Histo->GetYaxis()->FindBin(0+Array[0]*Sigmas);
	Int_t LowestBin      = Histo->GetYaxis()->FindBin(-1.5);
	Int_t HighestBin     = Histo->GetYaxis()->FindBin(1.5);

	//--check that the mean+-sigma is not larger or smaller than the histogram range
	if(SignalEdgeLow<LowestBin || SignalEdgeHigh>HighestBin)
	{
		cout<<"Error: Problem detected!"<<endl;
		cout<<"In Histo: "<<Histo->GetName()<<endl;
		cout<<"Signal range is reaching outside the histogram boundaries - please correct"<<endl;
		cout<<"bins lowes"<<LowestBin<<", edge "<<SignalEdgeLow<<", center"<<SignalCenter <<", uppedge"<< SignalEdgeHigh<<", highestbin "<< HighestBin<<endl;
		SignalEdgeLow=LowestBin;
		SignalEdgeHigh=HighestBin;
	}

    ProjectionName=Histo->GetName();
    ProjectionName+="PprojXSig";
	TH1D *PprojXSig  =Histo->ProjectionX(ProjectionName,SignalEdgeLow,SignalEdgeHigh);
    ProjectionName=Histo->GetName();
    ProjectionName+="PprojXSide1";
	TH1D *PprojXSide1=Histo->ProjectionX(ProjectionName,LowestBin,SignalEdgeLow);
    ProjectionName=Histo->GetName();
    ProjectionName+="PprojXSide2";
    TH1D *PprojXSide2=Histo->ProjectionX(ProjectionName,SignalEdgeHigh,HighestBin);



	//--perform now a projection of the 2Dhistogam in a given range x-sigmas outside the jet region
// /*in case you want to see left and right wing separatley*/ 	Canv->Divide(3);
	Can->cd(CanvasPad*2+1);
	//--project to plot the delta phi distribution in a certain Delta Eta intervall
	SetTH1Histo(PprojXSig,"","");
	PprojXSig->DrawCopy("E");
	TopLegendText=Form("Projection in range: %0.2f < #eta < %0.2f",0-Array[0]*Sigmas,0+Array[0]*Sigmas);
	plotTopLegend((char)TopLegendText,0.3,0.85,0.035);

	Can->cd(CanvasPad*2+2);
	PprojXSide1->Add(PprojXSide2);
	SetTH1Histo(PprojXSide1,"","");
	PprojXSide1->DrawCopy("E");
	TopLegendText=Form("Projection in range: %0.2f < #eta < %0.2f",-1.5,0-Array[0]*Sigmas);
	plotTopLegend((char)TopLegendText,0.35,0.85,0.035);
	TopLegendText=Form("                                    %0.2f < #eta < %0.2f",0+Array[0]*Sigmas,1.5);
	plotTopLegend((char)TopLegendText,0.35,0.81,0.035);

    //fit with the flow function
	for(Int_t g = 0; g < 4; g++)
	{
		BackgroundFunction->ReleaseParameter(g);
		BackgroundFunction->SetParameter(g,0);
		BackgroundFunction->SetParError(g,0.0);
	}
	//flat line
	BackgroundFunction->FixParameter(1,0.0);
	BackgroundFunction->FixParameter(2,0.0);
	BackgroundFunction->FixParameter(3,0.0);
	TString Name= BackgroundFunction->GetName();
	PprojXSide1->Fit(Name,"Q","",-90,90);//Q = quiet mode, no printout
//	PprojXSide1->Fit(Name,"Q","",-100,300);//Q = quiet mode, no printout
	BackgroundFunction->SetRange(-100,300);
	BackgroundFunction->DrawCopy("same");


	/*
	//in case you want to see left and right wing separatley
	Canv->cd(CanvasPad*2+3);
	SetTH1Histo(PprojXSide2,"","");
	PprojXSide2->DrawCopy("E");
	TopLegendText=Form("Projection in range: %0.2f < #eta < %0.2f",0+Array[0]*Sigmas,1.5);
	plotTopLegend((char)TopLegendText,0.35,0.85,0.035);
*/
	ProjectionName = PprojXSide1->GetName();
	ProjectionName+="_backgroundSubtracted";
	TH1D* BackgroundSubtraction = (TH1D*)PprojXSide1->Clone(ProjectionName);
	BackgroundSubtraction->Add(BackgroundFunction,-1);

    return BackgroundSubtraction;
}
//- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
//- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
//- - - - - - - Here are the high end functions that take the histograms, - - - - - - - - - - - - - -
//- - - - - - - integrate them and plot the extracted information - - - - - - - - - - - - - - - - - -
//- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
void ExtractInfo_Y_vs_Egamma(TH1D* Histo,Int_t Bin,TH1D* SummaryHisto[])
{
	Double_t BinWidth = Histo->GetBinWidth(1);
	for(Int_t i=0;i<4;i++)
	{
		//							        		   __
 		//integrate both sides (1,2) of the peak 1_/  \_2
		//Define limits below 180
		Int_t BIN_Start=Histo->FindBin(180-3*BinWidth*(i));
		Int_t BIN_end  =Histo->FindBin(180-3*BinWidth*(i-1));
		//summ bin contents:
		Double_t Yield=0;
        Double_t Error=0;

        //integrate the "full" range (currently 55�-180�)
        if(i==0)BIN_Start=Histo->FindBin(55);
        	if(i==0)BIN_end  =Histo->FindBin(180);

		for(Int_t j=BIN_Start;j<BIN_end;j++)
		{
		  Yield+= Histo->GetBinContent(j);
		  Error+= pow(Histo->GetBinError(j),2);
		  //cout<<"1) Adding bin nr. "<<j<<", with content: "<<Yield<<endl;
		}

		//Define limits above 180
		BIN_Start=Histo->FindBin(180+3*BinWidth*(i-1));
		BIN_end  =Histo->FindBin(180+3*BinWidth*(i));
		if(180+3*BinWidth*(i) >270) cout<<"ExtractInfo_Y_vs_Egamma::Error : End of histogram. Please correct!!"<<endl;

        //integrate the "full" range (currently 55�-180� =125�)
        if(i==0)BIN_Start=Histo->FindBin(180);
        	if(i==0)BIN_end  =Histo->FindBin(270);   //

        	for(Int_t j=BIN_Start;j<BIN_end;j++)
		{
		  Yield+= Histo->GetBinContent(j);
		  Error+= pow(Histo->GetBinError(j),2);
		  //cout<<"2) Adding bin nr. "<<j<<", with content: "<<Yield<<endl;
		}

        	//for large ranges integrate also the very beginning of the histogram!
        	if(i==0)
        	{
        		BIN_Start=Histo->FindBin(90);
        		BIN_end  =Histo->FindBin(90+35);
        		for(Int_t j=BIN_Start;j<BIN_end;j++)
        		{
        			Yield+= Histo->GetBinContent(j);
        			Error+= pow(Histo->GetBinError(j),2);
        		}
        	}

		if(Error<0)cout<<"ExtractInfo_Y_vs_Egamma:: strange error"<<endl;
		Error=sqrt(Error);
		SummaryHisto[i]->SetBinContent(Bin+1,Yield/SummaryHisto[i]->GetBinWidth(Bin+1));
		SummaryHisto[i]->SetBinError(Bin+1,Error/SummaryHisto[i]->GetBinWidth(Bin+1));
	}
}


