#include "VersionCompare_QA.h"

void VersionCompare_QA(Bool_t User)
{
    //..This is a basic QA macro.
	//..Its purpose is to compare different outputs
	//..and divides or subtracts them from each other.
	//..This way one can better track down software changes
	//..or the effects of different cuts applied to the data

	cout<<"..................................."<<endl;
    cout<<"Plot_GH_Correlation loaded for "<<flush;
    if(User==0)cout<<"Eliane"<<endl;
    if(User==1)cout<<"Tyler"<<endl;
	//..The User variable can be used to load user specific settings between
	//..Tyler and Eliane
	//..User==0 Eliane's settings
	//..User==1 Tyler's settings for Pi0-hadron wonderfulness


	gStyle->SetOptTitle(0);
	gStyle->SetOptStat(0);
	//gStyle->SetPalette(53);  //standard is 1
	gStyle->SetCanvasColor(10);
	TGaxis::SetMaxDigits(4);
	gStyle->SetPadTopMargin(0.1);
	gStyle->SetPadBottomMargin(0.15);
	gStyle->SetPadRightMargin(0.10);
	gStyle->SetPadLeftMargin(0.15);
	gStyle->SetFrameFillColor(10);
	gStyle->SetLabelSize(0.05,"X");
	gStyle->SetLabelSize(0.05,"Y");
	gStyle->SetTitleSize(5.0,"X");
	gStyle->SetTitleSize(5.0,"Y");
	gEnv->SetValue("Canvas.ShowEventStatus",1);  //shows the status bar in the canvas
	set_plot_style();

	TFile *InputFile_No1;
	TFile *InputFile_No2;
	TString InFilePath;
	TString InFilePath2;
	TString InFile_No1;
	TString InFile_No2;
	TString InFileName_No1;
	TString InFileName_No2;
    Int_t NameOfFirstList;
    Int_t NameOfSecondList;
	//-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-
	//
	//-.-.-.-.-.-.Get the Inputfile.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-
	//
	//-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-
	//if(User==0)InFilePath="/Users/Eliane/Software/Files/GH_rootFiles/";
	if(User==0)InFilePath="/Users/Eliane/Software/alicesw/aliphysics/master/src/PWGGA/EMCALTasks/macros/";
	if(User==0)InFilePath2="/Users/Eliane/Software/alice/ali-master/AliPhysics/PWGGA/EMCALTasks/macros/";
	if(User==1)InFilePath="/Users/thl7/GHMacroGit/WorkingYaleDev/";

	//..Take the first file and compare it to the second file
	if(User==0)
	{
		//..Elianes Part
		//InFile_No1 ="AnalysisResults_NewFW_2105XEvt.root"; //old run macro - new AddTask
		//InFile_No2 ="AnalysisResults_OldFW_21056Evt.root";

		//wrong triggers!!
		//InFile_No2 ="AnalysisResults_NewFW_2105XEvt_NoWeight.root"; //old run macro - new AddTask (different triggers??)
		//InFile_No2 ="AnalysisResults_OldFW_21056Evt_NoWeight.root";

		//InFile_No1 ="AnalysisResults_OldFW_21056Evt_NoWeight_NoPico.root";
		//InFile_No2 ="AnalysisResults_OldFW_21056Evt_NoWeight_NoPico2.root";
		//InFile_No1 ="AnalysisResults_New_NoClusTasks.root";
		//InFile_No1 ="AnalysisResults_New_ClusTasks.root";
		//InFile_No1 ="AnalysisResults_New_NoTenderClusTasks.root";
		//InFile_No1 ="AnalysisResults_New_NoTenderClusTask.root";
		//InFile_No1 ="AnalysisResults_NewOhneClusterizerFast.root";
		//InFile_No1 ="AnalysisResults_NewOhneEMCalClusMaker.root";
		//InFile_No2 ="AnalysisResults_Old_NoClusTasks.root";

		//InFile_No1 ="AnalysisResults_NewAllIn_IndependentTracks.root";
		//InFile_No2 ="AnalysisResults_OldAllIn_IndependentTracks.root";

		//InFile_No1 ="AnalysisResults_New2Files.root";
		//InFile_No2 ="AnalysisResults_Old2Files.root";
		//InFile_No2 ="AnalysisResults_Old2Files_wPico.root";

		//so the new stuff all seems to be alright
		//InFile_No1 ="AnalysisResults_SalvatoreOld.root";
		//InFile_No1 ="AnalysisResults_SalvatoreNew.root";//more files
		//InFile_No2 ="AnalysisResults_SalvatoreNew_Adaption4.root";

		//InFile_No1 ="AnalysisResults_SalvatoreNew_kEMCEGA.root"; //default energy without hadcorr!!
		//InFile_No1 ="AnalysisResults_SalvatoreNew_kEMCEGA_Full.root";
//		InFile_No1 ="AnalysisResults_NewFW_AllIn_Eselection.root"; //hadcorr energy!


		//compare 3 different steps
		//InFile_No1 ="AnalysisResults_NewFW_AllIn_StandardE_Full.root";

//**
		//InFile_No2 ="AnalysisResults_NewFW_AllIn_NonLinE_JetPrepCommented_AdaptedToOld.root";
//		InFile_No1 ="AnalysisResults_NewFW_AllIn_NonLinE_JetPrepCommented_AdaptedToOldV3.root";
//		InFile_No1 ="AnalysisResults_NewFW_AllIn_NonLinE_Full.root"; //hadcorr energy!

		//		InFile_No1 ="AnalysisResults_NewFW_AllIn_Eselection_Full.root"; //hadcorr energy!


		//original shit
		InFile_No1 ="AnalysisResults_OldFramework_Orig.root";  //calo cluster corr
		//InFile_No2 ="AnalysisResults_OldFramework_Orig_Adaption1.root";
		//InFile_No1 ="AnalysisResults_OldFramework_Orig_Adaption2.root";
		//InFile_No1 ="AnalysisResults_OldFramework_Orig_Adaption3.root"; //different name

//		InFile_No2 ="AnalysisResults_CaloClustersCorr.root";   //this was like the original stuff run

		//compare 3 different steps
//		InFile_No2 ="AnalysisResults_OldFW_AllInCaloClusters_Full.root";
//		InFile_No2 ="AnalysisResults_OldFW_AllInEmcCaloClusters_Full.root";
//		InFile_No2 ="AnalysisResults_OldFW_AllInEmcCaloClusters_JetPrepCommented.root";
//		InFile_No1 ="AnalysisResults_OldFW_AllInEmcCaloClusters_AdaptedNew1.root";
//		InFile_No1 ="AnalysisResults_OldFW_AllInEmcCaloClusters_AdaptedNew2.root";

//		InFile_No2 ="AnalysisResults_OldFW_AllIn_Full.root";

		//InFile_No2 ="AnalysisResults_caloClusters_comentedStuff.root";  //is not the same as uncommented (tender + jet prep) - the clusters are altered
		//InFile_No2 ="AnalysisResults_caloClusters_comentedJetPrep.root";

		//InFile_No2 ="AnalysisResults_NewFW_woMatcherHadCorr.root";
		//InFile_No2 ="AnalysisResults_NewFW_AllIn.root";
		//AnalysisResults_NewFW_AllIn.root

		//InFile_No1 ="AnalysisResults_OldFramework_Now.root";



		//raw energy  - looks good
		//InFile_No1 ="AnalysisResults_caloClusters.root";         //this is like the new framework
		//InFile_No2 ="AnalysisResults_NewFW_AllIn_NonLinE_JetPrepCommented.root";

		//with the corrections of the old fW (all these are absolute numbers)
	    //Yields and energy distribution is good. Still theta and phi of gammas differ - hadrons are fine
		InFile_No1 ="AnalysisResults_OldFW_2Files_May19.root";  //2 files
//		InFile_No2 ="AnalysisResults_NewFW_2Files_May19.root";  //2 files
//		InFile_No2 ="AnalysisResults_NewFW_rawPtCut_2Files_May19.root";     //with raw pt cut of 0.15 (as in old version)
//		InFile_No2 ="AnalysisResults_NewFW_highRawPtCut_2Files_May19.root"; //with raw pt cut of 5 (for testing)
//		InFile_No1 ="AnalysisResults_NewFW_RawPtCut2_2Files_May19.root";    //with raw pt cut of 0.3


		InFile_No2 ="AnalysisResults_May24.root";            //this version is ok!  (this is for absolute values!)
		InFile_No2 ="AnalysisResults_May24_perTrigger.root"; //this version is ok!  (this is for per trigger values!)

		//different eta binning - check mixed events
		//InFile_No1 ="AnalysisResults_May24_perTrigger_MER_3.root";//more
		//InFile_No2 ="AnalysisResults_May24_perTrigger_MEH_3.root";

		//Including the new 	->SetTargetValues(fTrackDepth, 0.1, 5);
		//InFile_No2 ="AnalysisResults_May24_perTrigger_MER_PoolReady.root";  //more
		//InFile_No2 ="AnalysisResults_May24_perTrigger_MEH_PoolReady.root";

		//try to save a pool an use it again
		InFile_No1 ="AnalysisResults_May24_perTrigger_MER_SavedPool_Try2.root";
		InFile_No2 ="AnalysisResults_May24_perTrigger_MER_SavedPool_ExtPool.root";


		NameOfFirstList =2;
		NameOfSecondList=2;
	}
	if(User==1)
	{
		InFile_No1 ="";
		InFile_No2 ="";
	}

	InFileName_No1 = InFilePath2+InFile_No1;
	InFileName_No2 = InFilePath2+InFile_No2;
	cout<<"InfileName of first file : "<<InFileName_No1<<endl;
	cout<<"InfileName of second file: "<<InFileName_No2<<endl;
	InputFile_No1 = TFile::Open(InFileName_No1);
	InputFile_No2 = TFile::Open(InFileName_No2);

	//-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-
	//
	//-.-.-.-.-.-.Get the Histograms from the Inputfile.-.-.-.-.-.-.-.-.-.-.-.-.-.-
	//
	//-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-
	TString HistName="";
	TString HistNameFile1="";
	TString HistNameFile2="";
	const Int_t Gamma_NBINS = 9;

	TH2D* dEdP_G_File1[Gamma_NBINS];
	TH2D* dEdP_G_File2[Gamma_NBINS];
	TH2D* dEdP_ZT_File1[7];
	TH2D* dEdP_ZT_File2[7];
	TH2D* dEdP_XI_File1[8];
	TH2D* dEdP_XI_File2[8];

	Int_t accEventsFile1=GetAccEvtFromFile(InputFile_No1,NameOfFirstList);
	Int_t accEventsFile2=GetAccEvtFromFile(InputFile_No2,NameOfSecondList);
	if(accEventsFile1==0 || accEventsFile2==0)
	{
		cout<<"No entries in root file: "<<accEventsFile1<<", 2: "<<accEventsFile2<<endl;
		accEventsFile1=1;
		accEventsFile2=1;
	}

	cout<<"Number of accepted events in File 1: "<<accEventsFile1<<endl;
	cout<<"Number of accepted events in File 2: "<<accEventsFile2<<endl;
	cout<<"Number of rejected events in File 1: "<<GetRejEvtFromFile(InputFile_No1,NameOfFirstList)<<endl;
	cout<<"Number of rejected events in File 2: "<<GetRejEvtFromFile(InputFile_No2,NameOfSecondList)<<endl;

	for(Int_t i=0;i<Gamma_NBINS;i++)
	{
		HistName    = Form("fHistDEtaDPhiG%d_Id0",i); //mixed event
//		HistName    = Form("fHistDEtaDPhiG%d_Id1",i);    //same event
//		HistName    = Form("fHistDEtaDPhiG%d_Id2",i);

		HistNameFile1=HistName;
		HistNameFile1+="_File1";
		HistNameFile2=HistName;
		HistNameFile2+="_File2";

		//..delta phi delta eta binned in gamma energy
		dEdP_G_File1[i] = Get2DHistoFromFile(InputFile_No1,"Different_Gamma_2DHistograms",HistName,NameOfFirstList);//
		dEdP_G_File1[i] ->SetName(HistNameFile1);
		dEdP_G_File1[i] ->Sumw2();
		dEdP_G_File1[i] ->Scale(1.0/accEventsFile1);
		dEdP_G_File2[i] = Get2DHistoFromFile(InputFile_No2,"Different_Gamma_2DHistograms",HistName,NameOfSecondList);//Different_Gamma_2DHistograms
		dEdP_G_File2[i] ->SetName(HistNameFile2);
		dEdP_G_File2[i] ->Sumw2();
		dEdP_G_File2[i] ->Scale(1.0/accEventsFile2);

		cout<<"Histo nr: "<<i<<", File1 "<<dEdP_G_File1[i]->GetEntries()<<", File2 "<<dEdP_G_File2[i]->GetEntries()<<endl;

		if(i<8)
		{
		    HistName    = Form("fHistDEtaDPhiXI%d_Id1",i);

			HistNameFile1=HistName;
			HistNameFile1+="_File1";
			HistNameFile2=HistName;
			HistNameFile2+="_File2";

			//..delta phi delta eta binned in gamma energy
			dEdP_XI_File1[i] = Get2DHistoFromFile(InputFile_No1,"Different_Xi_2DHistograms",HistName,NameOfFirstList);//
			dEdP_XI_File1[i] ->SetName(HistNameFile1);
			dEdP_XI_File1[i] ->Sumw2();
			dEdP_XI_File1[i] ->Scale(1.0/accEventsFile1);
			dEdP_XI_File2[i] = Get2DHistoFromFile(InputFile_No2,"Different_Xi_2DHistograms",HistName,NameOfSecondList);//
			dEdP_XI_File2[i] ->SetName(HistNameFile2);
			dEdP_XI_File2[i] ->Sumw2();
			dEdP_XI_File2[i] ->Scale(1.0/accEventsFile2);

		}
		if(i<7)
		{
			HistName    = Form("fHistDEtaDPhiZT%d_Id1",i);

			HistNameFile1=HistName;
			HistNameFile1+="_File1";
			HistNameFile2=HistName;
			HistNameFile2+="_File2";

			//..delta phi delta eta binned in gamma energy
			dEdP_ZT_File1[i] = Get2DHistoFromFile(InputFile_No1,"Different_Zt_2DHistograms",HistName,NameOfFirstList);//
			dEdP_ZT_File1[i] ->SetName(HistNameFile1);
			dEdP_ZT_File1[i] ->Sumw2();
			dEdP_ZT_File1[i] ->Scale(1.0/accEventsFile1);
			dEdP_ZT_File2[i] = Get2DHistoFromFile(InputFile_No2,"Different_Zt_2DHistograms",HistName,NameOfSecondList);//
			dEdP_ZT_File2[i] ->SetName(HistNameFile2);
			dEdP_ZT_File2[i] ->Sumw2();
			dEdP_ZT_File2[i] ->Scale(1.0/accEventsFile2);
		}
	}
	//..pt distributon of the gamma
	TH1D* PtGamma_File1 = Get1DHistoFromFile(InputFile_No1,"pT_distributions_of_the_gamma","fHistNoClusPt_1",NameOfFirstList);//
	PtGamma_File1->SetName("fHistNoClusPt_1_File1");
	PtGamma_File1->Sumw2();
	PtGamma_File1->Scale(1.0/accEventsFile1);
	TH1D* PtGamma_File2 = Get1DHistoFromFile(InputFile_No2,"pT_distributions_of_the_gamma","fHistNoClusPt_1",NameOfSecondList);//
	PtGamma_File2->SetName("fHistNoClusPt_1_File2");
	PtGamma_File2->Sumw2();
	PtGamma_File2->Scale(1.0/accEventsFile2);

	//.. gamma distribution in case there was a hadron _1 or _0 stands for same (1) or mixed (0) events
	TH1D* PtGammaH_File1 = Get1DHistoFromFile(InputFile_No1,"","fHistNoClusPtH_1",NameOfFirstList);//
	PtGammaH_File1->SetName("fHistNoClusPtH_1_File1");
	PtGammaH_File1->Sumw2();
	PtGammaH_File1->Scale(1.0/accEventsFile1);
	TH1D* PtGammaH_File2 = Get1DHistoFromFile(InputFile_No2,"","fHistNoClusPtH_1",NameOfSecondList);//
	PtGammaH_File2->SetName("fHistNoClusPtH_1_File2");
	PtGammaH_File2->Sumw2();
	PtGammaH_File2->Scale(1.0/accEventsFile2);

	//.. associated hadron p_T distribution _1 or _0 stands for same (1) or mixed (0) events
	TH1D* PtAssH_File1 = Get1DHistoFromFile(InputFile_No1,"","fHistAssHadron_pt_0_0",NameOfFirstList);//
	PtAssH_File1->SetName("fHistAssHadron_pt_0_0_File1");
	PtAssH_File1->Sumw2();
	PtAssH_File1->Scale(1.0/accEventsFile1);
	TH1D* PtAssH_File2 = Get1DHistoFromFile(InputFile_No2,"","fHistAssHadron_pt_0_0",NameOfSecondList);//
	PtAssH_File2->SetName("fHistAssHadron_pt_0_0_File2");
	PtAssH_File2->Sumw2();
	PtAssH_File2->Scale(1.0/accEventsFile2);

	//.. associated hadron p_T distribution _1 or _0 stands for same (1) or mixed (0) events
	TH1D* Centr_File1 = Get1DHistoFromFile(InputFile_No1,"","fHistCentrality",NameOfFirstList);//
	Centr_File1->SetName("fHistCentrality_File1");
	Centr_File1->Sumw2();
	Centr_File1->Scale(1.0/accEventsFile1);
	TH1D* Centr_File2 = Get1DHistoFromFile(InputFile_No2,"","fHistCentrality",NameOfSecondList);//
	Centr_File2->SetName("fHistCentrality_File2");
	Centr_File2->Sumw2();
	Centr_File2->Scale(1.0/accEventsFile2);

	//..eta phi distribution of gammas
	TH2D* GammaEtaPhi_File1 = Get2DHistoFromFile(InputFile_No1,"QA_histograms","fHistDEtaDPhiGammaQA0_Id0",NameOfFirstList);//ID2 should be the same
	GammaEtaPhi_File1->SetName("GammaEtaPhi_File1");
	GammaEtaPhi_File1->Sumw2();
	GammaEtaPhi_File1->Scale(1.0/accEventsFile1);
	TH2D* GammaEtaPhi_File2 = Get2DHistoFromFile(InputFile_No2,"QA_histograms","fHistDEtaDPhiGammaQA0_Id0",NameOfSecondList);//
	GammaEtaPhi_File2->SetName("GammaEtaPhi_File2");
	GammaEtaPhi_File2->Sumw2();
	GammaEtaPhi_File2->Scale(1.0/accEventsFile2);

	//..eta phi distribution of gammas
	TH2D* HadronEtaPhi_File1 = Get2DHistoFromFile(InputFile_No1,"QA_histograms","fHistDEtaDPhiTrackQA0_Id1",NameOfFirstList);//
	HadronEtaPhi_File1->SetName("HadronEtaPhi_File1");
	HadronEtaPhi_File1->Sumw2();
	HadronEtaPhi_File1->Scale(1.0/accEventsFile1);
	TH2D* HadronEtaPhi_File2 = Get2DHistoFromFile(InputFile_No2,"QA_histograms","fHistDEtaDPhiTrackQA0_Id1",NameOfSecondList);//
	HadronEtaPhi_File2->SetName("HadronEtaPhi_File2");
	HadronEtaPhi_File2->Sumw2();
	HadronEtaPhi_File2->Scale(1.0/accEventsFile2);

	//..rejection reason
	TH1D* EvtRejection_File1 = Get1DHistoFromFile(InputFile_No1,"","fHistEventRejection",NameOfFirstList);//
	EvtRejection_File1->SetName("EvtRejection_File1");
	EvtRejection_File1->Sumw2();
	EvtRejection_File1->Scale(1.0/accEventsFile1);
	TH1D* EvtRejection_File2 = Get1DHistoFromFile(InputFile_No2,"","fHistEventRejection",NameOfSecondList);//
	EvtRejection_File2->SetName("EvtRejection_File2");
	EvtRejection_File2->Sumw2();
	EvtRejection_File2->Scale(1.0/accEventsFile2);

	//..Trigger class
	TH1D* EvtTrigClass_File1 = Get1DHistoFromFile(InputFile_No1,"","fHistTriggerClasses",NameOfFirstList);//
	EvtTrigClass_File1->SetName("EvtTrigClass_File1");
	EvtTrigClass_File1->Sumw2();
	EvtTrigClass_File1->Scale(1.0/accEventsFile1);
	TH1D* EvtTrigClass_File2 = Get1DHistoFromFile(InputFile_No2,"","fHistTriggerClasses",NameOfSecondList);//
	EvtTrigClass_File2->SetName("EvtTrigClass_File2");
	EvtTrigClass_File2->Sumw2();
	EvtTrigClass_File2->Scale(1.0/accEventsFile2);

 	//-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-
	//
	//-.-.-.-.-.-.Plot 2D Histograms in canvases.-.-.-.-.-.-.-.-.-.-.-.-.-.-
	//
	//-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-

	//- - - - - - - - - - - - - - - - -
	//..GH Histograms - for different gamma energies
	cout<<"Draw GH correlations for different bins of gamma energies"<<endl;
	TCanvas *Raw_Plots_2D_Gamma = new TCanvas("Raw_2D_Plots","Raw 2D Plots - 9 gamma histograms",1400,900);
	Raw_Plots_2D_Gamma->Divide(3,3);
	for(Int_t i=0;i<3;i++)
	{
		Raw_Plots_2D_Gamma->cd(i+1);
		SetTH2Histo(dEdP_G_File1[i],"","");
		dEdP_G_File1[i]->Divide(dEdP_G_File2[i]);
		dEdP_G_File1[i]->DrawCopy("colz");

		Raw_Plots_2D_Gamma->cd(i+4);
		SetTH2Histo(dEdP_G_File1[i+3],"","");
		dEdP_G_File1[i+3]->Divide(dEdP_G_File2[i+3]);
		dEdP_G_File1[i+3]->DrawCopy("colz");

		Raw_Plots_2D_Gamma->cd(i+7);
		SetTH2Histo(dEdP_G_File1[i+6],"","");
		dEdP_G_File1[i+6]->Divide(dEdP_G_File2[i+6]);
		dEdP_G_File1[i+6]->DrawCopy("colz");
	}

	//- - - - - - - - - - - - - - - - -
	//..GH Histograms - for different ZT values
	cout<<"Draw GH correlations for different bins of Zt Values"<<endl;
	TCanvas *Raw_Plots_2D_Zt = new TCanvas("Raw_2D_PlotsZ","Raw 2D Plots - 9 Zt histograms",1400,900);
	Raw_Plots_2D_Zt->Divide(3,3);
	for(Int_t i=0;i<3;i++)
	{
		Raw_Plots_2D_Zt->cd(i+1);
		SetTH2Histo(dEdP_ZT_File1[i],"","");
		dEdP_ZT_File1[i]->Divide(dEdP_ZT_File2[i]);
		dEdP_ZT_File1[i]->DrawCopy("colz");

		Raw_Plots_2D_Zt->cd(i+4);
		SetTH2Histo(dEdP_ZT_File1[i+3],"","");
		dEdP_ZT_File1[i+3]->Divide(dEdP_ZT_File2[i+3]);
		dEdP_ZT_File1[i+3]->DrawCopy("colz");

		if(i<1)
		{
			Raw_Plots_2D_Zt->cd(i+7);
			SetTH2Histo(dEdP_ZT_File1[i+6],"","");
			dEdP_ZT_File1[i+6]->Divide(dEdP_ZT_File2[i+6]);
			dEdP_ZT_File1[i+6]->DrawCopy("colz");
		}
	}

	//- - - - - - - - - - - - - - - - -
	//GH Histograms - for different XI values
	cout<<"Draw GH correlations for different bins of Xi Values"<<endl;
	TCanvas *Raw_Plots_2D_Xi = new TCanvas("Raw_2D_PlotsXi","Raw 2D Plots - 9 Xi histograms",1400,900);
	Raw_Plots_2D_Xi->Divide(3,3);
	for(Int_t i=0;i<3;i++)
	{
		Raw_Plots_2D_Xi->cd(i+1);
		SetTH2Histo(dEdP_XI_File1[i],"","");
		dEdP_XI_File1[i]->Divide(dEdP_XI_File2[i]);
		dEdP_XI_File1[i]->DrawCopy("colz");

		Raw_Plots_2D_Xi->cd(i+4);
		SetTH2Histo(dEdP_XI_File1[i+3],"","");
		dEdP_XI_File1[i+3]->Divide(dEdP_XI_File2[i+3]);
		dEdP_XI_File1[i+3]->DrawCopy("colz");

		if(i<2)
		{
			Raw_Plots_2D_Xi->cd(i+7);
			SetTH2Histo(dEdP_XI_File1[i+6],"","");
			dEdP_XI_File1[i+6]->Divide(dEdP_XI_File2[i+6]);
			dEdP_XI_File1[i+6]->DrawCopy("colz");
		}
	}

	//- - - - - - - - - - - - - - - - -
	//GH Histograms - pt of the gamma
	TCanvas *Single_Figures = new TCanvas("Single_Figures","Single_Figures",900,900);
	Single_Figures->Divide(2,2);

	Single_Figures->cd(1);
	SetTH1Histo(Centr_File1,"","");
	Centr_File1->Divide(Centr_File2);
	Centr_File1->DrawCopy("hist");

	Single_Figures->cd(2);
	SetTH1Histo(PtGamma_File1,"","");
	PtGamma_File1->Divide(PtGamma_File2);
	PtGamma_File1->DrawCopy("hist");

	Single_Figures->cd(3);
	SetTH1Histo(PtGammaH_File1,"","");
	PtGammaH_File1->Divide(PtGammaH_File2);
	PtGammaH_File1->DrawCopy("hist");

	Single_Figures->cd(4);
	SetTH1Histo(PtAssH_File1,"","");
	PtAssH_File1->Divide(PtAssH_File2);
	PtAssH_File1->DrawCopy("hist");

	//- - - - - - - - - - - - - - - - -
	//Distribution of gammas and hadrons
	TCanvas *EtaPhiDistr = new TCanvas("EtaPhiDistr","EtaPhiDistr",900,450);
	EtaPhiDistr->Divide(2);
	EtaPhiDistr->cd(1);
	SetTH2Histo(GammaEtaPhi_File1,"","");
	GammaEtaPhi_File1->Divide(GammaEtaPhi_File2);
	GammaEtaPhi_File1->DrawCopy("colz");

	EtaPhiDistr->cd(2);
	SetTH2Histo(HadronEtaPhi_File1,"","");
	HadronEtaPhi_File1->Divide(HadronEtaPhi_File2);
	HadronEtaPhi_File1->DrawCopy("colz");

	TCanvas *EvtRejCanv = new TCanvas("EvtRejCanv","EvtRejCanv",900,450);
	EvtRejCanv->Divide(2);
	EvtRejCanv->cd(1);
	SetTH1Histo(EvtRejection_File1,"","");
	EvtRejection_File1->Divide(EvtRejection_File2);
	EvtRejection_File1->DrawCopy("hist");
	//EvtRejection_File2->DrawCopy("same");

	EvtRejCanv->cd(2);
	SetTH1Histo(EvtTrigClass_File1,"","");
	EvtTrigClass_File1->Divide(EvtTrigClass_File2);
	EvtTrigClass_File1->DrawCopy("hist");
}
