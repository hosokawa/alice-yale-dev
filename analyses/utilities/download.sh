#!/bin/bash

# Script to download files from the grid, and write them into a similar directory structure on your machine.
# Edit the config fields below to set the appropriate path of the files you want to download, and the destination path.
# It will also automatically write the file list if desired
# Author: Salvatore
# Edits by: Raymond

# Select system and period
# Possible systems are "PbPb", "pPb", and "pp"
system="pp"
period="LHC16e1"
# Select options:
# Max number of files to have locally
maxFiles=4
# Max number of files to download
maxFilesDownloaded=10
# Where to write the files
# Remember to include the trailing slash!
destPrefix="/Users/re239/code/alice/data/${period}/"
# Name of the folder containing the file list
# It will go in the destPrefix/../$fileListFolderName folder
# An empty filename will prevent the file list from being generated
fileListFolderName="fileLists"

# Other defaults
# They override the values set below
# data can be either "sim" or "data"
data=""
year=""
run=""
pass=""
# NOTE: The filetype may need to be followed by the production number, depending on the data set
fileType=""
productionNumber=""
# pT Hard Bins (when applicable)
ptHardBin=""
# Filename is often "root_archive.zip" or "aod_archive.zip", depending on the data set
filename=""

# Below are known systems, which should work out of the box.
# LHC12a15e_fix: pp MC in pt hard bins
if [[ "$system" == "pp" && "$period" == "LHC12a15e_fix" ]];
then
    data=${data:-"sim"}
    year=${year:-"2012"}
    run=${run:-"169838"}
    if [[ -z "${ptHardBin}" ]]; then
        echo "ERROR: pt hard bin is not selected! Please select one and try again!"
        exit 1
    fi
    fileType=${fileType:-"AOD"}
    productionNumber=${productionNumber:-"149"}
    filename=${filename:-"root_archive.zip"}

    searchpath="/alice/${data}/${year}/${period}/${run}/${ptHardBin}/${fileType}${productionNumber}"
fi
# LHC16e1: pp MC anchored to LHC15n
if [[ "$system" == "pp" && "$period" == "LHC16e1" ]];
then
    data=${data:-"sim"}
    year=${year:-"2016"}
    run=${run:-"244351"}
    if [[ -z "${ptHardBin}" ]]; then
        echo "ERROR: pt hard bin is not selected! Please select one and try again!"
        exit 1
    fi
    fileType=${fileType:-"AOD"}
    filename=${filename:-"root_archive.zip"}

    searchpath="/alice/${data}/${year}/${period}/${run}/${ptHardBin}/"
    if [[ "${fileType}" == "AOD" ]];
    then
        # Add "AOD" to searchpath and change filename
        searchpath="${searchpath}${fileType}/"
        filename="aod_archive.zip"
    fi
fi
# LHC15i2c: pp MC
if [[ "$system" == "pp" && "$period" == "LHC15i2c" ]];
then
    data=${data:-"sim"}
    year=${year:-"2015"}
    run=${run:-"120822"}
    pass=${pass:-"4"}
    fileType=${fileType:-"AOD"}
    filename="aod_archive.zip"

    searchpath="/alice/${data}/${year}/${period}/${run}/${pass}/${fileType}"
fi
# pPb 2013
if [[ "$system" == "pPb" && "$period" == *"LHC13"* ]];
then
    data=${data:-"data"}
    year=${year:-"2013"}
    run=${run:-"000195344"}
    pass=${pass:-"4"}
    fileType=${fileType:-"AOD"}
    filename=${filename:-"aod_archive.zip"}

    searchpath="/alice/${data}/${year}/${period}/${run}/pass${pass}/${fileType}"
fi
# LHC11h: 2011 PbPb 2.76
if [[ "$system" == "PbPb" && "$period" == "LHC11h" ]];
then
    data=${data:-"data"}
    year=${year:-"2011"}
    run=${run:-"000168464"}
    pass=${pass:-"2"}
    fileType=${fileType:-"AOD"}
    filename=${filename:-"root_archive.zip"}
    productionNumber=${productionNumber:-"145"}

    # /alice/data/2011/LHC11h_2/000167693/ESDs/pass2/AOD145
    searchpath="/alice/${data}/${year}/${period}/${run}/ESDs/pass${pass}/${fileType}${productionNumber}"
fi

# Fall back to default if it is an unknown period
if [[ -z "$searchpath" ]];
then
    echo "Unkonwn period and system selected. Using default path and values."
    searchpath="/alice/${data}/${year}/${period}/${run}/${pass}/${fileType}"
fi

echo "Looking for file ${filename} in path ${searchpath}"

read -p "Search the grid? Press enter to continue or ctrl-c to cancel!"

files=$(alien_find  ${searchpath} ${filename})

echo "Files found on the grid:"
echo "${files}"

echo "Downloading at most $maxFilesDownloaded files to have at most $maxFiles on the system!"
read -p "Ready to download the files? Press enter to continue or ctrl-c to cancel!"

nFiles=0
nFilesDownlaoded=0

for file in $files
do
    if [[ "${nFilesDownlaoded}" -ge "${maxFilesDownloaded}" ]]; then
        break
    fi
    if [[ "${nFiles}" -ge "${maxFiles}" ]]; then
        break
    fi
    
    if [[ ! $file == /alice* ]]; then
        continue
    fi
    localpath=${file/\/alice\/${data}\/${year}\/${period}\//}
    localpath=${localpath/\/${filename}/}

    dest="${destPrefix}${localpath}"
    fileDest="${dest}/${filename}"
    if [ ! -f ${fileDest} ]; then
        mkdir -p ${dest}
        echo "Copying alien://${file} to ${fileDest}"
        alien_cp alien://${file} ${fileDest}
        ((nFilesDownlaoded++))
    else
        echo "File already exists: ${fileDest}"
    fi
    ((nFiles++))
done

# Generate file list
if [[ -n "$fileListFolderName" ]];
then
    fileListDest="${destPrefix/\/${period}\//}/${fileListFolderName}"
    echo "Generating filelist ${period}.txt in ${fileListDest}"
    fileListFilename="${fileListDest}/${period}${fileType}.txt"
    # "%/" removes trailing slash
    find ${destPrefix%/} -name ${filename} > ${fileListFilename}
    # Add the proper extensions if necessary
    # ie ../root_archive.zip#AliAOD.root
    fileExtension="${filename##*.}"
    if [[ "${fileExtension}" == "zip" ]];
    then
        if [[ "${fileType}" == "AOD" ]];
        then
            addAfterHash="AOD"
        else
            addAfterHash="ESDs"
        fi

        # Append to each line
        awk -v addAfterHash="${addAfterHash}" '{print $0 "#Ali" addAfterHash ".root"}' ${fileListFilename} > ".tmp" && mv ".tmp" ${fileListFilename}
    fi
else
    echo "Not generating file list!"
fi
