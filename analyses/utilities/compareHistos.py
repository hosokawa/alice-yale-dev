#! /usr/bin/env python

# Script to compare histograms in a .root file for exact equality
# Set the input file, and the name of the output lists to compare

# Python 3 compatibility
from __future__ import print_function
from builtins import range

# General
import os
import sys
# Identify the system
import socket
# Handle args
import argparse
# ROOT
import ROOT
# For comparing types
import collections
# Convenience
import pprint

# Force root to ignore command line options...
ROOT.PyConfig.IgnoreCommandLineOptions = True
# Set ROOT into batch mode (as if we ran "root -b")
ROOT.gROOT.SetBatch()

def extractHistsFromList(listOfObjects, outputDict, correctionListName = "AliEmcalCorrectionTask_histos"):
    """ Extract histograms from TLists while still maintaining some structure to allow for comparison

    """
    for obj in listOfObjects:
        #print("Processing object {0} of type {1}".format(obj.GetName(), obj.ClassName()))
        # Handle TH1 derived or THnSparse
        if obj.InheritsFrom(ROOT.TH1.Class()):
            #print("Adding object {0} of type {1} as a hist".format(obj.GetName(), obj.ClassName()))
            outputDict[obj.GetName()] = obj
        elif obj.InheritsFrom(ROOT.THnSparse.Class()):
            #print("Adding object {0} of type {1} as a THnSparse".format(obj.GetName(), obj.ClassName()))
            # Loop over the axes of the THnSparse, Project each one, and add them to the dict
            for i in range(0, obj.GetNdimensions()):
                outputName = obj.GetName() + "_Axis{0}".format(i)
                #print("Projecting axis {0} as {1} from THnSparse".format(i, outputName))
                projection = obj.Projection(i)
                projection.SetName(outputName)
                outputDict[outputName] = projection
        # TList -> Recursive
        elif obj.InheritsFrom(ROOT.TList.Class()):
            #print("Recursing with object {0} of type {1}".format(obj.GetName(), obj.ClassName()))
            # If it is the main new correction list, we don't want to create a new list (since it just adds an unnecessary layer)
            if obj.GetName() == correctionListName:
                dictToPass = outputDict
            else:
                # If not, create a dict to hold the output for this TList
                outputDict[obj.GetName()] = dict()
                dictToPass = outputDict[obj.GetName()]
            # Call recursively to handle TLists
            extractHistsFromList(obj, dictToPass, correctionListName = correctionListName)
        # TDirectoryFile - Recursive
        elif obj.InheritsFrom(ROOT.TDirectoryFile.Class()):
            # TDirectoryFile isn't iterable, so we need to iterate the keys ourselves
            #print("Obj name: {0}".format(obj.GetName()))
            for key in obj.GetListOfKeys():
                #print("Obj key: {0}".format(key.GetName()))
                # Create an output dict for each key
                outputDict[key.GetName()] = dict()
                dictToPass = outputDict[key.GetName()]

                # Call recursively to handle TDirectoryFile keys
                extractHistsFromList(key.ReadObj(), dictToPass, correctionListName = correctionListName)

        # Unrecognized -> Throw warning and ignore
        else:
            print("Object {0} of type {1} is not of the type TH1, THnSparse, TList, or TDirectoryFile and cannot be handled. Please look into this more closely!".format(obj.GetName(), obj.ClassName()))

def runComparison(inputFile, newAnalysisList, oldAnalysisList, correctionListName, newToOldCorrections, printPDF, doStatisticalComparison, thresh):
    # Open the file
    f = ROOT.TFile(inputFile)

    # Get a list of the names of the new corrections
    newCorrectionsList = list()
    for obj in f.Get(correctionListName):
        newCorrectionsList.append(obj.GetName())

    # Add user analysis task input list name
    if newAnalysisList != "":
        newCorrectionsList.append(newAnalysisList)

    # Extract objects from keys to extract and organize hists
    print("\nLoading histograms...")
    keys = f.GetListOfKeys()
    inputList = list()
    for key in keys:
        inputList.append(key.ReadObj())

    # Extract histograms from the file and organize into a dict
    inputHists = dict()
    extractHistsFromList(inputList, inputHists, correctionListName = correctionListName)

    # Handle the old analysis name if the new analysis name is meaningful
    if newAnalysisList != "":
        # Guess old analysis name if none was passed
        if oldAnalysisList == "":
            # Often tasks name just differ by changing the tracks and clusters name.
            # The first letter was left off because it can vary for ESDs and AODs, but it is unlikely to match
            # anything else.
            oldAnalysisList = newAnalysisList.replace("racksNew", "racks").replace("aloClustersNew", "aloClusters").replace("ellsNew", "ells")
            print("Guessing old anaylsis output list name: \"{0}\"".format(oldAnalysisList))

        # Attempt to match new and old analysis
        if oldAnalysisList in inputHists:
            newToOldCorrections[newAnalysisList] = [oldAnalysisList]
            print("\nMatched new analysis name \"{0}\" to old analysis name \"{1}\"".format(newAnalysisList, oldAnalysisList))
        else:
            print("\nCould not find old analysis list name \"{0}\" in output. Please pass a name for the analysis with the old framework if the guess was wrong or confirm that it is the right value.".format(oldAnalysisList))
            print("Available names include:")
            print("\t{0}".format("\n\t".join(inputHists.keys())))
            sys.exit(1)
    else:
        print("No new analysis list name passed. Only analyzing the new vs old corrections!")

    # Print status if necessary
    #print("InputHists: ")
    #pprint.pprint(inputHists)
    #print("newCorrectionsList: ")
    #pprint.pprint(newCorrectionsList)
    #print("newToOldCorrections: ")
    #pprint.pprint(newToOldCorrections)

    print("\nProcessing with:")
    print("Tasks using the new correction framework: ")
    print("\t{0}".format("\n\t".join(newCorrectionsList)))
    print("Tasks using the old correction framework: ")
    # See: https://stackoverflow.com/a/952952
    print("\t{0}".format("\n\t".join(set(
        [oldCorrection for newCorrection in newCorrectionsList for oldCorrection in newToOldCorrections.get(newCorrection, [])]
        ))
    ))
    print("")
    if doStatisticalComparison:
      print("This may take a minute...")
      print("")

    # Loop through component histograms, and find matching histogram in old correction framework
    """NOTE:
        We will probably miss a comparison if the user has two histograms with the same name in the same task.
        For example, if the file looked like:
            fNEvents
            outputList/
                fNEvents
        There this a good chance that the second fNEvents could be matched up with the first fNEvents in the matched
        histogram. However, this shouldn't occur terribly often, and if so, it will just indicate a mismatch.
    """
    nNewCorrectionHists = 0
    nMatched = 0
    nUnmatched = 0
    nCorrect = 0
    nEmpty = 0
    nCouldNotFindOldComponent = 0
    # Start with each new component
    for newComponent in newCorrectionsList:
        # Attempt to iterate through the hist associated with the new correction component.
        # If the dict is not found, then it is skipped
        for newHist in getHist(inputHists.get(newComponent, dict())):
            #print("Searching for {0} in {1}".format(newHist, newComponent))
            nNewCorrectionHists += 1
            matchFound = False
            couldNotFindOldComponent = False
            # Get each old components histograms associated with the new component by the new to old corrections map
            # In principle there could be multiple components, although in practice there is usually just one
            for oldComponent in newToOldCorrections[newComponent]:
                # Attempt to get the old correction hists.
                # First attempt ESD names instead of AOD names if the AOD name doesn't exist
                if oldComponent not in inputHists:
                    #print("Attempting ESD name instead")
                    oldComponent = oldComponent.replace("caloClusters", "CaloClusters").replace("tracks", "Tracks").replace("emcalCells", "EMCALCells")
                if oldComponent in inputHists:
                    for oldHist in getHist(inputHists[oldComponent]):
                        # Determine matches initially by checking the names
                        if newHist.GetName() == oldHist.GetName():
                            matchFound = True
                            nMatched = nMatched + 1

                            # Compare the two histograms together
                            nCorrect = nCorrect + compare(newHist, oldHist, printPDF = printPDF, newComponentName = newComponent, doStatisticalComparison = doStatisticalComparison, thresh = thresh)

                            # Note empty histograms
                            if newHist.GetEntries() < 1e-4 and oldHist.GetEntries() < 1e-4:
                              nEmpty = nEmpty + 1
                            break
                else:
                    print("For new hist {0}:{1}, could not find old component {2} in inputHists! Skipping!".format(newComponent, newHist.GetName(), newToOldCorrections[newComponent]))
                    couldNotFindOldComponent = True
                    nCouldNotFindOldComponent = nCouldNotFindOldComponent+ 1

            # Note unmatched hists
            if matchFound is False and couldNotFindOldComponent is False:
                nUnmatched = nUnmatched + 1
                print("For new hist {0}:{1}, no matching histogram was found in {2}! Skipping!".format(newComponent, newHist.GetName(), oldComponent))

    print("\nNumber of histograms in new correction components: {0}".format(nNewCorrectionHists))
    print("Number of matched histograms: {0}".format(nMatched))
    print("Number of unmatched histograms due to not finding old component: {0}".format(nCouldNotFindOldComponent))
    print("Number of unmatched histograms: {0}".format(nUnmatched))
    print("Number of correct histograms: {0}".format(nCorrect))
    print("Number of incorrect histograms: {0}".format(nMatched - nCorrect))
    print("Number of empty histograms: {0}".format(nEmpty))

def getHist(component):
    """ This is a generator which will find the next histogram.

    If we pass a dict, then it will recursively search through that.

    See: https://stackoverflow.com/a/6036037 """
    if isinstance(component, collections.MutableMapping):
        #print("Seeing dict with # of values {0}. Going in another level".format(len(component)))
        for val in component.values():
            # We cannot yield getHist() directly, as this would be a generator.
            # Instead, we need to iterate through the results and yield them!
            for res in getHist(val):
                yield res
    else:
        #if not component:
        #    # Handle empty objects (likely dicts)
        #    print("Returning empty dict: {0}".format(component))
        #else:
        #    # Handle histograms
        #    print("Returning hist {0}".format(component.GetName()))
        yield component

    """NOTE:
        Cannot just yield here because the generator completes the function
        similarly to a case statement falling through if there is no explicit
        break. Consequently, we yield in the else statement to protect against
        yielding twice when we have a dict!
    """

def compare(h1, h2, printPDF, newComponentName, doStatisticalComparison, thresh):
    h1Entries = h1.GetEntries()
    h2Entries = h2.GetEntries()
    # Use this to compare so we can plot both later if desired
    comparisonHist = h1.Clone(h1.GetName() + "_Subtracted")

    comparisonHist.Add(h2, -1)

    match = 1 # flag for whether histograms agree (1) or not (0)
    epsilon = 1e-5
    divByZero = False # flag for whether fractional difference is infinite for some bin

    # Do bin-by-bin statistical comparison, if requested
    if doStatisticalComparison:
        histFractionalDiff = h1.Clone(h1.GetName() + "_FractionalDifference")
        histFractionalDiff.Add(h1, -1) 
        #nbins = h1.GetSize()
        
        if h1.InheritsFrom(ROOT.TH2.Class()):
            nbins = h1.GetNbinsX() * h1.GetNbinsY()
        elif h1.InheritsFrom(ROOT.TH1.Class()):
            nbins = h1.GetNbinsX()
        #print ("histogram " + h1.GetName() + " has " + str(nbins) + " bins")

        for bin in range(1, nbins):
            if h1.IsBinUnderflow(bin) or h1.IsBinOverflow(bin): continue

            subtractedVal = comparisonHist.GetBinContent(bin)
            if abs(subtractedVal) < epsilon: continue
            
            originalVal = h1.GetBinContent(bin)
            #print ("bin: " + str(bin) + " / sub: " + str(subtractedVal) + " / orig: " + str(originalVal))
            
            if abs(originalVal) < epsilon: # avoid division by zero
                fractionalDiff = 0
                print("Warning: bin %d fractional difference contains division by 0!" % bin)
                divByZero = True
            else:
                fractionalDiff = subtractedVal/originalVal
                histFractionalDiff.SetBinContent(bin, fractionalDiff)
                #print("fractional diff: " + str(fractionalDiff))
            
        # print out that histograms disagree if fractional difference exceeds threshold
        maxFracDiff = histFractionalDiff.GetMaximum()
        minFracDiff = histFractionalDiff.GetMinimum()
        if abs(minFracDiff) > maxFracDiff: maxFracDiff = abs(minFracDiff)
        if abs(maxFracDiff) > epsilon:
            print("maximum disagreement of " + h1.GetName() + ": " + str(maxFracDiff))

        if maxFracDiff > thresh:
            print("Histograms {0} in component {1} mismatch!".format(h1.GetName(), newComponentName))
            print("Old entries: {0}, New entries: {1}".format(h2Entries, h1Entries))
            print("max fractional disagreement = {0}".format(maxFracDiff))
            if "area" in h1.GetName().lower():
                print("\tWARNING: \"area\" is in the mismatched histogram name. If this is related to jet area, note that jet area is not reliable to calculate from one run to the next. Consequently, this difference may be ignorable.")
            print("Print the histogram with the \"-p\" option to investigate more closely.")
            match = 0

    # Otherwise, do exact comparison
    else:
        maxValue = comparisonHist.GetBinContent(comparisonHist.GetMaximumBin())
        minValue = comparisonHist.GetBinContent(comparisonHist.GetMinimumBin())
        if abs(maxValue) > epsilon or abs(minValue) > 0 or h1Entries != h2Entries:
            print("Histograms {0} in component {1} mismatch!".format(h1.GetName(), newComponentName))
            print("Old entries: {0}, New entries: {1}".format(h2Entries, h1Entries))
            print("subtracted max = {0}".format(maxValue))
            print("subtracted min = {0}".format(minValue))
            if "area" in h1.GetName().lower():
                print("\tWARNING: \"area\" is in the mismatched histogram name. If this is related to jet area, note that jet area is not reliable to calculate from one run to the next. Consequently, this difference may be ignorable.")
            print("Print the histogram with the \"-p\" option to investigate more closely.")
            match = 0

    # Print histograms, if desired
    if  match == 0 and printPDF:
        ROOT.gStyle.SetOptStat(1)
        # Create canvas
        canvas = ROOT.TCanvas("canvas", "canvas")
        canvas.Divide(2)
        
        # Set options
        # Should be the same for both
        drawOptions = ""
        if h1.InheritsFrom(ROOT.TH2.Class()):
            drawOptions = "colz"
            rangeAxisH2 = h2.GetZaxis()
        elif h1.InheritsFrom(ROOT.TH1.Class()):
            rangeAxisH2 = h2.GetYaxis()
            
        # Get maximum and minimum from h1
        minVal = h1.GetMinimum()
        maxVal = h1.GetMaximum()
        # Set the limits on h2 so that they are the same
        rangeAxisH2.SetRangeUser(minVal, maxVal)

        # Draw
        canvas.cd(1)
        h1.Draw(drawOptions)
        canvas.cd(2)
        h2.Draw(drawOptions)
        
        # Label old and new corrections
        textOld = ROOT.TLatex()
        textOld.SetNDC()
        canvas.cd(1)
        textOld.DrawLatex(0.30,0.92,"New corrections")
        textOld = ROOT.TLatex()
        textOld.SetNDC()
        canvas.cd(2)
        textOld.DrawLatex(0.30,0.92,"Old corrections")

        # Save hists
        outputFilename = os.path.join("output", newComponentName + "__" + h1.GetName() + ".pdf")
        canvas.SaveAs(outputFilename)

        # Draw the subtracted histograms and the fractional difference histograms
        ROOT.gStyle.SetOptStat(0)
        canvasDiff = ROOT.TCanvas("canvasSubtr", "canvasSubtr")
        canvasDiff.Divide(2)
        
        minValSubtr = comparisonHist.GetMinimum()
        maxValSubtr = comparisonHist.GetMaximum()
        rangeAxisH2.SetRangeUser(minValSubtr, maxValSubtr)
        canvasDiff.cd(1)
        comparisonHist.Draw(drawOptions)

        # Label subtracted panel
        textSubtr = ROOT.TLatex()
        textSubtr.SetNDC()
        canvasDiff.cd(1)
        textSubtr.DrawLatex(0.27,0.92,"Subtracted Difference")
        
        if doStatisticalComparison:
          minValFrac = histFractionalDiff.GetMinimum()
          maxValFrac = histFractionalDiff.GetMaximum()
          rangeAxisH2.SetRangeUser(minValFrac, maxValFrac)
          
          if h1.InheritsFrom(ROOT.TH2.Class()):
            rangeAxisFrac = histFractionalDiff.GetZaxis()
          elif h1.InheritsFrom(ROOT.TH1.Class()):
            rangeAxisFrac = histFractionalDiff.GetYaxis()
          rangeAxisFrac.SetTitle("Fractional Difference")
          
          canvasDiff.cd(2)
          histFractionalDiff.Draw(drawOptions)

        # Label fractional difference panel
          textFracDiff = ROOT.TLatex()
          textFracDiff.SetNDC()
          canvasDiff.cd(2)
          textFracDiff.DrawLatex(0.27,0.92,"Fractional Difference")
          if divByZero:
            textDivZero = ROOT.TLatex()
            textDivZero.SetNDC()
            canvasDiff.cd(2)
            textDivZero.DrawLatex(0.39,0.40,"Warning: Div by zero!")
            textDivZero.DrawLatex(0.39,0.36,"See stdout.")

        outputFilenameDiff = os.path.join("output", newComponentName + "__" + h1.GetName() + "_Diff" + ".pdf")
        canvasDiff.SaveAs(outputFilenameDiff)
            
    return match

if __name__ == '__main__':
    # Define arguments
    parser = argparse.ArgumentParser(description="Compare histograms to test the new EMCal corrections framework")
    parser.add_argument("-f", "--inputFile", action="store",
                        type=str, metavar="filename",
                        default="AnalysisResults.root",
                        help="Path of AnalysisResults.root file")
    parser.add_argument("-n", "--newAnalysisList", action="store",
                        type=str, metavar="newAnalysisList",
                        default="",
                        help="Name of the list generated by your task run using the new EMCal corrections framework")
    parser.add_argument("-o", "--oldAnalysisList", action="store",
                        type=str, metavar="oldAnalysisList",
                        default="",
                        help="Name of the list generated by your task run using the old EMCal corrections framework")
    parser.add_argument("-p", "--printPDF", action="store_true",
                        help="Print a pdf of the difference between any mismatched histograms.")
    parser.add_argument("-s", "--doStatisticalComparison", action="store_true", 
                        help="Compare for statistical agreement (rather than exact agreement).")
    parser.add_argument("-t", "--thresh", action="store",
                        type=float, metavar="thresh", default=0.003,
                        help="Set threshold for statistical comparison -- fractional difference.")

    # Parse the arguments
    args = parser.parse_args()

    # AliEmcalCorrections list name
    correctionListName = "AliEmcalCorrectionTask_histos"
    #correctionListName = "AliEmcalCorrectionTask_embed_histos"

    # Set tracks, clusters, cells names
    # (They usually don't have to change. Standard AOD and ESD branches are handled automatically)
    cellsName = "emcalCells"
    clustersName = "caloClusters"
    tracksName = "tracks"
    #clustersName = "EmcCaloClusters"
    #tracksName = "AODFilterTracks"

    # Map from the new corrections to the old corrections to know where to look for a matching histogram
    newToOldCorrections = dict()
    newToOldCorrections["AliEmcalCorrectionCellEnergy"] = ["emcal_tender_plots"]
    newToOldCorrections["AliEmcalCorrectionCellBadChannel"] = ["emcal_tender_plots"]
    newToOldCorrections["AliEmcalCorrectionCellTimeCalib"] = ["emcal_tender_plots"]
    newToOldCorrections["AliEmcalCorrectionClusterizer"] = []
    newToOldCorrections["AliEmcalCorrectionClusterExotics"] = ["EmcalClusterMaker_{clusters}".format(clusters = clustersName)]
    newToOldCorrections["AliEmcalCorrectionClusterNonLinearity"] = ["EmcalClusterMaker_{clusters}".format(clusters = clustersName)]
    #newToOldCorrections["AliEmcalCorrectionClusterExotics"] = ["EmcalClusterMaker_{clusters}".format(clusters = "caloClusters_EmcCaloClusters")]
    #newToOldCorrections["AliEmcalCorrectionClusterNonLinearity"] = ["EmcalClusterMaker_{clusters}".format(clusters = "caloClusters_EmcCaloClusters")]
    newToOldCorrections["AliEmcalCorrectionClusterTrackMatcher"] = ["ClusTrackMatcher_{tracks}_{clusters}_histos".format(clusters = clustersName, tracks = tracksName)]
    newToOldCorrections["AliEmcalCorrectionClusterHadronicCorrection"] = ["HadCorr_{tracks}_{clusters}".format(clusters = clustersName, tracks = tracksName)]
    #newToOldCorrections["AliEmcalCorrectionClusterTrackMatcher_embed"] = ["ClusTrackMatcher_{tracks}_{clusters}_histos".format(clusters = clustersName, tracks = tracksName)]
    #newToOldCorrections["AliEmcalCorrectionClusterHadronicCorrection_embed"] = ["HadCorr_{tracks}_{clusters}".format(clusters = clustersName, tracks = tracksName)]

    if "ray-MBP" in socket.gethostname() or "ray-desktop" in socket.gethostname():
        args.inputFile = "../emcalCorrections/" + args.inputFile

    print("Comparing histograms with:")
    print("inputFile: \"{0}\"".format(args.inputFile))
    print("newAnalysisList: \"{0}\"".format(args.newAnalysisList))
    if args.oldAnalysisList != "":
        print("oldAnalysisList: \"{0}\"".format(args.oldAnalysisList))

    if not os.path.exists(args.inputFile):
        print("File \"{0}\" does not exist! Exiting!".format(args.inputFile))
        sys.exit(0)

    # Create necessary output directory
    if args.printPDF and not os.path.exists("output"):
        os.makedirs("output")

    runComparison(inputFile = args.inputFile, newAnalysisList = args.newAnalysisList, oldAnalysisList = args.oldAnalysisList, correctionListName = correctionListName, newToOldCorrections = newToOldCorrections, printPDF = args.printPDF, doStatisticalComparison = args.doStatisticalComparison, thresh = args.thresh)
