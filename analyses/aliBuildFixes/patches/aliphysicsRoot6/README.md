AliPhysics ROOT6 Patches
========================

`fastjetanRoot6.patch`: Fixes linking issues with the `FASTJETAN` library, which assumes libraries are `.so`.

`qnCorrectionsUndefinedSymbol.patch`: Fixes linking issues with the QnCorrections library. The interface needs to be linked to the library...

`lateJune2016.patch`: A range of patches needed around the end of June 2016. See the patch description for a detailed description of the fixes

`fastjetanAndQVectorRoot6.patch`: Combines the fastjetan and qvector patches for submission since they were tested against root5
