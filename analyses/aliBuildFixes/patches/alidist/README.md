alidist patches
===============

`enableRootHttpModule.patch`: Enables the `http` module in root, which is required to use TBufferJSON. Consequently, it is required for OVERWATCH!

`rootPythonPath.patch`: Sets the `$PYTHONPATH` variable properly when the root module is loaded
