AliBuild Fixes
==============

AliBuild has many differences from the `alice-env.sh` script, so some care must be taken. This document and the included patches serve to document some of these issues and changes.

Some notes:

- It is now possible to use multiple development packages. Just use aliBuild as expected, and it should work now!

- Running with `--disable GEANT3,GEANT4_VMC` is highly recommended! It will save substantial compilation time, and is almost assuredly not needed!

- When running `aliBuild -z -w ../sw .... build`, the `-w ../sw` is telling aliBuild that the `sw` directory is one below where you are currently location (it is just a bash command, so the path is exactly what you would expect). This is fine if you are in, for example, the `~/alice/ali-master` directory, but will not work if you are in a different directory! NOTE: If this is done incorrectly, it will still rebuild, but it will take a very long time, it won't be put in the right directory, and it probably won't work afterwards!


aliBuild Clean
--------------

If run with multiple development packages, (ie one for root5 and one for root6), then aliBuild clean will clean too aggressively! This a bug, tracked [here](https://github.com/alisw/alibuild/issues/325).

Patches
-------

There are currently (6/2016) two sets of patches:

- The `aliphysicsRoot6` directory contains patches to enable AliPhysics compilation when using root6. In principle, these could (and probably should) be committed to AliPhysics, I (RE), don't have time to test them out against root5, etc. So for now, or until they are fixed, they will be collected here.

- The `alidist` directory contains patches for the alidist repository. Most should be self explanatory, and are generally speaking, not required, but can be quite nice. For instance, the `PYTHONPATH` environment variable is not set (and I suspect that it is not because they would prefer for python not to be a dependency). However, this prevents python from loading ROOT correctly. So there is a patch to set the path correctly when loading the root module.

**If adding a patch, please add a short description in the README.md file in the respective folder!**

To apply a patch, go to the root of the source tree for that package (for example, for alidist, it might be `~/alice/ali-master/alidist`, but it depends on your particular setup), and use `git am path/to/patch`.
