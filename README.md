# ALICE Yale Dev

This repository contains all of the Yale ALICE development projects.

The projects are sorted according to the general category. Currently (Feb 2016), we have projects related to:

- EMCal
- Event Generators
- TPC Upgrade
- Hardware and voltage controls in our lab

A few projects have graduated from this repository:

- [ALICE OVERWATCH](https://github.com/raymondEhlers/OVERWATCH) - Online Monitoring via the HLT.
- [ALICE Docker CVMFS](https://github.com/raymondEhlers/aliceDockerCVMFS) - Run CVMFS on a local system via Docker in order to access ALICE software.

To gain commit access to the repository, please contact:

- Salvatore Aiola: <salvatore.aiola@yale.edu>
- Raymond Ehlers: <raymond.ehlers@yale.edu>
