#!/usr/bin/env python

from __future__ import print_function

# Handle python 2/3
try:
    input = raw_input
except NameError:
    pass

import os
import argparse
import yaml
import subprocess

class jobConfiguration(object):
    """ Contains the configuration of the job"""

    def __init__(self,
                 cmEnergy,
                 qHat,
                 nEvents,
                 eventsPerProcess,
                 outputDirPrefix,
                 generate = False,
                 phase1 = False,
                 add = False,
                 loadConfig = "",
                 ptHardBins = None):
        """ Initialize job configuration"""

        # Load configuration if it exists
        if loadConfig != "":
            if os.path.exists(loadConfig):
                # Load configuration
                self.loadConfiguration(loadConfig)
            else:
                print("Configuration at %s does not exist! Please resolve this and try again!" % args.loadConfig)
                exit(-1)

            # Add only makes sense if we have loaded a configuration (since one must exist to have already done anything)
            if self.add == True:
                self.alreadyCreatedProcesses = self.nProcesses
                # Use the nEvents that was passed and should be increased
                self.nEvents = self.nEvents + int(nEvents)
                self.nProcesses = self.alreadyCreatedProcesses + int(nEvents)//eventsPerProcess

        else:
            # This ensures that the pt hard bin tuple is not shared between instances of this class
            # See here for the method: https://stackoverflow.com/a/2681507
            if ptHardBins == None:
                ptHardBins = (5,11,21,36,57,84,117,152,191,234)
                # Lower
                #ptHardBins = (5,11,21,36,57,84,117)
                # Upper
                #ptHardBins = (117,152,191,234)
                # Remember that we could potentially add a 0-5 bin!
                #ptHardBins = (5,11,21,36,57,84,117,152)

            # Assign variables
            self.qHat = qHat
            self.cmEnergy = cmEnergy
            self.nEvents = int(nEvents)
            self.eventsPerProcess = int(eventsPerProcess)
            self.generate = generate
            self.phase1 = phase1
            self.loadConfig = loadConfig
            self.add = True
            self.alreadyCreatedProcesses = 0
            self.ptHardBins = ptHardBins

            # Determine variables based on the assigned variables above
            self.nProcesses = int(nEvents)//eventsPerProcess
            if qHat == 0:
                self.collisionType = "pp"
            else:
                self.collisionType = "PbPb"

        # Basic sanity check for configuration
        self.checkValidityOfConfiguration()

        # We now have a valid configuration and can proceed with setup
        # Setup directories
        self.outputDir = os.path.join(outputDirPrefix, "output", self.collisionType, "cmEnergy%i" % self.cmEnergy, "qHat%.2f" % self.qHat)
        shownDirCreationWarning = False
        if not os.path.exists(self.outputDir):
            raw_input("Going to creating output directories at %s. Press enter to continue or Ctrl-c to quit." % self.outputDir)
            shownDirCreationWarning = True
            os.makedirs(self.outputDir)

        self.outputDirsPtHard = []
        self.logDirs = []
        for ptHardIndex in xrange(0, len(ptHardBins) - 1):
            # Define directories
            outputDirPtHardTemp =  os.path.join(self.outputDir, "qPythiaPtHard_%i_%i" % (ptHardBins[ptHardIndex], ptHardBins[ptHardIndex+1]) )
            logDirTemp = os.path.join(outputDirPtHardTemp, "log")

            # Create if necessary
            if not os.path.exists(outputDirPtHardTemp ):
                if ptHardIndex == 0 and shownDirCreationWarning == False:
                    raw_input("Going to creating output directories at %s. Press enter to continue or Ctrl-c to quit." % self.outputDirPtHardTemp)
                print("Creating directory %s" % outputDirPtHardTemp)
                os.makedirs(outputDirPtHardTemp)
            if not os.path.exists(logDirTemp):
                if ptHardIndex == 0 and shownDirCreationWarning == False:
                    raw_input("Going to creating log directories at %s. Press enter to continue or Ctrl-c to quit." % self.logDirTemp)
                print("Creating directory %s" % logDirTemp)
                os.makedirs(logDirTemp)

            # Add them to variables
            self.outputDirsPtHard.append(outputDirPtHardTemp)
            self.logDirs.append(logDirTemp)

    def checkValidityOfConfiguration(self):
        """ Basic sanity check of the configuration.

        Imposes some very high upper bounds to ensure that the numbers are not complete nonsense.

        It implicity tests the type of the variables by casting them in print()."""

        invalidInput = False
        if self.qHat < 0 or self.qHat > 200:
            print("qhat = %f cannot be outside the range of 0-200!" % self.qHat)
            invalidInput = True
        if self.cmEnergy < 0 or self.cmEnergy > 6000:
            print("cmEnergy = %f cannot be outside the range of 0-6000!" % self.cmEnergy)
            invalidInput = True
        if self.nEvents < 0 or self.nEvents > 500000:
            print("nEvents %i cannot be outside the range of 0-500000!" % self.nEvents)
            invalidInput = True
        if self.eventsPerProcess < 0 or self.eventsPerProcess > 50000:
            print("eventsPerProcess %i cannot be outside the range of 0-50000!" % self.eventsPerProcess)
            invalidInput = True
        if self.nProcesses < 0 or self.nProcesses > 50:
            print("nProcesses %i cannot be outside the range of 0-50!" % self.nProcesses)
            invalidInput = True
        if "pp" not in self.collisionType and "PbPb" not in self.collisionType:
            print("Invalid collision type %s" % self.collisionType)
            invalidInput = True

        if invalidInput == True:
            print("Please resolve these issues and try again!")
            exit(-1)

    def loadConfiguration(self, configFileLocation):
        """Load configuration from file"""
        # Retreive values from the config file
        print("Loading configuration from %s" % configFileLocation)
        with open(configFileLocation, "rb") as configFile:
            configVariablesDict = yaml.load(configFile)

        # Assigns them to the object that it was called with
        for var in configVariablesDict:
            setattr(self, var, configVariablesDict[var])

        # Basic sanity check for configuration
        self.checkValidityOfConfiguration()

    def saveConfiguration(self, configFilename):
        """Save configuration to file"""
        # Get a list of all members
        # From: https://stackoverflow.com/a/13286863
        members = [var for var in vars(self)]

        # Create a dict to write out which holds all of the variables.
        tempDict = {}
        for member in members:
            # Retreive value
            valueToSave = getattr(self, member)
            tempDict[member] = valueToSave

        # Open config file
        print("Saving configuration to %s" % os.path.join(self.outputDir, configFilename))
        with open(os.path.join(self.outputDir, configFilename), "wb") as configFile:
            # Dump with YAML
            yaml.dump(tempDict, configFile)

    # Print methods
    def __repr__(self):
        return self.__str__()

    def __str__(self):
        returnValue = "jobConfiguration class:\n"
        members = [var for var in vars(self)]
        for member in members:
            returnValue += "%s: %s\n" % (member, getattr(self, member))
            #returnValue += "%s: %s %s\n" % (member, getattr(self, member), type(getattr(self, member)))

        return returnValue

def submitJob(jobConfig, generate, phase1, runOnHEP, ptHardIndex, firstJobToBeSubmitted):
    #aliroot -b -q runGenerateQPythia.C\($eventsPerProcess,$ptHardMin,$ptHardMax,$qHat,$cmEnergy,$runIndex,\"$outputDir\"\) \&> ${logDir}/qPythia.$runIndex.log
    alirootCommand = "aliroot -b -q runGenerateQPythia.C(%s,%s,%s,%s,%s,%s,\"%s\") \&> %s" % (
                      jobConfig.eventsPerProcess,
                      jobConfig.ptHardBins[ptHardIndex],
                      jobConfig.ptHardBins[ptHardIndex+1],
                      jobConfig.qHat,
                      jobConfig.cmEnergy,
                      "%s",
                      jobConfig.outputDir,
                      os.path.join(jobConfig.logDirs[ptHardIndex], "qPythia.%s.log"))

    #echo -e "\n../analysis/phase1 --inputFilename "$outputDir/qPythia.${fileIndex[$runIndex]}.root" --outputFilename "$outputDir/phase1.${fileIndex[$runIndex]}.root" --ptHardBin &> ${logDir}/phase1.${fileIndex[$runIndex]}.log"
    #../analysis/phase1 --inputFilename "$outputDir/qPythia.merge.root" --outputFilename "$outputDir/qPythia.phase1.root" --ptHardBin &> ${logDir}/phase1.$runIndex.log"
    phase1Command = "../analysis/phase1 --inputFilename \"%s\" --outputFilename \"%s\" --ptHardBin \&> %s" % (
                     os.path.join(jobConfig.outputDirsPtHard[ptHardIndex], "qPythia.%s.root"),
                     os.path.join(jobConfig.outputDirsPtHard[ptHardIndex], "phase1.%s.root"),
                     os.path.join(jobConfig.logDirs[ptHardIndex], "phase1.%s.log"))

    # We only need to check at the start.
    if firstJobToBeSubmitted == True:
        # Pauses input so that the user can evaluate if everything looks correct
        raw_input("About to submit jobs. Press enter to continue or Ctrl-c to quit.")

    # Save job configuration for future use. Only save if we are actually running jobs
    job.saveConfiguration("job.conf")

    if runOnHEP == True:
        # We do not need to file the commands here since they will be evaluated by bash
        if jobConfig.generate == True:
            command = alirootCommand % ("${PBS_ARRAYID}", "${PBS_ARRAYID}")
        else:
            command = phase1Command % ("${PBS_ARRAYID}", "${PBS_ARRAYID}", "${PBS_ARRAYID}")

        # Specifies options for the batch system
        # See: https://github.com/jdherman/hpc-submission-scripts/blob/master/submit-pbs-loop.py
        jobFile = """#!/usr/bin/env bash
        #PBS -q hep
        #PBS -l nodes=1
        #PBS -l walltime=24:00:00
        #PBS -m a
        #PBS -M raymond.ehlers@yale.edu
        #PBS -o /home/hep/caines/re239/test/${PBS_ARRAYID}.log
        #PBS -j oe

        # Load modules
        source /home/hep/caines/re239/loadModules.sh

        # Load aliroot
        source /scratch/hep/caines/re239/aliceSW/alice-env.sh -n 1

        # Set the proper working directory
        workingDir=${PBS_O_WORKDIR:-$PWD}

        # Move to the proper directory
        cd $workingDir 
        echo "Working Directory: $PWD"

        %s
        """ % command.replace("(","\(").replace(")", "\)").replace('"','\\"')
        # The replace commands above escape the parenthesis
        # See: https://stackoverflow.com/a/6275812

        # Write job file
        print(jobFile)
        jobFilename = os.path.join(jobConfig.outputDirsPtHard[ptHardIndex], "job.pbs")
        with open(jobFilename, "wb") as outputFile:
            outputFile.write(jobFile)

        # Need to run qsub with the jobFile that we created above
        #qsub -t 0-$nProcesses -v EVENTSPERPROCESS=$eventsPerProcess,PTHARDMIN=${ptHardBins[i]},PTHARDMAX=${ptHardBins[i+1]},QHAT=$qHat,CMENERGY=$cmEnergy,OUTPUTDIR=$outputDir batchGenerateEvents.pbs
        # -1 since counting starts at 0
        jobCommand = ["qsub", "-t", "%i-%i" % (jobConfig.alreadyCreatedProcesses, jobConfig.nProcesses-1), jobFilename]

        print(jobCommand)
        subprocess.call(jobCommand)

    else:
        # Run locally
        for jobIndex in xrange(jobConfig.alreadyCreatedProcesses, jobConfig.nProcesses):
            if jobConfig.generate == True:
                command = alirootCommand % (str(jobIndex), str(jobIndex))
            else:
                command = phase1Command % (str(jobIndex), str(jobIndex), str(jobIndex))

            # Seems best to let Popen handle the logs
            # Redundant with above, but it seems like the easiest wawy to handle this for now.
            logFilename = os.path.join(jobConfig.logDirs[ptHardIndex], "qPythia.%i.log" % jobIndex)
            # Remove log from command. -1 to remove the extra space
            command = command[:command.find("\&")-1]

            # Split the command for Popen
            commandList = command.split(" ")

            # Print the job information
            print(commandList)
            print(logFilename)
            with open(logFilename, "wb") as logFile:
                subprocess.Popen(commandList, stderr=subprocess.STDOUT, stdout=logFile)

def parseArguments():
    # Initialize arg parser
    parser = argparse.ArgumentParser(description="Submit batch jobs for qPythia generation and analysis. Must specify either generate or phase1!")

    # Version
    parser.add_argument("-v", "--version", action="version", version="%(prog)s 0.1")

    # Initialize arugment group
    runType = parser.add_mutually_exclusive_group(required=True)
    # Add arguments to mutually exclusive group
    runType.add_argument("-g","--generate", action="store_true",
                         default=False,
                         help="Run qPythia.")
    runType.add_argument("-p","--phase1", action="store_true",
                         default=False,
                         help="Run phase1 of the analysis.")

    # Add general arguments
    config = parser.add_argument_group("Configuration", "Configure the running options")
    config.add_argument("-q", "--qHat", action="store",
                        metavar="val", default=50,
                        type=int, help="Set qHat")
    config.add_argument("-e", "--cmEnergy", action="store",
                        metavar="energy", default=5020,
                        type=int, help="Set energy")
    config.add_argument("-n", "--nEvents", action="store",
                        default=100000,
                        type=int, help="Set number of events.")
    config.add_argument("--eventsPerProcess", action="store",
                        metavar="N", default=5000,
                        type=int, help="Set number of events per process.")
    config.add_argument("-b", "--ptHardBin", action="store",
                        metavar="index", default=None,
                        type=int, help="Select a particular pt hard bin")

    # Load configuration options
    loadConfig = parser.add_argument_group("Load Config", "Load a configuration from file")
    loadConfig.add_argument("-l", "--loadConfig", action="store",
                            metavar="configFilename", default = "",
                            type=str, help="Load a configuration from a file.")

    # Allow adding to the config already found
    # Must also select how many more events
    loadConfig = parser.add_argument_group("Add Events", "Use more events in addition to the ones already existing. Works with generate and phase1. Must specify a config file and number of events. May specify eventsPerProcess.")
    loadConfig.add_argument("-a","--add", action="store_true",
                            default=False,
                            help="Add to the number of events generated rather than replace them.")

    # Actually parse the args
    args = parser.parse_args()

    return args

if __name__ == "__main__":
    # try is here just to catch Ctrl-c
    try:
        # Handle arguments
        args = parseArguments()

        # Check arguments
        # Determine running type
        if args.generate == True:
            print("Generating events with qPythia!")
        else:
            print("Performing phase1 analysis!")
            # Check that the phase1 analysis is compiled
            if not os.path.exists(os.path.join("..", "analysis", "phase1")):
                print("WARNING: ../analysis/Phase1 not found! Running make!")
                # Just need to pass the directory where the Makefile is located
                subprocess.call(["make", "-C", os.path.join("..", "analysis")])
                if not os.path.exists(os.path.join("..", "analysis", "phase1")):
                    print("ERROR: Failed to compile phase1! Please investigate!")
                    exit(-1)

        # Check arguments to add
        if args.add == True and args.loadConfig == "":
            print("Error! Must specify a configuration file when adding to events!")
            exit(-1)

        # Determine running conditions
        # Check if running on HEP
        runOnHEP = False
        outputDirPrefix = ""
        if "hep" in os.getenv("HOME"):
        #if "hep" not in os.getenv("HOME"):
            runOnHEP = True
            outputDirPrefix = os.path.join("/scratch", "hep", "caines", "re239", "qPythia")

        # Create configuration
        # Loads the configuration if it exists
        job = jobConfiguration(cmEnergy = args.cmEnergy,
                               qHat = args.qHat,
                               nEvents = args.nEvents,
                               eventsPerProcess = args.eventsPerProcess,
                               outputDirPrefix = outputDirPrefix,
                               generate = args.generate,
                               phase1 = args.phase1,
                               add = args.add,
                               loadConfig = args.loadConfig)

        # Print job for user
        if args.ptHardBin != None:
            print("ptHardBinIndex = %i" % args.ptHardBin)
        print(job)

        # Only pause on submission for the first job submitted
        firstJobToBeSubmitted = True

        # Loop over pt hard bins to submit jobs
        # length-1 since we want bins, so for example, 5 values in the tuple is 4 bins, so we need to submit 4 jobs
        for ptHardIndex in xrange(0, len(job.ptHardBins)-1):
            # Skip bins if we are only running over one pt hard bin
            if args.ptHardBin != None and ptHardIndex != args.ptHardBin:
                continue

            # Submit the job
            submitJob(jobConfig = job, generate = args.generate, phase1 = args.phase1, runOnHEP = runOnHEP, ptHardIndex = ptHardIndex, firstJobToBeSubmitted = firstJobToBeSubmitted)

            # Set to false so that it does not continue to ask
            firstJobToBeSubmitted = False
    
    except KeyboardInterrupt:
        print("\nCaught Ctrl-c. Exiting!")
        exit(0)
