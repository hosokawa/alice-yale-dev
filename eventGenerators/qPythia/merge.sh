#!/usr/bin/env bash

# Run root over each of the qPythia dirs

if [[ $HOME == *"hep"* ]]; then
    source /scratch/hep/caines/sa639/alicesw/alice-env.sh -n 1
else
    source $HOME/aliceSW/alice-env.sh -n 1
fi

if [[ -z $1 ]];
then
    echo "Must give path for merging. Exiting"
    exit 1
fi

source enumeratePaths.sh

for path in ${paths[@]};
do
    root -l -b -q mergeFiles.cxx+\(\"$path\"\)
done
