#include <sstream>
#include <string>
#include <fstream>
#include <cstdlib>
#include <vector>
#include <cstdlib>

#include <TFile.h> 
#include <TString.h>
#include <TObjArray.h>
#include <TObjString.h>
#include <TTree.h>

TString findToken(TObjArray * input, Int_t index)
{
	// Replace D -> E to get it recognize scientific notation
	return ((TObjString *) input->At(index))->String().ReplaceAll("D","E");
}

void convertOscarToRoot(TString inputFilename = "YaJEM_OSC_defaultProfile", TString outputFilename = "yajemDefaultProfile")
{
	// Set input file
	inputFilename = Form("%s.dat", inputFilename.Data());
	std::ifstream inFile(inputFilename.Data());

	// Check if inFile is open before preceding
	if (inFile.is_open() == false)
	{
		Printf("inputFilename \"%s\" is not found!", inputFilename.Data());
		std::exit(1);
	}

	// Variables used for parsing input
	std::string line = "";
	TString input = "";

	// Store basic properties about the loops and events
	unsigned long lineCounter = 0;
	Int_t nParticles = 0;
	Int_t startOfEvent = 0;
	Int_t eventNumber = 0;

	// Allows us to ensure that the tree is always filled, even after the last event
	bool justFilledTree = false;

	// Vectors to store event properties in the tree
	std::vector <int> particleID;
	std::vector <double> px;
	std::vector <double> py;
	std::vector <double> pz;
	std::vector <double> energy;
	std::vector <double> mass;

	// Set correct outputFilename
	outputFilename = Form("output/%s.root", outputFilename.Data());

	// Print the parameters of the conversion
	Printf("inputFilename: %s", inputFilename.Data());
	Printf("outputFilename: %s", outputFilename.Data());

	// Setup output tree
	TFile * fOut = TFile::Open(outputFilename, "RECREATE");

	// Check if file is open before preceeding
	if (fOut->IsOpen() == kFALSE)
	{
		Printf("outputFilename \"%s\" is not opened!", outputFilename.Data());
		std::exit(1);
	}

	// Create TTree
	TTree * tree = new TTree("T","YaJEM output");
	
	// Set branches in tree
	// Event parameters
	tree->Branch("eventNumber", &eventNumber);
	//tree->Branch("nParticles", &nParticles);
	// Vectors
	tree->Branch("particleID", &particleID);
	tree->Branch("px", &px);
	tree->Branch("py", &py);
	tree->Branch("pz", &pz);
	tree->Branch("energy", &energy);
	tree->Branch("mass", &mass);

	while (std::getline(inFile, line))
	{
		lineCounter++;
		justFilledTree = false;

		input = line;

		// Ignore the first two lines
		// The third line should be used to extract parameters
		if (lineCounter < 4) { continue; }

		TObjArray * tokenizedInput = input.Tokenize(" ");
		if ( (lineCounter  == 4) || ((startOfEvent + nParticles) == lineCounter) )
		{
			// Set the parameters of the event
			// When it is invoked when lineCounter != 4, then we also need to fill our Branch
			if ( lineCounter != 4)
			{
				//printf("Filling tree!\n");
				tree->Fill();
				justFilledTree = true;	

				// Clear vectors
				particleID.clear();
				px.clear();
				py.clear();
				pz.clear();
				energy.clear();
				mass.clear();
			}

			// Setting up for next event
			// +1 because the line we are currently processing is the event header.
			//  Consequently, the event particle list starts on the next line.
			startOfEvent = lineCounter + 1;
			eventNumber = std::atoi( findToken(tokenizedInput, 0) );
			nParticles = std::atoi( findToken(tokenizedInput, 1) );
		}

		if ( (lineCounter >= startOfEvent) && (lineCounter < (startOfEvent + nParticles)) )
		{
			// Add particles to vector
			particleID.push_back(std::atoi( findToken(tokenizedInput, 1) ) );
			px.push_back(std::atof( findToken(tokenizedInput, 2) ) );
			py.push_back(std::atof( findToken(tokenizedInput, 3) ) );
			pz.push_back(std::atof( findToken(tokenizedInput, 4) ) );
			energy.push_back(std::atof( findToken(tokenizedInput, 5) ) );
			mass.push_back(std::atof( findToken(tokenizedInput, 6) ) );
		}

		if ((eventNumber % 1000 == 0) && (eventNumber != 0) && (lineCounter == (startOfEvent +1))) { printf("eventNumber = %d\n", eventNumber); }

		// Deleted because it is the users responsibility according to the root docs
		// Although it is called on TObjArray, only a single call to delete is needed
		delete tokenizedInput;
	}

	if (justFilledTree == false)
	{
		tree->Fill();
	}

	Printf("Finished conversion!");

	fOut->Write();
	fOut->Close();
}

int main(int argc, char * argv[])
{
	TString inputFilename = "";
	TString outputFilename = "";

	if (argc == 3)
	{
		inputFilename = argv[1];
		outputFilename = argv[2];
	}
	else
	{
		if (argc != 1)
		{
			Printf("Must specifiy the input and output filename, or use the defaults by passing no arguments.");
			Printf("Defaults: inputFilename = \"YaJEM_OSC_defaultProfile\"");
			Printf("          outputFilename = \"yajemDefaultProfile\"");
			std::exit(1);
		}
	}

	/*Printf("inputFilename: %s", inputFilename.Data());
	Printf("outputFilename: %s", outputFilename.Data());

	std::exit(1);*/

	if (inputFilename == "")
	{
		convertOscarToRoot();
	}
	else
	{
		convertOscarToRoot(inputFilename, outputFilename);
	}

	return 0;
}
