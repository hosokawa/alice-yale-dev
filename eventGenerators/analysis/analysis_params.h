#ifndef ANALYSIS_PARAMS_H
#define ANALYSIS_PARAMS_H

// include file for pt bins, and cut information.

  // Choices for 2D background subtraction
  const int Nbkg2DMethods = 4;  
  const char *bkg2DMethodArray[Nbkg2DMethods] = {"NoBkgSub","2DBkgSub","dEtaBkgSub","dEtaFarSub"};
/*
  const int aNoBkgSub = 0;
  const int a2DFitSub = 1;
  const int aDEtaFitSub = 2;
  const int aDEtaFarSub = 3;
*/
  // Choices for dPhi Projection Fit Function
  const int NdPhiFitMethods = 6;  
  const char *dPhiFitMethodArray[NdPhiFitMethods] = {"1G1G","1M1M","2G1M","2G1GG","1GG1GG","2G1SH"};
  // Format a[NSFit][ASFit]
  // G = Gaussian
  // GG = Generalized Gaussian 
  // MG = Modified Gaussian
  // SH = sech(|x|^beta)

  // Both Fwhm and Sigma versions are used

/*  const int a1M1M = 0; 
  const int a2G1M = 1;
  const int a2G1GG = 2;
  const int a2G1SH = 3;
  // const int a1G1G = 4; // Does this still work?
*/
  //Choices for dPhi Projection Fit Background
  const int NdPhiBkgMethods = 3;  
  const char *dPhiBkgMethodArray[NdPhiBkgMethods] = {"NoDPhiBkg","FreeDPhiBkg","ZYAM"};

//  int aNoDPhiBkg = 0; // B = 0
//  int aFreeDPhiBkg = 1; // B is a free parameter
//  int aZYAM = 2;  // B is fixed by ZYAM



 // enum { nJetPtBins = 4, nParticlePtBins = 11 };
  int nJetPtBins = 4;
  int nParticlePtBins = 11;


  // Define the pt bins
  // Size of the bins is from STAR Jet-h paper
  // Desired bins are 0 - 2 based on the convention below.
//std::vector <double> jetPtBins = {10,15,20,40};
std::vector <double> jetPtBins = {10,15,20,40,60};
  // Desired bins are 0 - 10 based on the convention below.
std::vector <double> particlePtBins = {0, 0.25, 0.5, 1, 1.5, 2, 3, 4, 6, 8, 12, 17};

// Bins for HighPt analyses: eg. soft drop
	int nHighPtBins = 12;
std::vector <double> highPtBins = {20,40,60,80,100,120,140,160,180,200,250,300,500};


	// Minimum pt for all particles
	double particle_pt_min = 0.2;
	// Minimum pt for particles included in Jet Reconstruction
	// Warning: this can be changed in run-time by phase1.cc 
  double jet_constituent_cut = 2.0;
	// Minimum pt for at least one constituent of a jet with a hard core
  // used for specifc analyses.  For a global hard core cut on jets, see
  // globalHardCoreCut
	double jet_hard_core_cut = 6.0;
	double eta_cut = 1.0;

// jet cone radius / resolution parameter
	double R = 0.4;

	double eta_range = 2. * (eta_cut  - R);

  bool requireHardCore = false;
  double globalHardCoreCut = 6.0;

  double c_width = 600;
  double c_height = 600;

  bool use2DFit = false;
  bool useSecHFit = false;
  bool useModGausFit = true;
  bool useZYAM = true;

  // default settings
  int bkg2DMethod = 0;
  int dPhiFitMethod = 0;
  int dPhiBkgMethod = 1;



  bool doJetBkgSub = false;  // use backgronud subtraction for jetPt bins? (affects surf bias, jet-h correlations)

	bool noAwaySide = false; // Switch just for comparing the nearside peak from single peak simulations.
	bool noNearSide = false; // Switch just for comparing the awayside peak from single peak simulations.
	bool noFitting = false; // Don't do final fits, just show pre-fits.

	bool plotSigmaFits = true; // Use the sigma fits when ploting azimuthal projections with fits


#endif
