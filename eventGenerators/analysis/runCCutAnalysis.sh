#!/bin/bash
# Designed for Omega
# Runs phase1 analysis with different consituent cuts


if [ "$#" == "0" ] ; then
    echo "Usage $0 root/path/title_CCUT_"
    exit 1
fi

MPATH=$1
if [ ! -f "$MPATH""0.hist.root" ] ; then 
    echo "Error: $MPATH""* files not found!"
    exit 1
fi

# could just use the .0.hist.root file to get path
# but this would break without .0. files
# could alternately have a loop through all the 
# $MPATH.$n.hist.root files until on is found that exists


for name in `ls $MPATH*.hist.root`
do
    title=`echo $name | sed -e 's/.hist.root//' | sed -e "s/root\/[a-z]*\///" `
    echo $title
    JPATH=`echo $title | sed -e "s/\/.*//"`
    JCCUTLABEL=`echo $title | sed -e "s/.*CCUT_//"`
    JPATH="output/$JPATH/ccut_$JCCUTLABEL/"
    if [ ! -e "$JPATH" ] ; then 
      mkdir $JPATH
    fi 
#    echo $JPATH
#    echo $JCCUTLABEL
   
    echo "./analysis/phase2 $name output/$title.final.root "
    ./analysis/phase2 $name output/$title.final.root
    echo "mv output/*.pdf $JPATH"  
    mv output/*.pdf $JPATH

#    ./analysis/merge.sh $title
done


#./analysis/merge.sh $MPATH$a
#./analysis/merge.sh root/path/title





