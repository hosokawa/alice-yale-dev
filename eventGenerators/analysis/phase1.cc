// phase1 : take root files, run jetfinding and cuts, make histograms
// 
// Can compile with:
//  make phase1
//
//  or
//
//  g++ -Wl,--no-as-needed -std=c++11 phase1.cc -o phase1 -I`root-config --incdir` `root-config --libs` `fastjet-config --cxxflags --libs --plugins`
//
//  or (if you have readableStyle.h)
//
//  make USER_DEFINED=-DRE_STYLE=1 basicAnalysis

#ifdef RE_STYLE
#include <readableStyle.h>
#endif

#define PI TMath::Pi()

#include <vector>
#include <cmath>
#include <iostream>
#include <cstdlib>
#include <algorithm>
#include <string>
#include <iomanip>

#include <fastjet/ClusterSequence.hh>
#include <fastjet/AreaDefinition.hh>
#include <fastjet/ClusterSequenceArea.hh>

#include <TPDGCode.h>
#include <TFile.h>
#include <TString.h>
#include <TTree.h>
#include <TBranch.h>
#include <TLorentzVector.h>
#include <TH1F.h>
#include <TH2F.h>
#include <TH3F.h>
#include <TH1D.h>
#include <TF1.h>
#include <TMath.h>
#include <TCanvas.h>
#include <TAxis.h>
#include <TGraph.h>
#include <TGraphErrors.h>
#include <TLegend.h>
#include <TIterator.h>
#include <TObject.h>

#include "fastjet/contrib/SoftDrop.hh"

#include "analysis_params.h"

using namespace fastjet::contrib;

// Functions are defined at the end of file
void printHelp(std::string input = "");
void parseInput(int argc, char * argv[], TString & inputFilename, TString & outputFilename, double & jet_constituent_cut, bool & ptHardBinFlag);

// Function from Marco. Useful for handling pt hard bins with only a few counts
void removeOutliers(TH1* h1, Int_t minBinValue = 2) {
  // set bins with less than minBinValue entries to 0
  Int_t nbins = h1->GetNbinsX()+2;
  TH2* h2 = dynamic_cast<TH2*>(h1);
  if (h2 && h2->GetNbinsY() != 0)
    nbins *= h2->GetNbinsY()+2;
  TH3* h3 = dynamic_cast<TH3*>(h1);
  if (h3 && h3->GetNbinsZ() != 0)
    nbins *= h3->GetNbinsZ()+2;
  std::cout << "hist " << h1->GetName() << " nbins " << nbins << std::endl;
  for (Int_t ibin = 0; ibin < nbins; ibin++) {
    if (h1->GetBinContent(ibin) < minBinValue && h1->GetBinContent(ibin) > 0) {
      //cout << "Histo: " << h1->GetName() << " setting bin " << ibin << " to zero"<< endl;
      h1->SetBinContent(ibin,0);
    }
  }
}

// Accept particles within abs(eta) < 1, pt > 0.2 GeV
bool rejectParticle(const fastjet::PseudoJet & particle)
{
  return !( (particle.pt() > particle_pt_min) && (TMath::Abs(particle.eta()) < eta_cut) );
}


bool rejectJet(const fastjet::PseudoJet & jet)
{
  bool rejectFlag = ! jet.has_constituents();
  // Eta cut on jets. We only want jets within eta-R 
  if (requireHardCore == true) { 
 
    //FIXME FIXME
    // don't remember why I put two of these tags here, but I'd better leave them
    // to be on the safe side 
    rejectFlag = true; 

    std::vector <fastjet::PseudoJet> particles = jet.constituents();
    for (unsigned int i = 0; i < particles.size(); i++)
    {
      if ( particles.at(i).pt() >= globalHardCoreCut) { rejectFlag = false; break; }
      //			if ( particles.at(i).pt() >= jet_constituent_cut) { rejectFlag = false; break; }
    }
  }
  if ((rejectFlag == false) && (TMath::Abs(jet.eta()) >= (eta_cut-R))) { rejectFlag = true; }

  return rejectFlag;
}

bool rejectJetHardCore(const fastjet::PseudoJet & jet, double R, double cut)
{
  bool rejectFlag = true;
  if (jet.has_constituents() == true)
  {
    std::vector <fastjet::PseudoJet> particles = jet.constituents();
    for (unsigned int i = 0; i < particles.size(); i++)
    {
      if ( particles.at(i).pt() >= cut) { rejectFlag = false; break; }
      //			if ( particles.at(i).pt() >= jet_constituent_cut) { rejectFlag = false; break; }
    }
  }
  // Eta cut on jets. We only want jets within eta-R 
  if ((rejectFlag == false) && (TMath::Abs(jet.eta()) >= (eta_cut-R))) { rejectFlag = true; }

  return rejectFlag;
}

// method for Dijet.  reject jets whose hard (p_t > 2 GeV/c) constituents
// add up to less than 10 (or 20) GeV/c

bool rejectJetDijet(const fastjet::PseudoJet & jet, double R, double sum_cut)
{
  bool rejectFlag = true;
  double sum_hard = 0;
  if (jet.has_constituents() == true)
  {
    std::vector <fastjet::PseudoJet> particles = jet.constituents();
    for (unsigned int i = 0; i < particles.size(); i++)
    {
      if (particles.at(i).pt() >= 2) sum_hard += particles.at(i).pt();

      //			if ( particles.at(i).pt() >= jet_constituent_cut) { rejectFlag = false; break; }
    }
  }
  if (sum_hard > sum_cut) rejectFlag = false;

  // Eta cut on jets. We only want jets within eta-R 
  if ((rejectFlag == false) && (TMath::Abs(jet.eta()) >= (eta_cut-R))) { rejectFlag = true; }

  return rejectFlag;
}

double getHighestPtConst(const fastjet::PseudoJet & jet) {	
  double highest = 0;
  if (jet.has_constituents())
  {
    std::vector <fastjet::PseudoJet> particles = jet.constituents();
    for (unsigned int i = 0; i < particles.size(); i++)
    {
      highest = TMath::Max(highest,particles.at(i).pt());
    }
  }
  return highest;
}


// Put deltaPhi in the range of [-Pi/2, 3*Pi/2]
double calculateDeltaPhi(const double phiOne, const double phiTwo)
{
  double deltaPhi = phiOne - phiTwo;
  if (deltaPhi < -TMath::Pi()/2)
  {
    deltaPhi = deltaPhi + 2*TMath::Pi();
  }
  if (deltaPhi > 3*TMath::Pi()/2)
  {
    deltaPhi = deltaPhi - 2*TMath::Pi();
  }

  return deltaPhi;
}

// particles
/** list
 kGamma,             // Photon
 kElectron,          // Electron
 kMuonPlus,          // Muon 
 kPiPlus,            // Pion
 kKPlus,             // Kaon
 kK0Short,           // K0s
 kK0Long,            // K0l
 kProton,            // Proton 
 kNeutron,           // Neutron
 kLambda0,           // Lambda_0
 kSigmaMinus,        // Sigma Minus
 kSigmaPlus,         // Sigma Plus
 3312,               // Xsi Minus 
 3322,               // Xsi 
 3334,               // Omega
 kNuE,               // Electron Neutrino 
 kNuMu,              // Muon Neutrino
 kNuTau              // Tau Neutrino


 pi0??
 kPi0
 */

bool isNeutrino(int pid) {
  pid = TMath::Abs(pid);
  return pid == kNuE || pid == kNuMu || pid == kNuTau;
}


//It would probably be faster to use define statements for these functions,
// to minimize function calls during operation
bool isChargedHadron(int pid) {
  /*  list of stable particles:
    22,             // Photon
    11,          // Electron
    13,          // Muon 
    ,            // Pion
    kKPlus,             // Kaon
    kK0Short,           // K0s
    kK0Long,            // K0l
    kProton,            // Proton 
    kNeutron,           // Neutron
    kLambda0,           // Lambda_0
    kSigmaMinus,        // Sigma Minus
    kSigmaPlus,         // Sigma Plus
    3312,               // Xsi Minus 
    3322,               // Xsi 
    3334,               // Omega
    kNuE,               // Electron Neutrino 
    kNuMu,              // Muon Neutrino
    kNuTau              // Tau Neutrino
    */

  pid = TMath::Abs(pid);
  return pid==kProton || pid == kPiPlus || pid == kKPlus || pid == kSigmaPlus || pid == 3312 || pid == 3334;
}

bool isGamma(int pid) {
  return pid == kGamma;
}

bool isNeutralHadron(int pid) {
  pid = TMath::Abs(pid);
  return pid==kNeutron || pid == kPi0 || pid == kK0Short || pid == kK0Long || pid == kLambda0 || pid == 3322 ;
}


// get background from perpendicular cone away from jet.
double getBackground(fastjet::PseudoJet *thisJet, std::vector <fastjet::PseudoJet> *particles) {
  double rho = 0;
  double area = 0;
  double dEtaRange =  0.2;
  double dPhiRange =  PI / 4.0;

  for (int i = 0; i < particles->size(); i++) {
    fastjet::PseudoJet *part = &particles->at(i);
    // printf("particle at (eta,phi) = (%f,%f)\t\t",part->eta(),part->phi());
    //rho+= particles->at(i).pt();
    double dEta = part->eta() - thisJet->eta();
    double dPhi = part->delta_phi_to(*thisJet);
    //  printf(" DEta,DPhi = %f,%f\t",dEta,dPhi);      
    if (TMath::Abs(dEta) > dEtaRange || TMath::Abs(TMath::Abs(dPhi)- PI/2) > dPhiRange || part->pt() < jet_constituent_cut) {
      //   printf("\n");
      continue;
    }
    //  printf("Accepted!\n");
    rho+= part->pt();
  }
  // Divide by area
  rho = rho / (8 * dEtaRange * dPhiRange);
  return rho;
}

void phase1(TString inputFilename, TString outputFilename, double jet_constituent_cut, bool ptHardBinFlag)
{
  // Open input file
  //	inputFilename = Form("output/%s.root", inputFilename.Data());
  TFile * fIn = TFile::Open(inputFilename.Data());

  // Check if file is open
  if (fIn->IsOpen() == kFALSE)
  {
    Printf("inputFilename \"%s\" is not found!", inputFilename.Data());
    std::exit(1);
  }

  // The name of the tree is "T"
  TTree * tree = (TTree *) fIn->Get("T");

  TTree * runInfo = (TTree *) fIn->Get("runInfo");


  // Define our basic variables
  // Basic types don't need to be pointers, but classes do
  //Int_t eventNumber = 0;
  //Int_t nParticles = 0;
  std::vector <int> * particleID = 0;
  std::vector <double> * px = 0;
  std::vector <double> * py = 0;
  std::vector <double> * pz = 0;
  std::vector <double> * energy = 0;
  std::vector <double> * mass = 0;
  // for weighted data:
  double weight = 0;

  double vertex_x = 0;
  double vertex_y = 0;
  std::vector <int> * status = 0;

  double total_weight = 0;

  // Set Branches to our variables
  //tree->SetBranchAddress("eventNumber", &eventNumber);
  //tree->SetBranchAddress("nParticles", &nParticles);
  tree->SetBranchAddress("particleID", &particleID);
  tree->SetBranchAddress("px", &px);
  tree->SetBranchAddress("py", &py);
  tree->SetBranchAddress("pz", &pz);
  tree->SetBranchAddress("energy", &energy);
  tree->SetBranchAddress("mass", &mass);


  bool vertexOn = 0 != tree->GetBranch("vertex_x");
  if (vertexOn) {
    tree->SetBranchAddress("vertex_x",&vertex_x);
    tree->SetBranchAddress("vertex_y",&vertex_y);
  }
  bool statusOn = 0 != tree->GetBranch("status");
  if (statusOn) tree->SetBranchAddress("status",&status);

  //soft drop analysis
  double z_cut = 0.10;
  double beta  = 0.0;
  SoftDrop sd(beta,z_cut);
//  SoftDrop sd(z_cut,beta);

  //checking whether weighting is on.  For YaJEM output, weighting might 
  // not be present.
  // TODO: Make an argument
  bool weightingOn = 0 != tree->GetBranch("weight");
  if (weightingOn) {
    printf("Weighting is on.\n");    
    tree->SetBranchAddress("weight",&weight);
  } else {
    if ( ptHardBinFlag == true )
    {
      printf("Pt Hard Weighting is on. Weighting is set per run.\n");

      // Retreive run info
      // Define temporary variables to calculate weight
      double crossSection = 0;
      Int_t nEvents = 0;
      double totalCrossSection = 0;
      double totalEvents = 0;
      // For this case, it is weighted per run, so the "totalWeight" branch is just the weight for each run.
      // TFileMerger cannot sum up the values itself, so it is calculated here.
      runInfo->SetBranchAddress("crossSection", &crossSection);
      runInfo->SetBranchAddress("nEvents", &nEvents);
      for (Int_t i = 0; i < runInfo->GetEntries(); i++)
      {
        runInfo->GetEntry(i);
        totalCrossSection += crossSection;
        totalEvents += nEvents;
      }
      weight = totalCrossSection / ((Double_t) totalEvents);
    }
    else
    {
      printf("Weighting is off.  All events set to equal weight.\n");
      weight = 1;
    }
  }
  printf("Jet resolution paramter R = %f\n",R);



  fastjet::JetDefinition jetDef(fastjet::antikt_algorithm, R);
  double ghost_maxrap= 1.5;
  double ghost_area = 0.01;



  // Use either TLorentzVector or fastjet::PseudoJet for the particles
  // Need to use fastjet::PesudoJet for jets regardless
  //std::vector <TLorentzVector> particles;
  //vector for all particles 
  std::vector <fastjet::PseudoJet> particles;
  // subset of particles that are scattering centers from JEWEL
  std::vector <fastjet::PseudoJet> scatCents; 



  // vector for particles to be used for jet reconstruction
  std::vector <fastjet::PseudoJet> constParticles;
  std::vector <fastjet::PseudoJet> jets;
  //std::vector <fastjet::PseudoJet> rec_jets;

  std::vector <fastjet::PseudoJet> hardScatters;


  TString jetClass = "jetPt_%.0f_%.0f";
  TString particleClass = "particlePt_%.2f_%.2f";
  TString combinedClass = "";



  // Define histograms
  TH2F * dEtaDPhi = new TH2F("dEtaDPhi", "dEtaDPhi", 100, -2, 2, 100, -TMath::Pi()/2, 3.0*TMath::Pi()/2);
  TH2F * etaPhi = new TH2F("etaPhi", "etaPhi", 100, -5, 5, 100, 0, 2.0*TMath::Pi());
  TH2F * etaPt = new TH2F("etaPt", "etaPt", 100, -5, 5, 100, 0, 100);
  TH1F * dijetAj = new TH1F("dijetAj","A_{j};A_{j}",100,0,1); dijetAj->Sumw2();
  TH2F * dijetAjJetPt = new TH2F("dijetAjJetPt","A_{j} vs p_{t}^{jet};p_{t}^{jet};A_{j}",(int) nJetPtBins, (double *) &jetPtBins[0],100,0,1); dijetAjJetPt->Sumw2();
  TH1F * dijetXj = new TH1F("dijetXj","X_{j};X_{j}",100,0,1); dijetXj->Sumw2();
  TH2F * dijetXjJetPt = new TH2F("dijetXjJetPt","x_{j} vs p_{t}^{jet};p_{t}^{jet};x_{j}",(int) nJetPtBins, (double *) &jetPtBins[0],100,0,1); dijetXjJetPt->Sumw2();



  TH1F * vertexR = new TH1F("vertexR", "vertexR", 100, 0, 15);
  TH1F * vertexRHardCore = new TH1F("vertexRHardCore", "vertexRHardCore", 100, 0, 15);
  TH1F * vertexRNoHardCore = new TH1F("vertexRNoHardCore", "vertexNoHardCore", 100, 0, 15);
  // testing the leading and subleading cut's effect.
  TH1F * vertexRDijetCut = new TH1F("vertexRDijetCut","vertexRDijetCut;R_{vertex} (fm)",100,0,15);
  TH2F * vertexXY = new TH2F("vertexXY", "vertexXY", 100, -10, 10, 100, -10, 10);
  TH2F * vertexXYHardCore = new TH2F("vertexXYHardCore", "vertexXYHardCore", 100, -10, 10, 100, -10, 10);
  TH2F * vertexXYNoHardCore = new TH2F("vertexXYNoHardCore", "vertexXYNoHardCore", 100, -10, 10, 100, -10, 10);
  //vertex rotated so that the direction of the jet is -x
  // as in the plots made by T. Renk for his jet-h corr theory paper
  TH2F * vertexXYRot = new TH2F("vertexXYRot", "vertexXYRot", 100, -10, 10, 100, -10, 10);
  TH2F * vertexXYRotHardCore = new TH2F("vertexXYRotHardCore", "vertexXYRotHardCore", 100, -10, 10, 100, -10, 10);
  TH2F * vertexXYRotNoHardCore = new TH2F("vertexXYRotNoHardCore", "vertexXYRotNoHardCore", 100, -10, 10, 100, -10, 10);
  // binned by jet bin pt
  //	TH3F * vertexXYRotJetPt = new TH3F("vertexXYRotJetPt", "vertexXYRotJetPt", 100, -10, 10, 100, -10, 10,(int) nJetPtBins, (double *) &jetPtBins[0]);
  //	TH3F * vertexXYRotJetPtHardCore = new TH3F("vertexXYRotJetPtHardCore", "vertexXYRotJetPtHardCore", 100, -10, 10, 100, -10, 10,(int) nJetPtBins, (double *) &jetPtBins[0]);
  
  TH2F * ptLossVertexRLeadingJet = new TH2F("pttLossVertexRLeadingJet","Leading Jet p_{T} Loss vs Hard Scatter R;R_{h.s.} (fm);p_{T}^{parton} - p_{T}^{jet} (GeV/c)",25,0,7.5,50,0,50);
  TH2F * ptLossVertexRSubleadingJet = new TH2F("ptLossVertexRSubleadingJet","Sub-Leading Jet p_{T} Loss vs Hard Scatter R;R_{h.s.} (fm);p_{T}^{parton} - p_{T}^{jet} (GeV/c)",25,0,7.5,50,0,50);


  // Hard Scatter information
  TH1F * qqbar_PartonPtByJetBin[nJetPtBins];
  TH1F * qq_PartonPtByJetBin[nJetPtBins];
  // Gluon as leading jet
  TH1F * gq_PartonPtByJetBin[nJetPtBins];
  // Quark as leading jet
  TH1F * qg_PartonPtByJetBin[nJetPtBins];
  TH1F * gg_PartonPtByJetBin[nJetPtBins];



  TH2F * vertexXYRotJetPt[nJetPtBins];
  TH2F * vertexXYRotJetPtHardCore[nJetPtBins];

  for (unsigned int i = 0; i < nJetPtBins; i++)
  {
    // could update combinedClass paradigm to make labeling axes easier
    combinedClass = Form("vertexXYRotJetPt_" + jetClass,jetPtBins.at(i),jetPtBins.at(i+1));
    vertexXYRotJetPt[i] = new TH2F(combinedClass,combinedClass, 100,-10,10,100,-10,10);
    vertexXYRotJetPt[i]->GetXaxis()->SetTitle("x (fm)");
    vertexXYRotJetPt[i]->GetYaxis()->SetTitle("y (fm)");


    combinedClass = Form("vertexXYRotJetPtHardCore_" + jetClass,jetPtBins.at(i),jetPtBins.at(i+1));
    vertexXYRotJetPtHardCore[i] = new TH2F(combinedClass,combinedClass, 100,-10,10,100,-10,10);

    vertexXYRotJetPtHardCore[i]->GetXaxis()->SetTitle("x (fm)");
    vertexXYRotJetPtHardCore[i]->GetYaxis()->SetTitle("y (fm)");


    // Hard Scatter inaformation
    combinedClass = Form(jetClass,jetPtBins.at(i),jetPtBins.at(i+1));
    qqbar_PartonPtByJetBin[i] = new TH1F(Form("qqbar_PartonPtByJetBin_%s",combinedClass.Data()),Form("qqbar_PartonPtByJetBin_%s;p_{T}^{jet} (GeV/c)",combinedClass.Data()), 200,0,200);
    qqbar_PartonPtByJetBin[i]->Sumw2();
    qq_PartonPtByJetBin[i]  = new TH1F(Form("qq_PartonPtByJetBin_%s",combinedClass.Data()),Form("qq_PartonPtByJetBin_%s;p_{T}^{jet} (GeV/c)",combinedClass.Data()), 200,0,200);
    qq_PartonPtByJetBin[i]->Sumw2();
    // Gluon as leading jet
    gq_PartonPtByJetBin[i]  = new TH1F(Form("gq_PartonPtByJetBin_%s",combinedClass.Data()),Form("gq_PartonPtByJetBin_%s;p_{T}^{jet} (GeV/c)",combinedClass.Data()), 200,0,200);
    gq_PartonPtByJetBin[i]->Sumw2();
    // Quark as leading jet
    qg_PartonPtByJetBin[i]  = new TH1F(Form("qg_PartonPtByJetBin_%s",combinedClass.Data()),Form("qg_PartonPtByJetBin_%s;p_{T}^{jet} (GeV/c)",combinedClass.Data()), 200,0,200);
    qg_PartonPtByJetBin[i]->Sumw2();
    gg_PartonPtByJetBin[i]  = new TH1F(Form("gg_PartonPtByJetBin_%s",combinedClass.Data()),Form("gg_PartonPtByJetBin_%s;p_{T}^{jet} (GeV/c)",combinedClass.Data()), 200,0,200);
    gg_PartonPtByJetBin[i]->Sumw2();

//PartonPtByJetBin

  }


  TH2F * vertexXYRotDijet = new TH2F("vertexXYRotDijet", "vertexXYRotDijet", 100, -10, 10, 100, -10, 10);
  TH2F * vertexXRotJetPt = new TH2F("vertexXRotJetPt","x_{rot} vs p_{t}^{jet};p_{t}^{jet} (GeV/c);x_{rot} (fm)",(int) nJetPtBins, (double *) &jetPtBins[0],100,-15,15);
  TH2F * vertexXRotJetPtHardCore = new TH2F("vertexXRotJetPtHardCore","x_{rot} vs p_{t}^{jet} (Hard Core cut);p_{t}^{jet} (GeV/c);x_{rot} (fm)",(int) nJetPtBins, (double *) &jetPtBins[0],100,-15,15);
  TH2F * vertexHighestPtXRot = new TH2F("vertexHighestPtXRot","vertexHighestPtXRot;p_{T}^{HC};x (fm)",100,0,100,100,-15,15);	vertexHighestPtXRot->Sumw2();


  // vertexR as function of jetPt:
  TH2F * vertexJetPtR = new TH2F("vertexJetPtR","vertexJetPtR;p_{T}^{jet} (GeV/c);R (fm)",(int) nJetPtBins, (double *) &jetPtBins[0],100,0,15);
  TH2F * vertexJetPtRHardCore = new TH2F("vertexJetPtRHardCore","vertexJetPtRHardCore;p_{t}^{jet} (GeV/c);R (fm)",(int) nJetPtBins, (double *) &jetPtBins[0],100,0,15);
  TH2F * vertexJetPtRDijet = new TH2F("vertexJetPtRDijet","vertexJetPtRDijet;p_{t}^{jet} (GeV/c);R (fm)",(int) nJetPtBins, (double *) &jetPtBins[0],100,0,15); vertexJetPtRDijet->Sumw2();
  TH2F * vertexDijetAjR = new TH2F("vertexDijetAjR","R_{vertex} vs A_{j}^{Dijet}",100,0,1,25,0,15); vertexDijetAjR->Sumw2();


  TH2F * vertexHighestPtR = new TH2F("vertexHighestPtR","vertexHighestPtR;p_{T}^{HC}",100,0,100,100,0,15);	vertexHighestPtR->Sumw2();
  TH3F * vertexXYRotAjDijet = new TH3F("vertexXYRotAjDijet","vertexXYRotAjDijet;x (fm);y (fm); A_{j}",100,-10,10,100,-10,10,5,0,1);	vertexXYRotAjDijet->Sumw2();

  TH2F * vertexHighestJetZR = new TH2F("vertexHighestJetZR","Vertex R vs Highest z;z;R (fm)",100,0,1,100,0,15);
  TH2F * vertexHighestJetZXRot = new TH2F("vertexHighestJetZXRot","X_{rot} vs Highest z;z;x (fm)",100,0,1,100,0,15);

  // Studies for the Hadron-Lambda/k0 study
  TH2F * vertexXYRot4GeVTrigger = new TH2F("vertexXYRot4GeVTrigger","vertexXYRot4GeVTrigger;x (fm); y (fm)",100,-10,10,100,-10,10);
  TH2F * vertexXYRot5GeVTrigger = new TH2F("vertexXYRot5GeVTrigger","vertexXYRot5GeVTrigger;x (fm); y (fm)",100,-10,10,100,-10,10);
  TH2F * vertexXYRot20GeVTrigger = new TH2F("vertexXYRot20GeVTrigger","vertexXYRot20GeVTrigger;x (fm); y (fm)",100,-10,10,100,-10,10);

  // General Study of Hadron Trigger
  TH2F * vertexXRotTriggerPt = new TH2F("vertexXRotTriggerPt","vertexXRotTriggerPt; p_{T}^{h} (GeV/c); x (fm)",200,0,200,100,-10,10);


  vertexXYRot4GeVTrigger->Sumw2();
  vertexXYRot5GeVTrigger->Sumw2();
  vertexXYRot20GeVTrigger->Sumw2();

  vertexXRotTriggerPt->Sumw2();






  vertexR->Sumw2();
  vertexRHardCore->Sumw2();
  vertexRNoHardCore->Sumw2();
  vertexRDijetCut->Sumw2();
  vertexXY->Sumw2();
  vertexXYHardCore->Sumw2();
  vertexXYNoHardCore->Sumw2();
  vertexXYRot->Sumw2();
  vertexXYRotHardCore->Sumw2();
  vertexXYRotNoHardCore->Sumw2();
  vertexXRotJetPt->Sumw2();
  vertexXRotJetPtHardCore->Sumw2();
  vertexJetPtR->Sumw2();
  vertexJetPtRHardCore->Sumw2();
  vertexHighestJetZR->Sumw2();
  vertexHighestJetZXRot->Sumw2();



  TH1D * jetPt = new TH1D("jetPt", "jetPt;p_{T}^{jet} (GeV/c)", 100, 0, 100);
  TH1D * recJetPt = new TH1D("recJetPt", "recJetPt;p_{T}^{jet} (GeV/c)", 125, -25, 100);
	TH1D * leadingJetPt = new TH1D("leadingJetPt","Leading Jet p_{T};p_{T}^{jet} (GeV/c)",100,0,100);
  TH1D * jetPtUnweighted = new TH1D("jetPtUnweighted", "jetPtUnweighted;p_{T}^{jet} (GeV/c)", 100, 0, 100);
  TH1D * particleEnergy = new TH1D("particleEnergy", "Total Energy of Outgoing Particles; E_{T} (GeV)", 100, 0, 2000);
  //MHO
  TH2F * jetPhiPt = new TH2F("jetPhiPt", "jetPhiPt;#phi;p_{T}^{jet} (GeV/c)", 100, 0, 2.0*TMath::Pi(), 100, 0, 100);
  TH2F * jetEtaPhi = new TH2F("jetEtaPhi", "jetEtaPhi;#Delta#eta;#Delta#phi", 100, -5, 5, 100, 0, 2.0*TMath::Pi());
  TH2F * leadingJetEtaPhi = new TH2F("leadingJetEtaPhi", "leadingJetEtaPhi;#Delta#eta;#Delta#phi", 100, -5, 5, 100, 0, 2.0*TMath::Pi());
  // this histogram basically only exists to keep track of the number of events
  TH1I * hWeight = new TH1I("hWeight","hWeight",110,0,1.1);
  TH1D * gammaEt = new TH1D("gammaEt","gammaEt",100,0,100);
  TH1D * trackPt = new TH1D("trackPt","trackPt",100,0,100);

  // Scattering Center Research
  TH1F * scatCentPt = new TH1F("scatCentPt","scatCentPt",100,0,8);
  TH1F * scatCentE = new TH1F("scatCentE","scatCentE",100,0,20);
  TH1F * scatCentEtot = new TH1F("scatCentEtot","scatCentEtot",100,0,1000);

  // Hard Scatter info
  double max_pt_hs_spectrum = 500;
  TH1F * qqbar_pt = new TH1F("qqbar_pt","q#bar{q};p_{t} GeV/c",100,0,max_pt_hs_spectrum);
  TH1F * qq_pt = new TH1F("qq_pt","qq;p_{t} GeV/c",100,0,max_pt_hs_spectrum);
  TH1F * gg_pt = new TH1F("gg_pt","gg;p_{t} GeV/c",100,0,max_pt_hs_spectrum);
  TH1F * qg_pt = new TH1F("qg_pt","qg;p_{t} GeV/c",100,0,max_pt_hs_spectrum);
  TH2F * hsPtLeadingJetPt = new TH2F("hsPtLeadingJetPt","Leading Jet vs Hard Scatter;p_{t}^{hard} GeV/c;p_{t}^{jet} GeV/c",100,0,100,100,0,100);
  TH2F * hsPtSubLeadingJetPt = new TH2F("hsPtSubLeadingJetPt","Sub-Leading Jet vs Hard Scatter;p_{t}^{hard} GeV/c;p_{t}^{jet} GeV/c",100,0,100,100,0,100);
  TH2F * hsDEtaDPhiLeadingJet = new TH2F("hsDEtaDPhiLeadingJet","Hard Scatter - Leading Jet d#eta d#phi;d#eta;d#phi",100,-1,1,100,-1,1);
  TH2F * hsDEtaDPhiSubLeadingJet = new TH2F("hsDEtaDPhiSubLeadingJet","Hard Scatter - Sub-leading Jet d#eta d#phi;d#eta;d#phi",100,-1,1,100,-1,1);

  // Testing hard core extraction from JEWEL:
  TH2F * hs1hs2Pt = new TH2F("hs1hs2Pt","p_{T}^{hs1} vs p_{T}^{hs2};p_{T}^{hs1} (GeV/c); p_{T}^{hs2} (GeV/c)",150,0,150,150,0,150);  
  TH1F * hs1hs2dPhi = new TH1F("hs1hs2dPhi","#Delta#phi HS1 - HS2;#Delta#phi",100, -TMath::Pi()/2, 3.0*TMath::Pi()/2);
  hs1hs2Pt->Sumw2();
  hs1hs2dPhi->Sumw2();

  // hard core versions?

  qqbar_pt->Sumw2();
  qq_pt->Sumw2();
  gg_pt->Sumw2();
  qg_pt->Sumw2();


  hsPtLeadingJetPt->Sumw2();
  hsPtSubLeadingJetPt->Sumw2();
  hsDEtaDPhiLeadingJet->Sumw2();
  hsDEtaDPhiSubLeadingJet->Sumw2();

	leadingJetPt->Sumw2();
  jetPt->Sumw2();
  recJetPt->Sumw2();
  gammaEt->Sumw2();
  trackPt->Sumw2();
  jetPhiPt->Sumw2();

  scatCentPt->Sumw2();
  scatCentE->Sumw2();
  scatCentEtot->Sumw2();

  // Mass analysis
  TH2F *hJetPtMass = new TH2F("hJetPtMass","hJetPtMass",10,0,200,70,0,70);
  TH2F *hSDJetPtMass = new TH2F("hSDJetPtMass","hSDJetPtMass",10,0,200,70,0,70);
  hJetPtMass->Sumw2();
  hSDJetPtMass->Sumw2();


  // For softdrop analysis
 // TH2F *hJetPtZ  = new TH2F("hJetPtZ",  "hJetPtZ",  10, 0, 200, 50, 0.0, 1.0);
  TH2F *hJetPtZ  = new TH2F("hJetPtZ",  "hJetPtZ",  nHighPtBins, (double *) &highPtBins[0],50, 0.0, 1.0);
  hJetPtZ->Sumw2();
  TH1D * hSDJetPt = new TH1D("hSDJetPt","Soft Drop p_{T}^{jet};p_{T}^{jet} (GeV/c)",200,0,200);
  hSDJetPt->Sumw2();
  TH1D * hSDJetPtBkgSub = new TH1D("hSDJetPtBkgSub","Soft Drop p_{T}^{jet};p_{T}^{jet} (GeV/c",200,0,200);
  hSDJetPtBkgSub->Sumw2();


  // Jet-hadron is done more differentially
  //	enum { nJetPtBins = 3, nParticlePtBins = 11 };
  TH2F * jetHadron[nJetPtBins][nParticlePtBins];
  TH2F * jetHadronScatCent[nJetPtBins][nParticlePtBins];
  TH2F * jetHadronOnlyJet[nJetPtBins][nParticlePtBins];
  TH2F * jetHadronExcludeJet[nJetPtBins][nParticlePtBins];


  TH1F * ptBinHadronPt[nJetPtBins];
  // for Swift background
  TH1F *swiftJetYield[nJetPtBins];
  TH1F *swiftParticleYield[nJetPtBins][nParticlePtBins];



  for (unsigned int i = 0; i < nJetPtBins; i++)
  {
    combinedClass = Form("ptBinHadronPt_" + jetClass,jetPtBins.at(i),jetPtBins.at(i+1));
    ptBinHadronPt[i] = new TH1F(combinedClass,combinedClass, 200,0,50);
    ptBinHadronPt[i]->Sumw2();

    //FIXME
    combinedClass = Form("swiftJetYield_" + jetClass,jetPtBins.at(i),jetPtBins.at(i+1));
    swiftJetYield[i] = new TH1F(combinedClass,combinedClass,100,-(eta_cut-R),eta_cut-R); //x axis is eta
    swiftJetYield[i]->Sumw2();  // useful for fitting.


    for (unsigned int j = 0; j < nParticlePtBins; j++)
    {
      combinedClass = Form("jetHadron_" + jetClass + "_" + particleClass, jetPtBins.at(i), jetPtBins.at(i+1), particlePtBins.at(j), particlePtBins.at(j+1));
      jetHadron[i][j] = new TH2F(combinedClass, combinedClass, 100, -2, 2, 100, -TMath::Pi()/2, 3.0*TMath::Pi()/2);
      combinedClass = Form("jetHadronScatCent_" + jetClass + "_" + particleClass, jetPtBins.at(i), jetPtBins.at(i+1), particlePtBins.at(j), particlePtBins.at(j+1));
      jetHadronScatCent[i][j] = new TH2F(combinedClass, combinedClass, 100, -2, 2, 100, -TMath::Pi()/2, 3.0*TMath::Pi()/2);
      combinedClass = Form("jetHadronOnlyJet_" + jetClass + "_" + particleClass, jetPtBins.at(i), jetPtBins.at(i+1), particlePtBins.at(j), particlePtBins.at(j+1));
      jetHadronOnlyJet[i][j] = new TH2F(combinedClass, combinedClass, 100, -2, 2, 100, -TMath::Pi()/2, 3.0*TMath::Pi()/2);
      combinedClass = Form("jetHadronExcludeJet_" + jetClass + "_" + particleClass, jetPtBins.at(i), jetPtBins.at(i+1), particlePtBins.at(j), particlePtBins.at(j+1));
      jetHadronExcludeJet[i][j] = new TH2F(combinedClass, combinedClass, 100, -2, 2, 100, -TMath::Pi()/2, 3.0*TMath::Pi()/2);

      jetHadron[i][j]->Sumw2();
      jetHadronScatCent[i][j]->Sumw2();
      jetHadronOnlyJet[i][j]->Sumw2();
      jetHadronExcludeJet[i][j]->Sumw2();

      combinedClass = Form("swiftParticleYield_" + jetClass + "_" + particleClass, jetPtBins.at(i),jetPtBins.at(i+1), particlePtBins.at(j), particlePtBins.at(j+1));
      swiftParticleYield[i][j] = new TH1F(combinedClass,combinedClass,100,-eta_cut,eta_cut);
      swiftParticleYield[i][j]->Sumw2();
    }
  }
  int nGoodEvents = 0;
  
  int nevents = tree->GetEntries();
  // Event loop
  for (Int_t eventNumber = 0; eventNumber < nevents; eventNumber++)
  {
    tree->GetEntry(eventNumber);
  
    TString hashString = "";

    total_weight += weight * weightingOn;
    // Fill weight histogram
    hWeight->Fill(weight);

    vertexXY->Fill(vertex_x,vertex_y,weight);
    vertexR->Fill(TMath::Sqrt(pow(vertex_x,2) + pow(vertex_y,2)),weight);


    // Fill particles ito array
    for (unsigned int particleNumber = 0; particleNumber < px->size(); particleNumber++)
    {


      // For TLorentzVector
      /*			TLorentzVector particle;
            particle.SetPxPyPzE(px->at(particleNumber), py->at(particleNumber),
            pz->at(particleNumber), energy->at(particleNumber) );
            particles.push_back(particle);*/
      // For Fastjet
      fastjet::PseudoJet jet =  fastjet::PseudoJet( px->at(particleNumber), py->at(particleNumber),
          pz->at(particleNumber), energy->at(particleNumber) );
      hashString += TString::Format(" %e ",energy->at(particleNumber));

      if (statusOn && status->at(particleNumber) == 3) {
        scatCents.push_back(jet);
        continue; //so that we don't count this in the other particles lists
      }

      if (statusOn && status->at(particleNumber) == 6) { // lucky number
        jet.set_user_index(particleID->at(particleNumber)); // storing pid in user index
        hardScatters.push_back(jet);
        continue; // don't use later
      }

      if (isNeutrino(particleID->at(particleNumber))) {
        continue;  // ALICE,STAR don't detect neutrinos.
      }

      if (isGamma(particleID->at(particleNumber))) {
        gammaEt->Fill(jet.Et(),weight);
      }
      // setting a unique id code for each particle
      // to be used later for ID
      jet.set_user_index(particleNumber); 			
      // Cut to minimum momentum for reconstruction
      if (jet.pt() >= jet_constituent_cut) { 
        constParticles.push_back(jet);
      }
      particles.push_back(jet);
      //particles.push_back( fastjet::PseudoJet( px->at(particleNumber), py->at(particleNumber),
      //			pz->at(particleNumber), energy->at(particleNumber) ));
      // printf("%d\t%f\t%f\t%f\t%f\n",particleID->at(particleNumber),px->at(particleNumber),py->at(particleNumber),pz->at(particleNumber),energy->at(particleNumber));
    }



    // Cut particles using erase remove idiom to avoid worrying about removing particles incorrectly
    particles.erase( std::remove_if(particles.begin(), particles.end(), rejectParticle), particles.end() );
    //		scatCents.erase( std::remove_if(scatCents.begin(),scatCents.end(), rejectParticle), scatCents.end() );

    constParticles.erase( std::remove_if(constParticles.begin(), constParticles.end(), rejectParticle), constParticles.end() );



    // Refactor from here so that it can be used by everyone
    // Define jet algorithm 


    fastjet::AreaDefinition areaDef(fastjet::active_area,fastjet::GhostedAreaSpec(ghost_maxrap,1,ghost_area));



    // Perform jet finding
    //	fastjet::ClusterSequenceArea cs(particles, jetDef, areaDef);
    fastjet::ClusterSequenceArea cs(constParticles, jetDef, areaDef);
    //		fastjet::ClusterSequence cs(particles, jetDef);
    jets = fastjet::sorted_by_pt(cs.inclusive_jets());

    // removing jets that don't pass eta_cut, global hard core cut (if set)
    jets.erase( std::remove_if(jets.begin(), jets.end(), rejectJet), jets.end() );

    double jet_phi, jet_eta, rot_x, rot_y, vertex_r;

    // Begin the actual analysis 

    // Examine QCD Hard Scatter
    fastjet::PseudoJet hs1,hs2;
    int pid_sum;  // declaring here so I can use it later
    if (hardScatters.size()==2) {
      hs1 = hardScatters.at(0);
      hs2 = hardScatters.at(1);
      nGoodEvents++;
      pid_sum = TMath::Abs(hs1.user_index()) + TMath::Abs(hs2.user_index());

      // check eta cut for each hard scatter
      bool hs1pass = TMath::Abs(hs1.eta()) < eta_cut;
      bool hs2pass = TMath::Abs(hs2.eta()) < eta_cut;
      if (pid_sum < 13) { // 2 quarks/antiquarks
        if (hs1.user_index()*hs2.user_index() < 0 ) { //q qbar
          if (hs1pass) qqbar_pt->Fill(hs1.pt(),weight);
          if (hs2pass) qqbar_pt->Fill(hs2.pt(),weight);
        } else { // qq or qbarqbar
          if (hs1pass) qq_pt->Fill(hs1.pt(),weight);
          if (hs2pass) qq_pt->Fill(hs2.pt(),weight);
        }

      } else if (pid_sum == 42) { // 2 gluons
        if (hs1pass) gg_pt->Fill(hs1.pt(),weight);
        if (hs2pass) gg_pt->Fill(hs2.pt(),weight);
      } else if (pid_sum > 21 && pid_sum < 28) { // 1 gluon, 1 quark
        if (hs1pass) qg_pt->Fill(hs1.pt(),weight);
        if (hs2pass) qg_pt->Fill(hs2.pt(),weight);
      }
    }




    // Jet-h correlations?

    //leading jet
    if (jets.size()) {
  
      fastjet::PseudoJet * leadingJet = &jets.at(0);
      // Note: redefine leadingJet here to mess with the trigger
      // FIXME remember to remove this
//      leadingJet = &hs2;    


      jet_phi = leadingJet->phi();
      jet_eta = leadingJet->eta();
      rot_x = vertex_x * TMath::Cos(PI - jet_phi) - vertex_y * TMath::Sin(PI - jet_phi);
      rot_y = vertex_x * TMath::Sin(PI - jet_phi) + vertex_y * TMath::Cos(PI - jet_phi);
      vertex_r = TMath::Sqrt(pow(vertex_x,2) + pow(vertex_y,2));

      double leading_pt = leadingJet->pt();
      // doing background subtraction, if requested
      if (doJetBkgSub) {
        double rho = 0.0;
        rho = getBackground(leadingJet, &particles);
        double jet_area = leadingJet->area();
        leading_pt = leading_pt - rho * jet_area;
      }

			leadingJetPt->Fill(leading_pt,weight);

      std::vector<double>::iterator jetPtBinIter;
      jetPtBinIter = std::lower_bound(jetPtBins.begin(), jetPtBins.end(), leading_pt);
     // jetPtBinIter = std::lower_bound(jetPtBins.begin(), jetPtBins.end(), jets.at(0).pt());
      // The -1 is because lower_bound counts below the lowest value as defining the 0 index.
      // We are interested in everything above that index.
      Int_t jetPtBin = jetPtBinIter - jetPtBins.begin() - 1;


      leadingJetEtaPhi->Fill(jet_eta,jet_phi,weight);


      // Compare to QCD Hard Scatter, if available
      if (hardScatters.size()==2) {
        double hs1Phi = hs1.phi();
        double hs2Phi = hs2.phi();      
        double dPhi2hs1 = calculateDeltaPhi(jet_phi,hs1Phi);
        double dPhi2hs2 = calculateDeltaPhi(jet_phi,hs2Phi);
        double dEta2hs1 = jet_eta - hs1.eta();
        double dEta2hs2 = jet_eta - hs2.eta();

        hs1hs2Pt->Fill(hs1.pt(),hs2.pt(),weight);
        hs1hs2dPhi->Fill(calculateDeltaPhi(hs1Phi,hs2Phi),weight);


        // safe to assume leading jet is nearly within PI of one hard scatter
        if (TMath::Abs(dPhi2hs1) <= TMath::Abs(dPhi2hs2) && TMath::Abs(hs1.eta()) < eta_cut) { // leading jet is hs1.
          // and hs1 meets cut

          hsPtLeadingJetPt->Fill(hs1.pt(),leading_pt,weight);
          hsDEtaDPhiLeadingJet->Fill(-dEta2hs1,-dPhi2hs1,weight);         
          // Filling type histograms for hs1
          ptLossVertexRLeadingJet->Fill(vertex_r,hs1.pt() - leading_pt,weight);



          if ((jetPtBin >= 0) && (jetPtBin < nJetPtBins)) {
            if (pid_sum < 13) { // 2 quarks/antiquarks
              if (hs1.user_index()*hs2.user_index() < 0 ) { //q qbar
                qqbar_PartonPtByJetBin[jetPtBin]->Fill(hs1.pt(),weight);
              } else { // qq or qbarqbar
                qq_PartonPtByJetBin[jetPtBin]->Fill(hs1.pt(),weight);
              }

            } else if (pid_sum == 42) { // 2 gluons
              gg_PartonPtByJetBin[jetPtBin]->Fill(hs1.pt(),weight);   
            } else if (pid_sum > 21 && pid_sum < 28) { // 1 gluon, 1 quark
              if (hs1.user_index() == 21) { //hs 1 is the gluon
                gq_PartonPtByJetBin[jetPtBin]->Fill(hs1.pt(),weight);
              } else if (hs2.user_index() == 21) { // hs 1 is  the quark
                qg_PartonPtByJetBin[jetPtBin]->Fill(hs1.pt(),weight);
              }
            }
          }

          // check for first subleading jet within PI of hs2 (jets ordered by pt)
          // assume this is hs2. 
          if (TMath::Abs(hs2.eta()) < eta_cut) {
            for (unsigned int i = 1; i < jets.size(); i++)
            {
              if (TMath::Abs(calculateDeltaPhi(hs2Phi,jets.at(i).phi()) < PI/2)) {
                hsPtSubLeadingJetPt->Fill(hs2.pt(),jets.at(i).pt(),weight);
                hsDEtaDPhiSubLeadingJet->Fill(hs2.eta() - jets.at(i).eta(),calculateDeltaPhi(hs2.phi(),jets.at(i).phi()),weight);         
          			ptLossVertexRSubleadingJet->Fill(vertex_r,hs2.pt() - jets.at(i).pt(),weight);
                break;
              }
            }
          }
        } else if (TMath::Abs(hs2.eta()) < eta_cut) { // leading jet is hs2, hs2 meets cut
          hsPtLeadingJetPt->Fill(hs2.pt(),leading_pt,weight);
          hsDEtaDPhiLeadingJet->Fill(-dEta2hs2,-dPhi2hs2,weight);         
          ptLossVertexRLeadingJet->Fill(vertex_r,hs2.pt() - leading_pt,weight);

          // Filling type histograms for hs1
          if ((jetPtBin >= 0) && (jetPtBin < nJetPtBins)) {
            if (pid_sum < 13) { // 2 quarks/antiquarks
              if (hs1.user_index()*hs2.user_index() < 0 ) { //q qbar
                qqbar_PartonPtByJetBin[jetPtBin]->Fill(hs2.pt(),weight);
              } else { // qq or qbarqbar
                qq_PartonPtByJetBin[jetPtBin]->Fill(hs2.pt(),weight);
              }

            } else if (pid_sum == 42) { // 2 gluons
              gg_PartonPtByJetBin[jetPtBin]->Fill(hs2.pt(),weight);   
            } else if (pid_sum > 21 && pid_sum < 28) { // 1 gluon, 1 quark
              if (hs2.user_index() == 21) { //hs 2 is the gluon
                gq_PartonPtByJetBin[jetPtBin]->Fill(hs2.pt(),weight);
              } else if (hs1.user_index() == 21) { // hs 2 is  the quark
                qg_PartonPtByJetBin[jetPtBin]->Fill(hs2.pt(),weight);
              }
            }
          }

          // check for first subleading jet within PI of hs1
          if (TMath::Abs(hs1.eta()) < eta_cut) { // check that hs1 is in eta range
            for (unsigned int i = 1; i < jets.size(); i++)
            {
              if (TMath::Abs(calculateDeltaPhi(hs2Phi,jets.at(i).phi()) < PI/2)) {
                hsPtSubLeadingJetPt->Fill(hs1.pt(),jets.at(i).pt(),weight);
                hsDEtaDPhiSubLeadingJet->Fill(hs1.eta() - jets.at(i).eta(),calculateDeltaPhi(hs1.phi(),jets.at(i).phi()),weight);         
          			ptLossVertexRSubleadingJet->Fill(vertex_r,hs1.pt() - jets.at(i).pt(),weight);
                break;
              }
            }
          }
        }


      }
/*
      std::vector<double>::iterator jetPtBinIter;
      jetPtBinIter = std::lower_bound(jetPtBins.begin(), jetPtBins.end(), jets.at(0).pt());
      // The -1 is because lower_bound counts below the lowest value as defining the 0 index.
      // We are interested in everything above that index.
      Int_t jetPtBin = jetPtBinIter - jetPtBins.begin() - 1;
*/


      vertexXRotJetPt->Fill(leading_pt,rot_x,weight);
      if (!rejectJetHardCore(jets.at(0), R, jet_hard_core_cut)) { 
        vertexXRotJetPtHardCore->Fill(leading_pt,rot_x,weight);
      }


/* FIXME why was this here???
// correction, why was rejectJet used instead of rejectJetHardCore?
      vertexXRotJetPt->Fill(leading_pt,rot_x,weight);
      if (!rejectJet(jets.at(0), R)) { 
        vertexXRotJetPtHardCore->Fill(leading_pt,rot_x,weight);
      }
*/

      if ((jetPtBin >= 0) && (jetPtBin < nJetPtBins))
      {
        vertexXYRotJetPt[jetPtBin]->Fill(rot_x,rot_y,weight);
        if (!rejectJetHardCore(jets.at(0), R, jet_hard_core_cut)) { 
          vertexXYRotJetPtHardCore[jetPtBin]->Fill(rot_x,rot_y,weight);
        }
        swiftJetYield[jetPtBin]->Fill(jets.at(0).eta(),weight);
        // for jets we are using: assigning each particle to its jet using
        // the user_index feature
        // int 	user_index () const 
        // void 	set_user_index (const int index)
        /*			if (jets.at(i).has_constituents() == true)
              {
              std::vector <fastjet::PseudoJet> const_particles = jets.at(i).constituents();
              for (unsigned int j = 0; j < const_particles.size(); j++) 
              const_particles.at(j).set_user_index(i);
              }
              */
        //Printf("jetPt: %f, Bin: %d", jets.at(i).pt(), jetPtBin);
        std::vector <double>::iterator particlePtBinIter;
        Int_t particlePtBin = -1;
        for (unsigned int j = 0; j < particles.size(); j++)
        {
          particlePtBinIter = std::lower_bound(particlePtBins.begin(), particlePtBins.end(), particles.at(j).pt());
          // The -1 is because lower_bound counts below the lowest value as defining the 0 index.
          // We are interested in everything above that index.
          particlePtBin = particlePtBinIter - particlePtBins.begin() - 1;
          if ((particlePtBin >= 0) && (particlePtBin < nParticlePtBins))
          {
            bool jetContainsParticle = false;
            unsigned int particle_user_index = particles.at(j).user_index();
            if (jets.at(0).has_constituents()) {
              std::vector <fastjet::PseudoJet> const_particles = jets.at(0).constituents();
              for (unsigned int k = 0; k < const_particles.size(); k++) {
                //							const_particles.at(j).set_user_index(i);
                if (particle_user_index == const_particles.at(k).user_index()) {
                  jetContainsParticle = true;
                  break;
                }
              }
            }
            ptBinHadronPt[jetPtBin]->Fill(particles.at(j).pt(),weight);
            jetHadron[jetPtBin][particlePtBin]->Fill(leadingJet->eta()-particles.at(j).eta(), calculateDeltaPhi(leadingJet->phi(), particles.at(j).phi()),weight);
            swiftParticleYield[jetPtBin][particlePtBin]->Fill(particles.at(j).eta(),weight);

            if (jetContainsParticle) {
              jetHadronOnlyJet[jetPtBin][particlePtBin]->Fill(leadingJet->eta()-particles.at(j).eta(), calculateDeltaPhi(leadingJet->phi(), particles.at(j).phi()),weight);
            } else {
              jetHadronExcludeJet[jetPtBin][particlePtBin]->Fill(leadingJet->eta()-particles.at(j).eta(), calculateDeltaPhi(leadingJet->phi(), particles.at(j).phi()),weight);
            }
          }
        }
        // Making ScatCent Only histograms
        for (unsigned int j = 0; j < scatCents.size(); j++)
        { 
          particlePtBinIter = std::lower_bound(particlePtBins.begin(), particlePtBins.end(), scatCents.at(j).pt());
          // The -1 is because lower_bound counts below the lowest value as defining the 0 index.
          // We are interested in everything above that index.
          particlePtBin = particlePtBinIter - particlePtBins.begin() - 1;
          if ((particlePtBin >= 0) && (particlePtBin < nParticlePtBins))
          {
            jetHadronScatCent[jetPtBin][particlePtBin]->Fill(leadingJet->eta()-scatCents.at(j).eta(), calculateDeltaPhi(leadingJet->phi(), scatCents.at(j).phi()),weight);
          }
        }
      }
    }

    // Study of geo bias for hadron-Lambda/k0 paper
    for (unsigned int i = 0; i < particles.size(); i++) {
      double part_pt = particles.at(i).pt();

      if ( part_pt > 4. ) {
        double hadron_phi = particles.at(i).phi();
        rot_x = vertex_x * TMath::Cos(PI - hadron_phi) - vertex_y * TMath::Sin(PI - hadron_phi);
        rot_y = vertex_x * TMath::Sin(PI - hadron_phi) + vertex_y * TMath::Cos(PI - hadron_phi);
        vertexXYRot4GeVTrigger->Fill(rot_x,rot_y,weight);
        if ( part_pt > 5. ) {
          vertexXYRot5GeVTrigger->Fill(rot_x,rot_y,weight);
          if ( part_pt > 20. ) {
            vertexXYRot20GeVTrigger->Fill(rot_x,rot_y,weight);
          }
        }
      }

      vertexXRotTriggerPt->Fill(part_pt,rot_x,weight);

    }

    for (unsigned int i = 0; i < jets.size(); i++)
    {
//      if (rejectJet(jets.at(i), R)) continue;
      
      double jet_pt = jets.at(i).pt();


      if (doJetBkgSub) {
        double rho = 0.0;
        rho = getBackground(&jets.at(i), &particles);
        double jet_area = jets.at(i) .area();
        jet_pt = jet_pt - rho * jet_area;
      }



      // Fill jet spectra
      jetPt->Fill(jet_pt,weight);
      jetPtUnweighted->Fill(jet_pt);
      jetEtaPhi->Fill(jets.at(i).eta(),jets.at(i).phi(),weight);
      jetPhiPt->Fill(jets.at(i).phi(),jet_pt,weight);
     // jetPhiPt->Fill(jets.at(i).phi(),jets.at(i).pt(),weight);

      // Mass analysis
      hJetPtMass->Fill(jet_pt,sqrt(jets.at(i).m2()),weight);
      

      //Soft Drop analysis  
      fastjet::PseudoJet sd_jet = sd(jets.at(i));
      assert(sd_jet != 0); //because soft drop is a groomer (not a tagger), it should always return a soft-dropped jet
      hJetPtZ->Fill(sd_jet.pt(), sd_jet.structure_of<SoftDrop>().symmetry(),weight);
     // hJetPtZ->Fill(jets.at(i).pt(), sd_jet.structure_of<SoftDrop>().symmetry());
      hSDJetPtMass->Fill(sd_jet.pt(),sqrt(sd_jet.m2()),weight);   

      fastjet::PseudoJet thisJet = jets.at(i);

      // Try some background subtraction here
      double rho = 0.0;
      rho = getBackground(&thisJet, &particles);
      double jet_area = thisJet.area();
     
      hSDJetPt->Fill(sd_jet.pt());
      // experiment
      hSDJetPtBkgSub->Fill(sd_jet.pt() - rho * jet_area); // this makes no sense

      // After filling basic spectra, require hard core (ie particle > 6 GeV)
      // Removing the jets would break the loop above
      //jets.erase( std::remove_if(jets.begin(), jets.end(), rejectJet), jets.end() );
      jet_phi = jets.at(i).phi();
      rot_x = vertex_x * TMath::Cos(PI - jet_phi) - vertex_y * TMath::Sin(PI - jet_phi);
      rot_y = vertex_x * TMath::Sin(PI - jet_phi) + vertex_y * TMath::Cos(PI - jet_phi);
      vertex_r = TMath::Sqrt(pow(vertex_x,2) + pow(vertex_y,2));
      vertexXYRot->Fill(rot_x,rot_y,weight);

      //finding the highest pt constituent of the jet
      double highestPtConst = getHighestPtConst(jets.at(i));




      vertexHighestPtXRot->Fill(highestPtConst,rot_x,weight);
      vertexHighestPtR->Fill(highestPtConst,vertex_r,weight);
      vertexHighestJetZR->Fill(highestPtConst/jet_pt,vertex_r,weight);
      vertexHighestJetZXRot->Fill(highestPtConst/jet_pt,rot_x,weight);

      //Looking at surface bias with Dijet method:
      //looking for leading jets, then subleading jets with delta_phi > 2 pi / 3
      if (!rejectJetDijet(jets.at(i), R, 20.)) {
        for ( int j = i+1; j < jets.size(); j++ ) {
          // to save time, take advantage of ordering:
          double subleading_pt = jets.at(j).pt();
          if (subleading_pt < 10.) break;

          if (!rejectJetDijet(jets.at(j), R, 10.) && TMath::Abs(calculateDeltaPhi(jet_phi,jets.at(j).phi())) > 2. * PI/3.) {
            // Then these jets have passed the dijet cut			
            vertexRDijetCut->Fill(vertex_r,weight);
            vertexXYRotDijet->Fill(rot_x,rot_y,weight);
            vertexJetPtRDijet->Fill(jet_pt,vertex_r,weight);

            double a_j = (jet_pt - subleading_pt) / (jet_pt + subleading_pt);
            double x_j = subleading_pt / jet_pt;

            dijetAj->Fill(a_j,weight);
            dijetAjJetPt->Fill(jet_pt,a_j,weight);
            dijetXj->Fill(x_j,weight);
            dijetXjJetPt->Fill(jet_pt,x_j,weight);
            vertexDijetAjR->Fill(a_j,vertex_r,weight);
            vertexXYRotAjDijet->Fill(rot_x,rot_y,a_j,weight);
          }
        }
      }


      vertexJetPtR->Fill(jet_pt, TMath::Sqrt(pow(vertex_x,2) + pow(vertex_y,2)),weight);


      if (rejectJetHardCore(jets.at(i), R, jet_hard_core_cut) == true) {

        vertexXYNoHardCore->Fill(vertex_x,vertex_y,weight);
        vertexXYRotNoHardCore->Fill(rot_x,rot_y,weight);
        vertexRNoHardCore->Fill(vertex_r,weight);
      } else {

        vertexXYHardCore->Fill(vertex_x,vertex_y,weight);
        vertexXYRotHardCore->Fill(rot_x,rot_y,weight);
        vertexRHardCore->Fill(vertex_r,weight);
        vertexJetPtRHardCore->Fill(jet_pt, TMath::Sqrt(pow(vertex_x,2) + pow(vertex_y,2)),weight);
      }

//      if (rejectJet(jets.at(i), R)) continue;
  
      //std::vector <fastjet::PseudoJet> rec_jets;
        if (doJetBkgSub) recJetPt->Fill(jet_pt,weight);
        else recJetPt->Fill(thisJet.pt() - jet_area*rho,weight);

			}
    


    // Do angular correlations
    double particleEnergyTemp = 0;
    for (unsigned int i = 0; i < particles.size(); i++)
    {
      // Eta-Phi of each particle
      etaPhi->Fill(particles.at(i).eta(), particles.at(i).phi(),weight);
      // Eta-Pt of each particle
      // the purpose of this plot is to investigate the double ridge 
      // in all particles in AA
      etaPt->Fill(particles.at(i).eta(), particles.at(i).pt(),weight);

      trackPt->Fill(particles.at(i).pt(),weight);
      //		if (partic


      //MHO fix: we need to double count here 
      // to avoid biases in the ordering of particles
      //      for (unsigned int j = i+1; j < particles.size(); j++)
      for (unsigned int j = 0; j < particles.size(); j++)
      {
        // MHO
        if (i == j) continue;
        // deltaEta-deltaPhi or each particle pair
        // For TLorentzVector
        //dEtaDPhi->Fill(particles.at(i).Eta()-particles.at(j).Eta(), calculateDeltaPhi(particles.at(i).Phi(), particles.at(j).Phi()),weight);
        // For Fastjet
        dEtaDPhi->Fill(particles.at(i).eta()-particles.at(j).eta(), calculateDeltaPhi(particles.at(i).phi(), particles.at(j).phi()) ,weight);
      }
      particleEnergyTemp += particles.at(i).E();
    }

    // Fill total particle energy
    particleEnergy->Fill(particleEnergyTemp,weight);


    // Scattering Center research
    double scatCentTotalE = 0;

    for (unsigned int i = 0; i < scatCents.size(); i++) {
      scatCentPt->Fill(scatCents.at(i).pt(),weight); // note: no rejection criteria
      scatCentE->Fill(scatCents.at(i).E(),weight);
      scatCentTotalE += scatCents.at(i).E();
    }
    scatCentEtot->Fill(scatCentTotalE,weight);


    // Clear vector of PseudoJets after each event
    particles.clear();
    scatCents.clear();
    hardScatters.clear();
    constParticles.clear();
    jets.clear();


    if ((eventNumber % 1000 == 0) && (eventNumber != 0)) { printf("eventNumber = %d\n", eventNumber); }

  //hashString
//    printf("H %d\nW %e\n",(int) hashString.Hash(),weight);

    // For testing purposes
    //if (eventNumber > 99) {break;}
	}
  
  // fIn->Close();
  
  // Normalization:
  // Important note: if errors are added to the histograms, we must do Sumw2 here,
  //    before we normalize
  
  
  
  //	outputFilename = Form("output/%s.root", outputFilename.Data());
  TFile * fOut = TFile::Open(outputFilename.Data(),"RECREATE");
  if (fOut->IsOpen() == kFALSE)
  {
    Printf("outputFilename \"%s\" failed to open!", outputFilename.Data());
    std::exit(1);
  }
  
  
  
  fOut->Add(dEtaDPhi);
  fOut->Add(etaPhi);
  fOut->Add(etaPt);
  fOut->Add(dijetAj);
  fOut->Add(dijetAjJetPt);
  fOut->Add(dijetXj);
  fOut->Add(dijetXjJetPt);
  fOut->Add(vertexR);
  fOut->Add(vertexRHardCore);
  fOut->Add(vertexRNoHardCore);
  fOut->Add(vertexXY);
  fOut->Add(vertexXYHardCore);
  fOut->Add(vertexXYNoHardCore);
  fOut->Add(vertexXYRot);
  fOut->Add(vertexXYRotHardCore);
  fOut->Add(vertexXYRotNoHardCore);
  fOut->Add(vertexXYRotDijet);
  fOut->Add(vertexXYRotAjDijet);
  fOut->Add(vertexXRotJetPt);
  fOut->Add(vertexXRotJetPtHardCore);
  fOut->Add(vertexHighestPtXRot);
  
  fOut->Add(ptLossVertexRLeadingJet);
  fOut->Add(ptLossVertexRSubleadingJet);


  fOut->Add(vertexJetPtR);
  fOut->Add(vertexJetPtRHardCore);
  fOut->Add(vertexJetPtRDijet);
  fOut->Add(vertexRDijetCut);
  fOut->Add(vertexDijetAjR);
  fOut->Add(vertexHighestPtR);
  fOut->Add(vertexHighestJetZR);
  fOut->Add(vertexHighestJetZXRot);
  
  

  fOut->Add(jetPt);
	fOut->Add(leadingJetPt);
  fOut->Add(recJetPt);
  fOut->Add(gammaEt);
  fOut->Add(trackPt);
  fOut->Add(jetPtUnweighted);
  fOut->Add(particleEnergy);
  fOut->Add(jetEtaPhi);
  fOut->Add(leadingJetEtaPhi);
  fOut->Add(jetPhiPt);
  fOut->Add(hWeight);
  
  fOut->Add(scatCentPt);
  fOut->Add(scatCentE);
  fOut->Add(scatCentEtot);
  
  fOut->Add(qqbar_pt);
  fOut->Add(qq_pt);
  fOut->Add(gg_pt);
  fOut->Add(qg_pt);
  fOut->Add(hsPtLeadingJetPt);
  fOut->Add(hsPtSubLeadingJetPt);
  fOut->Add(hsDEtaDPhiLeadingJet);
  fOut->Add(hsDEtaDPhiSubLeadingJet);
  
  
  
  fOut->Add(vertexXYRot4GeVTrigger);
  fOut->Add(vertexXYRot5GeVTrigger);
  fOut->Add(vertexXYRot20GeVTrigger);
  
  fOut->Add(vertexXRotTriggerPt);
  fOut->Add(hJetPtZ);
  fOut->Add(hJetPtMass);  
  fOut->Add(hSDJetPtMass);  
  fOut->Add(hSDJetPt);
  fOut->Add(hSDJetPtBkgSub);

  fOut->Add(hs1hs2Pt);
  fOut->Add(hs1hs2dPhi);


  for (unsigned int i = 0; i < nJetPtBins; i++)
  {
    fOut->Add(ptBinHadronPt[i]);
    fOut->Add(swiftJetYield[i]);
    fOut->Add(vertexXYRotJetPt[i]);
    fOut->Add(vertexXYRotJetPtHardCore[i]);
    
    for (unsigned int j = 0; j < nParticlePtBins; j++)
    {
      fOut->Add(jetHadron[i][j]);
      fOut->Add(jetHadronScatCent[i][j]);
      fOut->Add(jetHadronOnlyJet[i][j]);
      fOut->Add(jetHadronExcludeJet[i][j]);
      fOut->Add(swiftParticleYield[i][j]);
    }

    fOut->Add(qqbar_PartonPtByJetBin[i]);
    fOut->Add(qq_PartonPtByJetBin[i]);
    fOut->Add(gq_PartonPtByJetBin[i]);
    fOut->Add(qg_PartonPtByJetBin[i]);
    fOut->Add(gg_PartonPtByJetBin[i]);

  }
  




  // useful code from rene
  // https://root.cern.ch/root/roottalk/roottalk02/3719.html
  
  // July 21: 
  // new idea: store sum of weights in a single entry in a branch of a TTree
  // then when merging the files, the number of entries will be the number of 
  // files.  Scale everything by 1 / n_files.
  // Can also add cross section later.  This requires reading the log file.
  // Depending on where I store the log files, it may be necessary to add another 
  // argument to phase1, indicating the path to the log.
  //
  // We could also add a branch for the energy, and for the type of event
  // That information is also in the logfile
  
  // August 19:
  // This will now be created first in the HepMC2Root stage.
  // For compatibility, it will also be created here if it does not exist
  // FIXME
  
	  

  if (!runInfo ) { //for compatibility 
 // if (!runInfo) delete runInfo; // Making a new runInfo in case the previous one was missing something
    printf("TTree runInfo not found.  Creating new one ... \n");
    runInfo = new TTree("runInfo","runInfo");
    TBranch *bTotalWeight = runInfo->Branch("totalWeight", &total_weight, "totalWeight/D");	
    TBranch *bNEvents = runInfo->Branch("nEvents", &nevents, "nEvents/I");	
//		TBranch *bCrossSection = runInfo->Branch("crossSection", &storedCrossSection, "crossSection/D");

    // to add: cross secion, sqrt(s), type
    // pt range?
    runInfo->Fill();
  }
  else {
    printf("TTree runInfo found!\n");
		// check if total weight, cross section info filled.
		// Not running if multiple entries in runInfo (ptHardBins on )
		if (runInfo->GetEntries() == 1) {
			double vTotalWeight = 0;
			int vNEvents = 0;
			double vNEventsDouble = 0;
			double vCrossSection = 0;
			runInfo->SetBranchAddress("totalWeight",&vTotalWeight);
			if (!strcmp(runInfo->GetBranch("nEvents")->GetTitle(),"nEvents/I")) runInfo->SetBranchAddress("nEvents",&vNEvents);
			else runInfo->SetBranchAddress("nEvents",&vNEventsDouble);
			runInfo->SetBranchAddress("crossSection",&vCrossSection);
			runInfo->GetEntry(0);
			if (vTotalWeight == 0 || (vNEvents + (int) vNEventsDouble == 0)) {  
				printf("Correcting total weight or nEvents branch.\n");
		//		vTotalWeight = total_weight;
		//		vNEvents = nevents;	
				// create a new runinfo tree cause this one is broken.
				delete runInfo;
				runInfo = new TTree("runInfo","runInfo");
				TBranch *bTotalWeight = runInfo->Branch("totalWeight", &total_weight, "totalWeight/D");	
				TBranch *bNEvents = runInfo->Branch("nEvents", &nevents, "nEvents/I");	
				TBranch *bCrossSection = runInfo->Branch("crossSection", &vCrossSection, "crossSection/D");	
				runInfo->Fill();
	
			} 
			else { 
				// Everything is fine.
				runInfo->CloneTree(); 
			}
		} 
		else {
			runInfo->CloneTree(); 
		}
  }
  
  // Creating TTree for parameter information
  TTree * parameters = new TTree("parameters","parameters");
  TBranch * bJetR = parameters->Branch("jetR",&R,"jetR/D");
  TBranch * bConstCut = parameters->Branch("constCut",&jet_constituent_cut,"constCut/D");
  TBranch * bEtaCut = parameters->Branch("etaCut",&eta_cut,"etaCut/D");
  TBranch * bParticlePtCut = parameters->Branch("particlePtCut",&particle_pt_min,"particlePtCut/D");
  
  parameters->Fill();
  
  
  
  
  
  
  
  
  if (false && weightingOn) {
  //if (weightingOn) {
  
    // I have no idea what i should be doing here
  
    //total_weight = nevents / total_weight;
    //FIXME	
    //double rescale = 1 / total_weight;
    double rescale = nevents / total_weight;
  
  
    TObjLink *lnk = gDirectory->GetList()->FirstLink();
    TObject *obj;
    TH1 *hobj;
    while (lnk) {
      obj = gDirectory->FindObject(lnk->GetObject()->GetName());    
      obj = gDirectory->Get(lnk->GetObject()->GetName());    
      //   printf("name: %s named %s\n",obj->ClassName(),obj->GetName()); 
      lnk = lnk->Next();
      if (obj->InheritsFrom("TH1") && strcmp(obj->GetName(),"hWeight") && strcmp(obj->GetName(),"jetPtUnweighted")) {
        //      printf("\tscaling %s by %f \n",obj->GetName(),total_weight);
        hobj = (TH1 *) obj;
        hobj->Scale(rescale);
      }    
    }
  } 
  
  printf("Analyzed %d good events out of %d events!\n",nGoodEvents,nevents); 
  
  
  fOut->Write();
  fOut->Close();
  
  
  
  
  return;
  
  //CUT HERE-ISH

}

int main(int argc, char * argv[])
{
  // Defined to allow rehlersi to use a different style for printing
  // It can be enabled by passing USER_DEFINED=-DRE_STYLE=1 to the makefile
  #ifdef RE_STYLE
  // Setup TStyle
  TStyle * readableStyle = initializeReadableStyle();
  #endif

  // Parse input
  TString inputFilename = "root/pp.root";
  TString outputFilename = "root/pp.hist.root";
  // Defined in analysis_params.h
  //double jet_constituent_cut = 0;
  bool ptHardBinFlag = false;

  // Parse input and set the appropriate values
  parseInput(argc, argv, inputFilename, outputFilename, jet_constituent_cut, ptHardBinFlag);

	if (!inputFilename.CompareTo(outputFilename)) {
		fprintf(stderr,"Error: output file is same as input file.\n");
		exit(1);
	}

  phase1(inputFilename, outputFilename, jet_constituent_cut, ptHardBinFlag);

  #ifdef RE_STYLE
  delete readableStyle;
  #endif

  return 0;
}

// Process command line inputs. This is not nearly as robust as using something like Boost::program_options,
// but that seemed a bit heavy for this instance, making program_options less useful. The code here is simple,
// but it worked well and allowed for quick development, while getting the job done.
void parseInput(int argc, char * argv[], TString & inputFilename, TString & outputFilename, double & jet_constituent_cut, bool & ptHardBinFlag)
{
  if (argc == 1)
  {
    std::cout << "Error: Must specify an option!" << std::endl;
    printHelp();
  }

  for (int i=1; i < argc; i++)
  {
    if ((argv[i] == std::string("--help")) || (argv[i] == std::string("-h")))
    {
      // Displays help
      printHelp();
    }
    if ((argv[i] == std::string("--ptHardBin")) || (argv[i] == std::string("-p")))
    {
      // Use pt hard bin 
      ptHardBinFlag = true;
      continue;
    }
    if ((argv[i] == std::string("--constituentCut")) || (argv[i] == std::string("-c")) )
    {
      // Check for consituent cut value
      if (argc > i+1)
      {
        jet_constituent_cut = std::stod(argv[i+1]);
        i++;
        continue;
      }
      else
      {
        std::cout << "An value for the constituent cut must be passed!" << std::endl;
        printHelp();
      }
    }
    if ((argv[i] == std::string("--jetResolutionParameter")) || (argv[i] == std::string("-r")) )
    {
      if (argc > i+1)
      {
        R = std::stod(argv[i+1]);
        i++;
        continue;
      }
      else
      {
        std::cout << "An value for the jet resolution parameter must be passed!" << std::endl;
        printHelp();
      }
    }
    if ((argv[i] == std::string("--inputFilename")) || (argv[i] == std::string("-i")) )
    {
      // Sets input filename
      if (argc > i+1)
      {
        inputFilename = argv[i+1];
        i++;
        continue;
      }
      else
      {
        std::cout << "An input filename must be passed!" << std::endl;
        printHelp();
      }
    }
    if ((argv[i] == std::string("--outputFilename")) || (argv[i] == std::string("-o")) )
    {
      // Sets output filename
      if (argc > i+1)
      {
        outputFilename = argv[i+1];
        i++;
        continue;
      }
      else
      {
        std::cout << "An output filename must be passed!" << std::endl;
        printHelp();
      }
    }
    if ((argv[i] == std::string("--hardCore")) || (argv[i] == std::string("-k")) )
    {
      // Check for hard core cut value
      if (argc > i+1)
      {
        globalHardCoreCut = std::stod(argv[i+1]);
        if (globalHardCoreCut > 0) requireHardCore = true; 
        i++;
        continue;
      }
      else
      {
        std::cout << "A value for the global hard core cut must be passed!" << std::endl;
        printHelp();
      }
    }
    if ((argv[i] == std::string("--doJetBkgSub")) || (argv[i] == std::string("-b")) ) 
    {
      doJetBkgSub = true; 
    }
    else
    {
      printHelp(argv[i]);
    }
  }
}

// Print help for using pionDecay and exit afterwards
// Default argument defined at the top of the file
void printHelp(std::string input)
{
  if (input != "")
  {
    std::cout << "Invalid option: " << input << std::endl;
  }

  // Prints help message
  // The formatting below is carefully written, but not nearly as fraigle as it used to be.
  std::cout << "Help: This output describes the possible parameters." << std::endl;
  std::cout << "Note: Any options that are not explicitly set are assumed to be default." << std::endl << std::endl;
  std::cout << "Valid options:" << std::endl
    << std::setw(5) << std::left << "\t-h" << "\t--help" 
    << "\t\t\t\t-> Displays this help message" << std::endl
    << std::setw(5) << std::left << "\t-i" << "\t--intputFilename <filename>" 
    << "\t-> Sets the input filename. Default: \"root/pp.root\"" << std::endl
    << std::setw(5) << std::left << "\t-o" << "\t--outputFilename <filename>" 
    << "\t-> Sets the output filename. Default: \"root/pp.hist.root\"" << std::endl			
    << std::setw(5) << std::left << "\t-c" << "\t--constituentCut <value>" 
    << "\t-> Sets the constituent cut for jet finding. Default: 0." << std::endl
    << std::setw(5) << std::left << "\t-r" << "\t--jetResolutionParameter <value>" 
    << "\t-> Sets the jet resolution parameter R for jet finding. Default: 0." << std::endl
    << std::setw(5) << std::left << "\t-k" << "\t--hardCore <value>" 
    << "\t-> Sets the requirement of a hard core to be true, and sets global hard core cut. Default: false." << std::endl
    << std::setw(5) << std::left << "\t-p" << "\t--ptHardBin"
    << "\t\t\t-> Enables pt hard bin weighting. Default: false." << std::endl 
    << std::setw(5) << std::left << "\t-b" << "\t--doJetBkgSub"
    << "\t\t\t-> Uses an out of cone background subtraction for jet pT for jetPt bins. Default: false." << std::endl << std::endl;

  std::exit(-1);
}
